<?php
   $txtRefId = $txtLName = $txtFName = $txtMidName = $txtAgencyId = "";
   if ($_SESSION["SelectedEMP"] != "") {
      $row = FindFirst("employees","WHERE RefId = ".$_SESSION["SelectedEMP"],"*");
      $txtRefId = $_SESSION["SelectedEMP"];
      $txtLName = $row["LastName"];
      $txtFName = $row["FirstName"];
      $txtMidName = $row["MiddleName"];
      $txtAgencyId = $row["AgencyId"];
   }
?>            

         <div class="row" id="EmpSearchCriteria">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-2">
                     <span class="label txt-center">Employee ID:</span>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" name="txtAgencyId" placeholder="Emp Id" class="form-input rptCriteria--" value="<?php echo $txtAgencyId; ?>">
                     <input type="text" class="form-input rptCriteria-- number--" name="txtRefId" placeholder="Emp.RefId" value="<?php echo $txtRefId; ?>" style="display: none;">
                  </div>
                  <div class="col-xs-3">
                     <a href="#" name="btnEmpLkUp" id="btnEmpLkUp" title="Employees Look Up">
                        <i class="fa fa-bars" aria-hidden="true"></i>
                     </a>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <span class="label txt-center">Employees Name:</span>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" class="form-input rptCriteria--" name="txtLName" placeholder="Last Name" value="<?php echo $txtLName; ?>">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" class="form-input rptCriteria--" name="txtFName" placeholder="First Name" value="<?php echo $txtFName; ?>">
                  </div>
                  <div class="col-xs-2">
                     <input type="text" class="form-input rptCriteria--" name="txtMidName" placeholder="Middle Name" value="<?php echo $txtMidName; ?>">
                  </div>
                  <div class="col-xs-4">
                     <a href="javascript:clearFields('txtLName,txtFName,txtMidName,txtRefId,txtAgencyId');" class="clearflds--">
                        <i class="fa fa-minus-square-o" aria-hidden="true" title="Clear Entry"></i>
                     </a>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <span class="label txt-center">Civil Status</span>
                  </div>
                  <div class="col-xs-2">
                     <select class="form-input rptCriteria--" name="drpCvlStat" id="drpCvlStats">
                        <option value="">Select Civil Status</option>
                        <option value="Si">Single</option>
                        <option value="Ma">Married</option>
                        <option value="An">Annulled</option>
                        <option value="Wi">Widowed</option>
                        <option value="Se">Separated</option>
                        <option value="Ot">Others, Please specify</option>
                     </select>
                  </div>
                  <div class="col-xs-2">
                     <span class="label">Sex&nbsp;</span>
                  </div>
                  <div class="col-xs-2">
                     <?php createSelectGender("Gender","","");?>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <span class="label txt-center">Filter By:</span>
                  </div>
                  <div class="col-xs-2">
                     <select name="filter_table" id="filter_table" name="filter_table" class="form-input rptCriteria--" onchange="populateFilter($(this).val())">
                        <option value="">Select Filter</option>
                        <option value="Division">Division</option>
                        <option value="EmpStatus">Employment Status</option>
                        <option value="Department">Department</option>
                        <option value="Office">Office</option>
                        <option value="Agency">Agency</option>
                     </select>
                  </div>
                  <div class="col-xs-2">
                     <span class="label txt-center">Filter Value:</span>
                  </div>
                  <div class="col-xs-2">
                     <select name="filter_value" id="filter_value" class="form-input rptCriteria--">
                     </select>
                  </div>
                  <div class="col-xs-2">
                     <span class="label txt-center">Employee Status</span>
                  </div>
                  <div class="col-xs-2">
                     <select class="form-input rptCriteria--" name="drpEmpStat" id="drpEmpStat">
                        <option value="0" selected>Active</option>
                        <option value="1">Inactive</option>
                     </select>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <span class="label txt-center">Signatory:</span>
                  </div>
                  <div class="col-xs-4">
                     <input type="text" class="form-input" name="signatory" id="signatory">
                     <input type="hidden" name="signatory_refid" id="signatory_refid" class="rptCriteria--">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <span class="label txt-center">Attendance Date From:</span>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" class="form-input date-- rptCriteria--" name="attendance_date_from" id="attendace_date_from">
                  </div>
                  <div class="col-xs-2">
                     <span class="label txt-center">Attendance Date To:</span>
                  </div>
                  <div class="col-xs-4">
                     <input type="text" class="form-input date-- rptCriteria--" name="attendance_date_to" id="attendace_date_to">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-2">
                     <span class="label txt-center">No. of years:</span>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" class="form-input rptCriteria-- number-- text-center" 
                            name="loyalty_years" 
                            id="loyalty_years">
                  </div>
               </div>
               <!-- <div class="row margin-top">
                  <div class="col-xs-1">
                     <span class="label">Birth Date:</span>
                  </div>
                  <div class="col-xs-1">
                     <span class="label">From:</span>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" class="form-input date-- rptCriteria--" name="txtDOBFrom" placeholder="Lower Date">
                  </div>
                  <div class="col-xs-1">
                     <span class="label">To:</span>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" class="form-input date-- rptCriteria--" name="txtDOBTo" placeholder="Higher Date">
                  </div>
                  <a href="javascript:clearFields('txtDOBFrom,txtDOBTo');" class="clearflds--">
                     <i class="fa fa-minus-square-o" aria-hidden="true" title="Clear Entry"></i>
                  </a>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-1">
                     <span class="label">Height:</span>
                  </div>
                  <div class="col-xs-1">
                     <span class="label">From:</span>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" class="form-input number-- rptCriteria--" name="txtHeightFrom">
                  </div>
                  <div class="col-xs-1">
                     <span class="label">To:</span>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" class="form-input number-- rptCriteria--" name="txtHeightTo">
                  </div>
                  <a href="javascript:clearFields('txtHeightFrom,txtHeightTo');" class="clearflds--">
                  <i class="fa fa-minus-square-o" aria-hidden="true" title="Clear Entry"></i>
                  </a>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-1">
                     <span class="label">Weight:</span>
                  </div>
                  <div class="col-xs-1">
                     <span class="label">From:</span>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" class="form-input number-- rptCriteria--" name="txtWeightFrom">
                  </div>
                  <div class="col-xs-1">
                     <span class="label">To:</span>
                  </div>
                  <div class="col-xs-2">
                     <input type="text" class="form-input number-- rptCriteria--" name="txtWeightTo">
                  </div>
                  <a href="javascript:clearFields('txtWeightFrom,txtWeightTo');" class="clearflds--">
                  <i class="fa fa-minus-square-o" aria-hidden="true" title="Clear Entry"></i>
                  </a>
               </div>

               <div class="row margin-top">
                  <div class="col-xs-2">
                     <span class="label">Resi. Province:</span>
                  </div>
                  <div class="col-xs-2">
                     <div class="form-group">
                        <?php
                           // createSelect("Province",
                           //              "drpRProvince",
                           //              "",100,"Name","","");
                           ?>
                     </div>
                  </div>
                  <div class="col-xs-1">
                     <span class="label">Resi City:</span>
                  </div>
                  <div class="col-xs-2">
                     <div class="form-group">
                           <?php
                              // createSelect("City",
                              //           "drpRCity",
                              //           "",100,"Name","","");
                           ?>
                     </div>
                  </div>
               </div> -->
            </div>
         </div>
         
         