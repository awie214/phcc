<?php
	include 'conn.e2e.php';
	include 'constant.e2e.php';
	include pathClass.'0620functions.e2e.php';
	include pathClass.'DTRFunction.e2e.php';
	$month_arr = ["January","February","March","April","May","June","July","August","September","October","November","December"];
?>
<!DOCTYPE>
<html>
<head>
	<title></title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.js"></script>
	<style type="text/css">
		.margin-top {
			margin-top: 10px;
		}
	</style>
	<script type="text/javascript">
		$(document).ready(function () {
			
			$("[id*='employee_']").each(function () {
				$(this).click(function () {
					var selected_month = $("#month").val();
					var selected_year = $("#year").val();
					var id = $(this).attr("id");
					var emprefid = id.split("_")[1];
					var workschedule = id.split("_")[2];
					var value = $(this).html();
					//alert(emprefid + "_" +selected_month + "_" + workschedule);
					$(this).html("Processing....");
					dtr_process(emprefid,selected_month,workschedule,id,value,selected_year);
				});
			});
		});
		function dtr_process(emprefid,selected_month,workschedule,id,value,selected_year) {
		   $.get("DTRTrn.e2e.php",
		   {
		      	fn:"dtr_process",
		      	emprefid:emprefid,
		      	month:selected_month,
		      	workschedule:workschedule,
		      	year:selected_year
		   },
		   function(data,status){
		      	if (status == 'success') {
		        	if (data == 1) {
		        		$("#"+id).html("Successfull " + value);
		        		$("#"+id).prop("disabled",true);
		        	} else if (data == 2){
		        		$("#"+id).html("Updated " + value);
		        		$("#"+id).prop("disabled",true);
		        	} else {
		        		$("#"+id).html("Reprocess?");
		        	}
		      	}
		   });
		}
	</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row margin-top">
			<div class="col-xs-6">
				<div class="panel panel-info">
					<div class="panel-heading">
						DTR PROCESS
					</div>
					<div class="panel-body">
						<div class="row margin-top">
							<div class="col-xs-6">
								<label>SELECT MONTH</label>
								<select class="form-control" id="month" name="month">
									<?php
										for ($i=1; $i <= 12; $i++) { 
											if ($i < 10) $i = "0".$i;
											$a = (intval($i) - 1);
											echo '<option value="'.$i.'">'.$month_arr[$a].'</option>';
										}
									?>
								</select>
							</div>
							<div class="col-xs-6">
								<label>YEAR</label>
								<input type="text" name="year" id="year" class="form-control">
							</div>
						</div>
						<div class="row margin-top">
							<div class="col-xs-6">
								<?php
									$where = "WHERE (Inactive != 1 OR Inactive IS NULL) ORDER BY LastName";
									$rs = SelectEach("employees",$where);
									if ($rs) {
										$count = 0;
										while ($row = mysqli_fetch_assoc($rs)) {
											$emprefid 		= $row["RefId"];
											$LastName       = $row["LastName"];
											$FirstName      = $row["FirstName"];
											$Name 			= strtoupper($FirstName." ".$LastName);
											$workschedule 	= FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","WorkScheduleRefId");
											if (is_numeric($workschedule)) {

												echo '
													<div class="margin-top">
													<button type="button" class="btn btn-info" id="employee_'.$emprefid.'_'.$workschedule.'">
														PROCESS DTR FOR '.$Name.'
													</button>
													</div>
												';
											}
											
										}
									}
								?>
							</div>
						</div>
					</div>
					<div class="panel-footer">
					</div>
				</div>
				
			</div>
		</div>
	</div>
</body>
</html>