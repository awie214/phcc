<div class="mypanel">
   <div class="row">
      <div class="col-sm-12">
         <div class="row margin-top">
            <div class="col-sm-6">
               <label>Salary Rate 30,000.00:</label>
            </div>
            <div class="col-sm-6">
               <label>Basic Net Pay 13,846.15:</label>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3"></div>
            <div class="col-sm-3 txt-center"><label>Actual</label></div>
            <div class="col-sm-3 txt-center"><label>Adjustment</label></div>
            <div class="col-sm-3 txt-center"><label>Total</label></div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>Work Days:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="ActualWordays" class="form-input">
            </div>
            <div class="col-sm-3">
               <input type="text" name="AdjustmentWorkdays" class="form-input">
            </div>
            <div class="col-sm-3">
               <input type="text" name="TotalWorkdays" class="form-input">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>Absent:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="ActualAbsent" class="form-input">
            </div>
            <div class="col-sm-3">
               <input type="text" name="AdjustmentAbsent" class="form-input">
            </div>
            <div class="col-sm-3">
               <input type="text" name="TotalAbsent" class="form-input">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>Tardiness:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="ActualTardiness" class="form-input">
            </div>
            <div class="col-sm-3">
               <input type="text" name="AdjustmentTardiness" class="form-input">
            </div>
            <div class="col-sm-3">
               <input type="text" name="TotalTardiness" class="form-input">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-3 label">
               <label>Undertime:</label>
            </div>
            <div class="col-sm-3">
               <input type="text" name="ActualUndertime" class="form-input">
            </div>
            <div class="col-sm-3">
               <input type="text" name="AdjustmentUndertime" class="form-input">
            </div>
            <div class="col-sm-3">
               <input type="text" name="TotalUndertime" class="form-input">
            </div>
         </div>
         <?php bar(); ?>
         <div class="row margin-top">
            <div class="col-sm-3">
            </div>
            <div class="col-sm-3">
               <label>[Actual 0.00]</label>
            </div>
            <div class="col-sm-3">
               <label>[Adjust 0.00]</label>
            </div>
            <div class="col-sm-3">
               <label>[Total 0.00]</label>
            </div>
         </div>
         <div class="col-sm-12">
            <div class="row margin-top">
               <label>Overtime</label>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-10">
               <div class="row">
                  <div class="col-sm-3 txt-center">
                     <label>OT Type</label>
                  </div>
                  <div class="col-sm-3 txt-center">
                     <label>Rate</label>
                  </div>
                  <div class="col-sm-3 txt-center">
                     <label>Actual</label>
                  </div>
                  <div class="col-sm-3 txt-center">
                     <label>Adjustment</label>
                  </div>
               </div>
            </div>
            <div class="col-sm-2 txt-center">
               <label>Total</label>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-10">
               <div class="row">
                  <div class="col-sm-3 label">
                     <label>RG Day:</label>
                  </div>
                  <div class="col-sm-3">
                     <input type="text" name="OTTypeRGDay" class="form-input number--">
                  </div>
                  <div class="col-sm-3">
                     <input type="text" name="RateRGDay" class="form-input number--">
                  </div>
                  <div class="col-sm-3">
                     <input type="text" name="ActualRGDay" class="form-input number--">
                  </div>
               </div>
            </div>
            <div class="col-sm-2">
               <input type="text" name="AdjustmentRGDay" class="form-input number--">
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-sm-10">
               <div class="row">
                  <div class="col-sm-3 label">
                     <label>WE/HD/RD:</label>
                  </div>
                  <div class="col-sm-3">
                     <input type="text" name="OTTyWEHDRD" class="form-input number--">
                  </div>
                  <div class="col-sm-3">
                     <input type="text" name="RateWEHDRD" class="form-input number--">
                  </div>
                  <div class="col-sm-3">
                     <input type="text" name="ActualWEHDRD" class="form-input number--">
                  </div>
               </div>
            </div>
            <div class="col-sm-2">
               <input type="text" name="AdjustmentWEHDRD" class="form-input number--">
            </div>
         </div>
      </div>
   </div>
   <div class="row margin-top txt-right">
      <?php createButton("ADJUSTMENTS","btnAdjust","btn-cls4-sea","",""); ?>
   </div>
</div>