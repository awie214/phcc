<?php
//require 'SystemFunction.php';
session_start();
require "conn.e2e.php";
include 'constant.e2e.php';
require_once pathClass."0620functions.e2e.php";
require_once pathClass."SysFunctions.e2e.php";
$sys = new SysFunctions();
$task = getvalue("task");
$sess = getvalue("hSess");
$user = getvalue("hUser");
$trnmode = getvalue("hTrnMode");
$selected_refid = getvalue("refid");
$uid = getvalue("uid");
$dateLOG = date("mdY")." ".date("h:i:s A");
$MAC = "";
$CompanyId = getvalue("hCompanyID");
$BranchId = getvalue("hBranchID");

if ($task=="ajxDeleteRecord") {
   $js = "";
   $dbtable = getvalue("table");
   $refid = getvalue("refid");
   $criteria  = " WHERE RefId = $refid";
   $criteria .= " LIMIT 1";
   $recordSet = f_Find($dbtable,$criteria);
   $rowcount = mysqli_num_rows($recordSet);
   if ($rowcount) {
      $tableRS   = array();
      $tableRS   = mysqli_fetch_assoc($recordSet);
      $fldsNVal = json_encode($tableRS);
      $MAC = getMAC();
      $vals = "'$dbtable','','$fldsNVal','D',";
      $flds = "`TableName`, `ModuleName`, `FldnVal`, `TrnType`,";
      //$SaveSuccessfull = f_SaveRecord("NEWSAVE","trnlog",$flds,$vals);
      $SaveSuccessfull = 1;
      if (is_numeric($SaveSuccessfull)) {
         $count = mysqli_query($conn,"DELETE FROM `$dbtable` WHERE RefId = $refid");
         //$count = $db->exec("DELETE FROM `$dbtable` WHERE RefId = $refid");
         if ($count) {
            $js .= "afterDelete();";
         } else {
            $js .= "alert('Delete Error !!!\n');";
         }
      }
   } else {
      $js .= "alert('No Record Found');";
   }
   echo $js;
}
else if ($task=="ajxSaveRecord") {
   $cookie_name = "cookies_Object_Value";
   $Field_and_Value = "";
   $FieldType = "";
   $Fields = "";
   $Values = "";
   $SaveSuccessfull = false;
   $dbtable = getvalue("table");
   $refid   = getvalue("refid");
   $Field_and_Value = getvalue("obj_n_value");
   $js = "";
   if ($dbtable != "") {
      //if(!isset($_COOKIE[$cookie_name])) {
      if ($Field_and_Value == "") {
         $js .= 'alert("Fields and Values are not set! [SysErr]")';
      } else {
         if ($refid == 0) {
            if ($dbtable=="updates201") {
               $t = time();
               $Field_and_Value .= "hidden|date_TrnDate|".date("Y-m-d",$t)."!";
               $Field_and_Value .= "hidden|date_TrnTime|".date("H:i:s",$t)."!";
               $Field_and_Value .= "hidden|char_Status|P!";
            }
            $Field_and_Value_Array = explode("!",$Field_and_Value);
            for ($i=0;$i<count($Field_and_Value_Array) - 1;$i++) {
               $Items = $Field_and_Value_Array[$i];
               $Items_Arr = explode("|",$Items);
               $FieldType_Arr = explode("_",$Items_Arr[1]);
               $FieldType = $FieldType_Arr[0];

               if ($FieldType_Arr[1] == "hNewReToken") {
                  $Fields .= "`Password`, ";
               } else {
                  $Fields .= "`$FieldType_Arr[1]`, ";
               }
               $trimmedValue = trim($Items_Arr[2]);
               $trimmedValue = mysqli_real_escape_string($conn,$trimmedValue);

               if ($FieldType == "sint" ||
                   $FieldType == "bint" ||
                   $FieldType == "deci") {
                  if ($Items_Arr[2] == '') {
                     $Values .= "'0', ";
                  } else {
                     $Values .= "'$trimmedValue', ";
                  }
               } else if ($FieldType == "date") {
                  if ($Items_Arr[2] == '') {
                     $Values .= "'1901-01-01', ";
                  } else {
                     $Values .= "'$trimmedValue', ";
                  }
               } else {
                  $Values .= "'$trimmedValue', ";
               }
               /*$Values .= "'$Items_Arr[2]', ";*/
            }
            $SaveSuccessfull = f_SaveRecord("NEWSAVE",$dbtable,$Fields,$Values);
            if (is_numeric($SaveSuccessfull)) {
               $js .= "afterNewSave($SaveSuccessfull);";
            } else {
               $savingError = fopen($sys->publicPath('syslog/savingError.log'), 'a+');
               fwrite($savingError,$dateLOG."-- [ADD MODE]".$SaveSuccessfull."\n");
               fclose($savingError);
               $js .= 'alert("'.$SaveSuccessfull.'");';
            }
         } else {
            //$Field_and_Value = $_COOKIE[$cookie_name];
            $Field_and_Value_Array = explode("!",$Field_and_Value);
            $FieldsNValues = "";
            $currentRemarks = FFirstRefId($dbtable,$refid,"Remarks");

            for ($i=0;$i<count($Field_and_Value_Array) - 1;$i++) {
               $Items = $Field_and_Value_Array[$i];
               $Items_Arr = explode("|",$Items);
               $FieldType_Arr = explode("_",$Items_Arr[1]);
               $FieldType = $FieldType_Arr[0];
               $Fields .= "`$FieldType_Arr[1]`, ";


               /*if ($FieldType == "sint" ||
                   $FieldType == "bint" ||
                   $FieldType == "deci") {
                  $FieldsNValues .= "$FieldType_Arr[1] = $Items_Arr[2], ";
               }
               else {
                  $fldvalue = $Items_Arr[2];
                  if ($FieldType_Arr[1] == "Remarks") {
                     $t = time();
                     $fldvalue = date("Y-m-d",$t).":".$fldvalue."\n".$currentRemarks;
                  }
                  $FieldsNValues .= "`$FieldType_Arr[1]` = '$fldvalue', ";
               }*/

               if ($FieldType == "sint" ||
                   $FieldType == "bint" ||
                   $FieldType == "deci") {
                  if ($Items_Arr[2] == '') {
                     $FieldsNValues .= "$FieldType_Arr[1] = '0', ";
                  } else {
                     $FieldsNValues .= "$FieldType_Arr[1] = '$Items_Arr[2]', ";
                  }
               } else if ($FieldType == "date") {
                  if ($Items_Arr[2] == '') {
                     $FieldsNValues .= "$FieldType_Arr[1] = '0000-00-00', ";
                  } else {
                     $FieldsNValues .= "$FieldType_Arr[1] = '$Items_Arr[2]', ";
                  }
               } else {
                  $fldvalue = $Items_Arr[2];
                  if ($FieldType_Arr[1] == "Remarks") {
                     $t = time();
                     $fldvalue = date("Y-m-d",$t).":".$fldvalue."\n".$currentRemarks;
                  }
                  $FieldsNValues .= "`$FieldType_Arr[1]` = '$fldvalue', ";
               }


            }
            //echo $FieldsNValues;

            $SaveSuccessfull = f_SaveRecord("EDITSAVE",$dbtable,$FieldsNValues,$refid);
            if ($SaveSuccessfull!="") {
               $savingError = fopen($sys->publicPath('syslog/savingError.log'), 'a+');
               fwrite($savingError,$dateLOG."-- [EDIT MODE]".$SaveSuccessfull."\n");
               fclose($savingError);
               $js .= 'alert("'.$SaveSuccessfull.'");';
            } else {
               $js .= "afterEditSave(".$refid.");";
            }
         }

      }
   } else {
      $js .= 'alert("Oooops!!! No table assigned!!!");';
   }
   /*$js .= 'delCookie("cookies_Object_Value");';*/
   echo $js;
}
else if ($task=="ajxSaveRecordMulti") {
   $dbtable = getvalue("tbl");
   $fldnval = getvalue("fnv");
   $isMultiple = getvalue("multi");
   echo saveRecord($dbtable,$fldnval,$isMultiple);
}
else if ($task=="checkEntry") {
   $entry    = getvalue("entry");
   $dbtable  = getvalue("table");
   $user     = getvalue("user");
   $scrnFile = getvalue("prog");
   $errCount = 0;
   $str      = "";
   $str      .= $user."->".$scrnFile."->".$dbtable."\nWrong Name Format--\n";
   $Field_and_Value_Array = explode("!",$entry);
   $FieldsNValues = "";
   for ($i=0;$i<count($Field_and_Value_Array) - 1;$i++) {
      $Items         = $Field_and_Value_Array[$i];
      $Items_Arr     = explode("|",$Items);
      $FieldType_Arr = explode("_",$Items_Arr[1]);
      $FieldType     = $FieldType_Arr[0];

      if (strpos($Items_Arr[1],"_") <= 0) {
         $str .= $Items_Arr[1]."\n";
         $errCount++;
      } else {
         if (!($FieldType == "sint" ||
               $FieldType == "bint" ||
               $FieldType == "deci" ||
               $FieldType == "char" ||
               $FieldType == "date"))
         {
            $str .= $Items_Arr[1]."\n";
            $errCount++;
         }
      }
   }
   $str .= "----------------------------------------------------------------------";
   if ($errCount>0) {
      $dateLOG = date("mdY")." ".date("h:i:s A");
      $err = fopen('syslog/Error.log', 'a+');
      fwrite($err,$dateLOG."--".$str."\n");
      fclose($err);
   }
   echo $errCount;
} else if ($task=="popTableFieldName") {
   echo
   '<select name="char_FieldName">
      <option value="">Select Field Name to Update</option>';

      $dbtable = getvalue("dbtable");
      $sql = "SHOW COLUMNS FROM `$dbtable`";
      $result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
      while($row = mysqli_fetch_array($result)){
         echo '<option value="'.$row['Field'].'">'.$row['Field'].'</option>';
      }
   echo
   '</select>';
} else if ($task=="getEmployeesInfo") {
   $refid = getvalue("emprefid");
   $criteria  = " WHERE RefId = $refid";
   $criteria .= " LIMIT 1";
   $recordSet = f_Find("Employees",$criteria);
   if (mysqli_num_rows($recordSet)) {
      $employees = array();
      $employees = mysqli_fetch_assoc($recordSet);
      echo json_encode($employees);
   } else {
      echo "No Record Available";
   }
} else if ($task=="InsertWorkExp") {
   
   //$CompanyId = getvalue("hCompanyID");
   //$BranchId = getvalue("hBranchID");
   $empRefId = getvalue("emprefid");
   // updates first the work experience
   $where = "WHERE CompanyRefId = ".$CompanyId." AND BranchRefId = ".$BranchId;
   $where .= " AND EmployeesRefId = $empRefId";
   $rowEmpInfo = FindLast("empinformation",$where,"*");
   if ($rowEmpInfo) {
      $flds = "";
      $values = "";
      $fldnval = "";
      $wAgencyRefId        = $rowEmpInfo["AgencyRefId"];
      $wPositionRefId      = $rowEmpInfo["PositionRefId"];
      $wEmpStatusRefId     = $rowEmpInfo["EmpStatusRefId"];
      $wSalaryGradeRefId   = $rowEmpInfo["SalaryGradeRefId"];
      $wStepIncrementRefId = $rowEmpInfo["StepIncrementRefId"];
      $wPayrateRefId       = $rowEmpInfo["PayRateRefId"];
      $wWorkStartDate      = $rowEmpInfo["StartDate"];
      $wWorkEndDate        = $rowEmpInfo["EndDate"];
      $wSalaryAmount       = $rowEmpInfo["SalaryAmount"];
      $wLWOP               = "";
      $wReason             = "Movement";
      $wExtDate            = $rowEmpInfo["RehiredDate"];
      $wSeparatedDate      = $rowEmpInfo["ResignedDate"];
      $wApptStatusRefId    = 0;
      $wisGovtService      = 1;
      $wPayGroup           = $rowEmpInfo["PayPeriod"];
      $flds .= "`EmployeesRefId`,";
      $values .= "'$empRefId',";
      if ($wAgencyRefId != "") {
         $flds .= "`AgencyRefId`, ";
         $values .= "'$wAgencyRefId', ";
         $fldnval .= "AgencyRefId = '$wAgencyRefId',";
      }
      if ($wPositionRefId != "") {
         $flds .= "`PositionRefId`, ";
         $values .= "'$wPositionRefId', ";
         $fldnval .= "PositionRefId = '$wPositionRefId',";
      }
      if ($wEmpStatusRefId != "") {
         $flds .= "`EmpStatusRefId`, ";
         $values .= "'$wEmpStatusRefId', ";
         $fldnval .= "EmpStatusRefId = '$wEmpStatusRefId',";
      }
      if ($wSalaryGradeRefId != "") {
         $flds .= "`SalaryGradeRefId`, ";
         $values .= "'$wSalaryGradeRefId', ";
         $fldnval .= "SalaryGradeRefId = '$wSalaryGradeRefId',";
      }
      if ($wStepIncrementRefId != "") {
         $flds .= "`StepIncrementRefId`, ";
         $values .= "'$wStepIncrementRefId', ";
         $fldnval .= "StepIncrementRefId = '$wStepIncrementRefId',";
      }
      if ($wPayrateRefId != "") {
         $flds .= "`PayrateRefId`, ";
         $values .= "'$wPayrateRefId', ";
         $fldnval .= "PayrateRefId = '$wPayrateRefId',";
      }
      if ($wWorkStartDate != "") {
         $flds .= "`WorkStartDate`, ";
         $values .= "'$wWorkStartDate', ";
         $fldnval .= "StartDate = '$wWorkStartDate',";
      }
      if ($wWorkEndDate != "") {
         $flds .= "`WorkEndDate`, ";
         $values .= "'$wWorkEndDate', ";
         $fldnval .= "EndDate = '$wWorkEndDate',";
      }
      if ($wSalaryAmount != "") {
         $flds .= "`SalaryAmount`, ";
         $values .= "'$wSalaryAmount', ";
         $fldnval .= "SalaryAmount = '$wSalaryAmount',";
      }
      if ($wLWOP != "") {
         $flds .= "`LWOP`, ";
         $values .= "'$wLWOP', ";
      }
      if ($wReason != "") {
         $flds .= "`Reason`, ";
         $values .= "'$wReason', ";
      }
      if ($wExtDate != "") {
         $flds .= "`ExtDate`, ";
         $values .= "'$wExtDate', ";
         $fldnval .= "RehiredDate = '$wExtDate',";
      }
      if ($wSeparatedDate != "") {
         $flds .= "`SeparatedDate`, ";
         $values .= "'$wSeparatedDate', ";
         $fldnval .= "ResignedDate = '$wSeparatedDate',";
      }
      if ($wApptStatusRefId != "") {
         $flds .= "`ApptStatusRefId`, ";
         $values .= "'$wApptStatusRefId', ";
         $fldnval .= "ApptStatusRefId = '$wApptStatusRefId',";
      }
      if ($wisGovtService != "") {
         $flds .= "`isGovtService`, ";
         $values .= "'$wisGovtService', ";
      }
      if ($wPayGroup != "") {
         $flds .= "`PayGroup`, ";
         $values .= "'$wPayGroup', ";
      }
      $flds .= "`Remarks`, ";
      $values .= "'Auto Add From Emp. Movement Module', ";
      //echo $flds."<br><br>".$values."<br><br>";
      $new_refid = f_SaveRecord("NEWSAVE","employeesworkexperience",$flds,$values);
      //echo $new_refid;
      if ($new_refid > 0) {
         $update_empinfo = f_SaveRecord("EDITSAVE","empinformation",$fldnval,$empRefId);
         if ($update_empinfo == "") {
            echo "success";   
         }
      }
   } else {
      echo "NO EMP. INFORMATION RECORD\nyou need update Employment Information Module";
   }
} else if ($task == "getRecord") {
   $table = getvalue("tbl");
   $refid = getvalue("refid");

   $criteria  = " WHERE RefId = $refid";
   $criteria .= " LIMIT 1";
   $recordSet = f_Find($table,$criteria);
   $rowcount = mysqli_num_rows($recordSet);
   if ($rowcount > 0) {
      $tblRows = array();
      $tblRows = mysqli_fetch_assoc($recordSet);
      if ($table == "accountability" && $tblRows['CustodianRefId']) {
         $qry = "select `LastName`,`FirstName` FROM employees WHERE RefId = ".$tblRows['CustodianRefId'];
         $result = mysqli_query($conn,$qry) or die(mysqli_error($conn));
         if ($result) {
            $row = mysqli_fetch_assoc($result);
            $CustodianName = $row["LastName"].", ".$row["FirstName"];
            $newArr = ["CustodianName"=>$CustodianName];
            echo json_encode(array_merge($tblRows,$newArr));
         }
         else echo json_encode($tblRows);
      }
      else echo json_encode($tblRows);
   }
   else echo "No Record";

} else if ($task == "writesyslog") {
   $file = getvalue("file");
   $param = getvalue("param");
   $savingError = fopen($sys->publicPath('syslog/ParamError.log'), 'a+');
   fwrite($savingError,$dateLOG."-- [File]".$file." [Param] ".$param." \n");
   fclose($savingError);
   echo 'self.location="idx.e2e.php";';
}
/*else if ($task == "scrn") {
   $gPARAM = "";
   $gPARAM .= "hSess=" . $_SESSION['sess_Session'];
   $gPARAM .= "&hUser=" . $_SESSION['sess_user'];
   $gPARAM .= "&hUserLvl=" . $_SESSION['sess_ulvl'];
   $gPARAM .= "&hCompanyID=" . $_SESSION['CompanyId'];
   $gPARAM .= "&hBranchID=" . $_SESSION['BranchId'];
   $gPARAM .= "&hUserRefID=" . $_SESSION['sess_user_refid'];
   $gPARAM .= "&usrCode=" . $_SESSION['SysGroupRefId'];
   $gPARAM .= "&file=" . getvalue("file");
   $gPARAM .= getvalue("param");
   //need validation if user is currently login
   header ("Location: GlobalCaller.e2e.php?".$gPARAM);
}*/
mysqli_close($conn);
?>