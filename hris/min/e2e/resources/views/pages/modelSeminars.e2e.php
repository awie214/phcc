<?php
   require_once "constant.e2e.php";
   require_once "inc_model.e2e.php";
   require_once "incUtilitiesJS.e2e.php";
   
   $noofhrs = "";
   if (isset($rowcount)) {
      if ($rowcount) {
         $noofhrs = $row["NoOfHrs"];
      }
   }
   
   /*
   $rs = SelectEach("seminarclass","");
   $SeminarClass = $rs["RefId"];

   $row = SelectEach("sponsor","");
   if ($row) $sponsor = $row["RefId"];   
   
   $qry = SelectEach("seminartype","");
   if ($qry) $semtype = $qry["RefId"];
   
   $recordset = SelectEach("seminarplace","");
   if ($recordset) $SemPlace = $recordset["RefId"];
   */
   

   
?>
   <div class="container" id="EntryScrn">
      <div class="row">
         <div class="col-xs-6">
            <?php if ($ScrnMode != 1) { ?>
               <div class="row">
                  <ul class="nav nav-pills">
                     <li class="active" style="font-size:12pt;font-weight:600;">
                        <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                        <?php echo $refid; ?>
                        </span></a>
                     </li>
                  </ul>
               </div>
            <?php } ?>
            <!--<div class="row margin-top10">
               <div class="form-group">
                  <label class="control-label" for="inputs">CODE:</label><br>
                  <input class="form-input saveFields-- uCase-- mandatory" type="text" placeholder="Code" <?php if ($ScrnMode == 2) { echo "disabled"; } else { echo $disabled; }  ?>
                     id="inputs" name="char_Code" style='width:75%' value="<?php echo $code; ?>" autofocus>
               </div>
            </div>-->
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">NAME:</label><br>
                  <input class="form-input saveFields-- uCase-- mandatory" type="text" placeholder="name" <?php echo $disabled; ?>
                     id="inputs" name="char_Name" style='width:75%' value="<?php echo $name; ?>">
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">DESCRIPTION:</label>
                  <textarea class="form-input saveFields--" rows="5" name="char_Description" <?php echo $disabled; ?>
                  placeholder="Description" value="<?php echo $description; ?>"></textarea>
               </div>
            </div>
            <!--
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">SEMINAR CLASSIFICATION:</label><br>
                  <?php
                     createSelect("SeminarClass",
                                  "sint_SeminarClassRefId",
                                  "",100,"Name","Select Seminar Classification",$disabled);
                  ?>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">SPONSOR:</label><br>
                  <?php
                     createSelect("Sponsor",
                                  "sint_SponsorRefId",
                                  "",100,"Name","Select Sponsor",$disabled);
                  ?>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">SEMINAR PLACE:</label><br>
                  <?php
                     createSelect("SeminarPlace",
                                  "sint_SeminarPlaceRefId",
                                  "",100,"Name","Select Seminar Place",$disabled);
                  ?>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">SEMINAR TYPE:</label><br>
                  <?php
                     createSelect("SeminarType",
                                  "sint_SeminarTypeRefId",
                                  "",100,"Name","Select Seminar Type",$disabled);
                  ?>
               </div>
            </div>
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">NO. OF HRS.:</label><br>
                  <input class="form-input saveFields-- number--" type="text" placeholder="no. of hrs." <?php echo $disabled; ?>
                     id="inputs" name="sint_NumofHrs" style='width:50%' value="<?php echo $noofhrs; ?>">
               </div>
            </div>
            -->
            <div class="row">
               <div class="form-group">
                  <label class="control-label" for="inputs">REMARKS:</label>
                  <textarea class="form-input saveFields--" rows="5" name="char_Remarks" <?php echo $disabled; ?>
                  placeholder="remarks"><?php echo $remarks; ?></textarea>
               </div>
            </div>
         </div>
      </div>
   </div>
   