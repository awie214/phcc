<?php
   $object     = file_get_contents(json."system.json");
   $obj_arr    = json_decode($object, true);
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <style type="text/css">
         
      </style>
      <script type="text/javascript">
         $(document).ready(function () {
            remIconDL();
            $("#saveManagement").click(function () {
               var obj = "";
               var emprefid = $("#hRefIdSelected").val();
               $("[id*='idx_']").each(function () {
                  if ($(this).is(":checked")) {
                     var idx = $(this).attr("id").split("_")[1];
                     obj += idx + "|";
                  }
               });
               if (emprefid == "") {
                  alert("No User Selected");
               } else {
                  $.post("process.e2e.php",
                  {
                     fn:"UpdateUserManagement",
                     obj:obj,
                     emprefid:emprefid,
                     division: $("#sint_DivisionRefId").val()

                  },
                  function(data,status) {
                     if(status == "error") return false;
                     if(status == "success"){
                        try {
                           eval(data);
                           gotoscrn($("#hProg").val(),"");
                        }
                        catch (e)
                        {
                           if (e instanceof SyntaxError) {
                               alert(e.message);
                           }
                        }
                     }
                  });
               }
               
            });
         });
         function selectMe(emprefid) {
            var value = $("#emp_" + emprefid).html();
            $("[id*='idx_']").attr("checked",false);
            $("#hRefIdSelected").val(emprefid);
            $("#panel-txt").html();
            $("#panel-txt").html(value);
            $.get("process.e2e.php",
            {
               fn:"getUserManagement",
               emprefid:emprefid
            },
            function(data,status) {
               if(status == "error") return false;
               if(status == "success"){
                  try {
                     eval(data);
                     
                  }
                  catch (e)
                  {
                     if (e instanceof SyntaxError) {
                         alert(e.message);
                     }
                  }
               }
            });
         }
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($modTitle); ?>
            <div class="container-fluid margin-top10">
               <div class="row">
                  <div class="col-xs-4">
                     <div class="myPanel">
                        <div class="panel-top">Admin Users</div>
                        <div class="panel-mid">
                           <?php
                              $rs       = SelectEach("sysuser","");
                              if ($rs) {
                                 $count = 0;
                                 while ($row = mysqli_fetch_assoc($rs)) {
                                    $count++;
                                    $emprefid = $row["EmployeesRefId"];
                                    $where   = "WHERE RefId = '$emprefid'";
                                    $rows    = FindFirst("employees",$where,"`LastName`, `FirstName`, `RefId`");
                                    echo '<div class="Employees--" refid="'.$rows["RefId"].'" style="padding:6px;" id="emp_'.$rows["RefId"].'">';
                                    echo $count.' - '.strtoupper($rows["LastName"]).", ".strtoupper($rows["FirstName"]);

                                    echo '</div>';
                                 }
                              }

                           ?>
                        </div>
                        <div class="panel-bottom">
                           <input type="hidden" id="hRefIdSelected" name="hRefIdSelected" value="">
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-8">
                     <div class="myPanel">
                        <div class="panel-top">
                           SYSTEMS FOR <span id="panel-txt"></span>
                        </div>
                        <div class="panel-mid">
                           <div class="row">
                              <div class="col-xs-6">
                                 <label>Select Division:</label>
                                 <?php
                                    createSelect("Division",
                                                 "sint_DivisionRefId",
                                                 "",100,"Name","Select Division","");
                                 ?>
                              </div>
                           </div>
                           <br>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <table width="100%" border="1" class="table table-order-column table-striped table-bordered table-hover">
                                    <thead>
                                       <tr>
                                          <th style="width: 10%;">
                                             
                                          </th>
                                          <th style="width: 90%;">
                                             System
                                          </th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <?php
                                          foreach ($obj_arr["SPMS"] as $key => $value) {
                                             echo '
                                                <tr>
                                                   <td class="text-center">
                                                      <input type="checkbox" id="idx_'.$key.'">
                                                   </td>
                                                   <td>
                                                      <label>'.strtoupper($value).'</label>
                                                   </td>
                                                </tr>
                                             ';
                                          }
                                       ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                           <button class="btn-cls4-sea" type="button" id="saveManagement">
                              <i class="fa fa-save"></i>
                              SAVE
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

            <?php
               footer();
               
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>