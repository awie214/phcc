<?php
	error_reporting(E_ALL);
   	ini_set('display_errors', 1);
	session_start();
	include_once "constant.e2e.php";
	include_once pathClass.'0620functions.e2e.php';
	$user = getvalue("hUser");

	$funcname = getvalue("fn");
	$params   = getvalue("params");
	if (!empty($funcname)) {
   	$funcname($params);
	} else {
   	echo 'alert("Error... No Function defined");';
	}
   function getSPMS() {
      $refid   = getvalue("refid");
      $table   = getvalue("table");
      $row = FindFirst($table,"WHERE RefId = '$refid'","*");
      if ($row) {
         switch ($table) {
            case 'spms_ops':
               $OfficeRefId       = $row["OfficeRefId"];      
               echo '$("#sint_OfficeRefId").val("'.$OfficeRefId.'");'."\n";
               break;
            case 'spms_dps':
               $DivisionRefId       = $row["DivisionRefId"];
               echo '$("#sint_DivisionRefId").val("'.$DivisionRefId.'");'."\n";
               break;
            case 'spms_ips':
               $question1           = $row["question1"];
               $question2           = $row["question2"];
               $question3           = $row["question3"];
               echo '$("#question1").val("'.$question1.'");'."\n";
               echo '$("#question2").val("'.$question2.'");'."\n";
               echo '$("#question3").val("'.$question3.'");'."\n";
               break;
         }
         
         $semester            = $row["semester"];
         $year                = $row["year"];
         $rating              = $row["rating"];
         $total_rating        = $row["total_rating"];
         $premium_point       = $row["premium_point"];
         $overall_rating      = $row["overall_rating"];
         $adjectival_rating   = $row["adjectival_rating"];

         echo '$("#semester").val("'.$semester.'");'."\n";
         echo '$("#year").val("'.$year.'");'."\n";
         echo '$("#rating").val("'.$rating.'");'."\n";
         echo '$("#total_rating").val("'.$total_rating.'");'."\n";
         echo '$("#premium_point").val("'.$premium_point.'");'."\n";
         echo '$("#overall_rating").val("'.$overall_rating.'");'."\n";
         echo '$("#adjectival_rating").val("'.$adjectival_rating.'");'."\n";
         
      } 
   }
   function selectRating($value) {
      $str = '';
      switch ($value) {
         case '0':
            $str .= '<option value="0" selected>0</option>';
            $str .= '<option value="1">1</option>';
            $str .= '<option value="2">2</option>';
            $str .= '<option value="3">3</option>';
            $str .= '<option value="4">4</option>';
            $str .= '<option value="5">5</option>';
            break;
         case '1':
            $str .= '<option value="0">0</option>';
            $str .= '<option value="1" selected>1</option>';
            $str .= '<option value="2">2</option>';
            $str .= '<option value="3">3</option>';
            $str .= '<option value="4">4</option>';
            $str .= '<option value="5">5</option>';
            break;
         case '2':
            $str .= '<option value="0">0</option>';
            $str .= '<option value="1">1</option>';
            $str .= '<option value="2" selected>2</option>';
            $str .= '<option value="3">3</option>';
            $str .= '<option value="4">4</option>';
            $str .= '<option value="5">5</option>';
            break;
         case '3':
            $str .= '<option value="0">0</option>';
            $str .= '<option value="1">1</option>';
            $str .= '<option value="2">2</option>';
            $str .= '<option value="3" selected>3</option>';
            $str .= '<option value="4">4</option>';
            $str .= '<option value="5">5</option>';
            break;
         case '4':
            $str .= '<option value="0">0</option>';
            $str .= '<option value="1">1</option>';
            $str .= '<option value="2">2</option>';
            $str .= '<option value="3">3</option>';
            $str .= '<option value="4" selected>4</option>';
            $str .= '<option value="5">5</option>';
            break;
         case '5':
            $str .= '<option value="0">0</option>';
            $str .= '<option value="1">1</option>';
            $str .= '<option value="2">2</option>';
            $str .= '<option value="3">3</option>';
            $str .= '<option value="4">4</option>';
            $str .= '<option value="5" selected>5</option>';
            break;
      }
      return $str;
   }

   function selectType($value) {
      $str = "";
      switch ($value) {
         case 'Core Function':
            $str .= '<option value="Core Function" selected>Core Function</option>';
            $str .= '<option value="Strategic Function">Strategic Function</option>';
            break;
         case 'Strategic Function':
            $str .= '<option value="Core Function">Core Function</option>';
            $str .= '<option value="Strategic Function" selected>Strategic Function</option>';
            break;
         default:
            $str .= '<option value="Core Function">Core Function</option>';
            $str .= '<option value="Strategic Function">Strategic Function</option>';
            break;
      }
      return $str;
   }

   function showObjectives($table,$idx,$value) {
      if ($table == "spms_ops") {
         $value = getRecord("objectives",$value,"Name");
         $str = '
            <div class="row margin-top">
               <div class="col-xs-12">
                  <label>Objectives:</label>
                  <input type="text" class="form-input" name="objective_'.$idx.'" id="objective_'.$idx.'" value="'.$value.'">
               </div>
            </div>
         ';
      } else {
         $str = '
            <div class="row margin-top">
               <div class="col-xs-12">
                  <label>Objectives:</label>
                  <select class="form-input" id="objective_'.$idx.'" name="objective_'.$idx.'">
                     <option value="0">Select Objective</option>
         ';
                     $rs = SelectEach("objectives","");
                     if ($rs) {
                        while ($row = mysqli_fetch_assoc($rs)) {
                           $refid = $row["RefId"];
                           $name  = $row["Name"];
                           $name  = strtoupper($name);
                           if ($refid == $value) {
                              $str .= '<option value="'.$refid.'" selected>'.$name.'</option>';
                           } else {
                              $str .= '<option value="'.$refid.'">'.$name.'</option>';
                           }
                        }
                     }
         echo '
                  </select>
                  
               </div>
            </div>
         ';
         //'.createSelect("objectives","objective_".$idx,$value,100,"Name","Select Objective","").'
      }
      return $str;
   }
   function showOtherObjectives($table,$idx) {
      $str = "";
      if ($table != "spms_ops") {
         $str = '
            <div class="row margin-top">
               <div class="col-xs-12">
                  <input type="checkbox" name="other_'.$idx.'" id="other_'.$idx.'" onclick="enableOther(this.id);">
                  <label for="other_'.$idx.'">Other objective:</label>
                  <input type="text" class="form-input" name="newobjective_'.$idx.'" id="newobjective_'.$idx.'" disabled>
               </div>
            </div>
         ';
      }
      return $str;
   }
   function showAccountableBudget($table,$count,$accountable,$budget) {
      $str = "";
      if ($table == "spms_ops") {
         $str = '
            <div class="col-xs-4">
               <div class="row">
                  <div class="col-xs-12">
                     <label>Accountable Division:</label>
                     <input type="text" class="form-input" name="accountable_'.$count.'" id="accountable_'.$count.'" value="'.$accountable.'">
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-12">
                     <label>Alloted Budget:</label>
                     <input type="text" class="form-input" name="budget_'.$count.'" id="budget_'.$count.'" value="'.$budget.'">
                  </div>
               </div>
            </div>
         ';
      }
      return $str;

   }
   function getSPMS_detail() {
      $refid   = getvalue("refid");
      $table   = getvalue("table");
      $count   = 0;
      $str     = "";
      switch ($table) {
         case 'spms_ops':
            $where = "WHERE ops_id = '$refid'";
            $table2 = "ops_details";
            break;
         case 'spms_dps':
            $where = "WHERE dps_id = '$refid'";
            $table2 = "dps_details";
            break;
         case 'spms_ips':
            $where = "WHERE ips_id = '$refid'";
            $table2 = "ips_details";
            break;
         default:
            $where = "";
            break;
      }
      if ($where != "") {
         $rs = SelectEach($table2,$where);
         if ($rs) {
            while ($row = mysqli_fetch_assoc($rs)) {
               $count++;
               $spms_refid       = $row["RefId"];
               $type             = $row["type"];
               $objectives_id    = $row["objectives_id"];
               $measure          = $row["measure"];
               $target           = $row["target"];
               $weight           = $row["weight"];
               $weightedscore    = $row["weightedscore"];
               $rawscore         = $row["rawscore"];
               $accomplishment   = $row["accomplishment"];
               $quality          = $row["quality"];
               $effectiveness    = $row["effectiveness"];
               $timeliness       = $row["timeliness"];
               $q1               = $row["q1"];
               $q2               = $row["q2"];
               $q3               = $row["q3"];
               $q4               = $row["q4"];
               $q5               = $row["q5"];
               $e1               = $row["e1"];
               $e2               = $row["e2"];
               $e3               = $row["e3"];
               $e4               = $row["e4"];
               $e5               = $row["e5"];
               $t1               = $row["t1"];
               $t2               = $row["t2"];
               $t3               = $row["t3"];
               $t4               = $row["t4"];
               $t5               = $row["t5"];
               if ($table == "spms_ops") {
                  $accountable = $row["accountable"];
                  $budget      = $row["budget"];
               } else {
                  $accountable = $budget = "";
               }
               $str .= '
                  <div id="Entry_'.$count.'" class="entry201">
                     <div class="row margin-top">
                        <div class="col-xs-6">
                           <label>#'.$count.' </label>
                           <input type="hidden" name="refid_'.$count.'" id="refid_'.$count.'" value="'.$spms_refid.'">
                        </div>
                     </div>
                     <?php bar(); ?>
                     <div class="row margin-top">
                        <div class="col-xs-4">
                           <label>Select Type:</label>
                           <select class="form-input" name="type_'.$count.'" id="type_'.$count.'">
                              '.selectType($type).'
                           </select>
                        </div>
                     </div>
                     '.showObjectives($table,$count,$objectives_id).'
                     '.showOtherObjectives($table,$count).'
                     <div class="row margin-top">
                        <div class="col-xs-4">
                           <label>Measure:</label>
                           <textarea class="form-input" rows="4" name="measure_'.$count.'" id="measure_'.$count.'">'.htmlentities($measure).'</textarea>
                        </div>
                        <div class="col-xs-4">
                           <label>Target:</label>
                           <textarea class="form-input" rows="4" name="target_'.$count.'" id="target_'.$count.'">'.htmlentities($target).'</textarea>
                        </div>
                        <div class="col-xs-4">
                           <label>Actual Accomplishments:</label>
                           <textarea class="form-input" rows="4" name="accomplishment_'.$count.'" id="accomplishment_'.$count.'">'.htmlentities($accomplishment).'</textarea>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-4">
                           <div class="row">
                              <div class="col-xs-12 text-center">
                                 <label>RATING</label>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4">
                                 <div class="row">
                                    <div class="col-xs-12 text-right">
                                       <label>Quality:</label>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12 text-right">
                                       <label>Effectiveness:</label>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12 text-right">
                                       <label>Timeliness:</label>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-8">
                                 <div class="row">
                                    <div class="col-xs-12">
                                       <select class="form-input" name="quality_'.$count.'" id="quality_'.$count.'">
                                          '.selectRating($quality).'
                                       </select>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <select class="form-input" name="effectiveness_'.$count.'" id="effectiveness_'.$count.'">
                                          '.selectRating($effectiveness).'
                                       </select>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <select class="form-input" name="timeliness_'.$count.'" id="timeliness_'.$count.'">
                                          '.selectRating($timeliness).'
                                       </select>
                                    </div>
                                 </div>  
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-2">
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <label>Weight:</label>
                                 <input type="text" class="form-input" name="weight_<?php echo $x; ?>" id="weight_<?php echo $x; ?>">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <label>Weighted Score:</label>
                                 <input type="text" class="form-input" name="weightedscore_'.$count.'" id="weightedscore_'.$count.'" value="'.$weightedscore.'">
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-2">
                           <div class="row">
                              <div class="col-xs-12">
                                 <label>Raw Score:</label>
                                 <input type="text" class="form-input" name="rawscore_'.$count.'" id="rawscore_'.$count.'" value="'.$rawscore.'">
                              </div>
                           </div>
                        </div>
                        '.showAccountableBudget($table,$count,$accountable,$budget).'
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-12 text-center">
                           <label>RATING MATRIX</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label>Quality:</label>
                        </div>
                     </div>
                     <?php bar(); ?>
                     <div class="row margin-top">
                        <div class="col-xs-2">
                           <label>Rating 1:</label>
                           <textarea class="form-input" rows="2" name="q1_'.$count.'" id="q1_'.$count.'">'.htmlentities($q1).'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 2:</label>
                           <textarea class="form-input" rows="2" name="q2_'.$count.'" id="q2_'.$count.'">'.htmlentities($q2).'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 3:</label>
                           <textarea class="form-input" rows="2" name="q3_'.$count.'" id="q3_'.$count.'">'.htmlentities($q3).'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 4:</label>
                           <textarea class="form-input" rows="2" name="q4_'.$count.'" id="q4_'.$count.'">'.htmlentities($q4).'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 5:</label>
                           <textarea class="form-input" rows="2" name="q5_'.$count.'" id="q5_'.$count.'">'.htmlentities($q5).'</textarea>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label>Effectiveness</label>
                        </div>
                     </div>
                     <?php bar(); ?>
                     <div class="row margin-top">
                        <div class="col-xs-2">
                           <label>Rating 1:</label>
                           <textarea class="form-input" rows="2" name="e1_'.$count.'" id="e1_'.$count.'">'.htmlentities($e1).'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 2:</label>
                           <textarea class="form-input" rows="2" name="e2_'.$count.'" id="e2_'.$count.'">'.htmlentities($e2).'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 3:</label>
                           <textarea class="form-input" rows="2" name="e3_'.$count.'" id="e3_'.$count.'">'.htmlentities($e3).'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 4:</label>
                           <textarea class="form-input" rows="2" name="e4_'.$count.'" id="e4_'.$count.'">'.htmlentities($e4).'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 5:</label>
                           <textarea class="form-input" rows="2" name="e5_'.$count.'" id="e5_'.$count.'">'.htmlentities($e5).'</textarea>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label>Timeliness:</label>
                        </div>
                     </div>
                     <?php bar(); ?>
                     <div class="row margin-top">
                        <div class="col-xs-2">
                           <label>Rating 1:</label>
                           <textarea class="form-input" rows="2" name="t1_'.$count.'" id="t1_'.$count.'">'.htmlentities($t1).'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 2:</label>
                           <textarea class="form-input" rows="2" name="t2_'.$count.'" id="t2_'.$count.'">'.htmlentities($t2).'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 3:</label>
                           <textarea class="form-input" rows="2" name="t3_'.$count.'" id="t3_'.$count.'">'.htmlentities($t3).'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 4:</label>
                           <textarea class="form-input" rows="2" name="t4_'.$count.'" id="t4_'.$count.'">'.htmlentities($t4).'</textarea>
                        </div>
                        <div class="col-xs-2">
                           <label>Rating 5:</label>
                           <textarea class="form-input" rows="2" name="t5_'.$count.'" id="t5_'.$count.'">'.htmlentities($t5).'</textarea>
                        </div>
                     </div>
                  </div>
                  <br>
               ';
            }
         }
      }
      echo $str;
   }
   function saveSPMS() {
      // print_r($_POST);
      // return false;
      $user_code           = getvalue("hUserGroup");
      $emprefid            = getvalue("hEmpRefId");
      $office              = getvalue("sint_OfficeRefId");
      $division            = getvalue("sint_DivisionRefId");
      $semester            = getvalue("semester");
      $year                = getvalue("year");
      $rating              = getvalue("rating");
      $total_rating        = getvalue("total_rating");
      $premium_point       = getvalue("premium_point");
      $overall_rating      = getvalue("overall_rating");
      $adjectival_rating   = getvalue("adjectival_rating");
      $spmsrefid           = getvalue("sint_SPMSRefId");
      $table2              = getvalue("table2");
      $count               = getvalue("count");
      $table               = getvalue("hTable");
      $question1           = getvalue("question1");
      $question2           = getvalue("question2");
      $question3           = getvalue("question3");

      $error               = 0;
      if ($spmsrefid == 0) {
         $Fields = "`EmployeesRefId`, `semester`, `year`, `rating`, `total_rating`, `premium_point`, ";
         $Fields .= "`overall_rating`, `adjectival_rating`, ";
         if ($table == "spms_ops") {
            $Fields .= "`OfficeRefId`, "; 
         } else if ($table == "spms_dps") {
            $Fields .= "`DivisionRefId`, ";
         } else if ($table == "spms_ips") {
            $Fields .= "`question1`, `question2`, `question3`, ";
         }
         $Values = "'$emprefid', '$semester', '$year', '$rating', '$total_rating', '$premium_point', '$overall_rating',";
         $Values .= "'$adjectival_rating', ";
         if ($table == "spms_ops") {
            $Values .= "'$office', "; 
         } else if ($table == "spms_dps") {
            $Values .= "'$division', ";
         } else if ($table == "spms_ips") {
            $Values .= "'$question1', '$question2', '$question3',";
         }
         $new_refid = f_SaveRecord("NEWSAVE",$table,$Fields,$Values);
         if (is_numeric($new_refid)) {
            for ($a=1; $a <= $count ; $a++) { 
               $type             = getvalue("type_".$a);
               $objective        = realEscape(getvalue("objective_".$a));
               $measure          = realEscape(getvalue("measure_".$a));
               $target           = realEscape(getvalue("target_".$a));
               $weight           = realEscape(getvalue("weight_".$a));
               $quality          = getvalue("quality_".$a);
               $effectiveness    = getvalue("effectiveness_".$a);
               $timeliness       = getvalue("timeliness_".$a);
               $rawscore         = getvalue("rawscore_".$a);
               $weightedscore    = getvalue("weightedscore_".$a);
               $accomplishment   = realEscape(getvalue("accomplishment_".$a));
               $q1               = realEscape(getvalue("q1_".$a));
               $q2               = realEscape(getvalue("q2_".$a));
               $q3               = realEscape(getvalue("q3_".$a));
               $q4               = realEscape(getvalue("q4_".$a));
               $q5               = realEscape(getvalue("q5_".$a));
               $e1               = realEscape(getvalue("e1_".$a));
               $e2               = realEscape(getvalue("e2_".$a));
               $e3               = realEscape(getvalue("e3_".$a));
               $e4               = realEscape(getvalue("e4_".$a));
               $e5               = realEscape(getvalue("e5_".$a));
               $t1               = realEscape(getvalue("t1_".$a));
               $t2               = realEscape(getvalue("t2_".$a));
               $t3               = realEscape(getvalue("t3_".$a));
               $t4               = realEscape(getvalue("t4_".$a));
               $t5               = realEscape(getvalue("t5_".$a));
               if ($table != "spms_ops") {
                  if(isset($_POST["other_".$a])) {
                     $objective = realEscape(getvalue("newobjective_".$a));
                     $objectives_id    = chkFM("objectives",$objective);
                  } else {
                     $objectives_id = $objective;
                  }
               } else {
                  $objectives_id    = chkFM("objectives",$objective);   
                  $accountable      = realEscape(getvalue("accountable_".$a));
                  $budget           = realEscape(getvalue("budget_".$a));
               }
               if (is_numeric($objectives_id)) {
                  $flds = "`type`, `objectives_id`, `measure`, `target`, `weight`, `quality`, `effectiveness`, `timeliness`, ";
                  $flds .= "`rawscore`, `weightedscore`, `accomplishment`, ";
                  $flds .= "`q1`, `q2`, `q3`, `q4`, `q5`, ";
                  $flds .= "`e1`, `e2`, `e3`, `e4`, `e5`, ";
                  $flds .= "`t1`, `t2`, `t3`, `t4`, `t5`, ";
                  $vals = "'$type', '$objectives_id', '$measure', '$target', '$weight', '$quality', '$effectiveness', '$timeliness', ";
                  $vals .= "'$rawscore', '$weightedscore', '$accomplishment', ";
                  $vals .= "'$q1', '$q2', '$q3', '$q4', '$q5', ";
                  $vals .= "'$e1', '$e2', '$e3', '$e4', '$e5', ";
                  $vals .= "'$t1', '$t2', '$t3', '$t4', '$t5', ";
                  if ($table == "spms_ops") {
                     $flds .= "`ops_id`, `accountable`, `budget`,";
                     $vals .= "'$new_refid', '$accountable', '$budget', ";
                  } else if ($table == "spms_dps") {
                     $flds .= "`dps_id`, ";
                     $vals .= "'$new_refid', ";
                  } else if ($table == "spms_ips") {
                     $flds .= "`ips_id`, ";
                     $vals .= "'$new_refid', ";
                  }
                  $save_detail = f_SaveRecord("NEWSAVE",$table2,$flds,$vals);
                  if (!is_numeric($save_detail)) {
                     $error++;
                     echo 'errorSave();';
                     return false;
                  }
               }
            }   
         } else {
            $error++;
            echo 'errorSave();';
            return false;
         }
         if ($error == 0) {
            echo 'afterSave();';
         }
      } else {
         $Fldnval = "`semester` = '$semester', ";
         $Fldnval .= "`year` = '$year', ";
         $Fldnval .= "`rating` = '$rating', ";
         $Fldnval .= "`total_rating` = '$total_rating', ";
         $Fldnval .= "`premium_point` = '$premium_point', ";
         $Fldnval .= "`overall_rating` = '$overall_rating', ";
         $Fldnval .= "`adjectival_rating` = '$adjectival_rating', ";
         if ($table == "spms_ops") {
            $Fldnval .= "`OfficeRefId` = '$office', ";
         } else if ($table == "spms_dps") {
            $Fldnval .= "`DivisionRefId` = '$division', ";
         } else if ($table == "spms_ips") {
            $Fldnval .= "`question1` = '$question1', ";
            $Fldnval .= "`question2` = '$question2', ";
            $Fldnval .= "`question3` = '$question3', ";
         }
         $update = f_SaveRecord("EDITSAVE",$table,$Fldnval,$spmsrefid);
         if ($update == "") {
            for ($a=1; $a <= $count ; $a++) { 
               $refid            = getvalue("refid_".$a);
               $type             = getvalue("type_".$a);
               $objective        = realEscape(getvalue("objective_".$a));
               $measure          = realEscape(getvalue("measure_".$a));
               $target           = realEscape(getvalue("target_".$a));
               $weight           = realEscape(getvalue("weight_".$a));
               $quality          = getvalue("quality_".$a);
               $effectiveness    = getvalue("effectiveness_".$a);
               $timeliness       = getvalue("timeliness_".$a);
               $rawscore         = getvalue("rawscore_".$a);
               $weightedscore    = getvalue("weightedscore_".$a);
               $accomplishment   = realEscape(getvalue("accomplishment_".$a));
               $q1               = realEscape(getvalue("q1_".$a));
               $q2               = realEscape(getvalue("q2_".$a));
               $q3               = realEscape(getvalue("q3_".$a));
               $q4               = realEscape(getvalue("q4_".$a));
               $q5               = realEscape(getvalue("q5_".$a));
               $e1               = realEscape(getvalue("e1_".$a));
               $e2               = realEscape(getvalue("e2_".$a));
               $e3               = realEscape(getvalue("e3_".$a));
               $e4               = realEscape(getvalue("e4_".$a));
               $e5               = realEscape(getvalue("e5_".$a));
               $t1               = realEscape(getvalue("t1_".$a));
               $t2               = realEscape(getvalue("t2_".$a));
               $t3               = realEscape(getvalue("t3_".$a));
               $t4               = realEscape(getvalue("t4_".$a));
               $t5               = realEscape(getvalue("t5_".$a));
               if ($table != "spms_ops") {
                  if(isset($_POST["other_".$a])) {
                     $objective = realEscape(getvalue("newobjective_".$a));
                     $objectives_id    = chkFM("objectives",$objective);
                  } else {
                     $objectives_id = $objective;
                  }
               } else {
                  $objectives_id    = chkFM("objectives",$objective);   
                  $accountable      = realEscape(getvalue("accountable_".$a));
                  $budget           = realEscape(getvalue("budget_".$a));
               }
               if (is_numeric($objectives_id)) {
                  if (intval($refid) > 0) {
                     $fldnval = "`type` = '$type', ";
                     $fldnval .= "`objectives_id` = '$objectives_id', ";
                     $fldnval .= "`measure` = '$measure', ";
                     $fldnval .= "`target` = '$target', ";
                     $fldnval .= "`weight` = '$weight', ";
                     $fldnval .= "`quality` = '$quality', ";
                     $fldnval .= "`effectiveness` = '$effectiveness', ";
                     $fldnval .= "`timeliness` = '$timeliness', ";
                     $fldnval .= "`rawscore` = '$rawscore', ";
                     $fldnval .= "`weightedscore` = '$weightedscore', ";
                     $fldnval .= "`accomplishment` = '$accomplishment', ";

                     $fldnval .= "`q1` = '$q1', ";
                     $fldnval .= "`q2` = '$q2', ";
                     $fldnval .= "`q3` = '$q3', ";
                     $fldnval .= "`q4` = '$q4', ";
                     $fldnval .= "`q5` = '$q5', ";

                     $fldnval .= "`e1` = '$e1', ";
                     $fldnval .= "`e2` = '$e2', ";
                     $fldnval .= "`e3` = '$e3', ";
                     $fldnval .= "`e4` = '$e4', ";
                     $fldnval .= "`e5` = '$e5', ";

                     $fldnval .= "`t1` = '$t1', ";
                     $fldnval .= "`t2` = '$t2', ";
                     $fldnval .= "`t3` = '$t3', ";
                     $fldnval .= "`t4` = '$t4', ";
                     $fldnval .= "`t5` = '$t5', ";

                     if ($table == "spms_ops") {
                        $fldnval .= "`accountable` = '$accountable',";
                        $fldnval .= "`budget` = '$budget',";
                     }

                     $update_detail = f_SaveRecord("EDITSAVE",$table2,$fldnval,$refid);
                     if ($update_detail != "") {
                        $error++;
                        echo 'errorSave();';
                        return false;
                     }  
                  } else {
                     $flds = "`type`, `objectives_id`, `measure`, `target`, `weight`, `quality`, `effectiveness`, `timeliness`, ";
                     $flds .= "`rawscore`, `weightedscore`, `accomplishment`, ";
                     $flds .= "`q1`, `q2`, `q3`, `q4`, `q5`, ";
                     $flds .= "`e1`, `e2`, `e3`, `e4`, `e5`, ";
                     $flds .= "`t1`, `t2`, `t3`, `t4`, `t5`, ";
                     $vals = "'$type', '$objectives_id', '$measure', '$target', '$weight', '$quality', '$effectiveness', '$timeliness', ";
                     $vals .= "'$rawscore', '$weightedscore', '$accomplishment', ";
                     $vals .= "'$q1', '$q2', '$q3', '$q4', '$q5', ";
                     $vals .= "'$e1', '$e2', '$e3', '$e4', '$e5', ";
                     $vals .= "'$t1', '$t2', '$t3', '$t4', '$t5', ";
                     if ($table == "spms_ops") {
                        $flds .= "`ops_id`, `accountable`, `budget`, ";
                        $vals .= "'$spmsrefid', '$accountable', '$budget',";
                     } else if ($table == "spms_dps") {
                        $flds .= "`dps_id`, ";
                        $vals .= "'$spmsrefid', ";
                     } else if ($table == "spms_ips") {
                        $flds .= "`ips_id`, ";
                        $vals .= "'$spmsrefid', ";
                     }
                     $save_detail = f_SaveRecord("NEWSAVE",$table2,$flds,$vals);
                     if (!is_numeric($save_detail)) {
                        $error++;
                        echo 'errorSave();';
                        return false;
                     }
                  }
                  
               }
            } 
         } else {
            $error++;
            echo 'errorUpdate()';
         }
         if ($error == 0) {
            echo 'afterEdit();';
         }
      }
   }
?>