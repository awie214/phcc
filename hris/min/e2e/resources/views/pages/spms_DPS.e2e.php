<?php
   $emprefid = getvalue("hEmpRefId");
   $where_access = "WHERE EmployeesRefId = '".$emprefid."'";
   $division = FindFirst("usermanagement",$where_access,"DivisionRefId");
   $division = intval($division);
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_AfterTrn"); ?>"></script>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_SPMS"); ?>"></script>
      <style type="text/css">
         .td-input {
            padding: 2px;
         }
         textarea {resize: none;}
      </style>
      <script type="text/javascript">
         $(document).ready(function () {
            <?php
               if ($division > 0) {
                  echo '$("#sint_DivisionRefId").val("'.$division.'");';
                  echo '$("#sint_DivisionRefId").prop("readonly",true);';
               }
            ?>
         });
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("DIVISION PERFORMANCE SCORECARD (DPS)"); ?>
            <div class="container-fluid margin-top10">
               <div class="row" id="spmsList">
                  <div class="col-xs-12">
                     <div class="panel-top">
                        LIST OF PERFORMANCE MANAGEMENT
                        <span id="selectedEmployees">&nbsp;</span>
                     </div>
                     <div class="panel-mid">
                        <div id="spGridTable">
                           <?php
                              
                              if ($division > 0) {
                                 $sql = "SELECT * FROM `spms_dps` WHERE DivisionRefId = '$division' ORDER BY RefId Desc";
                              } else {
                                 $sql = "SELECT * FROM `spms_dps` ORDER BY RefId Desc";
                              }

                              $action = [true,true,false,false];
                              
                              $gridTableHdr_arr = ["Employee","Division","Semester", "Year"];
                              $gridTableFld_arr = ["EmployeesRefId","DivisionRefId","semester", "year"];
                              doGridTable($table,
                                          $gridTableHdr_arr,
                                          $gridTableFld_arr,
                                          $sql,
                                          $action,
                                          $_SESSION["module_gridTable_ID"]);
                           ?>
                        </div>
                     </div>
                     <div class="panel-bottom">
                        <button type="button"
                             class="btn-cls4-sea trnbtn"
                             id="btnINSERTSPMS" name="btnINSERTSPMS">
                           <i class="fa fa-file" aria-hidden="true"></i>
                           &nbsp;Insert New
                        </button>
                     </div>
                  </div>
               </div>
               <div class="row" id="insert_spms"><!--  -->
                  <div class="col-xs-12">
                     <div class="panel-top">ADD NEW</div>
                     <div class="panel-mid" id="contentDiv">
                        <div class="row">
                           <div class="col-xs-4">
                              <label>Division</label>
                              <?php
                                 createSelect("Division",
                                              "sint_DivisionRefId",
                                              "",100,"Name","Select Division","");
                              ?>
                           </div>
                           <div class="col-xs-4">
                              <label>Semester</label>
                              <select class="form-input" name="semester" id="semester">
                                 <option value="0">Select Semester</option>
                                 <option value="1">1st Half</option>
                                 <option value="2">2nd Half</option>
                              </select>
                           </div>
                           <div class="col-xs-4">
                              <label>Year</label>
                              <select class="form-input" name="year" id="year">
                                 <?php
                                    $past_year = date("Y",time()) - 2;
                                    $curr_year = date("Y",time());
                                    $future_year = date("Y",time()) + 2;
                                    for ($i=$past_year; $i <= $future_year ; $i++) { 
                                       if ($i == $curr_year) {
                                          echo '<option value="'.$i.'" selected>'.$i.'</option>';   
                                       } else {
                                          echo '<option value="'.$i.'">'.$i.'</option>';
                                       }
                                       
                                    }
                                 ?>
                              </select>
                           </div>

                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-4">
                              <label>Rating:</label>
                              <input type="text" class="form-input" name="rating" id="rating">
                           </div>
                           <div class="col-xs-4">
                              <label>Total Rating:</label>
                              <input type="text" class="form-input" name="total_rating" id="total_rating">
                           </div>
                           <div class="col-xs-4">
                              <label>Premium Points:</label>
                              <input type="text" class="form-input" name="premium_point" id="premium_point">
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-4">
                              <label>Overall Numerical Rating:</label>
                              <input type="text" class="form-input" name="overall_rating" id="overall_rating">
                           </div>
                           <div class="col-xs-4">
                              <label>Adjectival Rating:</label>
                              <input type="text" class="form-input" name="adjectival_rating" id="adjectival_rating">
                           </div>
                        </div>
                        <br>
                        <div id="canvas">
                           <?php
                              for ($x=1; $x <=1; $x++) { 
                           ?>
                           <div id="Entry_<?php echo $x; ?>" class="entry201">
                              <div class="row margin-top">
                                 <div class="col-xs-6">
                                    <label>#<?php echo $x; ?> </label>
                                    <input type="hidden" name="refid_<?php echo $x; ?>" id="refid_<?php echo $x; ?>">
                                 </div>
                              </div>
                              <?php bar(); ?>
                              <div class="row margin-top">
                                 <div class="col-xs-4">
                                    <label>Select Type:</label>
                                    <select class="form-input" name="type_<?php echo $x; ?>" id="type_<?php echo $x; ?>">
                                       <option value="Core Function">Core Function</option>
                                       <option value="Strategic Function">Strategic Function</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <label>Objectives:</label>
                                    <?php
                                       createSelect("objectives",
                                                    "objective_".$x,
                                                    "",100,"Name","Select Objective","");
                                    ?>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <input type="checkbox" name="other_<?php echo $x; ?>" id="other_<?php echo $x; ?>" onclick="enableOther(this.id);">
                                    <label for="other_<?php echo $x; ?>">Other objective:</label>
                                    <input type="text" class="form-input" name="newobjective_<?php echo $x; ?>" id="newobjective_<?php echo $x; ?>" disabled>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-4">
                                    <label>Measure:</label>
                                    <textarea class="form-input" rows="4" name="measure_<?php echo $x; ?>" id="measure_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-4">
                                    <label>Target:</label>
                                    <textarea class="form-input" rows="4" name="target_<?php echo $x; ?>" id="target_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-4">
                                    <label>Actual Accomplishments:</label>
                                    <textarea class="form-input" rows="4" name="accomplishment_<?php echo $x; ?>" id="accomplishment_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-4">
                                    <div class="row">
                                       <div class="col-xs-12 text-center">
                                          <label>RATING</label>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-4">
                                          <div class="row">
                                             <div class="col-xs-12 text-right">
                                                <label>Quality:</label>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12 text-right">
                                                <label>Effectiveness:</label>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12 text-right">
                                                <label>Timeliness:</label>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xs-8">
                                          <div class="row">
                                             <div class="col-xs-12">
                                                <select class="form-input" name="quality_<?php echo $x; ?>" id="quality_<?php echo $x; ?>">
                                                   <option value="0">0</option>
                                                   <option value="1">1</option>
                                                   <option value="2">2</option>
                                                   <option value="3">3</option>
                                                   <option value="4">4</option>
                                                   <option value="5">5</option>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <select class="form-input" name="effectiveness_<?php echo $x; ?>" id="effectiveness_<?php echo $x; ?>">
                                                   <option value="0">0</option>
                                                   <option value="1">1</option>
                                                   <option value="2">2</option>
                                                   <option value="3">3</option>
                                                   <option value="4">4</option>
                                                   <option value="5">5</option>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <select class="form-input" name="timeliness_<?php echo $x; ?>" id="timeliness_<?php echo $x; ?>">
                                                   <option value="0">0</option>
                                                   <option value="1">1</option>
                                                   <option value="2">2</option>
                                                   <option value="3">3</option>
                                                   <option value="4">4</option>
                                                   <option value="5">5</option>
                                                </select>
                                             </div>
                                          </div>  
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-xs-2">
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <label>Weight:</label>
                                          <input type="text" class="form-input" name="weight_<?php echo $x; ?>" id="weight_<?php echo $x; ?>">
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <label>Weighted Score:</label>
                                          <input type="text" class="form-input" name="weightedscore_<?php echo $x; ?>" id="weightedscore_<?php echo $x; ?>">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-xs-2">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <label>Raw Score:</label>
                                          <input type="text" class="form-input" name="rawscore_<?php echo $x; ?>" id="rawscore_<?php echo $x; ?>">
                                       </div>
                                    </div>
                                 </div>

                              </div>
                              <br>
                              <div class="row margin-top">
                                 <div class="col-xs-12 text-center">
                                    <label>RATING MATRIX</label>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <label>Quality:</label>
                                 </div>
                              </div>
                              <?php bar(); ?>
                              <div class="row margin-top">
                                 <div class="col-xs-2">
                                    <label>Rating 1:</label>
                                    <textarea class="form-input" rows="2" name="q1_<?php echo $x; ?>" id="q1_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 2:</label>
                                    <textarea class="form-input" rows="2" name="q2_<?php echo $x; ?>" id="q2_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 3:</label>
                                    <textarea class="form-input" rows="2" name="q3_<?php echo $x; ?>" id="q3_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 4:</label>
                                    <textarea class="form-input" rows="2" name="q4_<?php echo $x; ?>" id="q4_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 5:</label>
                                    <textarea class="form-input" rows="2" name="q5_<?php echo $x; ?>" id="q5_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <label>Effectiveness</label>
                                 </div>
                              </div>
                              <?php bar(); ?>
                              <div class="row margin-top">
                                 <div class="col-xs-2">
                                    <label>Rating 1:</label>
                                    <textarea class="form-input" rows="2" name="e1_<?php echo $x; ?>" id="e1_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 2:</label>
                                    <textarea class="form-input" rows="2" name="e2_<?php echo $x; ?>" id="e2_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 3:</label>
                                    <textarea class="form-input" rows="2" name="e3_<?php echo $x; ?>" id="e3_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 4:</label>
                                    <textarea class="form-input" rows="2" name="e4_<?php echo $x; ?>" id="e4_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 5:</label>
                                    <textarea class="form-input" rows="2" name="e5_<?php echo $x; ?>" id="e5_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-12">
                                    <label>Timeliness:</label>
                                 </div>
                              </div>
                              <?php bar(); ?>
                              <div class="row margin-top">
                                 <div class="col-xs-2">
                                    <label>Rating 1:</label>
                                    <textarea class="form-input" rows="2" name="t1_<?php echo $x; ?>" id="t1_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 2:</label>
                                    <textarea class="form-input" rows="2" name="t2_<?php echo $x; ?>" id="t2_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 3:</label>
                                    <textarea class="form-input" rows="2" name="t3_<?php echo $x; ?>" id="t3_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 4:</label>
                                    <textarea class="form-input" rows="2" name="t4_<?php echo $x; ?>" id="t4_<?php echo $x; ?>"></textarea>
                                 </div>
                                 <div class="col-xs-2">
                                    <label>Rating 5:</label>
                                    <textarea class="form-input" rows="2" name="t5_<?php echo $x; ?>" id="t5_<?php echo $x; ?>"></textarea>
                                 </div>
                              </div>
                           </div>
                           <br>
                           <?php
                              }
                           ?>   
                        </div>
                        
                     </div>
                     <div class="panel-bottom">
                        <button type="button" id="addSPMS" class="btn-cls4-tree">
                           <i class="fa fa-plus"></i>&nbsp;ADD ROW
                        </button>
                        <input type="hidden" name="sint_SPMSRefId" id="sint_SPMSRefId" value="0">
                        <input type="hidden" name="table2" id="table2" value="dps_details">
                        <input type="hidden" name="count" id="count">
                        <input type="hidden" name="fn" id="fn" value="saveSPMS">
                        <button type="button" id="btnSAVESPMS" class="btn-cls4-sea">
                           <i class="fa fa-save"></i>&nbsp;SAVE
                        </button>
                        <button type="button" id="btnCANCELSPMS" class="btn-cls4-red">
                           <i class="fa fa-times"></i>&nbsp;CANCEL
                        </button>
                        <button type="button" id="btnEDITSPMS" class="btn-cls4-sea">
                           <i class="fa fa-save"></i>&nbsp;UPDATE
                        </button>
                        <button type="button" id="btnBACKSPMS" class="btn-cls4-red">
                           <i class="fa fa-backward"></i>&nbsp;BACK
                        </button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "spms_dps";
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         
      });
      
   </script>
</html>