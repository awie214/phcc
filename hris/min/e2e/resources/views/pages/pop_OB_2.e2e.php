<?php
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';

   $Office = $Position = $Division = $FullName = $end_time = $start_time = $date = "&nbsp;";
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		td {
			border: 2px solid black;
		}
	</style>
</head>
<body>
	<div class="container-fluid rptBody">
		<div style="page-break-after: always;">
	        <?php
	            rptHeader("Official Business Pass Slip");
	        ?>
	        <div class="row">
	         	<div class="col-xs-12">
	         		<table width="100%">
	         			<tr>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Date:
	         							<br>
	         							<?php
	         								echo $date;
	         							?>
	         						</div>
	         					</div>
	         				</td>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Time Of Departure
	         							<br>
	         							<?php
	         								echo $start_time;
	         							?>
	         						</div>
	         					</div>
	         				</td>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Expect Time of Return:
	         							<br>
	         							<?php
	         								echo $end_time;
	         							?>
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Printed Name and Signature of <br>
	         							Official/Employee:
	         							<br>
	         							<?php
	         								echo $FullName;
	         							?>
	         						</div>
	         					</div>
	         				</td>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Position Title:
	         							<br>
	         							<br>
	         							<?php
	         								echo $Position;
	         							?>
	         						</div>
	         					</div>
	         				</td>
	         				<td>
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Office/Division:
	         							<br>
	         							<br>
	         							<?php
	         								echo $Office." ".$Division;
	         							?>
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td colspan="2">
	         					Destination and Purpose of Official Business
	         				</td>
	         				<td rowspan="2">
	         					Recommending Approval:
	         					<?php spacer(100); ?>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td rowspan="4" colspan="2" valign="top">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Destination:
	         							<?php spacer(90); ?>
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-12">
	         							Purpose:
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td class="text-center">Immediate Supervisor</td>
	         			</tr>
	         			<tr>
	         				<td>
	         					Approved:
	         					<?php spacer(100); ?>
	         				</td>
	         			</tr>
	         			<tr>
	         				<td class="text-center">Next Higher Official</td>
	         			</tr>
	         			<tr>
	         				<td colspan="2" valign="top">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Recorded in Security Logbook:
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-6">
	         							Time Left Office:
	         						</div>
	         						<div class="col-xs-6 text-right">
	         							________________________________
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-6">
	         							Time Returned:
	         						</div>
	         						<div class="col-xs-6 text-right">
	         							________________________________
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-12 text-center">
	         							<?php spacer(80); ?>
	         							<br>
	         							Security Officer:
	         						</div>
	         					</div>
	         				</td>
	         				<td valign="top">
	         					<div class="row">
	         						<div class="col-xs-12">
	         							Recorded by HRDD:
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-12">
	         							____ Leave Card
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-12">
	         							____ Others:
	         						</div>
	         					</div>
	         					<div class="row margin-top">
	         						<div class="col-xs-12 text-center">
	         							<?php spacer(80); ?>
	         							_______________________
	         						</div>
	         					</div>
	         				</td>
	         			</tr>
	         		</table>
	         	</div>
	        </div>
	    </div>
    </div>
</body>
</html>