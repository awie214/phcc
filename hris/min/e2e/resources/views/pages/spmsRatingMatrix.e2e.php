<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_IPS"); ?>"></script>
      <script type="text/javascript">
         $(document).ready(function () {
            $("input").prop("disabled",true);
         });
         function edit_button() {
            $("input").prop("disabled",false);
         }
      </script>
      <style type="text/css">
         td {
            padding: 5px;
            border: 1px solid black;
         }
         .company {
            font-family: Copperplate, "Copperplate Gothic Light" !important;
            font-size: 15pt;
         }
      </style>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("RATING MATRIX"); ?>
            <div class="container-fluid margin-top10">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="row">
                        <div class="col-xs-12 text-center company" style="border-bottom: 2px solid black; padding: 5px;">
                           <b>Philippine Competition Commission</b>
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-12 text-center">
                           <span style="font-size: 15pt;">RATING MATRIX</span>
                        </div>
                     </div>
                     <br>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <button type="button"
                                class="btn-cls4-sea trnbtn"
                                id="" name="" onclick="edit_button();">
                              <i class="fa fa-edit" aria-hidden="true"></i>
                              &nbsp;EDIT
                           </button>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <table border="2" width="100%">
                              <tr>
                                 <td rowspan="2" class="text-center"><b>MEASURE</b></td>
                                 <td colspan="6" class="text-center"><b>RATING SCALE</b></td>
                              </tr>
                              <tr>
                                 <td colspan="2" class="text-center"><b>QUALITY</b></td>
                                 <td colspan="2" class="text-center"><b>EFFICIENCY</b></td>
                                 <td colspan="2" class="text-center"><b>TIMELINESS</b></td>
                              </tr>
                              <tr>
                                 <td colspan="7" class=""><b>STRATEGIC OBJECTIVES</b></td>
                              </tr>
                              <?php for($a = 5; $a > 0; $a--) { 
                              ?>
                              <tr>
                                 <td class="" style="width: 16%;"></td>
                                 <td class="text-center " style="width: 5%;"><?php echo $a; ?></td>
                                 <td class="" style="width: 23%;">
                                    <input type="text" name="" id="" class="form-input">
                                 </td>
                                 <td class="text-center " style="width: 5%;"><?php echo $a; ?></td>
                                 <td class="" style="width: 23%;">
                                    <input type="text" name="" id="" class="form-input">
                                 </td>
                                 <td class="text-center " style="width: 5%;"><?php echo $a; ?></td>
                                 <td class="" style="width: 23%;">
                                    <input type="text" name="" id="" class="form-input">
                                 </td>   
                              </tr>
                              <?php
                                 }
                              ?>
                              <tr>
                                 <td colspan="7" class=""><b>CORE FUNCTIONS</b></td>
                              </tr>
                              <?php for($b = 5; $b > 0; $b--) { 
                              ?>
                              <tr>
                                 <td class="" style="width: 16%;"></td>
                                 <td class="text-center " style="width: 5%;"><?php echo $b; ?></td>
                                 <td class="" style="width: 23%;">
                                    <input type="text" name="" id="" class="form-input">
                                 </td>
                                 <td class="text-center " style="width: 5%;"><?php echo $b; ?></td>
                                 <td class="" style="width: 23%;">
                                    <input type="text" name="" id="" class="form-input">
                                 </td>
                                 <td class="text-center " style="width: 5%;"><?php echo $b; ?></td>
                                 <td class="" style="width: 23%;">
                                    <input type="text" name="" id="" class="form-input">
                                 </td>   
                              </tr>
                              <?php
                                 }
                              ?>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "spms_ipcr";
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
</html>