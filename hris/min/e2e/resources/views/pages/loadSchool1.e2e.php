<?php
	include 'conn.e2e.php';
	include 'constant.e2e.php';	
	include pathClass.'0620functions.e2e.php';

	$offer = getvalue("offer");
	$objname = getvalue("objname");
	mysqli_query($conn,"set @rownum = 0");
	$result = mysqli_query($conn,"SELECT *, (@rownum := @rownum + 1) AS rank FROM schools ORDER BY name");
	$count = mysqli_num_rows($result);
		    echo '
			<div class="row" style="padding: 5px;">
				<div class="col-xs-4">
					<input type="text" class="form-input" placeholder="Search.." name="search">
				</div>
			</div>
			<div class="row" style="padding: 10px;">
				<table class="table" border=1>
					<thead>
						<tr>
						    <th class="text-center"></th>
							<th class="text-center">ACTION</th>
							<th class="text-center">REFID</th>
							<th class="text-center">SCHOOL NAME</th>
						</tr>
					</thead>
					<tbody>';
                $batch = 3;
                $seekRow_Start = (($batch - 1) * 50) + 1;
                $seekRow_End = ($seekRow_Start + 50) - 1;
                $j = 0;
                while ($row = mysqli_fetch_assoc($result)) {

                	if ($row["rank"] >= $seekRow_Start && 
                		$row["rank"] <= $seekRow_End) {
                	    echo '	
						<tr>
						    <td>'.$row["rank"].'</td>
							<td class="text-center">
							</td>
							<td class="text-center">'.$row["RefId"].'</td>
							<td>'.$row["Name"].'</td>
						</tr>';	
                	}   
					
				}

			echo '
					</tbody>
				</table>
			</div>
			<div class="row margin-top text-right" style="padding: 5px;">
				<ul class="pagination">
					<li><a href="#">PREV</a></li>
				  	<li><a href="#">2</a></li>
				  	<li><a href="#">3</a></li>
				  	<li><a href="#">4</a></li>
				  	<li><a href="#">NEXT</a></li>
				</ul> 
			</div>';
	
?>