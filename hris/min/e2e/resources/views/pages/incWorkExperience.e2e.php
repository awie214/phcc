<?php
   include "conn.e2e.php";
?>
<div class="mypanel">
   <div class="panel-top" for="WorkExp">
      <!-- <div class="row">
         <div class="col-xs-3 txt-center">
            <div class="row">
               <div class="col-xs-12 txt-center">
                  INCLUSIVE DATES<br>(mm/dd/yyyy)
               </div>
            </div>
            <div class="row">
               <div class="col-xs-6 txt-center">
                  FROM
               </div>
               <div class="col-xs-6 txt-center">
                  TO
               </div>
            </div>
         </div>
         <div class="col-xs-3 txt-center">
            POSITION TITLE<br>(Write in Full)
         </div>
         <div class="col-xs-3 txt-center">
            DEPT/AGENCY/OFFICE<br>(Write in Full)
         </div>
         <div class="col-xs-3 txt-center">
            MONTHLY<br>SALARY
         </div>
      </div>
      -->
      <!--
         <div class="col-xs-1 txt-center">
            SG STEP<br>INCREMENT<br>(Format "00-0")
         </div>
         <div class="col-xs-1 txt-center">
            STATUS<br>OF<br>APPT.
         </div>
         <div class="col-xs-2 txt-center">
            GOV'T<br>SERVICE<br>(yes / no)
         </div>
      -->
   </div>
   <div class="panel-mid-litebg" id="WorkExp">
      <?php
         if ($EmployeesRefId) {
            $rs = SelectEach("employeesworkexperience","WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId");
            if ($rs) {
               $createEntry = mysqli_num_rows($rs);
            } else {
               $createEntry = 1;
            }
         } else {
            $createEntry = 1;   
         }
         $disabled = "disabled";
         for ($j=1;$j<=$createEntry;$j++) {
            include "incScrnWorkExperience_Admin.e2e.php";
            /*if (getvalue("hUserGroup") == "COMPEMP") {
               include_once "incScrnWorkExperience_User.e2e.php";
            } else {
            }*/
         }

      ?>
      <?php if (getvalue("hUserGroup") == "COMPEMP") { ?>
         <div class="panel-bottom bgSilver">
            <a href="javascript:void(0);" class="addRow" id="addRowWorkExp">Add Row</a>
         </div>
      <?php } ?>
   </div>
   <div class="panel-bottom"></div>
</div>
<script type="text/javascript">
   $(document).ready(function() {
      $(function() {
         $("#WorkExp [name*='date_'], [name*='sint_PositionRefId_'], [name*='sint_AgencyRefId_'], [name*='deci_SalaryAmount_']").addClass("mandatory");
         $("#WorkExp .saveFields--").attr("tabname","Work Experience");
         $("#appt .newDataLibrary").hide();
         $("#appt .createSelect--").css("width","100%");
         $("#sg .newDataLibrary").hide();
         $("#sg .createSelect--").css("width","100%");
         $("[name*='sint_SalaryGradeRefId_']").attr("focus","validateGrade");
         $("[name*='sint_JobGradeRefId_']").attr("focus","validateGrade");
         $("[name*='sint_EmpStatusRefId_']").addClass("mandatory");
         $("[name*='sint_SalaryGradeRefId_']").each(function () {
            var idx = $(this).attr("name").split("_")[2];
            $(this).attr("partner","sint_JobGradeRefId_" + idx);
         });
         <?php 
            if (getvalue("hUserGroup") != "COMPEMP") { 
               echo '$(".addRow").hide();';
            }
         ?>  
      });
   });

   function isGovt(objName) {
      var name = objName;
      var val = $("[name='"+name+"']").val();
      var idx = $("[name='"+name+"']").attr("idx");
      var partnerObj1 = "sint_SalaryGradeRefId_"+idx;
      var partnerObj2 = "sint_JobGradeRefId_"+idx;
      var partnerObj3 = "sint_StepIncrementRefId_"+idx;
      if (val == "1") {
         $("[name='"+partnerObj3+"']").addClass("mandatory");
         $("[name='"+partnerObj1+"'], [name='"+partnerObj2+"'], [name='"+partnerObj3+"']").prop("disabled",false);

         if (
               $("[name='"+partnerObj1+"']").val() > 0 ||
               $("[name='"+partnerObj2+"']").val() > 0
            )
         {
            $("[name='"+partnerObj2+"']").val(0);
            $("[name='"+partnerObj1+"']").val(0);
            $("[name='"+partnerObj1+"']").notify("Ooops!!! Atleast Salary Grade OR Job Grade must supplied value if you selected Govt. Service","error","{position:'top center'}");

         }

      } else if (val == "0") {
         $("[name='"+partnerObj1+"']").val(0);
         $("[name='"+partnerObj2+"']").val(0);
         $("[name='"+partnerObj3+"']").val(0);
         $("[name='"+partnerObj1+"'], [name='"+partnerObj2+"'], [name='"+partnerObj3+"']").removeClass("mandatory");
         $("[name='"+partnerObj1+"'], [name='"+partnerObj2+"'], [name='"+partnerObj3+"']").prop("disabled",true);
      } else {
         $("[name='"+partnerObj1+"']").val(0);
         $("[name='"+partnerObj2+"']").val(0);
         $("[name='"+partnerObj3+"']").val(0);
         $("[name='"+partnerObj1+"'], [name='"+partnerObj2+"'], [name='"+partnerObj3+"']").prop("disabled",false);
      }

      $("[name='sint_EmpStatusRefId_"+idx+"']").empty();
      $("[name='sint_AgencyRefId_"+idx+"']").empty();
      $("[name='sint_PositionRefId_"+idx+"']").empty();

      $.get("trn.e2e.php",
      {
         fn:"getPrivate",
         val:val,
         idx:idx,
         obj:"sint_PositionRefId_|sint_AgencyRefId_|sint_EmpStatusRefId_"
      },
      function(data,status){
         if (status == 'success') {
            eval(data);
         }
      });

   }
</script>