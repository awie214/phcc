<div class="row">
   <div class="col-sm-4">
      <button type="button" class="trnButton" style="width:70px;" name="bAdd" id="bAdd">Insert</button>
   </div>
   <div class="col-sm-4">
   </div>
   <div class="col-sm-4 txt-right">
      <input type="text" class="form-input" style="width:150px;" placeholder="Search Here">
      <a href="javascript:void(0);">
         <i class="fa fa-search" aria-hidden="true"></i>
      </a>
   </div>
</div>
<?php spacer(2); ?>
<div class="row" style="max-height:500px;overflow:auto;">
   <table class="grid_Table" border=1>
      <thead> 
         <tr>
            <th class="txt-center">Action</th>
            <?php 
               for ($i=0;$i<count($objs);$i++) {
                  echo '<th class="txt-center">'.$label[$i].'</th>';
               }
            ?>
         </tr>
         </thead>
         <tbody>
         <?php
            $rs = SelectEach($module["Table"],'ORDER BY RefId DESC');
            if ($rs) {
               $action = $module["Action"];
               while ($row = mysqli_fetch_assoc($rs)) {
                  $refid = $row["RefId"];
                  echo '<tr id="refid_'.$refid.'" title="Record '.$refid.'">'."\n";
                  echo
                  '<td align="center" nowrap style="width:90px;">'."\n";
                     if (isset($action[0]) && $action[0]) {
                        echo
                        '<a class="actionIcon" title="View this Record">
                           <img src="'.img("view.png").'" onclick="viewRecord('.$refid.');">
                        </a>'."\n";
                     }
                     if (isset($action[1]) && $action[1]) {
                        echo
                        '<a class="actionIcon" title="Modify this Record">
                           <img src="'.img("edit.png").'" onclick="modifyRecord('.$refid.');">
                        </a>'."\n";
                     }
                     if (isset($action[2]) && $action[2]) {
                        echo
                        '<a class="actionIcon" onclick="deleteRecord('.$refid.');" title="Delete This Record">
                           <img src="'.img("delete.png").'">
                        </a>'."\n";
                     }
                     if (isset($action[3]) && $action[3]) {
                        echo
                        '<input type="radio" name="obj" id="idx_'.$refid.'" onclick="selectMe('.$refid.');">'."\n";
                     }
                  echo
                  '</td>'."\n";
                  $td_value = "";
                  for ($i=0;$i<count($objs);$i++) {
                     if ($css[$i] != "") {
                        $cssStyle = 'style="'.$css[$i].';"';
                     } else {
                        $cssStyle = "";
                     }
                     $td_value = $row[$objs[$i]];  
                     if (strpos($objs[$i],"RefId") > 0) {
                        $table = trim(str_replace("RefId","",$objs[$i]));
                        $td_value = getRecordSet($table,$td_value)["Name"];
                     }
                     echo '<td '.$cssStyle.'>'.$td_value.'</td>'."\n";
                  }
                  echo '</tr>'."\n";
               }
            }
         ?>
         </tbody>
   </table>
</div>
<div class="txt-center">
   <ul class = "pagination">
      <li><a href = "#">&laquo;</a></li>
      <li><a href = "#">1</a></li>
      <li><a href = "#">2</a></li>
      <li><a href = "#">3</a></li>
      <li><a href = "#">4</a></li>
      <li><a href = "#">5</a></li>
      <li><a href = "#">&raquo;</a></li>
   </ul>
</div>   