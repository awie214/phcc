<?php

	//SET ALL VARIABLE TO NULL
	for ($i=1; $i <=31 ; $i++) { 
		$map["[[d$i]]"] = "";
		$map["[[din$i]]"] = "";
		$map["[[lin$i]]"] = "";
		$map["[[lout$i]]"] = "";
		$map["[[dout$i]]"] = "";
		$map["[[reg$i]]"] = "";
		$map["[[exh$i]]"] = "";
		$map["[[tdy$i]]"] = "";
		$map["[[ot$i]]"] = "";
		$map["[[ut$i]]"] = "";
		$map["[[rem$i]]"] = "";
	}
	include 'conn.e2e.php';
	/*$ttsql = "CREATE TEMPORARY TABLE `tt_Attendance` (
             AttendanceDate DATE DEFAULT NULL,
             AttendanceTime INT(10) DEFAULT NULL,
             UTC INT (50) DEFAULT NULL,
             TimeIn INT(10) DEFAULT NULL,
             LunchOut INT(10) DEFAULT NULL,
             LunchIn INT(10) DEFAULT NULL,
             TimeOut INT(10) DEFAULT NULL,
             Day VARCHAR(2) DEFAULT NULL,
             Week INT(2) DEFAULT NULL,
             KEntry VARCHAR(1) DEFAULT NULL)";
   	mysqli_query($conn,$ttsql) or die(mysqli_error());*/

   	$dtr = [
   		"AttendanceDate" => 0,
   		"AttendanceTime" => 0,
   		"UTC" => 0,
   		"TimeIn" => 0,
   		"LunchOut" => 0,
   		"LunchIn" => 0,
   		"TimeOut" => 0,
   		"Day" => "",
   		"Week" => 0,
   		"KEntry" => 0
   		];

   	function HoursFormat($timeInMin) {
   		if ($timeInMin > 0) {
   			$hr = explode(".",$timeInMin / 60)[0];
   			if ($hr <= 9) {
   				$hr = "0".$hr;
   			}
   			$mod_min = ($timeInMin % 60);
   			if ($mod_min <= 9) {
   				$mod_min = "0".$mod_min;
   			}
   			return $hr.":".$mod_min;	
   		} else {
   			return "&nbsp;";
   		}
   	}

	function get_today_minute($utc) {
		$baseline = strtotime(date("Y-m-d",$utc));
		return intval(($utc - $baseline) / 60);
	}

	function get_minute($time_in,$time_out) {
		return ((($time_in - $time_out) / 60) - 60);
	}

	$arr = array();
    $rsCompany = FindFirst("company","","*");
    $map["[[CompanyAddress]]"] = $rsCompany["Address"];
	$NewMonth = getvalue("hNewMonth");
	$NewYear = getvalue("hNewYear");
	$num_of_days = cal_days_in_month(CAL_GREGORIAN,$NewMonth,$NewYear);
	$new_week = "";
	$week_count = 0;
	if ($NewMonth <= 9) {
		$NewMonth = "0".$NewMonth;
	}
	$month_start = $NewYear."-".$NewMonth."-01";
	$month_end = $NewYear."-".$NewMonth."-".$num_of_days;
	for($i=1;$i<=$num_of_days;$i++) {
		$y = $NewYear."-".$NewMonth."-".$i;
		$day = date("D",strtotime($y));	
		$map["[[d$i]]"] = $day;
		if ($i <= 9) {
			$i = "0".$i;
		}
		$map["[[din$i]]"] = "";
		$map["[[lin$i]]"] = "";
		$map["[[lout$i]]"] = "";
		$map["[[dout$i]]"] = "";
		$map["[[reg$i]]"] = "";
		$map["[[exh$i]]"] = "";
		$map["[[tdy$i]]"] = "";
		$map["[[ot$i]]"] = "";
		$map["[[ut$i]]"] = "";
		$map["[[rem$i]]"] = "";
		$y = $NewYear."-".$NewMonth."-".$i;
		$week = date("W",strtotime($y));
		if ($week != $new_week) {
			$week_count++;
		}
		/*
		$flds = "AttendanceDate,Day,Week";
		$vals = "'".$y."','".$i."','$week_count'";
		$sql = "INSERT INTO tt_Attendance ($flds) VALUES ($vals)";
		mysqli_query($conn,$sql);
		*/
		$dtr = [
	   		"AttendanceDate" => "$y",
	   		"AttendanceTime" => "0",
	   		"UTC" => "0",
	   		"TimeIn" => "0",
	   		"LunchOut" => "0",
	   		"LunchIn" => "0",
	   		"TimeOut" => "0",
	   		"Day" => "$i",
	   		"Week" => "$week_count",
	   		"KEntry" => "0",
	   	];
	   	$arr["ARR"][$y] = $dtr;
		$new_week = $week;
	}

	$userID = 0;
    if (getvalue("hucode") != "COMPEMP") {
    	$emprefid = getvalue("emprefid");	
    } else {
    	$emprefid = getvalue("hEmpRefId");
    }
	$row = getRecordSet("employees",$emprefid);
	if ($row) {
		$EmpName = $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];	
		$biometricsID = $row["BiometricsID"];
		$CompanyID = $row["CompanyRefId"];
		$KEntryContent = file_get_contents(json."bioKindEntry_".$CompanyID.".json");
      	$KEntry_json   = json_decode($KEntryContent, true);
		$map["[[EmployeesName]]"] = $EmpName;

		$EmpInfo = f_Find("empinformation","WHERE EmployeesRefId = ".$emprefid);
		if ($EmpInfo) {
			$fetch = mysqli_fetch_assoc($EmpInfo);
			$map["[[Position]]"] = getRecord("position",$fetch["PositionRefId"],"Name");
			$map["[[Office]]"] = getRecord("office",$fetch["OfficeRefId"],"Name");
			$map["[[WorkSchedule]]"] = getRecord("workschedule",$fetch["WorkScheduleRefId"],"Name");
		}


		$Emp_WorkSched = FindFirst("workschedule"," WHERE RefId = ".$fetch["WorkScheduleRefId"],"*");
		$where_empAtt = "WHERE EmployeesRefId = '".$emprefid."' AND AttendanceDate BETWEEN '".$month_start."' AND '".$month_end."'";
		$rs_empAtt = SelectEach("employeesattendance",$where_empAtt);
		if ($rs_empAtt) {
			while ($row = mysqli_fetch_assoc($rs_empAtt)) {
				$AttendanceDate 	= $row["AttendanceDate"];
				$AttendanceTime 	= $row["AttendanceTime"];
				$UTC 				= $row["CheckTime"];
				$KEntry         	= $row["KindOfEntry"];
				$fldnval        	= "AttendanceTime = '".$UTC."',";
				$fldnval        	.= "UTC = '".$UTC."',";
				$fld 				= "";
				$val 				= "";
				switch ($KEntry) {
					case 1:
						$fld 		= "TimeIn";
						$val     	= get_today_minute($UTC);
						$fldnval	.= "TimeIn = '".get_today_minute($UTC)."',";
						break;
					case 2:
						$fld 		= "LunchOut";
						$val     	= get_today_minute($UTC);
						$fldnval	.= "LunchOut = '".get_today_minute($UTC)."',";
						break;
					case 3:
						$fld 		= "LunchIn";
						$val     	= get_today_minute($UTC);
						$fldnval	.= "LunchIn = '".get_today_minute($UTC)."',";
						break;
					case 4:
						$fld 		= "TimeOut";
						$val     	= get_today_minute($UTC);
						$fldnval	.= "TimeOut = '".get_today_minute($UTC)."',";
						break;
				}
				$arr["ARR"][$AttendanceDate][$fld] = $val;
				$arr["ARR"][$AttendanceDate]["UTC"] = $UTC;
			}
		}
		$biometricsID = FindFirst("employees","WHERE RefId = $emprefid","BiometricsID");
	    $query = "SELECT USERID, Badgenumber, Name FROM USERINFO";
	    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
	    if ($result) {
	        foreach($result as $row) {
	           	switch ($CompanyID) {
	              	case "1000":
	                	$mdb_field = "Badgenumber";
	              		break;
	              	case "2":
	                	$mdb_field = "Name";
	              		break;
	           	}

	           	if ($row[$mdb_field] == $biometricsID) {
	              	$userID = $row["USERID"];
	              	break;
	           	}
	        }
	    }
	    $query = 'SELECT USERID, CHECKTIME, CHECKTYPE, VERIFYCODE, SENSORID, sn FROM CHECKINOUT WHERE USERID = '.$userID.' AND VERIFYCODE = 1';
	    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
	    foreach($result as $row) {
	       	$utc 				= strtotime($row["CHECKTIME"]);
	       	$d 					= date("Y-m-d",$utc);
	    	$mdbTime 			= date("H:i",$utc);
	    	$mdbDate 			= date("Y-m-d",$utc);
	    	$fldnval        	= "AttendanceTime = '".$utc."',";
			$fldnval        	.= "UTC = '".$utc."',";
			$where 				= "WHERE AttendanceDate = '".$mdbDate."'";
			$fld 				= "";
			$val 				= "";
	    	switch ($row["CHECKTYPE"]) {
	          	case "I":
	          		$fld 		= $KEntry_json["FieldTimeIn"];
	          		$val 		= get_today_minute($utc);
	             	$fldnval	.= $KEntry_json["FieldTimeIn"]." = '".get_today_minute($utc)."'";
	             	$where 		.= " AND TimeIn IS NULL";
	             	break;
	          	case "0":
	          		$fld 		= $KEntry_json["FieldLunchOut"];
	          		$val 		= get_today_minute($utc);
	             	$fldnval	.= $KEntry_json["FieldLunchOut"]." = '".get_today_minute($utc)."'";
	             	$where 		.= " AND LunchOut IS NULL";
	             	break;
	          	case "1":
	          		$fld 		= $KEntry_json["FieldLunchIn"];
	          		$val 		= get_today_minute($utc);
	             	$fldnval	.= $KEntry_json["FieldLunchIn"]." = '".get_today_minute($utc)."'";
	             	$where 		.= " AND LunchIn IS NULL";
	             	break;
	          	case "O":
	          		$fld 		= $KEntry_json["FieldTimeOut"];
	          		$val 		= get_today_minute($utc);
	            	$fldnval	.= $KEntry_json["FieldTimeOut"]." = '".get_today_minute($utc)."'";
	            	$where 		.= " AND TimeOut IS NULL";
	            	break;
	    	}
	    	if (isset($arr["ARR"][$d]["AttendanceDate"])) {

	    		if ($arr["ARR"][$d]["AttendanceDate"] == $d) {
		    		if ($arr["ARR"][$d][$fld] == 0) {
			    		$arr["ARR"][$d][$fld] = $val;
			    		$arr["ARR"][$d]["UTC"] = $utc;
			    	}	
		    	}	
	    	}
		}
		/*
		$where_empAtt = "WHERE EmployeesRefId = '".$emprefid."' AND AttendanceDate BETWEEN '".$month_start."' AND '".$month_end."'";
		$rs_empAtt = SelectEach("employeesattendance",$where_empAtt);
		if ($rs_empAtt) {
			while ($row = mysqli_fetch_assoc($rs_empAtt)) {
				$AttendanceDate 	= $row["AttendanceDate"];
				$AttendanceTime 	= $row["AttendanceTime"];
				$UTC 				= $row["CheckTime"];
				$KEntry         	= $row["KindOfEntry"];
				$fldnval        	= "AttendanceTime = '".$UTC."',";
				$fldnval        	.= "UTC = '".$UTC."',";
				switch ($KEntry) {
					case 1:
						$fldnval	.= "TimeIn = '".get_today_minute($UTC)."',";
						break;
					case 2:
						$fldnval	.= "LunchOut = '".get_today_minute($UTC)."',";
						break;
					case 3:
						$fldnval	.= "LunchIn = '".get_today_minute($UTC)."',";
						break;
					case 4:
						$fldnval	.= "TimeOut = '".get_today_minute($UTC)."',";
						break;
				}
				$fldnval			.= "KEntry = '".$KEntry."'";
				$qry 				= "UPDATE tt_Attendance SET $fldnval WHERE AttendanceDate = '".$AttendanceDate."'";
				$result = mysqli_query($conn,$qry);
				if (!$result) {
					echo "Error in updating Temp Table ---> ".$qry;
				}
			}
		}
		$biometricsID = FindFirst("employees","WHERE RefId = $emprefid","BiometricsID");
        $query = "SELECT USERID, Badgenumber, Name FROM USERINFO";
        $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
        if ($result) {
            foreach($result as $row) {
               	switch ($CompanyID) {
                  	case "1000":
                    	$mdb_field = "Badgenumber";
                  		break;
                  	case "2":
                    	$mdb_field = "Name";
                  		break;
               	}

               	if ($row[$mdb_field] == $biometricsID) {
                  	$userID = $row["USERID"];
                  	break;
               	}
            }
        }
        $query = 'SELECT USERID, CHECKTIME, CHECKTYPE, VERIFYCODE, SENSORID, sn FROM CHECKINOUT WHERE USERID = '.$userID.' AND VERIFYCODE = 1';
        $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
        foreach($result as $row) {
           	$utc 				= strtotime($row["CHECKTIME"]);
           	$d 					= date("Y-m-d",$utc);
        	$mdbTime 			= date("H:i",$utc);
        	$mdbDate 			= date("Y-m-d",$utc);
        	$fldnval        	= "AttendanceTime = '".$utc."',";
			$fldnval        	.= "UTC = '".$utc."',";
			$where 				= "WHERE AttendanceDate = '".$mdbDate."'";
        	switch ($row["CHECKTYPE"]) {
              	case "I":
              		
              		
                 	$fldnval	.= $KEntry_json["FieldTimeIn"]." = '".get_today_minute($utc)."'";
                 	$where 		.= " AND TimeIn IS NULL";
                 	break;
              	case "0":
              		//$fldnval	.= "LunchIn = '".get_today_minute($utc)."'";
              		
                 	$fldnval	.= $KEntry_json["FieldLunchOut"]." = '".get_today_minute($utc)."'";
                 	$where 		.= " AND LunchOut IS NULL";
                 	break;
              	case "1":
              		//$fldnval	.= "TimeOut = '".get_today_minute($utc)."'";
              		
                 	$fldnval	.= $KEntry_json["FieldLunchIn"]." = '".get_today_minute($utc)."'";
                 	$where 		.= " AND LunchIn IS NULL";
                 	break;
              	case "O":
              		//$fldnval	.= "LunchOut = '".get_today_minute($utc)."'";
              		
                	$fldnval	.= $KEntry_json["FieldTimeOut"]." = '".get_today_minute($utc)."'";
                	$where 		.= " AND TimeOut IS NULL";
                	break;
        	}
        	$check_mdb 			= "SELECT * FROM tt_Attendance ".$where;
        	$rs_mdb				= mysqli_query($conn,$check_mdb);
        	if (mysqli_num_rows($rs_mdb)) {
        		$qry 			= "UPDATE tt_Attendance SET $fldnval WHERE AttendanceDate = '".$mdbDate."'";
				$result = mysqli_query($conn,$qry);
				if (!$result) {
					echo "Error in updating Temp Table ---> ".$qry;
				}
        	}
		}
		*/
	/*==============================================================================================================*/
	//START OF COMPUTATION AND SHOOTING
	/*==============================================================================================================*/
	$day 		 			= "";
	$remarks     			= "";
	$RG_Time_Per_Week		= Array(0,0,0,0,0,0);
	$Tardy_Time_Per_Week	= Array(0,0,0,0,0,0);
	$UT_Time_Per_Week		= Array(0,0,0,0,0,0);
	$Excess_Time_Per_Week 	= Array(0,0,0,0,0,0);
	$Absent_Time_Per_Week 	= Array(0,0,0,0,0,0);

	$Absent_Count_Per_Week 	= Array(0,0,0,0,0,0);
	$Tardy_Count_Per_Week	= Array(0,0,0,0,0,0);
	$UT_Count_Per_Week		= Array(0,0,0,0,0,0);

	/*
	$rs = mysqli_query($conn,"SELECT * FROM tt_Attendance ORDER BY UTC");
	while ($row = mysqli_fetch_assoc($rs)) {
	*/
	foreach ($arr as $value) {
		foreach ($value as $key => $row) {
			$day_name = date("D",$row['UTC']);
			if ($day_name != "") {
				switch ($day_name) {
					case 'Mon':
						$day_name = "Monday";
						break;
					case 'Tue':
						$day_name = "Tuesday";
						break;
					case 'Wed':
						$day_name = "Wednesday";
						break;
					case 'Thu':
						$day_name = "Thursday";
						break;
					case 'Fri':
						$day_name = "Friday";
						break;
					case 'Sat':
						$day_name = "Saturday";
						break;
					case 'Sun':
						$day_name = "Sunday";
						break;
				}
			}

			
			$day = $row["Day"];  
			$underTime 	 				= "";
			$late 						= "";
			$day_in             	   	= $day_name."In";
			$day_out            	   	= $day_name."Out";
			$day_flexi          	   	= $day_name."FlexiTime";
			$day_LBOut					= $day_name."LBOut";
			$day_LBIn					= $day_name."LBIn";

			$data_flexi             	= $Emp_WorkSched[$day_flexi];
			$data_timein            	= $Emp_WorkSched[$day_in];
			$data_timeout 				= $Emp_WorkSched[$day_out];
			$data_LunchOut 				= $Emp_WorkSched[$day_LBOut];
			$data_LunchIn 				= $Emp_WorkSched[$day_LBIn];
			$day_work_hours_count   	= ($data_timeout - $data_timein) - 60;
			$Emp_Timein 				= $row["TimeIn"];
			$Emp_TimeOut 				= $row["TimeOut"];
			$Week 						= $row["Week"];	
			$AutoLB						= $Emp_WorkSched["AutoLB"];

			/*if ($row["UTC"] != "") {
				$day_name = date("D",$row['UTC']);
				if ($day_name != "Sat" || $day_name != "Sun") {
				
					if ($Emp_Timein == "" || $Emp_TimeOut == "") {
						echo $day_name."<br>";
						switch ($Week) {
							case 1:
								$Absent_Time_Per_Week[1]++;
								$Absent_Count_Per_Week[1]++;
								break;
							case 2:
								$Absent_Time_Per_Week[2]++;
								$Absent_Count_Per_Week[2]++;
								break;
							case 3:
								$Absent_Time_Per_Week[3]++;
								$Absent_Count_Per_Week[3]++;
								break;
							case 4:
								$Absent_Time_Per_Week[4]++;
								$Absent_Count_Per_Week[4]++;
								break;
							case 5:
								$Absent_Time_Per_Week[5]++;
								$Absent_Count_Per_Week[5]++;
								break;
						}	
					}
				}	
			}*/
			


			/*==============================================================================================================*/
			//MAXIMUM TIME FROM FOR EMPLOYEE TO ENTER WITHOUT LATE
			/*==============================================================================================================*/
			if ($Emp_WorkSched["ScheduleType"] == "Fl") {
				$data_flexi = $data_flexi;
			} else if ($Emp_WorkSched["ScheduleType"] == "Fi") {
				$data_flexi = $data_timein;
			} else {
				$data_flexi = $data_timein;
			}
			/*==============================================================================================================*/
			//GETTING THE LATE OF THE EMPLOYEE
			/*==============================================================================================================*/
			if ($row["TimeIn"] != "") {
				if ($row["TimeIn"] >= $data_timein && $row["TimeIn"] <= $data_flexi) {
					$late = "";
				} else {
					$late = ($row["TimeIn"] - $data_flexi);
					switch ($Week) {
						case 1:
							$Tardy_Time_Per_Week[1] = $Tardy_Time_Per_Week[1] + $late;
							$Tardy_Count_Per_Week[1]++;
							break;
						case 2:
							$Tardy_Time_Per_Week[2] = $Tardy_Time_Per_Week[2] + $late;
							$Tardy_Count_Per_Week[2]++;
							break;
						case 3:
							$Tardy_Time_Per_Week[3] = $Tardy_Time_Per_Week[3] + $late;
							$Tardy_Count_Per_Week[3]++;
							break;
						case 4:
							$Tardy_Time_Per_Week[4] = $Tardy_Time_Per_Week[4] + $late;
							$Tardy_Count_Per_Week[4]++;
							break;
						case 5:
							$Tardy_Time_Per_Week[5] = $Tardy_Time_Per_Week[5] + $late;
							$Tardy_Count_Per_Week[5]++;
							break;
					}
				}
			}
			
			/*==============================================================================================================*/
			//GETTING THE WORKING AND EXCESS HOURS OF THE EMPLOYEE
			/*==============================================================================================================*/
			if ($row["TimeOut"] != "" && $row["TimeIn"] != "") {
				if ($Emp_Timein <= $data_timein) {
					$Emp_Timein = $data_timein;
				}
				$consume_time = (($Emp_TimeOut - $Emp_Timein) - 60);
				$excess_time = $consume_time - $day_work_hours_count; 	
				if ($excess_time > 0) {
					if ($Week == 1) $Excess_Time_Per_Week[1] = $Excess_Time_Per_Week[1] + $excess_time;
					if ($Week == 2) $Excess_Time_Per_Week[2] = $Excess_Time_Per_Week[2] + $excess_time;
					if ($Week == 3) $Excess_Time_Per_Week[3] = $Excess_Time_Per_Week[3] + $excess_time;
					if ($Week == 4) $Excess_Time_Per_Week[4] = $Excess_Time_Per_Week[4] + $excess_time;
					if ($Week == 5) $Excess_Time_Per_Week[5] = $Excess_Time_Per_Week[5] + $excess_time;
				}
			} else {
				$excess_time = "";
				$consume_time = "";
				$remarks = "<span style='color:red;'>No Time Out</span>";
			}
			/*==============================================================================================================*/
			//GETTING THE UNDERTIME AND EXCESS HOURS OF THE EMPLOYEE
			/*==============================================================================================================*/
			if ($consume_time >= $day_work_hours_count) {
				if ($day_work_hours_count != "") {
					if ($Week == 1) $RG_Time_Per_Week[1] = $RG_Time_Per_Week[1] + $day_work_hours_count;
					if ($Week == 2) $RG_Time_Per_Week[2] = $RG_Time_Per_Week[2] + $day_work_hours_count;
					if ($Week == 3) $RG_Time_Per_Week[3] = $RG_Time_Per_Week[3] + $day_work_hours_count;
					if ($Week == 4) $RG_Time_Per_Week[4] = $RG_Time_Per_Week[4] + $day_work_hours_count;
					if ($Week == 5) $RG_Time_Per_Week[5] = $RG_Time_Per_Week[5] + $day_work_hours_count;
				}
				$consume_time = $day_work_hours_count;
			} else {
				if ($row["TimeOut"] != "" && $row["TimeIn"] != "") {
					//$excess_time = "-".HoursFormat($day_work_hours_count - $consume_time);
					$excess_time = "";
					$underTime = $day_work_hours_count - $consume_time;
					switch ($Week) {
						case 1:
							$UT_Time_Per_Week[1] = $UT_Time_Per_Week[1] + $underTime;
							$UT_Count_Per_Week[1]++;
							break;
						case 2:
							$UT_Time_Per_Week[2] = $UT_Time_Per_Week[2] + $underTime;
							$UT_Count_Per_Week[2]++;
							break;
						case 3:
							$UT_Time_Per_Week[3] = $UT_Time_Per_Week[3] + $underTime;
							$UT_Count_Per_Week[3]++;
							break;
						case 4:
							$UT_Time_Per_Week[4] = $UT_Time_Per_Week[4] + $underTime;
							$UT_Count_Per_Week[4]++;
							break;
						case 5:
							$UT_Time_Per_Week[5] = $UT_Time_Per_Week[5] + $underTime;
							$UT_Count_Per_Week[5]++;
							break;
					}
				} else {
					$excess_time = "";
				}
				if ($consume_time != "") {
					if ($Week == 1) $RG_Time_Per_Week[1] = $RG_Time_Per_Week[1] + $consume_time;	
					if ($Week == 2) $RG_Time_Per_Week[2] = $RG_Time_Per_Week[2] + $consume_time;
					if ($Week == 3) $RG_Time_Per_Week[3] = $RG_Time_Per_Week[3] + $consume_time;
					if ($Week == 4) $RG_Time_Per_Week[4] = $RG_Time_Per_Week[4] + $consume_time;
					if ($Week == 5) $RG_Time_Per_Week[5] = $RG_Time_Per_Week[5] + $consume_time;
				}
				$consume_time = $consume_time;
			}
			/*==============================================================================================================*/
			//AUTO LUNCH BREAK
			/*==============================================================================================================*/
			if ($AutoLB == 1) {
				if ($row["TimeIn"] != "") {
					$Lunch_Out = $data_LunchOut;
					$Lunch_In = $data_LunchIn;
				} else {
					$Lunch_Out = "";
					$Lunch_In = "";
				}
			} else {
				$Lunch_Out = $row["LunchOut"];
				$Lunch_In = $row["LunchIn"];
			}	

			/*==============================================================================================================*/
			//MAPPING
			/*==============================================================================================================*/
			$map["[[din".$day."]]"]    = HoursFormat($row["TimeIn"]);	
			$map["[[dout".$day."]]"]   = HoursFormat($row["TimeOut"]);
			$map["[[lout".$day."]]"]   = HoursFormat($Lunch_Out);
			$map["[[lin".$day."]]"]    = HoursFormat($Lunch_In);

			if ($row["TimeIn"] != "" && $row["TimeOut"] == "") {
				$map["[[dout".$day."]]"]   = "<span style='color:red'>NO OUT</span>";
			}
			if ($row["TimeIn"] == "" && $row["TimeOut"] != "") {
				$map["[[din".$day."]]"]    = "<span style='color:red'>NO IN</span>";
			}
			if ($row["TimeIn"] != "" && $Lunch_Out == "") {
				$map["[[lout".$day."]]"]   = "<span style='color:red'>NO LOUT</span>";
			}
			if ($row["TimeIn"] != "" && $Lunch_In == "") {
				$map["[[lin".$day."]]"]   = "<span style='color:red'>NO LIN</span>";
			}
			
			$map["[[reg$day]]"]   	   = HoursFormat($consume_time);
			$map["[[tdy$day]]"]        = HoursFormat($late);
			$map["[[exh$day]]"]   	   = HoursFormat($excess_time);
			$map["[[ut$day]]"]    	   = HoursFormat($underTime);	
			$map["[[rem$day]]"]    	   = $remarks;	
		}
	}



	for ($i=1; $i <= 5; $i++) { 
		$map["[[rgwk$i]]"]     = HoursFormat($RG_Time_Per_Week[$i]);		
		$map["[[twk$i]]"]      = HoursFormat($Tardy_Time_Per_Week[$i]);	
		$map["[[ulwk$i]]"]     = HoursFormat($UT_Time_Per_Week[$i]);	
		$map["[[ehwk$i]]"]     = HoursFormat($Excess_Time_Per_Week[$i]);
		$map["[[ttwk$i]]"]     = $Tardy_Count_Per_Week[$i];
		$map["[[tuwk$i]]"]     = $UT_Count_Per_Week[$i];
		$map["[[awk$i]]"]      = $Absent_Count_Per_Week[$i];;
		$map["[[cowk$i]]"]     = "";
	}
	$map["[[totrg]]"]   = HoursFormat($RG_Time_Per_Week[1] + 
									  $RG_Time_Per_Week[2] +
									  $RG_Time_Per_Week[3] + 
									  $RG_Time_Per_Week[4] + 
									  $RG_Time_Per_Week[5]);

	$map["[[tott]]"]    = HoursFormat($Tardy_Time_Per_Week[1] + 
									  $Tardy_Time_Per_Week[2] + 
									  $Tardy_Time_Per_Week[3] + 
									  $Tardy_Time_Per_Week[4] + 
									  $Tardy_Time_Per_Week[5]);

	$map["[[totul]]"]   = HoursFormat($UT_Time_Per_Week[1] + 
									  $UT_Time_Per_Week[2] + 
									  $UT_Time_Per_Week[3] + 
									  $UT_Time_Per_Week[4] + 
									  $UT_Time_Per_Week[5]);
	
	$map["[[toteh]]"]   = HoursFormat($Excess_Time_Per_Week[1] + 
		                              $Excess_Time_Per_Week[2] + 
		                              $Excess_Time_Per_Week[3] + 
		                              $Excess_Time_Per_Week[4] +
		                              $Excess_Time_Per_Week[5]);

	$map["[[tottt]]"]   = $Tardy_Count_Per_Week[1] + 
                          $Tardy_Count_Per_Week[2] + 
                          $Tardy_Count_Per_Week[3] + 
                          $Tardy_Count_Per_Week[4] +
                          $Tardy_Count_Per_Week[5];

	$map["[[tottu]]"]   = $UT_Count_Per_Week[1] + 
                          $UT_Count_Per_Week[2] + 
                          $UT_Count_Per_Week[3] + 
                          $UT_Count_Per_Week[4] +
                          $UT_Count_Per_Week[5];

  	$map["[[tota]]"]   = $Absent_Count_Per_Week[1] + 
                          $Absent_Count_Per_Week[2] + 
                          $Absent_Count_Per_Week[3] + 
                          $Absent_Count_Per_Week[4] +
                          $Absent_Count_Per_Week[5];

	$map["[[totcc]]"] = "";
	$map["[[lcvl]]"] = "";
	$map["[[lcsl]]"] = "";
	$map["[[lcspl]]"] = "";


	/*==============================================================================================================*/
	//END OF COMPUTATION AND SHOOTING
	/*==============================================================================================================*/
	}





	/*==============================================================================================================*/
	//DEBUGGING
	/*==============================================================================================================*/
	/*$dbg = false;
	if ($dbg) {
		echo '
			<table border=1 style="width:100%;">
				<tr>
					<td>AttendanceDate</td>
					<td>AttendanceTime</td>
					<td>UTC</td>
					<td>TimeIn</td>
					<td>LunchOut</td>
					<td>LunchIn</td>
					<td>TimeOut</td>
					<td>Day</td>
					<td>Week</td>
					<td>KEntry</td>
					<td>WC</td>
					<td>Day Name</td>
				</tr>
		';
		$week_count = 0;
		$new = "";
		$prev = "";
		$x = mysqli_query($conn,"SELECT * FROM tt_Attendance ORDER BY AttendanceDate");
		while ($f = mysqli_fetch_assoc($x)) {
			$prev = date("W",$f['UTC']);
			if ($prev != $new) {
				$week_count++;
			}
			echo '
				<tr>
					<td>'.$f["AttendanceDate"].'</td>
					<td>'.$f["AttendanceTime"].'</td>
					<td>'.$f["UTC"].'</td>
					<td>'.$f["TimeIn"].'</td>
					<td>'.$f["LunchOut"].'</td>
					<td>'.$f["LunchIn"].'</td>
					<td>'.$f["TimeOut"].'</td>
					<td>'.$f["Day"].'</td>
					<td>'.$f["Week"].'</td>
					<td>'.$f["KEntry"].'</td>
					<td>'.date("W",$f['UTC']).'</td>
					<td>'.date("D",$f['UTC']).'</td>
				</tr>		
			';
			$new = date("W",$f['UTC']);
		}
		echo '</table>';	
	}*/
	$dbg = true;

	if ($dbg) {
		//print_r(json_encode($arr));
	
		echo '
			<table border=1 style="width:100%;">
				<tr>
					<td>AttendanceDate</td>
					<td>AttendanceTime</td>
					<td>UTC</td>
					<td>TimeIn</td>
					<td>LunchOut</td>
					<td>LunchIn</td>
					<td>TimeOut</td>
					<td>Day</td>
					<td>Week</td>
					<td>KEntry</td>
				</tr>
		';
		foreach ($arr as $value) {
			foreach ($value as $key => $new_val) {
				//print_r(json_encode($new_val));
				
				echo '
					<tr>
						<td>'.$new_val["AttendanceDate"].'</td>
						<td>'.$new_val["AttendanceTime"].'</td>
						<td>'.$new_val["UTC"].'</td>
						<td>'.$new_val["TimeIn"].'</td>
						<td>'.$new_val["LunchOut"].'</td>
						<td>'.$new_val["LunchIn"].'</td>
						<td>'.$new_val["TimeOut"].'</td>
						<td>'.$new_val["Day"].'</td>
						<td>'.$new_val["Week"].'</td>
						<td>'.$new_val["KEntry"].'</td>
					</tr>
				';
				
			}
			
		}
		echo '</table>';

	}
	//mysqli_query($conn,"DROP TEMPORARY TABLE tt_Attendance");
?>