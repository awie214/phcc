<?php
	include_once 'conn.e2e.php';
	$bio_arr 		= array();
	$biometricsID 	= strtolower($biometricsID);
	$sql 			= "SELECT * FROM USERINFO where NAME = '$biometricsID' ORDER BY USERID";
	$userID 		= 0;
	$rs 			= mysqli_query($bio_conn,$sql);
	if ($rs) {
		while ($row = mysqli_fetch_assoc($rs)) {
			$count = 0;
			$userID = $row["USERID"];
			$where_zk = "WHERE USERID = $userID";
			$where_zk .= " AND CHECKTIME >= '$start_this_month'";
			$new_sql = "SELECT * FROM CHECKINOUT ".$where_zk." ORDER BY CHECKTIME";
			$new_rs = mysqli_query($bio_conn,$new_sql);
			if ($new_rs) {
				while ($n_row = mysqli_fetch_assoc($new_rs)) {
					$count++;
					if (get_today_minute(strtotime($n_row["CHECKTIME"])) > 240) {
						$date = date("Y-m-d",strtotime($n_row["CHECKTIME"]));
						$bio_arr[$userID][$date][$count] = $n_row["CHECKTIME"];
					}
				}
			}
		}
	} else {
		echo "No Record Found";
	}
	if ($userID != 0) {
		echo '$("#pop_dtr").prop("disabled",false);'."\n";
       	echo '$("#btnProcess").prop("disabled",false);'."\n";
		foreach ($bio_arr as $key => $value) {
			foreach ($value as $nkey => $nvalue) {
				$count_logs = count($nvalue);
				$first 	= reset($nvalue);
				$last 	= end($nvalue);

				$utc_f 		= strtotime($first);
		        $fulldate_f = date("Y-m-d",$utc_f);
		        $AttendanceFormatted_f = date("h:i A",$utc_f);
		        $type_f 	= "1";

		        $where_f 	= "WHERE EmployeesRefId = $EmpRefId";
		        $where_f    .= " AND AttendanceDate = '".$fulldate_f."'";
		        $where_f    .= "  AND KindOfEntry = '".$type_f."'";

		        $compare_mdb_empatt_f = FindFirst("employeesattendance",$where_f,"RefId");
		        
		        


		        $utc_l 		= strtotime($last);
		        $fulldate_l = date("Y-m-d",$utc_l);
		        $AttendanceFormatted_l = date("h:i A",$utc_l);
		        $type_l 	= "4";

		        $where_l 	= "WHERE EmployeesRefId = $EmpRefId";
		        $where_l    .= " AND AttendanceDate = '".$fulldate_l."'";
		        $where_l    .= "  AND KindOfEntry = '".$type_l."'";
		        
		        $compare_mdb_empatt_l = FindFirst("employeesattendance",$where_l,"RefId");
		        if (count($nvalue) > 1) {
			        if (get_today_minute($utc_f) <= 720 || get_today_minute($utc_l) <= 720) {
		        		if (!$compare_mdb_empatt_f) {
				        	if(isset($arr["ARR"][$fulldate_f])) {
				              	$arr["ARR"][$fulldate_f]["Has"] = 1;
				           	}
				           	echo '$("#TI_'.$fulldate_f.'").html("'.$AttendanceFormatted_f.'");'."\n";
		                 	echo '$("#TI_'.$fulldate_f.'").attr("checktime","'.$utc_f.'");'."\n";
				        }
		        	}
		        	if (get_today_minute($utc_f) >= 721 || get_today_minute($utc_l) >= 721) {
		        		if (!$compare_mdb_empatt_l) {
				        	if(isset($arr["ARR"][$fulldate_l])) {
				              	$arr["ARR"][$fulldate_l]["Has"] = 1;
				           	}
				           	echo '$("#TO_'.$fulldate_l.'").html("'.$AttendanceFormatted_l.'");'."\n";
		                 	echo '$("#TO_'.$fulldate_l.'").attr("checktime","'.$utc_l.'");'."\n";
				        }	 
		        	}
		        } else {
		        	if (get_today_minute($utc_f) <= 720) {
		        		if (!$compare_mdb_empatt_f) {
				        	if(isset($arr["ARR"][$fulldate_f])) {
				              	$arr["ARR"][$fulldate_f]["Has"] = 1;
				           	}
				           	echo '$("#TI_'.$fulldate_f.'").html("'.$AttendanceFormatted_f.'");'."\n";
		                 	echo '$("#TI_'.$fulldate_f.'").attr("checktime","'.$utc_f.'");'."\n";
				        }
		        	} else {
		        		if (!$compare_mdb_empatt_l) {
				        	if(isset($arr["ARR"][$fulldate_f])) {
				              	$arr["ARR"][$fulldate_f]["Has"] = 1;
				           	}
				           	echo '$("#TO_'.$fulldate_f.'").html("'.$AttendanceFormatted_f.'");'."\n";
		                 	echo '$("#TO_'.$fulldate_f.'").attr("checktime","'.$utc_f.'");'."\n";
				        }	 
		        	}
		        	
		        }
		        		
			}
		}	
	} else {
		echo '$.notify("Incorrect Biometrics ID '.trim($biometricsID).'");'."\n";
	}
?>