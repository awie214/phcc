<?php
	function HoursFormat($timeInMin) {
        if ($timeInMin > 0) {
           $hr = explode(".",$timeInMin / 60)[0];
           if ($hr <= 9) {
              $hr = "0".$hr;
           }
           $mod_min = ($timeInMin % 60);
           if ($mod_min <= 9) {
              $mod_min = "0".$mod_min;
           }
           return $hr.":".$mod_min;   
        } else {
           return "&nbsp;";
        }
    }

	//SET ALL VARIABLE TO NULL

	$map["[[otp]]"] = "";
	$map["[[cocbal]]"] = "";
	for ($i=1; $i <=31 ; $i++) { 
		$map["[[d$i]]"] = "";
		$map["[[din$i]]"] = "";
		$map["[[lin$i]]"] = "";
		$map["[[lout$i]]"] = "";
		$map["[[dout$i]]"] = "";
		$map["[[reg$i]]"] = "";
		$map["[[exh$i]]"] = "";
		$map["[[tdy$i]]"] = "";
		$map["[[ot$i]]"] = "";
		$map["[[ut$i]]"] = "";
		$map["[[rem$i]]"] = "";
		if ($i < 10) $i = "0".$i;
		$map["[[date$i]]"] = "";
		$map["[[otin$i]]"] = "";
		$map["[[otout$i]]"] = "";
		$map["[[ob$i]]"] = "";
		$map["[[TDY$i]]"] = "";
		$map["[[UT$i]]"] = "";
		$map["[[OT$i]]"] = "";
		$map["[[REG$i]]"] = "";
		$map["[[EXH$i]]"] = "";
		$map["[[REM$i]] "] = "";
	}
	include 'conn.e2e.php';

   	$curr_date						= date("Y-m-d",time());
	$arr = array();
    $rsCompany = FindFirst("company","","*");
    $map["[[CompanyAddress]]"] = $rsCompany["Address"];
    $map["[[SmallLogo]]"] = "<img src='../../../public/images/".$rsCompany["RefId"]."/".$rsCompany["SmallLogo"]."' style='height:60px;'>";
    
	$NewMonth = getvalue("hNewMonth");
	$NewYear = getvalue("hNewYear");
	$num_of_days = cal_days_in_month(CAL_GREGORIAN,$NewMonth,$NewYear);
	$new_week = "";
	$week_count = 0;
	if ($NewMonth <= 9) {
		$NewMonth = "0".$NewMonth;
	}
	$month_start = $NewYear."-".$NewMonth."-01";
	$month_end = $NewYear."-".$NewMonth."-".$num_of_days;
	if (strtotime($curr_date) <= strtotime($month_end)) {
		$month_end = $curr_date;
	} else {
		$month_end = $month_end;
	}
	$num_of_days = date("d",strtotime($month_end));

	$map["[[Period]]"] = $NewMonth."/01/".$NewYear." - ".$NewMonth."/".$num_of_days."/".$NewYear;
	for($i=1;$i<=$num_of_days;$i++) {
		$y = $NewYear."-".$NewMonth."-".$i;
		$day = date("D",strtotime($y));	
		if ($day == "Sat" || $day == "Sun") {
			$day = "<span style='color:red;'>".$day."</span>";
		}
		$map["[[d$i]]"] = $day;
		if ($i <= 9) {
			$i = "0".$i;
		}
		$map["[[din$i]]"] = "";
		$map["[[lin$i]]"] = "";
		$map["[[lout$i]]"] = "";
		$map["[[dout$i]]"] = "";
		$map["[[reg$i]]"] = "";
		$map["[[exh$i]]"] = "";
		$map["[[tdy$i]]"] = "";
		$map["[[ot$i]]"] = "";
		$map["[[ut$i]]"] = "";
		$map["[[rem$i]]"] = "";
		$y = $NewYear."-".$NewMonth."-".$i;
		$week = date("W",strtotime($y));
		if ($week != $new_week) {
			$week_count++;
		}
		$dtr = [
	   		"AttendanceDate" => "$y",
	   		"AttendanceTime" => "",
	   		"UTC" => "",
	   		"TimeIn" => "",
	   		"LunchOut" => "",
	   		"LunchIn" => "",
	   		"TimeOut" => "",
	   		"OBOut" => "",
	   		"OBIn" => "",
	   		"Day" => "$i",
	   		"Week" => "$week_count",
	   		"KEntry" => "",
	   		"Holiday" => "",
	   		"CTO"=>"",
	   		"Leave"=> "",
	   		"OffSus"=>"",
	   		"OffSusName"=>"",
	   		"HasOT"=>""
	   	];
	   	$arr["ARR"][$y] = $dtr;
		$new_week = $week;
	}

	$userID = 0;
    if (getvalue("hucode") != "COMPEMP") {
    	$emprefid = getvalue("emprefid");	
    } else {
    	$emprefid = getvalue("hEmpRefId");
    }
	$row = getRecordSet("employees",$emprefid);
	if ($row) {
		$EmpName = $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];	
		$biometricsID 				= $row["BiometricsID"];
		$CompanyID 					= $row["CompanyRefId"];
		$BranchID					= $row["BranchRefId"];
		$Default_qry				= "WHERE CompanyRefId = ".$CompanyID." AND BranchRefId = ".$BranchID;
		$map["[[EmployeesName]]"] 	= $EmpName;


		$KEntryContent = file_get_contents(json."Settings_".$CompanyID.".json");
      	$KEntry_json   = json_decode($KEntryContent, true);
      	$OTMinTime = $KEntry_json["OTMinTime"];
      	$COCMinTime = $KEntry_json["COCMinTime"];

		$EmpInfo = f_Find("empinformation",$Default_qry." AND EmployeesRefId = ".$emprefid);
		if ($EmpInfo) {
			$fetch = mysqli_fetch_assoc($EmpInfo);
			$map["[[Position]]"] 		= getRecord("position",$fetch["PositionRefId"],"Name");
			$map["[[Office]]"] 			= getRecord("office",$fetch["OfficeRefId"],"Name");
			$map["[[WorkSchedule]]"] 	= getRecord("workschedule",$fetch["WorkScheduleRefId"],"Name");
			$map["[[Division]]"]		= getRecord("division",$fetch["DivisionRefId"],"Name");
			$worksched 					= $fetch["WorkScheduleRefId"];
		}

		$Emp_WorkSched 		= FindFirst("workschedule"," WHERE RefId = ".$worksched,"*");
		$where_empAtt 		= $Default_qry;
		$where_empAtt 		.= " AND EmployeesRefId = '".$emprefid."'";
		$where_empAtt       .= " AND AttendanceDate BETWEEN '".$month_start."' AND '".$month_end."'";
		$rs_empAtt 			= SelectEach("employeesattendance",$where_empAtt);
		if ($rs_empAtt) {
			while ($row = mysqli_fetch_assoc($rs_empAtt)) {
				$AttendanceDate 	= $row["AttendanceDate"];
				$AttendanceTime 	= $row["AttendanceTime"];
				$UTC 				= $row["CheckTime"];
				$KEntry         	= $row["KindOfEntry"];
				$fld 				= "";
				$val 				= "";
				switch ($KEntry) {
					case 1:
						$fld 		= "TimeIn";
						$val     	= get_today_minute($UTC);
						break;
					case 2:
						$fld 		= "LunchOut";
						$val     	= get_today_minute($UTC);
						break;
					case 3:
						$fld 		= "LunchIn";
						$val     	= get_today_minute($UTC);
						break;
					case 4:
						$fld 		= "TimeOut";
						$val     	= get_today_minute($UTC);
						break;
					case 7:
						$fld 		= "OBOut";
						$val     	= get_today_minute($UTC);
						break;
					case 8:
						$fld 		= "OBIn";
						$val     	= get_today_minute($UTC);
						break;
				}
				$arr["ARR"][$AttendanceDate][$fld] = $val;
				$arr["ARR"][$AttendanceDate]["UTC"] = $UTC;
			}
		}
		if (!empty($connection)) {
			$biometricsID = FindFirst("employees","WHERE RefId = $emprefid","BiometricsID");
		    $query = "SELECT USERID, Badgenumber, Name FROM USERINFO";
		    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
		    if ($result) {
		        foreach($result as $row) {
		           	switch ($CompanyID) {
		              	case "1000":
		                	$mdb_field = "Badgenumber";
		              		break;
		              	case "2":
		                	$mdb_field = "Name";
		              		break;
		           	}

		           	if ($row[$mdb_field] == $biometricsID) {
		              	$userID = $row["USERID"];
		              	break;
		           	}
		        }
		    }
		    $query = 'SELECT USERID, CHECKTIME, CHECKTYPE, VERIFYCODE, SENSORID, sn FROM CHECKINOUT WHERE USERID = '.$userID.' AND VERIFYCODE = 1';
		    $result = $connection->query($query)->fetchAll(PDO::FETCH_ASSOC);
		    foreach($result as $row) {
		       	$utc 				= strtotime($row["CHECKTIME"]);
		       	$d 					= date("Y-m-d",$utc);
		    	$mdbTime 			= date("H:i",$utc);
		    	$mdbDate 			= date("Y-m-d",$utc);
				$where 				= "WHERE AttendanceDate = '".$mdbDate."'";
				$fld 				= "";
				$val 				= "";
		    	switch ($row["CHECKTYPE"]) {
		          	case "I":
		          		$fld 		= $KEntry_json["FieldTimeIn"];
		          		$val 		= get_today_minute($utc);
		             	$where 		.= " AND TimeIn IS NULL";
		             	break;
		          	case "0":
		          		$fld 		= $KEntry_json["FieldLunchOut"];
		          		$val 		= get_today_minute($utc);
		             	$where 		.= " AND LunchOut IS NULL";
		             	break;
		          	case "1":
		          		$fld 		= $KEntry_json["FieldLunchIn"];
		          		$val 		= get_today_minute($utc);
		             	$where 		.= " AND LunchIn IS NULL";
		             	break;
		          	case "O":
		          		$fld 		= $KEntry_json["FieldTimeOut"];
		          		$val 		= get_today_minute($utc);
		            	$where 		.= " AND TimeOut IS NULL";
		            	break;
		    	}
		    	if (isset($arr["ARR"][$d]["AttendanceDate"])) {
		    		if ($arr["ARR"][$d]["AttendanceDate"] == $d) {
			    		if ($arr["ARR"][$d][$fld] == "") {
				    		$arr["ARR"][$d][$fld] = $val;
				    		$arr["ARR"][$d]["UTC"] = $utc;
				    	}	
			    	}	
		    	}
			}
		}
		$holiday = SelectEach("holiday","");
	    if ($holiday) {
	        while ($row = mysqli_fetch_assoc($holiday)) {
	            $StartDate        = $row["StartDate"];
	            $EndDate          = $row["EndDate"];
	            $holiday_date_diff   = dateDifference($StartDate,$EndDate);
	            $Name             = $row["Name"];
	            $EveryYr          = $row["isApplyEveryYr"];
	            $Legal            = $row["isLegal"];
	            $temp_arr         = explode("-", $StartDate);
	            $temp_date        = $NewYear."-".$temp_arr[1]."-".$temp_arr[2];
	            //echo $holiday_date_diff."---->Holiday Count";
	            if ($holiday_date_diff > 1) {
	               	for ($H=0; $H <= $holiday_date_diff-1; $H++) { 
	                  	$holiday_NewDate = date('Y-m-d', strtotime($temp_date. ' + '.$H.' days'));
                  		if ($EveryYr == 1) {
	                     	if(isset($arr["ARR"][$holiday_NewDate])) {
		                        $arr["ARR"][$holiday_NewDate]["Holiday"] = $Name;
	                    	}   
	                  	} else {
	                     	if(isset($arr["ARR"][$holiday_NewDate])) {
	                        	$arr["ARR"][$holiday_NewDate]["Holiday"] = $Name;
	                     	}
	                  	}
	               	}
	            } else {
	               	if ($EveryYr == 1) {
	                  	if(isset($arr["ARR"][$temp_date])) {
	                     	$arr["ARR"][$temp_date]["Holiday"] = $Name;
	                  	}   
	               	} else {
	                  	if(isset($arr["ARR"][$StartDate])) {
	                     	$arr["ARR"][$StartDate]["Holiday"] = $Name;
	                  	}
	               	}
	            }               
	        }
	    }
      	$officesuspension = SelectEach("officesuspension","");
      	if ($officesuspension) {
         	while ($row = mysqli_fetch_assoc($officesuspension)) {
	            $StartDate        = $row["StartDate"];
            	$EndDate          = $row["EndDate"];
            	$OffSus_date_diff = dateDifference($StartDate,$EndDate);
            	$Name             = $row["Name"];
            	$OffSusTime       = $row["StartTime"];
            	$temp_arr         = explode("-", $StartDate);
            	$temp_date        = $NewYear."-".$temp_arr[1]."-".$temp_arr[2];
            	if ($OffSus_date_diff > 1) {
               		for ($OS=0; $OS <= $OffSus_date_diff-1 ; $OS++) { 
                  		$OffSus_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$OS.' days'));
                  		if(isset($arr["ARR"][$OffSus_NewDate])) {
                     		$arr["ARR"][$OffSus_NewDate]["OffSusName"] 	= $Name;
	                    	$arr["ARR"][$OffSus_NewDate]["OffSus"] 		= $OffSusTime;
                  		}   
               		}   
            	} else {
               		if(isset($arr["ARR"][$StartDate])) {
                  		$arr["ARR"][$StartDate]["OffSusName"] 	= $Name;
	                    $arr["ARR"][$StartDate]["OffSus"] 		= $OffSusTime;
               		}
            	}
         	}
      	}

        $where_leave 		= $Default_qry;
		$where_leave		.= " AND EmployeesRefId = ".$emprefid;
		$where_leave		.= " AND ApplicationDateFrom BETWEEN '".$month_start."' AND '".$month_end."' AND Status = 'Approved'";
		$rs_leave			= SelectEach("employeesleave",$where_leave);
		if ($rs_leave) {
			while ($row = mysqli_fetch_assoc($rs_leave)) {
				$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
				if ($diff == 0) {
					if(isset($arr["ARR"][$row["ApplicationDateFrom"]])) {
                     	$arr["ARR"][$row["ApplicationDateFrom"]]["Leave"] = $row["LeavesRefId"];
                  	}
				} else {
					for ($i=0; $i <= $diff ; $i++) { 
						$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
						if(isset($arr["ARR"][$temp_date])) {
	                     	$arr["ARR"][$temp_date]["Leave"] = $row["LeavesRefId"];
	                  	}
					}
				}
				
			}
		}
		$where_cto 		= $Default_qry;
		$where_cto		.= " AND EmployeesRefId = ".$emprefid;
		$where_cto		.= " AND ApplicationDateFrom BETWEEN '".$month_start."' AND '".$month_end."'";
		$where_cto 		.= " AND Status = 'Approved'";
		$rs_cto			= SelectEach("employeescto",$where_cto);
		if ($rs_cto) {
			while ($row = mysqli_fetch_assoc($rs_cto)) {
				$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
				if ($diff == 0) {
					if(isset($arr["ARR"][$row["ApplicationDateFrom"]])) {
                     	$arr["ARR"][$row["ApplicationDateFrom"]]["CTO"] = $row["Hours"]*60;
                  	}
				} else {
					for ($i=0; $i <= $diff ; $i++) { 
						$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
						if(isset($arr["ARR"][$temp_date])) {
	                     	$arr["ARR"][$temp_date]["CTO"] = $row["Hours"]*60;
	                  	}
					}
				}
				
			}
		}
		/*
		$where_offsus 	= $Default_qry;
		$where_offsus	.= " AND EmployeesRefId = ".$emprefid;
		$where_offsus	.= " AND StartDate BETWEEN '".$month_start."' AND '".$month_end."'";
		$rs_offsus		= SelectEach("officesuspension",$where_offsus);
		if ($rs_offsus) {
			while ($row = mysqli_fetch_assoc($rs_offsus)) {
				$diff = dateDifference($row["StartDate"],$row["EndDate"]);
				if ($diff == 0) {
					if(isset($arr["ARR"][$row["StartDate"]])) {
                     	$arr["ARR"][$row["StartDate"]]["OffSus"] = $row["StartTime"];
                  	}
				} else {
					for ($i=0; $i <= $diff ; $i++) { 
						$temp_date = date('Y-m-d', strtotime($row["StartDate"] . ' +'.$i.' day'));
						if(isset($arr["ARR"][$temp_date])) {
	                     	$arr["ARR"][$temp_date]["OffSus"] = $row["StartTime"];
	                     	$arr["ARR"][$temp_date]["OffSusName"] = $row["Name"];
	                     	
	                  	}
					}
				}
			}
		}
		*/
		$where_OT    = $Default_qry;
		$where_OT   .= " AND EmployeesRefId = ".$emprefid;
		$where_OT   .= " AND StartDate BETWEEN '".$month_start."' AND '".$month_end."'";
		$where_OT   .= " AND Status = 'Approved'";
		$rs_OT 		 = SelectEach("overtime_request",$where_OT);
		if ($rs_OT) {
			while ($row = mysqli_fetch_assoc($rs_OT)) {
				$StartDate 		= $row["StartDate"];
				$EndDate 		= $row["EndDate"];
				$WithPay        = $row["WithPay"];
				$OT_DateDiff 	= dateDifference($StartDate,$EndDate);
				for ($O=0; $O <= $OT_DateDiff-1 ; $O++) { 
               		$OT_NewDate = date('Y-m-d', strtotime($StartDate. ' + '.$O.' days'));
               		if ($WithPay == 1) {
               			$Pay = 1;
               		} else {
               			$Pay = 0;
               		}
               		if(isset($arr["ARR"][$OT_NewDate])) {
                     	$arr["ARR"][$OT_NewDate]["HasOT"] = $Pay;
                  	}
               	}
			}
		}



		/*==============================================================================================================*/
		//START OF COMPUTATION AND SHOOTING
		/*==============================================================================================================*/
		$day 		 			= "";
		
		$day_count_diff			= 0;
		$OT_Count				= 0;
		$Total_Days				= 0;
		$RG_Time_Per_Week		= Array(0,0,0,0,0,0,0);
		$Tardy_Time_Per_Week	= Array(0,0,0,0,0,0,0);
		$UT_Time_Per_Week		= Array(0,0,0,0,0,0,0);
		$Excess_Time_Per_Week 	= Array(0,0,0,0,0,0,0);
		$Absent_Time_Per_Week 	= Array(0,0,0,0,0,0,0);

		$Absent_Count_Per_Week 	= Array(0,0,0,0,0,0,0);
		$Tardy_Count_Per_Week	= Array(0,0,0,0,0,0,0);
		$UT_Count_Per_Week		= Array(0,0,0,0,0,0,0);
		$COC_Time_Per_Week		= Array(0,0,0,0,0,0,0);

		foreach ($arr as $value) {
			foreach ($value as $key => $row) {
				$day_name = date("D",strtotime($row['AttendanceDate']));
				if ($day_name != "") {
					switch ($day_name) {
						case 'Mon':
							$day_name = "Monday";
							break;
						case 'Tue':
							$day_name = "Tuesday";
							break;
						case 'Wed':
							$day_name = "Wednesday";
							break;
						case 'Thu':
							$day_name = "Thursday";
							break;
						case 'Fri':
							$day_name = "Friday";
							break;
						case 'Sat':
							$day_name = "Saturday";
							break;
						case 'Sun':
							$day_name = "Sunday";
							break;
					}
				}	
				
				if ($row["TimeIn"] != "" || $row["OBOut"] != "") {
					$Total_Days++;
				}
				$day = $row["Day"]; 
				$underTime 	 				= "";
				$late 						= "";
				$remarks     				= "";
				$day_in             	   	= $day_name."In";
				$day_out            	   	= $day_name."Out";
				$day_flexi          	   	= $day_name."FlexiTime";
				$day_LBOut					= $day_name."LBOut";
				$day_LBIn					= $day_name."LBIn";
				$day_RestDay				= $day_name."isRestDay";
				$day_isflexi          	   	= $day_name."isFlexi";


				$data_flexi             	= $Emp_WorkSched[$day_flexi];
				$data_isflexi             	= $Emp_WorkSched[$day_isflexi];
				$data_timein            	= $Emp_WorkSched[$day_in];
				$data_timeout 				= $Emp_WorkSched[$day_out];
				$data_LunchOut 				= $Emp_WorkSched[$day_LBOut];
				$data_LunchIn 				= $Emp_WorkSched[$day_LBIn];
				$data_RestDay      			= $Emp_WorkSched[$day_RestDay];
				$day_work_hours_count   	= ($data_timeout - $data_timein) - ($data_LunchIn - $data_LunchOut);
				$Week 						= $row["Week"];	
				$AutoLB						= $Emp_WorkSched["AutoLB"];
				

				$arr_TI						= $row["TimeIn"];
				$arr_OBO					= $row["OBOut"];
				$arr_TO						= $row["TimeOut"];
				$arr_OBI					= $row["OBIn"];
				$arr_Holiday				= $row["Holiday"];
				$arr_leave 					= $row["Leave"];
				$arr_COC					= $row["CTO"];
				$arr_OffSus					= $row["OffSus"];
				$arr_HasOT 					= $row["HasOT"];


				if ($arr_TI == "" && $arr_leave != "") {
					$arr_TI = $data_timein;
					$arr_TO = $data_timeout;
				}
				if ($arr_TI == "" && $arr_OBO != "") {
					$arr_TI = $arr_OBO;
				}
				if ($arr_TO == "" && $arr_OBI != "") {
					$arr_TO = $arr_OBI;
				}

				if ($arr_Holiday != "" && $arr_TI == "") {
					$arr_TI = $data_timein;
					$arr_TO = $data_timeout;
				}
				
				if ($arr_COC != "") {
					if ($arr_COC >= $day_work_hours_count) {
						$arr_TI = $data_timein;
						$arr_TO = $data_timeout;
					} else {
						if ($arr_TI == "") {
							$arr_TI = $data_timein;
						}
						if ($arr_TO == "") {
							$arr_TO = $data_timein + $arr_COC;
						} else {
							$arr_TO = $arr_TO + $arr_COC;
						}
						/*echo "COC: ".$arr_COC."<br>"; 
						echo "Work Hours: ".$day_work_hours_count."<br>"; 
						echo "Time In : ".$arr_TI."<br>"; 
						echo "Time Out: ".$arr_TO."<br>"; */
					}	
				}

				if ($arr_OffSus != "") {
					if ($arr_TI == "") {
						$arr_TI = $arr_OffSus;
					}
					if ($arr_TO == "") {
						$arr_TO = $data_timeout;
					}
				}
				
				
				/*==============================================================================================================*/
				//MAXIMUM TIME FROM FOR EMPLOYEE TO ENTER WITHOUT LATE
				/*==============================================================================================================*/
				if ($Emp_WorkSched["ScheduleType"] == "Fl" || $Emp_WorkSched["ScheduleType"] == "Co") {
					$data_flexi = $data_flexi;
				} else if ($Emp_WorkSched["ScheduleType"] == "Fi") {
					$data_flexi = $data_timein;
				} else {
					$data_flexi = $data_flexi;
				}

				/*==============================================================================================================*/
				//GETTING THE LATE OF THE EMPLOYEE
				/*==============================================================================================================*/
				
				if ($data_RestDay != 1) {
					if ($arr_TI != "" || $arr_OBO != "") {
						if ($data_isflexi != 1) {
							if ($arr_TI >= $data_timein) {
								$late = "";
							} else {
								$late = ($arr_TI - $data_flexi);
								switch ($Week) {
									case 1:
										$Tardy_Time_Per_Week[1] = $Tardy_Time_Per_Week[1] + $late;
										$Tardy_Count_Per_Week[1]++;
										break;
									case 2:
										$Tardy_Time_Per_Week[2] = $Tardy_Time_Per_Week[2] + $late;
										$Tardy_Count_Per_Week[2]++;
										break;
									case 3:
										$Tardy_Time_Per_Week[3] = $Tardy_Time_Per_Week[3] + $late;
										$Tardy_Count_Per_Week[3]++;
										break;
									case 4:
										$Tardy_Time_Per_Week[4] = $Tardy_Time_Per_Week[4] + $late;
										$Tardy_Count_Per_Week[4]++;
										break;
									case 5:
										$Tardy_Time_Per_Week[5] = $Tardy_Time_Per_Week[5] + $late;
										$Tardy_Count_Per_Week[5]++;
										break;
								}		
							}
						} else {
							if ($arr_TI <= $data_flexi) {
								$late = "";
							} else {
								$late = ($arr_TI - $data_flexi);

								switch ($Week) {
									case 1:
										$Tardy_Time_Per_Week[1] = $Tardy_Time_Per_Week[1] + $late;
										$Tardy_Count_Per_Week[1]++;
										break;
									case 2:
										$Tardy_Time_Per_Week[2] = $Tardy_Time_Per_Week[2] + $late;
										$Tardy_Count_Per_Week[2]++;
										break;
									case 3:
										$Tardy_Time_Per_Week[3] = $Tardy_Time_Per_Week[3] + $late;
										$Tardy_Count_Per_Week[3]++;
										break;
									case 4:
										$Tardy_Time_Per_Week[4] = $Tardy_Time_Per_Week[4] + $late;
										$Tardy_Count_Per_Week[4]++;
										break;
									case 5:
										$Tardy_Time_Per_Week[5] = $Tardy_Time_Per_Week[5] + $late;
										$Tardy_Count_Per_Week[5]++;
										break;
								}		
							}
						}
					}	
				}
				
				
				/*==============================================================================================================*/
				//GETTING THE WORKING AND EXCESS HOURS OF THE EMPLOYEE
				/*==============================================================================================================*/
				
				if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
					if ($arr_TI <= $data_timein) {
						$arr_TI = $data_timein;
					}
					$consume_time = (($arr_TO - $arr_TI) - ($data_LunchIn - $data_LunchOut));
					$excess_time = $consume_time - $day_work_hours_count; 	
					if ($excess_time > 0) {
						if ($Week == 1) $Excess_Time_Per_Week[1] = $Excess_Time_Per_Week[1] + $excess_time;
						if ($Week == 2) $Excess_Time_Per_Week[2] = $Excess_Time_Per_Week[2] + $excess_time;
						if ($Week == 3) $Excess_Time_Per_Week[3] = $Excess_Time_Per_Week[3] + $excess_time;
						if ($Week == 4) $Excess_Time_Per_Week[4] = $Excess_Time_Per_Week[4] + $excess_time;
						if ($Week == 5) $Excess_Time_Per_Week[5] = $Excess_Time_Per_Week[5] + $excess_time;
					}
				} else {
					$excess_time = "";
					$consume_time = "";
				}
				
				/*==============================================================================================================*/
				//GETTING THE UNDERTIME AND EXCESS HOURS OF THE EMPLOYEE
				/*==============================================================================================================*/
				if ($consume_time >= $day_work_hours_count) {
					if ($day_work_hours_count != "") {
						if ($Week == 1) $RG_Time_Per_Week[1] = $RG_Time_Per_Week[1] + $day_work_hours_count;
						if ($Week == 2) $RG_Time_Per_Week[2] = $RG_Time_Per_Week[2] + $day_work_hours_count;
						if ($Week == 3) $RG_Time_Per_Week[3] = $RG_Time_Per_Week[3] + $day_work_hours_count;
						if ($Week == 4) $RG_Time_Per_Week[4] = $RG_Time_Per_Week[4] + $day_work_hours_count;
						if ($Week == 5) $RG_Time_Per_Week[5] = $RG_Time_Per_Week[5] + $day_work_hours_count;
					}
					$consume_time = $day_work_hours_count;
				} else {
					if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
						$excess_time = "";
						$underTime = $day_work_hours_count - $consume_time;
						switch ($Week) {
							case 1:
								$UT_Time_Per_Week[1] = $UT_Time_Per_Week[1] + $underTime;
								$UT_Count_Per_Week[1]++;
								break;
							case 2:
								$UT_Time_Per_Week[2] = $UT_Time_Per_Week[2] + $underTime;
								$UT_Count_Per_Week[2]++;
								break;
							case 3:
								$UT_Time_Per_Week[3] = $UT_Time_Per_Week[3] + $underTime;
								$UT_Count_Per_Week[3]++;
								break;
							case 4:
								$UT_Time_Per_Week[4] = $UT_Time_Per_Week[4] + $underTime;
								$UT_Count_Per_Week[4]++;
								break;
							case 5:
								$UT_Time_Per_Week[5] = $UT_Time_Per_Week[5] + $underTime;
								$UT_Count_Per_Week[5]++;
								break;
						}
					} else {
						$excess_time = "";
					}
					if ($consume_time != "") {
						if ($Week == 1) $RG_Time_Per_Week[1] = $RG_Time_Per_Week[1] + $consume_time;	
						if ($Week == 2) $RG_Time_Per_Week[2] = $RG_Time_Per_Week[2] + $consume_time;
						if ($Week == 3) $RG_Time_Per_Week[3] = $RG_Time_Per_Week[3] + $consume_time;
						if ($Week == 4) $RG_Time_Per_Week[4] = $RG_Time_Per_Week[4] + $consume_time;
						if ($Week == 5) $RG_Time_Per_Week[5] = $RG_Time_Per_Week[5] + $consume_time;
					}
					$consume_time = $consume_time;
				}
				/*==============================================================================================================*/
				//AUTO LUNCH BREAK
				/*==============================================================================================================*/
				if ($AutoLB == 1) {
					if ($arr_TI != "") {
						$Lunch_Out = $data_LunchOut;
						$Lunch_In = $data_LunchIn;
					} else {
						$Lunch_Out = "";
						$Lunch_In = "";
					}
				} else {
					$Lunch_Out = $row["LunchOut"];
					$Lunch_In = $row["LunchIn"];
				}	

				



				/*==============================================================================================================*/
				//MAPPING
				/*==============================================================================================================*/
				$map["[[date".$day."]]"] = $row["AttendanceDate"];
				if ($data_RestDay == 1) {
					$map["[[rem".$day."]]"]    = "<span style='color:red;'>RestDay</span>";
				} else {
					$map["[[din".$day."]]"]    = HoursFormat($row["TimeIn"]);	
					$map["[[dout".$day."]]"]   = HoursFormat($row["TimeOut"]);
					$map["[[lout".$day."]]"]   = HoursFormat($Lunch_Out);
					$map["[[lin".$day."]]"]    = HoursFormat($Lunch_In);

						
					if ($row["TimeIn"] != "" && $row["TimeOut"] == "") {
						$remarks = "NO TIME OUT";
					} else if ($row["TimeIn"] == "" && $row["TimeOut"] != "") {
						$remarks = "NO TIME IN";
					} else if ($row["TimeIn"] != "" && $Lunch_Out == "") {
						$remarks = "NO LUNCH OUT";
					} else if ($row["TimeIn"] != "" && $Lunch_In == "") {
						$remarks = "NO LUNCH IN";
					}


					$map["[[reg".$day."]]"]   	   = HoursFormat($consume_time);
					$map["[[tdy".$day."]]"]        = HoursFormat($late);
					$map["[[ut".$day."]]"]    	   = HoursFormat($underTime);	
					$map["[[rem".$day."]]"]    	   = "<span style='color:black;'>".$remarks."</span>";
				}

				if ($late < 0) {
					if ($arr_HasOT == "1") {
						if ($excess_time >= $OTMinTime) {
							$OT_Count = $OT_Count + $excess_time;
							$map["[[ot".$day."]]"]   	   		  = HoursFormat($excess_time);
							$map["[[rem".$day."]]"]  			  = "With Pay";
						} else {
							$map["[[ot".$day."]]"] = "";
						}
					} else if ($arr_HasOT == "0") {
						if ($excess_time >= $COCMinTime) {
							
							if ($Week == 1) $COC_Time_Per_Week[1] = $COC_Time_Per_Week[1] + $excess_time;
							if ($Week == 2) $COC_Time_Per_Week[2] = $COC_Time_Per_Week[2] + $excess_time;
							if ($Week == 3) $COC_Time_Per_Week[3] = $COC_Time_Per_Week[3] + $excess_time;
							if ($Week == 4) $COC_Time_Per_Week[4] = $COC_Time_Per_Week[4] + $excess_time;
							if ($Week == 5) $COC_Time_Per_Week[5] = $COC_Time_Per_Week[5] + $excess_time;		
							echo $excess_time."->$day->$arr_HasOT<br>";
							$map["[[ot".$day."]]"]   	   		  = HoursFormat($excess_time);
							$map["[[rem".$day."]]"] 			  = "Without Pay";
						} else {
							$map["[[ot".$day."]]"] = "";
						}
					} else {
						$map["[[exh".$day."]]"]   	   			  = HoursFormat($excess_time);
					}	
				}
				

				/*==============================================================================================================*/
				//GETTING ABSENT
				/*==============================================================================================================*/
				if ($arr_TI == "" && $arr_OBO == "" && $data_RestDay != 1) {
					if ($Week == 1) $Absent_Count_Per_Week[1]++;
					if ($Week == 2) $Absent_Count_Per_Week[2]++;
					if ($Week == 3) $Absent_Count_Per_Week[3]++;
					if ($Week == 4) $Absent_Count_Per_Week[4]++;
					if ($Week == 5) $Absent_Count_Per_Week[5]++;
					$map["[[rem".$day."]]"]    = "<span style='color:black;'>Absent</span>";
				}
				if ($row["Leave"] != "") {
	              	$leave = getRecord("leaves",$row["Leave"],"Name");
	              	$map["[[rem".$day."]]"]    = "<span style='color:black;'>".$leave."</span>";
	              	$map["[[lin".$day."]]"]    = "";
					$map["[[lout".$day."]]"]   = "";
					$map["[[reg".$day."]]"]    = "";
	            }
				if ($arr_OBO != "") {
					if ($row["TimeIn"] == "" && $row["TimeOut"] == "") {
						$map["[[lin".$day."]]"]    = "";
						$map["[[lout".$day."]]"]   = "";
					}
					$map["[[rem".$day."]]"]    = "<span style='color:black;'>Office Authority</span>";
				}
				if ($arr_Holiday != "") {
					$map["[[rem".$day."]]"]    = "<span style='color:black;'>".$arr_Holiday."</span>";
					$map["[[lin".$day."]]"]    = "";
					$map["[[lout".$day."]]"]   = "";
					$map["[[reg".$day."]]"]    = "";
				}
				if ($arr_COC != "") {
					if ($arr_COC >= $day_work_hours_count) {
						$map["[[lin".$day."]]"]    = "";
						$map["[[lout".$day."]]"]   = "";
					}
					$map["[[rem".$day."]]"]    = "<span style='color:black;'>CTO</span>";
					$map["[[reg".$day."]]"]    = "";
					
				}
				if ($arr_OffSus != "") {
					if ($row["TimeIn"] == "" && $row["TimeOut"] == "") {
						$map["[[lin".$day."]]"]    = "";
						$map["[[lout".$day."]]"]   = "";
					}
					$map["[[rem".$day."]]"]    = "<span style='color:black;'>".$row["OffSusName"]."</span>";
				}
			}
		}



		for ($i=1; $i <= 6; $i++) { 
			$map["[[rgwk$i]]"]     = HoursFormat($RG_Time_Per_Week[$i]);		
			$map["[[twk$i]]"]      = HoursFormat($Tardy_Time_Per_Week[$i]);	
			$map["[[ulwk$i]]"]     = HoursFormat($UT_Time_Per_Week[$i]);	
			$map["[[ehwk$i]]"]     = HoursFormat($Excess_Time_Per_Week[$i]);
			$map["[[ttwk$i]]"]     = $Tardy_Count_Per_Week[$i];
			$map["[[tuwk$i]]"]     = $UT_Count_Per_Week[$i];
			$map["[[awk$i]]"]      = $Absent_Count_Per_Week[$i];
			$map["[[cowk$i]]"]     = HoursFormat($COC_Time_Per_Week[$i]);
		}
		$map["[[totrg]]"]   = HoursFormat($RG_Time_Per_Week[1] + 
										  $RG_Time_Per_Week[2] +
										  $RG_Time_Per_Week[3] + 
										  $RG_Time_Per_Week[4] + 
										  $RG_Time_Per_Week[5]);

		$map["[[tott]]"]    = HoursFormat($Tardy_Time_Per_Week[1] + 
										  $Tardy_Time_Per_Week[2] + 
										  $Tardy_Time_Per_Week[3] + 
										  $Tardy_Time_Per_Week[4] + 
										  $Tardy_Time_Per_Week[5]);

		$map["[[totul]]"]   = HoursFormat($UT_Time_Per_Week[1] + 
										  $UT_Time_Per_Week[2] + 
										  $UT_Time_Per_Week[3] + 
										  $UT_Time_Per_Week[4] + 
										  $UT_Time_Per_Week[5]);
		
		$map["[[toteh]]"]   = HoursFormat($Excess_Time_Per_Week[1] + 
			                              $Excess_Time_Per_Week[2] + 
			                              $Excess_Time_Per_Week[3] + 
			                              $Excess_Time_Per_Week[4] +
			                              $Excess_Time_Per_Week[5]);

		$map["[[totcc]]"]  	= HoursFormat($COC_Time_Per_Week[1] + 
				                          $COC_Time_Per_Week[2] + 
				                          $COC_Time_Per_Week[3] + 
				                          $COC_Time_Per_Week[4] +
				                          $COC_Time_Per_Week[5]);

		$map["[[tottt]]"]   = $Tardy_Count_Per_Week[1] + 
	                          $Tardy_Count_Per_Week[2] + 
	                          $Tardy_Count_Per_Week[3] + 
	                          $Tardy_Count_Per_Week[4] +
	                          $Tardy_Count_Per_Week[5];

		$map["[[tottu]]"]   = $UT_Count_Per_Week[1] + 
	                          $UT_Count_Per_Week[2] + 
	                          $UT_Count_Per_Week[3] + 
	                          $UT_Count_Per_Week[4] +
	                          $UT_Count_Per_Week[5];

	  	$map["[[tota]]"]   =  $Absent_Count_Per_Week[1] + 
	                          $Absent_Count_Per_Week[2] + 
	                          $Absent_Count_Per_Week[3] + 
	                          $Absent_Count_Per_Week[4] +
	                          $Absent_Count_Per_Week[5];

	  	


	    /*
	    $whereSL 			= "WHERE EmployeesRefId = ".$emprefid." AND NameCredits = 'SL'";
	    $whereVL 			= "WHERE EmployeesRefId = ".$emprefid." AND NameCredits = 'VL'";
	    $sl 				= FindFirst("employeescreditbalance",$whereSL,"BeginningBalance");
	    $vl 				= FindFirst("employeescreditbalance",$whereVL,"BeginningBalance");
	    $day_eq_vl 			= FindFirst("slvlearneddaily","WHERE NoOfDays =".$Total_Days,"VLEarned");
	    $day_eq_sl 			= FindFirst("slvlearneddaily","WHERE NoOfDays =".$Total_Days,"SLEarned");
		//$map["[[lcvl]]"] 	= $vl." + ".$day_eq_vl;
		//$map["[[lcsl]]"] 	= $sl." + ".$day_eq_sl;
		$map["[[lcvl]]"] 	= $vl + $day_eq_vl;
		$map["[[lcsl]]"] 	= $sl + $day_eq_sl;
		$map["[[lcspl]]"] 	= 3;
		$map["[[otp]]"] 	= HoursFormat($OT_Count);

		*/
		$where_leave		= $Default_qry." AND EmployeesRefId = ".$emprefid;
		$where_leave		.= " AND ApplicationDateFrom BETWEEN '".$month_start."' AND '".$month_end."' AND Status = 'Approved'";
		$rs_leave			= SelectEach("employeesleave",$where_leave);
		if ($rs_leave) {
			while ($row = mysqli_fetch_assoc($rs_leave)) {
				$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
				$day_count_diff = $day_count_diff + $diff;
			}
		}





		$whereSL 			= "WHERE EmployeesRefId = ".$emprefid." AND NameCredits = 'SL'";
		$whereVL 			= "WHERE EmployeesRefId = ".$emprefid." AND NameCredits = 'VL'";
		$whereOT 			= "WHERE EmployeesRefId = ".$emprefid." AND NameCredits = 'OT'";
		$day_eq_vl 			= FindFirst("slvlearneddaily","WHERE NoOfDays =".$Total_Days,"VLEarned");
	    $day_eq_sl 			= FindFirst("slvlearneddaily","WHERE NoOfDays =".$Total_Days,"SLEarned");
	    $rowsl 				= FindFirst("employeescreditbalance",$whereSL,"*");	
	    $rowvl 				= FindFirst("employeescreditbalance",$whereVL,"*");
	    $rowot 				= FindFirst("employeescreditbalance",$whereOT,"*");
	    if ($rowsl["OutstandingBalance"] != "") {
	    	$sl = $rowsl["OutstandingBalance"];
	    } else {
	    	$sl = $rowsl["BeginningBalance"];
	    }
	    if ($rowvl["OutstandingBalance"] != "") {
	    	$vl = $rowvl["OutstandingBalance"];
	    } else {
	    	$vl = $rowvl["BeginningBalance"];
	    }
	    if ($rowot["OutstandingBalance"] != "") {
	    	$OTBal = $rowot["OutstandingBalance"];
	    } else {
	    	$OTBal = $rowot["BeginningBalance"];
	    }
	    $VLEarned 			= $day_eq_vl;
	   	$SLEarned 			= $day_eq_sl;
	   	$VLBal 				= $vl + $day_eq_vl;
		$SLBal 				= $sl + "1.25";

		if ($day_count_diff > 0) {
			$leave_eq_sl	= FindFirst("slvlearneddaily","WHERE NoOfDays =".$day_count_diff,"SLEarned");
			$leave_eq_vl	= FindFirst("slvlearneddaily","WHERE NoOfDays =".$day_count_diff,"VLEarned");
			$VLBal			= $VLBal - $leave_eq_vl;
			$SLBal			= $SLBal - $leave_eq_sl;
		}


	    
	    $where_acc_sl		= "WHERE EmployeesRefId = ".$emprefid." AND AccumDate >= '".$month_end."'"; 
	    $where_acc_sl		.= " AND NameCredits = 'SL' AND Balance = $SLBal";
	    $check_acc_sl		= FindLast("accumcreditbalance",$where_acc_sl,"Balance");
	    if (is_numeric($check_acc_sl)) {
	    	$sl 			= $check_acc_sl;
	    }


	    
	    $where_acc_vl		= "WHERE EmployeesRefId = ".$emprefid." AND AccumDate >= '".$month_end."'"; 
	    $where_acc_vl		.= " AND NameCredits = 'VL' AND Balance = $VLBal";
	    $check_acc_vl		= FindLast("accumcreditbalance",$where_acc_vl,"Balance");
	    if (is_numeric($check_acc_vl)) {
	    	$vl 			= $check_acc_vl;
	    }

	    $map["[[lcvl]]"] 	= $VLBal;
		$map["[[lcsl]]"] 	= $SLBal;
		$map["[[cocbal]]"] 	= $OTBal;
		$map["[[lcspl]]"] 	= 3;
		$map["[[otp]]"] 	= HoursFormat($OT_Count);

		/*
		/*==============================================================================================================*/
		//GETTING VL
		/*==============================================================================================================*/
	    /*
	    $vl = 0;
	    $whereVL = "WHERE EmployeesRefId = ".$emprefid." AND NameCredits = 'VL'";
	    $whereVL .= " AND BegBalAsOfDate BETWEEN '".$month_start."' AND '".$month_end."'";
	    $rs_vl = SelectEach("employeescreditbalance",$whereVL);
	    if ($rs_vl) {
	    	while ($row = mysqli_fetch_assoc($rs_vl)) {
	    		$vl = $vl + $row["BeginningBalance"];
	    	}
	    }
	    $map["[[lcvl]]"] = $vl;
		*/
	    /*==============================================================================================================*/
		//GETTING SL
		/*==============================================================================================================*/
	    /*
	    $sl = 0;
	    $whereSL = "WHERE EmployeesRefId = ".$emprefid." AND NameCredits = 'SL'";
	    $whereSL .= " AND BegBalAsOfDate BETWEEN '".$month_start."' AND '".$month_end."'";
	    $rs_sl = SelectEach("employeescreditbalance",$whereSL);
	    if ($rs_sl) {
	    	while ($row = mysqli_fetch_assoc($rs_sl)) {
	    		$sl = $sl + $row["BeginningBalance"];
	    	}
	    }
		$map["[[lcsl]]"] = $sl;
		$map["[[lcspl]]"] = 3;
		$map["[[otp]]"] = HoursFormat($OT_Count);
		*/


		/*==============================================================================================================*/
		//END OF COMPUTATION AND SHOOTING
		/*==============================================================================================================*/
	}
	$dbg = false;

	if ($dbg) {
		//print_r(json_encode($arr));
	
		echo '
			<table border=1 style="width:100%;">
				<tr>
					<td>AttendanceDate</td>
					<td>AttendanceTime</td>
					<td>UTC</td>
					<td>TimeIn</td>
					<td>LunchOut</td>
					<td>LunchIn</td>
					<td>TimeOut</td>
					<td>OBOut</td>
					<td>OBIn</td>
					<td>Day</td>
					<td>Week</td>
					<td>KEntry</td>
					<td>Holiday</td>
					<td>Leave</td>
					<td>CTO</td>
					<td>OFFSUS</td>
					<td>HasOT</td>
				</tr>
		';
		foreach ($arr as $value) {
			foreach ($value as $key => $new_val) {
				//print_r(json_encode($new_val));
				
				echo '
					<tr>
						<td>'.$new_val["AttendanceDate"].'</td>
						<td>'.$new_val["AttendanceTime"].'</td>
						<td>'.$new_val["UTC"].'</td>
						<td>'.$new_val["TimeIn"].'</td>
						<td>'.$new_val["LunchOut"].'</td>
						<td>'.$new_val["LunchIn"].'</td>
						<td>'.$new_val["TimeOut"].'</td>
						<td>'.$new_val["OBOut"].'</td>
						<td>'.$new_val["OBIn"].'</td>
						<td>'.$new_val["Day"].'</td>
						<td>'.$new_val["Week"].'</td>
						<td>'.$new_val["KEntry"].'</td>
						<td>'.$new_val["Holiday"].'</td>
						<td>'.$new_val["Leave"].'</td>
						<td>'.$new_val["CTO"].'</td>
						<td>'.$new_val["OffSus"].'</td>
						<td>'.$new_val["HasOT"].'</td>
					</tr>
				';
				
			}
			
		}
		echo '</table>';

	}
	//mysqli_query($conn,"DROP TEMPORARY TABLE tt_Attendance");
?>