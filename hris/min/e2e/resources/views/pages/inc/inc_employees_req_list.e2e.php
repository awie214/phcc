<?php
	$curr_date = date("Y-m-d",time());
	$notif_date =  date('Y-m-d', strtotime($curr_date. ' - 5 days'));
	$count = 0;
	$Leave = SelectEach("employeesleave","WHERE EmployeesRefId = $EmpRefId AND FiledDate >= '$notif_date'");
	if ($Leave) {
		while ($leave_row = mysqli_fetch_assoc($Leave)) {
			$leave_name = getRecord("leaves",$leave_row["LeavesRefId"],"Name");
			$Status = $leave_row["Status"];
			switch ($Status) {
             	case 'Approved':
                	$Status = "Approved";
                	break;
             	case 'Cancelled':
                	$Status = "Cancelled";
                	break;
             	case 'Rejected':
                	$Status = "Disapproved";
                	break;
             	case 'Request For Cancellation':
	                $Status = "Request For Cancellation";
                	break;
             	default:
	                $Status = "For Approval";
                	break;
          	}
			$Leaves_RefId = $leave_row["RefId"];
			$file = "rpt_Leave_Availment";
			if ($leave_row["isForceLeave"] == 1) {
				echo '
					<li class="request" onclick="printAvail('.$Leaves_RefId.',\''.$file.'\');">
						Forced Leave ('.$Status.')
					</li>
				';	
			} else {
				echo '
					<li class="request" onclick="printAvail('.$Leaves_RefId.',\''.$file.'\');">
						'.$leave_name.' ('.$Status.')
					</li>
				';
			}
			
			$count++;
		}
	}
	$OB = SelectEach("employeesauthority","WHERE EmployeesRefId = $EmpRefId AND FiledDate >= '$notif_date'");
	if ($OB) {
		while ($OB_row = mysqli_fetch_assoc($OB)) {
			$OB_name = getRecord("absences",$OB_row["AbsencesRefId"],"Name");
			$Status = $OB_row["Status"];
			switch ($Status) {
             	case 'Approved':
                	$Status = "Approved";
                	break;
             	case 'Cancelled':
                	$Status = "Cancelled";
                	break;
             	case 'Rejected':
                	$Status = "Disapproved";
                	break;
             	case 'Request For Cancellation':
	                $Status = "Request For Cancellation";
                	break;
             	default:
	                $Status = "For Approval";
                	break;
          	}
			$OB_RefId = $OB_row["RefId"];
			$file = "rpt_OB_2";
			echo '
				<li class="request" onclick="printAvail('.$OB_RefId.',\''.$file.'\');">
					'.$OB_name.' ('.$Status.')
				</li>
			';
			$count++;
		}
	}
	$OT = SelectEach("overtime_request","WHERE EmployeesRefId = $EmpRefId AND FiledDate >= '$notif_date'");
	if ($OT) {
		while ($OT_row = mysqli_fetch_assoc($OT)) {
			$Status = $OT_row["Status"];
			switch ($Status) {
             	case 'Approved':
                	$Status = "Approved";
                	break;
             	case 'Cancelled':
                	$Status = "Cancelled";
                	break;
             	case 'Rejected':
                	$Status = "Disapproved";
                	break;
             	case 'Request For Cancellation':
	                $Status = "Request For Cancellation";
                	break;
             	default:
	                $Status = "For Approval";
                	break;
          	}
			$OT_RefId = $OT_row["RefId"];
			$file = "rpt_Overtime_2";
			$WithPay = $OT_row["WithPay"];
	      	if (intval($WithPay) > 0) {
	         	$WithPay = "OT Pay";
	      	} else {
	         	$WithPay = "COC";
	      	}
			echo '
				<li class="request" onclick="printAvail('.$OT_RefId.',\''.$file.'\');">
					'.$WithPay.' ('.$Status.')
				</li>
			';
			$count++;
		}
	}
	$CTO = SelectEach("employeescto","WHERE EmployeesRefId = $EmpRefId AND FiledDate >= '$notif_date'");
	if ($CTO) {
		while ($CTO_row = mysqli_fetch_assoc($CTO)) {
			$Status = $CTO_row["Status"];
			switch ($Status) {
             	case 'Approved':
                	$Status = "Approved";
                	break;
             	case 'Cancelled':
                	$Status = "Cancelled";
                	break;
             	case 'Rejected':
                	$Status = "Disapproved";
                	break;
             	case 'Request For Cancellation':
	                $Status = "Request For Cancellation";
                	break;
             	default:
	                $Status = "For Approval";
                	break;
          	}
			$CTO_RefId = $CTO_row["RefId"];
			$file = "rpt_CTO_2";
			$Hours = $CTO_row["Hours"];
			echo '
				<li class="request" onclick="printAvail('.$CTO_RefId.',\''.$file.'\');">
					CTO ('.$Hours.' Hours) ('.$Status.')
				</li>
			';
			$count++;
		}
	}

	$AttendanceReq = SelectEach("attendance_request","WHERE EmployeesRefId = $EmpRefId AND FiledDate >= '$notif_date'");
	if ($AttendanceReq) {
		while ($AttendanceReq_row = mysqli_fetch_assoc($AttendanceReq)) {
			$Status = $AttendanceReq_row["Status"];
			switch ($Status) {
             	case 'Approved':
                	$Status = "Approved";
                	break;
             	case 'Cancelled':
                	$Status = "Cancelled";
                	break;
             	case 'Rejected':
                	$Status = "Disapproved";
                	break;
             	case 'Request For Cancellation':
	                $Status = "Request For Cancellation";
                	break;
             	default:
	                $Status = "For Approval";
                	break;
          	}
			$AttendanceReq_RefId = $AttendanceReq_row["RefId"];
			$file = "rpt_Attendance_Request";
			$date = date("F d, Y",strtotime($AttendanceReq_row["AppliedDateFor"]));
			echo '
				<li class="request" onclick="printAvail('.$AttendanceReq_RefId.',\''.$file.'\');">
					Attendance Request For '.$date.' ('.$Status.')
				</li>
			';
			$count++;
		}
	}

	if ($count > 0) {
		echo '
			<script>
				$(document).ready(function () {
					$("#myrequestView").show();
				});
			</script>
		';
	}
?> 