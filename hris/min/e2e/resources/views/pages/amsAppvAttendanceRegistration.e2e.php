<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_approval") ?>"></script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php
               doTitleBar("For Aprroval > Attendance Registration");
               spacer(5);

            ?>
            <div class="row">
               <div class="col-sm-12">
                  <?php
                     $EmpRefId = getvalue("txtRefId");
                     $attr = ["empRefId"=>getvalue("txtRefId"),
                              "empLName"=>getvalue("txtLName"),
                              "empFName"=>getvalue("txtFName"),
                              "empMName"=>getvalue("txtMidName")];
                     $EmpRefId = EmployeesSearch($attr);
                     bar();
                     $sql = "SELECT *,attendance_request.RefId as asRefId FROM attendance_request
                     INNER JOIN employees 
                     ON attendance_request.CompanyRefId = employees.CompanyRefId
                     AND attendance_request.BranchRefId = employees.BranchRefId
                     AND attendance_request.EmployeesRefId = employees.RefId";

                     if (getvalue("txtLName") != "") {
                        $sql .= " AND employees.LastName LIKE '".getvalue("txtLName")."%'";
                     }
                     if (getvalue("txtFName") != "") {
                        $sql .= " AND employees.FirstName LIKE '".getvalue("txtFName")."%'";
                     }
                     if (getvalue("txtMidName") != "") {
                        $sql .= " AND employees.MiddleName LIKE '".getvalue("txtMidName")."%'";
                     }
                     if (getvalue("txtRefId") != "") {
                        $sql .= " AND attendance_request.EmployeesRefId = '".getvalue("txtRefId")."'";
                     } 
                     $sql .= " AND attendance_request.Status IS NULL ORDER BY FiledDate LIMIT 100";
                     //echo $sql;  
                     
                     $rs = mysqli_query($conn,$sql) or die(mysqli_error($conn));

                  ?>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-1"></div>
               <div class="col-sm-10 padd5">
                  <?php
                     if ($rs) {
                        while ($row = mysqli_fetch_array($rs)) {
                           $refid = $row["asRefId"];
                  ?>
                           <div class="mypanel pull-left padd5" style="margin:5px;width:40%;" id="card_<?php echo $refid; ?>">
                              <div class="panel-top">REF ID:&nbsp;<?php echo $refid; ?></div>
                              <div class="panel-mid">
                                 <div class="row txt-right" style="margin-right:10px;">
                                    <label>DATE FILE:</label><span style="margin-left:15px;"><?php echo $row["FiledDate"];?></span>
                                 </div>
                                 <?php
                                    $rsEmp = FFirstRefId("employees",$row["EmployeesRefId"],"*");
                                    $empinformation = FindFirst('empinformation',"WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = ".$rsEmp["RefId"],"*");
                                    $info = array_merge($rsEmp,$empinformation);
                                    echo '
                                    <div class="row margin-top padd5">
                                       <div class="row margin-top">
                                          <div class="col-sm-4 txt-center">
                                             <div class="border" style="height:1.5in;width:1.3in;">
                                                <img src="'.img($rsEmp['CompanyRefId']."/EmployeesPhoto/".$rsEmp['PicFilename']).'" style="width:100%;height:100%;">
                                             </div>
                                          </div>
                                          <div class="col-sm-8 txt-center padd5">';
                                             $templ->btn_apprvReject(2,$refid);
                                          echo    
                                          '</div>
                                       </div>
                                       <div class="row margin-top">   
                                          <div class="col-sm-12">';
                                             $templ->doEmployeeInfo($info);
                                    echo       
                                          '</div>   
                                       </div>   
                                    </div>';
                                    bar();  
                                 ?>
                                 <div class="row">
                                    <div class="col-sm-6">
                                       <label>APPLIED DATE FOR:</label>
                                          <br>
                                       <span>&nbsp;<?php echo date("F d, Y",strtotime($row["AppliedDateFor"]));?></span>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-bottom"></div>
                           </div>
                        <?php
                        }
                     } else {
                        alert("Information","No For Approval");
                     }
                  ?>
               </div>
               <div class="col-sm-1"></div>
            </div>
            <?php
               footer();
               $table = "attendance_request";
               modalReject();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>