<?php
	include_once 'constant.e2e.php';
	include_once pathClass.'0620functions.e2e.php';
	include_once 'conn.e2e.php';
?>
<!DOCTYPE html>
<html>
<head>
	<?php
		include 'pageHEAD.e2e.php';
	?>
	<script type="text/javascript">
		function close_after() {
			$("#msg").append("Closing in ");
			var count = 4;
			setInterval(function(){
				count--;
			 	$("#msg").append(count + "... ");
			}, 900);
			setTimeout(function(){
				self.location = "../../../../../../../../../HRIS_PMS/public";	
			}, 2000);
		}
	</script>
</head>
<body>
	<h3>Scanning payroll employees...</h3>
	<br>
	<?php
		$count = 0;
		$pis_employee = SelectEach("employees","WHERE Inactive = 0 OR Inactive IS NULL");
		if ($pis_employee) {
			while ($pis_row = mysqli_fetch_assoc($pis_employee)) {
				$empid 		= $pis_row["AgencyId"];
				$cid 		= $pis_row["CompanyRefId"];
				$bid 		= $pis_row["BranchRefId"];
				$lastname 	= $pis_row["LastName"];
				$firstname 	= $pis_row["FirstName"];
				$middlename = $pis_row["MiddleName"];
				$check = pms_FindFirst("pms_employees","WHERE employee_number = '$empid'","*");
				if (!$check) {
					$count++;
					$Flds 				= "company_id, branch_id, employee_number, lastname, firstname, middlename, active";
					$Vals 				= "'$cid', '$bid', '$empid', '$lastname', '$firstname', '$middlename', '1'";
					$sql_save_employee 	= "INSERT INTO pms_employees ($Flds) VALUES ($Vals)";
					$result 			= mysqli_query($conn,$sql_save_employee);
					if ($result) {
						echo "$count. [ ".$empid." ] ".$pis_row["FirstName"]." ".$pis_row["LastName"]." added in payroll.<br>";	
						$pms_emp_info 		= sync_pms($pis_row["RefId"],"2");
						$pms_salary_info 	= sync_pms($pis_row["RefId"],"3");
						if ($pms_emp_info != "") echo "Error in updating Payroll Employee Info.<br>";
						if ($pms_salary_info != "") echo "Error in updating Payroll Salary Info.<br>";
					} else {
						echo "$count. [ ".$empid." ] ".$pis_row["FirstName"]." ".$pis_row["LastName"]." error in adding in payroll.<br>";	
					}
				}
			}
		}

		$inactive_employee = SelectEach("employees","WHERE Inactive = 1");
		if ($inactive_employee) {
			while ($inactive_row = mysqli_fetch_assoc($inactive_employee)) {
				$id 			= $inactive_row["RefId"];
				$lastname 		= $inactive_row["LastName"];
				$firstname 		= $inactive_row["FirstName"];
				$empid 			= $inactive_row["AgencyId"];
				$check_inactive = pms_FindFirst("pms_employees","WHERE employee_number = '$empid' AND active = 1","*");
				if ($check_inactive) {
					$update_inactive = "UPDATE pms_employees SET active = '0' WHERE id = '".$check_inactive["id"]."'";
					$result_inactive = mysqli_query($conn,$update_inactive);
					if ($result_inactive) {
						echo "$firstname $lastname is now inactive.<br>";
					} else {
						echo "Error in Updating Record for [ ".$empid." ].<br>";
					}
				}

			}
		}
		// $pms_employee = SelectEach("pms_employees","WHERE active = 1");
		// if ($pms_employee) {
		// 	while ($pms_row = mysqli_fetch_assoc($pms_employee)) {
		// 		if (strlen($employee_number) > 10) {
		// 			$special_char = substr($employee_number, 0, 1);
		// 			$employee_number = str_replace($special_char, '', $employee_number);
		// 			echo $id." -> [ ".trim($employee_number)." ] [ ".strlen(str_replace($special_char, '', $employee_number))." ]].<br>";	
		// 			$update = "UPDATE pms_employees SET employee_number = '$employee_number' WHERE id = '$id'";
		// 			mysqli_query($conn,$update);
		// 		}
		// 	}
		// }
	?>
	<span id="msg"></span>
</body>
</html>