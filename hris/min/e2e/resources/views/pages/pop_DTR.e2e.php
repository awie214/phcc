<!DOCTYPE HTML>
<html>
   <head>
      <?php 
         $file = "Daily Time Record";
         include_once $files["inc"]["pageHEAD"]; 
      ?>
   </head>
   <body>
      <div class="container-fluid">
         <button onclick="print();">PRINT</button>
         <div class="row text-center">
            <label>Daily Time Record for November 1, 2017 To November 30, 2017</label>
         </div>
         <div class="row">
            <div class="col-xs-12"><label>Employee Name:</label></div>
         </div>
         <table border="1" width="100%">
            <tr align="center">
               <td>OT</td>
               <td>In 1</td>
               <td>Out 1</td>
               <td>In 2</td>
               <td>Out 2</td>
               <td>OT In</td>
               <td>OT Out</td>
               <td>Reg Hrs.</td>
               <td>OT Hrs.</td>
               <td>OB Per.</td>
            </tr>
         <?php for ($i = 1;$i<31;$i++) {?>
            <tr>
               <td>Nov. <?php echo $i;?>, 2017</td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
               <td></td>
            </tr>
         <?php }?>
         </table>
         <br>
         <div class="row" style="border-top:3px solid black;">
            <div class="col-xs-6">
               <label>Daily Time Record Statistics:</label>
            </div>
            <div class="col-xs-6 text-right">
               <label>*OB Official ^OB Personal</label>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3"></div>
            <div class="col-xs-9">
               <div class="row">
                  <div class="col-xs-2 text-center">Week No. 1</div>
                  <div class="col-xs-2 text-center">Week No. 2</div>
                  <div class="col-xs-2 text-center">Week No. 3</div>
                  <div class="col-xs-2 text-center">Week No. 4</div>
                  <div class="col-xs-2 text-center">Week No. 5</div>
                  <div class="col-xs-2 text-center">Totals</div>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <div class="row">
                  <div class="col-xs-12">
                     Regular Hours
                  </div>
               </div>
            </div>
            <div class="col-xs-9">
               <div class="row">
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <div class="row">
                  <div class="col-xs-12">
                     Total Undertime Hours
                  </div>
               </div>
            </div>
            <div class="col-xs-9">
               <div class="row">
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <div class="row">
                  <div class="col-xs-12">
                     Hours Tardy
                  </div>
               </div>
            </div>
            <div class="col-xs-9">
               <div class="row">
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <div class="row">
                  <div class="col-xs-12">
                     Hours Undertime
                  </div>
               </div>
            </div>
            <div class="col-xs-9">
               <div class="row">
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <div class="row">
                  <div class="col-xs-12">
                     Times Tardy
                  </div>
               </div>
            </div>
            <div class="col-xs-9">
               <div class="row">
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <div class="row">
                  <div class="col-xs-12">
                     Times Undertime
                  </div>
               </div>
            </div>
            <div class="col-xs-9">
               <div class="row">
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <div class="row">
                  <div class="col-xs-12">
                     Times Absent
                  </div>
               </div>
            </div>
            <div class="col-xs-9">
               <div class="row">
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
                  <div class="col-xs-2 text-right">0</div>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-3">
               <div class="row">
                  <div class="col-xs-12">
                     Overtime Hours
                  </div>
               </div>
            </div>
            <div class="col-xs-9">
               <div class="row">
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
                  <div class="col-xs-2 ">0:00</div>
               </div>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-12">
               <p style="text-indent:20px;">I certify on my honor that the above is a true and correct report of the hours of work performed, record of which was made daily at the time of arrival at and departure from office.</p>
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-4">
               ___________________________________
               <br>
               <center>Employee's Signature</center>
               <br>
               Prepared By:_______________________
            </div>
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
               Attested by:
               ___________________________________
               <br>
               <center>Supervisor</center>
            </div>
         </div>
      </div>
   </body>
</html>