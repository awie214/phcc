<!DOCTYPE html>
<html>
   <head>
      <script type="text/javascript" src="<?php echo $path; ?>jquery/jquery.js"></script>
      <script type="text/javascript" src="<?php echo $path; ?>js/js7_login.js"></script>
      <script type="text/javascript" src="<?php echo $path; ?>js/jsSHAver2/src/sha.js"></script>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_NewEmp"); ?>"></script>
      <script type="text/javascript">
         function getEmpID() {
            $.get("trn.e2e.php",
            {
               fn:"getEmpID"
            },
            function(data,status){
               if (status == 'success') {
                  eval(data);
               } else {
                  alert(data);
               }
            });
         }
      </script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("CREATING NEW EMPLOYEES"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-6" id="div_CONTENT">
                     <div class="mypanel" id="panel_NewEmployees">
                        <div class="panel-top"></div>
                        <div class="panel-mid">
                           <input type="hidden" name="fn" value="SaveNewEmp">
                           <input type="hidden" name="hPass" value="">
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>LAST NAME:</label>
                              </div>
                              <div class="col-xs-4">
                                 <input type="text" name="LastName" id="LastName" class="form-input saveFields-- uCase-- mandatory">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>FIRST NAME:</label>
                              </div>
                              <div class="col-xs-4">
                                 <input type="text" name="FirstName" id="FirstName" class="form-input saveFields-- uCase-- mandatory">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>MIDDLE NAME:</label>
                              </div>
                              <div class="col-xs-4">
                                 <input type="text" name="MiddleName" id="MiddleName" class="form-input saveFields-- uCase--">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>EXTENSION NAME:</label>
                              </div>
                              <div class="col-xs-4">
                                 <input type="text" name="ExtName" id="ExtName" class="form-input saveFields-- uCase--">
                              </div>
                           </div>
                           <?php bar(); ?>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>BIRTH DATE:</label>
                              </div>
                              <div class="col-xs-4">
                                 <input type="text" name="BirthDate" id="BirthDate" class="form-input saveFields-- uCase-- mandatory date--">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>AGENCY ID NO:</label>
                              </div>
                              <div class="col-xs-4">
                                 <input type="text" name="AgencyId" id="char_NewEmployeesId" class="form-input saveFields-- uCase-- mandatory">
                              </div>
                           </div>
                           <!--
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>BIOMETRICS ID NO:</label>
                              </div>
                              <div class="col-xs-4">
                                 <input type="text" name="BiometricsID" id="BiometricsID" class="form-input saveFields-- uCase-- mandatory">
                              </div>
                           </div>
                           -->
                           <?php bar(); ?>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>PLANTILLA ITEM NO.:</label>
                              </div>
                              <div class="col-xs-4">
                                 <?php
                                    createSelect("PositionItem",
                                                 "PositionItemRefId",
                                                 "",100,"Name","Select Position Item","");
                                 ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>POSITION:</label>
                              </div>
                              <div class="col-xs-4">
                                 <?php
                                    createSelect2("Position",
                                                 "PositionRefId",
                                                 "",100,"Name","Select Position","","");
                                 ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>OFFICE:</label>
                              </div>
                              <div class="col-xs-4">
                                 <?php
                                    createSelect2("Office",
                                                 "OfficeRefId",
                                                 "",100,"Name","Select Office","","");
                                 ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>DIVISION:</label>
                              </div>
                              <div class="col-xs-4">
                                 <?php
                                    createSelect2("Division",
                                                 "DivisionRefId",
                                                 "",100,"Name","Select Division","","");
                                 ?>
                              </div>
                           </div>
                           <?php
                              if (getvalue("hCompanyID") == "2") {
                           ?>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>INTERIM POSITION:</label>
                              </div>
                              <div class="col-xs-4">
                                 <?php
                                    createSelect2("InterimPosition",
                                                 "InterimPositionRefId",
                                                 "",100,"Name","Select Interim Position","","");
                                 ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>INTERIM OFFICE:</label>
                              </div>
                              <div class="col-xs-4">
                                 <?php
                                    createSelect2("InterimOffice",
                                                 "InterimOfficeRefId",
                                                 "",100,"Name","Select Interim Office","","");
                                 ?>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>INTERIM DIVISION:</label>
                              </div>
                              <div class="col-xs-4">
                                 <?php
                                    createSelect2("InterimDivision",
                                                 "InterimDivisionRefId",
                                                 "",100,"Name","Select Interim Division","","");
                                 ?>
                              </div>
                           </div>
                           <?php
                              }
                           ?>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>EMP. STATUS:</label>
                              </div>
                              <div class="col-xs-4">
                                 <?php
                                    createSelect("EmpStatus",
                                                 "EmpStatusRefId",
                                                 "",100,"Name","Select Emp. Status","");
                                 ?>
                              </div>
                           </div>
                           <?php bar(); ?>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>USERNAME:</label>
                              </div>
                              <div class="col-xs-4">
                                 <input type="text" name="UserName" id="UserName" class="form-input saveFields-- mandatory">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-4 label">
                                 <label>PASSWORD:</label>
                              </div>
                              <div class="col-xs-4">
                                 <input type="text" name="pw" id="pw" class="form-input saveFields-- mandatory" readonly value="123456">
                              </div>
                           </div>
                           <input type="hidden" name="isFilipino" id="isFilipino" value="1">
                        </div>
                        <div class="panel-bottom">
                           <div class="row" style="padding: 5px;">
                              <div class="col-xs-12">
                                 <!-- <button type="button" class="btn-cls4-sea" id="btnNew201" name="btnNew201" value="submit new 201">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    &nbsp;Create Now
                                 </button> -->
                                 <button type="button" class="btn-cls4-sea" id="dummybtn">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    &nbsp;Create Now
                                 </button>
                                 <button type="button" class="btn-cls4-red" id="btnNew201Cancel" name="btnNew201Cancel" onclick="gotoscrn('scrn201File','');">
                                    <i class="fa fa-undo" aria-hidden="true"></i>
                                    &nbsp;Cancel
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



