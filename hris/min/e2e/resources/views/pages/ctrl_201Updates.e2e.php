<?php
session_start();
require_once "conn.e2e.php";
require_once "constant.e2e.php";
require_once pathClass.'0620functions.e2e.php';
require_once pathClass.'SysFunctions.e2e.php';
$sys = new SysFunctions();

$task = getvalue("task");
$p_dbtable  = getvalue("dbtable");
$p_empRefid = getvalue("empRefId");
$CompanyId = getvalue("hCompanyID");
$BranchId = getvalue("hBranchID");
$user = getvalue("user");
$clause = "WHERE BranchRefId = $BranchId AND CompanyRefId = $CompanyId ";
if ($p_dbtable == "employeeseduc") {
   $opt = "";
   $college_opt = "";
   $elemRS = FindFirst("employeeseduc",$clause." AND EmployeesRefId = '$p_empRefid' AND LevelType = 1","LevelType");
   if (!$elemRS){
      $opt .= '<option value="1">ELEMENTARY</option>';
   }
   $secondaryRS = FindFirst("employeeseduc",$clause." AND EmployeesRefId = '$p_empRefid' AND LevelType = 2","LevelType");
   if (!$secondaryRS){
      $opt .= '<option value="2">SECONDARY</option>';
   }
   $collegeRS = FindFirst("employeeseduc",$clause." AND EmployeesRefId = '$p_empRefid' AND LevelType = 4","LevelType");
   if (!$collegeRS){
      $college_opt .= '<option value="4">COLLEGE</option>';
   }
   $select_educLvl = '
                  <select name="sint_LevelType" class="form-input">
                     '.$opt.'
                     <option value="3">VOCATIONAL-TRADE COURSES</option>
                     '.$college_opt.'
                     <option value="5">GRADUATE STUDIES</option>
                  </select>

                  ';
}

$fields = file_get_contents(json.'fieldName.json');
$fldLabel = json_decode($fields, true); 
$alphafields = file_get_contents(json.'alphaFields.json');
$alpha = json_decode($alphafields, true);

   if ($task == "LoadTabDtl") {
      if ($p_dbtable!="") {
         echo '<script language="JavaScript" src="'.jsCtrl("ajax_201Updates").'"></script>';
         echo '<script language="JavaScript" src="'.path."js/js6_jquery_utilities.js".'"></script>';
?>
         <div class="mypanel">
            <div class="panel-top">
               <div class="row txt-center">
                  <div class="col-xs-3">
                     <!--<input id="chkAll" type="checkbox" title="Check All">-->
                     Field Name
                  </div>
                  <div class="col-xs-3">
                     Current Value
                  </div>
                  <div class="col-xs-3">
                     New Value
                  </div>
                  <div class="col-xs-3">
                     Actions
                  </div>
               </div>
            </div>
            <div class="panel-mid-litebg" style="overflow:auto;max-height:400px;">
            <?php
               $singleRec = false;
               if ($p_dbtable == "employees") {
                  $rs = SelectEach($p_dbtable,"WHERE RefId = $p_empRefid");
                  $singleRec = true;
               } 
               else if ($p_dbtable == "employeesfamily" ||
                        $p_dbtable == "employeespdsq") {
                  $rs = SelectEach($p_dbtable,$clause." AND EmployeesRefId = $p_empRefid");
                  $singleRec = true;
               }
               else {
                  $rs = SelectEach($p_dbtable,$clause." AND EmployeesRefId = $p_empRefid ORDER BY RefId");
                  if ($rs) {
                     $numrow = mysqli_num_rows($rs);
                  }
                  $tbl = "employees";
                  if (
                        $p_dbtable == $tbl."child" ||
                        $p_dbtable == $tbl."educ" ||
                        $p_dbtable == $tbl."elegibility" ||
                        $p_dbtable == $tbl."otherinfo" ||
                        $p_dbtable == $tbl."reference" ||
                        $p_dbtable == $tbl."training" ||
                        $p_dbtable == $tbl."voluntary" || 
                        $p_dbtable == $tbl."workexperience"
                     ) {
                     if ($p_dbtable == "employeesreference" && $numrow == 3) {
                        createButton("Add New Record",
                                  "btnAddNewRecord",
                                  "btn-cls2-def trnbtn",
                                  "fa-plus-circle",
                                  "disabled");   
                     } else {
                        createButton("Add New Record",
                                  "btnAddNewRecord",
                                  "btn-cls2-def trnbtn",
                                  "fa-plus-circle",
                                  "");
                     }
                     
                  } 
               }
               if ($rs) {
                  $lgShow = false;
                  $recCount = 0;
                  $fldCount = 0;
                  while($table_row = mysqli_fetch_assoc($rs)) {
                     $sql = "SHOW FULL COLUMNS FROM `$p_dbtable`";
                     $Colresult = mysqli_query($conn,$sql) or die(mysqli_error($conn));
                     $recCount = $recCount + 1;

                     spacer(5);
                     echo
                     '<div class="mypanel">';
                     if ($singleRec) {
                        echo
                        '<div class="panel-top bgSilver">Details</div>';
                     } else {
                        echo 
                        '<div class="panel-top bgSilver">'.$recCount.' of '.$numrow.'</div>';
                     }
                     echo
                     '<div class="panel-mid">';
                     //$row['Field'] == "AgencyId" ||
                     while($row = mysqli_fetch_array($Colresult))
                     {
                        if ($row['Field'] == "RefId" ||
                            $row['Field'] == "CompanyRefId" ||
                            $row['Field'] == "BranchRefId" ||
                            $row['Field'] == "EmployeesRefId" ||
                            $row['Field'] == "LastUpdateBy" ||
                            $row['Field'] == "LastUpdateDate" ||
                            $row['Field'] == "LastUpdateTime" ||
                            $row['Field'] == "Data" ||
                            $row['Field'] == "Remarks" ||
                            $row['Field'] == "Inactive" ||
                            $row['Field'] == "PicFilename" ||
                            $row['Field'] == "Terminal" ||
                            $row['Field'] == "Session" ||
                            $row['Field'] == "isLogin" ||
                            $row['Field'] == "UserName" ||
                            $row['Field'] == "MotherExtName" ||
                            $row['Field'] == "pw") {
                           $lgShow = false;
                        } else {
                           $lgShow = true;
                        }
                        if ($p_dbtable == "employeesfamily") {
                           $where = "WHERE CompanyRefId = ".$table_row["CompanyRefId"]." AND BranchRefId = ".$table_row["BranchRefId"]." AND RefId = ".$table_row["EmployeesRefId"];
                           $cvlStatus = FindFirst("employees",$where,"CivilStatus");
                           if ($cvlStatus == "Si") {
                              if ($row['Field'] == "SpouseLastName" ||
                                 $row['Field'] == "SpouseFirstName" ||
                                 $row['Field'] == "SpouseMiddleName" ||
                                 $row['Field'] == "SpouseExtName" ||
                                 $row['Field'] == "SpouseMobileNo" ||
                                 $row['Field'] == "OccupationsRefId" ||
                                 $row['Field'] == "EmployersName" ||
                                 $row['Field'] == "SpouseBirthDate" ||
                                 $row['Field'] == "BusinessAddress") {
                                 $lgShow = false;
                              }
                           }
                        }
                        if ($p_dbtable == "employeesworkexperience") {
                           if ($table_row["CompanyRefId"] == 2) {
                              if ($row['Field'] == "JobGradeRefId") {
                                 $lgShow = true;
                              } else {
                                 $lgShow = false;
                              }
                           }
                           if ($row['Field'] == "YearRate" ||
                               $row['Field'] == "LWOP" ||
                               $row['Field'] == "Reason" ||
                               $row['Field'] == "ExtDate" ||
                               $row['Field'] == "SeparatedDate" ||
                               $row['Field'] == "PayGroup" ||
                               $row['Field'] == "PayrateRefId"
                              ) {
                              $lgShow = false;
                           }
                        }

                        $objName = "";
                        $isFKey = 0;
                        $isFKey = stripos($row['Field'],"RefId");
                        if ($isFKey > 0) $isFKey = true;

                        if ($lgShow) {
                           $CurrentValue = $table_row[$row['Field']];
                           $disabled = "";
                           if (stripos($CurrentValue,"This Record is Added From 201 Updates")) {
                              $disabled = "disabled";
                           }

                           bar();
                           $fldCount++;
                           $class = "";

                           if (stripos($row['Type'],"char")) {
                              if (isset($alpha[$row['Field']])) {
                                 $class .= " alpha--";
                              }
                           }
                           else if ($row['Type'] == "date") {
                              $class .= "date--";
                           } else if (
                                 stripos($row['Type'],"nt(")  ||
                                 stripos($row['Type'],"ecimal")
                              ) {
                              $class .= " number--";
                           }

                           if ($fldCount == 1) {
                              $class .= " focus--";
                           }
            ?>
                        <input type="hidden" id="RefId_<?php echo $fldCount; ?>"
                                     value="<?php echo $table_row["RefId"]; ?>">
                        <div class="row margin-top">
                           <div class="col-xs-3">
                              <div><?php echo $row['Comment'] ?></div>
                              <input id="<?php echo $fldCount; ?>" name="chk_<?php echo $fldCount; ?>"
                              class="fldname--" type="checkbox" <?php echo $disabled; ?>>
                              <input type="hidden" id="FldName_<?php echo $fldCount; ?>"
                                     value="<?php echo $row['Field'] ?>">
                              <span>
                                 <?php
                                    if ($row['Comment'] == "") {
                                       if (isset($fldLabel[$row['Field']])) {
                                          if ($fldLabel[$row['Field']] == "") {
                                             echo $row['Field'];
                                          } else {
                                             echo $fldLabel[$row['Field']];
                                          }
                                       } else {
                                          echo $row['Field'];
                                       }
                                    } else {
                                       echo "&nbsp;";
                                    }
                                 ?>
                              </span>
                           </div>

                           <!-- Curent VALUE -->
                           <div class="col-xs-3">
                              <b id="currValue_<?php echo $fldCount; ?>">
                              <?php
                                 if ($isFKey) {
                                    $tblused = explode("RefId",$row['Field'])[0];
                                    switch (explode("RefId",$row['Field'])[0]) {
                                       case "CountryCitizen":
                                       case "ResiCountry":
                                       case "PermanentCountry":
                                          $tblused = "Country";
                                       break;
                                       case "ResiAddCity":
                                       case "PermanentAddCity":
                                          $tblused = "City";
                                       break;
                                       case "ResiAddProvince":
                                       case "PermanentAddProvince":
                                          $tblused = "Province";
                                       break;
                                    }
                                    echo getRecord($tblused,$CurrentValue,"Name");
                                 }
                                 else {
                                    if ($row['Type'] == "int(1)" || $row['Type'] == "tinyint(1)" || $row['Type'] == "int(11)") {
                                       if ($row['Field'] != "LevelType")  {
                                          if ($CurrentValue == 1) {
                                             echo "YES";
                                          } else {
                                             echo "NO";
                                          }
                                       } else {
                                          switch ($CurrentValue) {
                                             case 1:
                                                echo "ELEMENTARY";
                                             break;
                                             case 2:
                                                echo "SECONDARY";
                                             break;
                                             case 3:
                                                echo "VOCATIONAL";
                                             break;
                                             case 4:
                                                echo "COLLEGE";
                                             break;
                                             case 5:
                                                echo "GRADUATE STUDIES";
                                             break;
                                             default:
                                                echo "$CurrentValue is NOT ASSIGNED";
                                             break;
                                          }
                                       }
                                    } else {
                                       echo $CurrentValue;
                                    }
                                 }
                              ?>
                              </b>
                              <input type="hidden" id="CurrentValue_<?php echo $fldCount; ?>"
                                     value="<?php echo $CurrentValue; ?>">
                           </div>

                           <!-- New VALUE -->
                           <div class="col-xs-3">
                              <?php
                                 if ($isFKey) {
                                    $tblused = explode("RefId",$row['Field'])[0];
                                    switch (explode("RefId",$row['Field'])[0]) {
                                       case "CountryCitizen":
                                       case "ResiCountry":
                                       case "PermanentCountry":
                                          $tblused = "Country";
                                       break;
                                       case "ResiAddCity":
                                       case "PermanentAddCity":
                                          $tblused = "City";
                                       break;
                                       case "ResiAddProvince":
                                       case "PermanentAddProvince":
                                          $tblused = "Province";
                                       break;
                                    }
                                    createSelect($tblused,
                                                 "NewValue_$fldCount",
                                                 $CurrentValue,100,"Name","","disabled");
                                 }
                                 else if (
                                          $row['Field'] == "Sex" ||
                                          $row['Field'] == "Gender") {
                                    createSelectGender("NewValue_$fldCount",$CurrentValue,"disabled");
                                 }
                                 else if ($row['Field'] == "CivilStatus") {
                                    createSelectCivilStatus("NewValue_$fldCount",$CurrentValue,"disabled");
                                 }
                                 // else if ($row['Field'] == "Present") {
                                 //    '<select class="form-input saveFields--" style="width:100px;" disabled
                                 //       id="NewValue_'.$fldCount.'"
                                 //       name="NewValue_'.$fldCount.'">
                                 //       <option value=0 selected>NO</option>
                                 //       <option value=1>YES</option>
                                 //    </select>';
                                 // }
                                 else if ($row['Field'] == "PayGroup") {
                                    echo
                                    '<select class="form-input saveFields--" id="PayGroup"
                                    name="NewValue_'.$fldCount.'" title="Payment Group">
                                       <option value="">Select Pay Group</option>';
                                       foreach ($_SESSION["PayGroup"] as $e) {
                                          echo '<option value="'.$e.'">'.$e.'</option>';
                                       }
                                    echo
                                    '</select>';
                                 }
                                 else if ($row['Type'] == "int(1)" || $row['Type'] == "tinyint(1)" || $row['Type'] == "int(11)") {
                                    if ($row['Field'] != "LevelType")  {
                                       echo
                                       '<select class="form-input saveFields--" style="width:100px;" disabled
                                          id="NewValue_'.$fldCount.'"
                                          name="NewValue_'.$fldCount.'">
                                          <option value=0 selected>NO</option>
                                          <option value=1>YES</option>
                                       </select>';
                                    } else {
                                       echo
                                       '<select class="form-input saveFields--" style="width:200px;" disabled
                                          id="NewValue_'.$fldCount.'"
                                          name="NewValue_'.$fldCount.'">
                                          <option value=0 selected></option>
                                          <option value=1>ELEMENTARY</option>
                                          <option value=2>SECONDARY</option>
                                          <option value=3>VOCATIONAL</option>
                                          <option value=4>COLLEGE</option>
                                          <option value=5>GRADUATE STUDIES</option>
                                       </select>';
                                    }

                                 } else {
                                    $initValue = "";
                                    if ($class == "number--"){
                                       $initValue = 0;
                                    } else {
                                       $initValue = "";
                                    }
                              ?>
                                    <input type="text" class="form-input saveFields-- <?php echo $class; ?>"
                                       id="NewValue_<?php echo $fldCount; ?>"
                                       name="NewValue_<?php echo $fldCount; ?>" 
                                       value="<?php echo $CurrentValue; ?>" disabled>
                              <?php
                                 }
                              ?>
                           </div>

                           <div class="col-xs-3">
                              <button type="button"
                                      name="btn_<?php echo $fldCount; ?>" id="btnSAVE_<?php echo $fldCount; ?>" disabled>
                                 <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                    &nbsp;&nbsp;SAVE
                              </button>
                           </div>
                        </div>
               <?php
                        }
                     }
                  echo
                  '</div>
                  </div>';
                  }
               } else {
                  echo "<div>No Record Found</div>";
               }
               spacer(10);
               ?>

            </div>
         </div>
<?php
      } else {
         echo "ERROR";
      }
   }
   /*========================================================================================================================*/
   else if ($task == "SaveAllNew201Updates") {
         $Field_and_Value = "";
         $FieldType = "";
         $Fields = "";
         $Values = "";
         $SaveSuccessfull = false;
         $dbtable = "updates201";
         $refid   = getvalue("refid");
         $js = "";
         if ($dbtable != "") {
            if ($refid == 0) {
               $t = time();
               $Field_and_Value  = getvalue("fv");
               $Field_and_Value .= "hidden|date_TrnDate|".date("Y-m-d",$t)."!";
               $Field_and_Value .= "hidden|date_TrnTime|".date("H:i:s",$t)."!";
               //hidden|sint_EmployeesRefId|116
               $Field_and_Value_Array = explode("!",$Field_and_Value);
               for ($i=0;$i<count($Field_and_Value_Array) - 1;$i++) {
                  $Items = $Field_and_Value_Array[$i];
                  $Items_Arr = explode("|",$Items);
                  $FieldType_Arr = explode("_",$Items_Arr[1]);
                  $Items_Arr[1];
                  $FieldType = $FieldType_Arr[0];

                  if ($FieldType_Arr[1] == "hNewReToken") {
                     $Fields .= "`Password`, ";
                  } else {
                     $Fields .= "`$FieldType_Arr[1]`, ";
                  }
                  $Values .= "'$Items_Arr[2]', ";

               }
               $SaveSuccessfull = f_SaveRecord("NEWSAVE",$dbtable,$Fields,$Values);
               if (is_numeric($SaveSuccessfull)) {
                  $Field_and_Value = getvalue("ov");
                  $FieldType = "";
                  $Fields = "";
                  $Values = "";
                  $dbtable = getvalue("table2");
                  $Field_and_Value_Array = explode("!",$Field_and_Value);
                  for ($i=0;$i<count($Field_and_Value_Array) - 1;$i++) {
                     $Items = $Field_and_Value_Array[$i];
                     $Items_Arr = explode("|",$Items);
                     $FieldType_Arr = explode("_",$Items_Arr[1]);
                     $FieldType = $FieldType_Arr[0];
                     if ($FieldType_Arr[1] == "hNewReToken") {
                        $Fields .= "`Password`, ";
                     } else {
                        $Fields .= "`$FieldType_Arr[1]`, ";
                     }
                     $Values .= "'$Items_Arr[2]', ";
                  }

                  $SaveSuccessfull = f_SaveRecord("NEWSAVE",$dbtable,$Fields,$Values);
                  if (is_numeric($SaveSuccessfull)) {
                     $js .= 'loadEmpDtl("'.$dbtable.'");';
                     $js .= 'afterNewSave('.$SaveSuccessfull.');';
                  }
                  else {
                     $js .= 'alert("Transaction Aborted!!!\nErr Saving in '.$dbtable.' : '.$SaveSuccessfull.'");';
                  }
               }
               else {
                  $js .= 'alert("Transaction Aborted!!!\nErr Saving in Updates : '.$SaveSuccessfull.'");';
               }
            }
         }
         else {
            $js .= 'alert("Oooops!!! No table assigned!!!");';
         }

         echo $js;
   }
   else if ($task == "AddNewEntry") {
      $type = "text";
      echo '<script language="JavaScript" src="'.jsCtrl("ajax_201Updates").'"></script>';
      echo '<script language="JavaScript" src="'.path."js/js6_jquery_utilities.js".'"></script>';
      $sql = "SHOW FULL COLUMNS FROM `$p_dbtable`";
      $Colresult = mysqli_query($conn,$sql) or die(mysqli_error($conn));
      $f = 0;
      spacer(5);
      echo '<div id="entryAddNewEntry">';
      while($row = mysqli_fetch_array($Colresult))
      {

         if ($row['Field'] == "RefId" ||
             $row['Field'] == "CompanyRefId" ||
             $row['Field'] == "BranchRefId" ||
             $row['Field'] == "EmployeesRefId" ||
             $row['Field'] == "LastUpdateBy" ||
             $row['Field'] == "LastUpdateDate" ||
             $row['Field'] == "LastUpdateTime" ||
             $row['Field'] == "Data" ||
             $row['Field'] == "Remarks" || 
             $row['Field'] == "PayrateRefId" || 
             $row['Field'] == "JobGradeRefId") {
            $lgShow = false;
         } else {
            $lgShow = true;
         }
         if ($p_dbtable == "employeesworkexperience") {
            if ($row['Field'] == "PayGroup" || 
                $row['Field'] == "ExtDate" ||
                $row['Field'] == "SeparatedDate" || 
                $row['Field'] == "YearRate" || 
                $row['Field'] == "LWOP" || 
                $row['Field'] == "Reason" || 
                $row['Field'] == "ApptStatusRefId") {
               $lgShow = false;
            } 
         }
         if ($p_dbtable == "employeesworkexperience" && $CompanyId == 2) {
            if ($row['Field'] == "JobGradeRefId") {
               $lgShow = true;
            } 
         }

         $isFKey = stripos($row['Field'],"RefId");
         if ($isFKey > 0){
            $isFKey = true;
         } else {
            $isFKey = false;
         }


         if ($lgShow)
         {
            $f++;
            $class = "";

            if (stripos($row['Type'],"char")) {
               if (isset($alpha[$row['Field']])) {
                  $class .= " alpha--";
               }
            }
            else if ($row['Type'] == "date") {
               //$class .= "date--";
               $class .= "";
               $type = "date";
            } else if (
                  stripos($row['Type'],"nt(")  ||
                  stripos($row['Type'],"ecimal")
               ) {
               $class .= " number--";
            }
            
            if ($f == 1) {
               $class .= " focus--";
            }

            $objName = "";
            $initValue = "";
            if ($row['Type'] == "date") {
               $objName = "date_".$row['Field'];
               $initValue = "";
            } else if (stripos($row['Type'],"nt(")) {
               $objName = "sint_".$row['Field'];
               $initValue = "0";
            } else if (stripos($row['Type'],"ecimal")) {
               $objName = "deci_".$row['Field'];
               $initValue = "0.00";
            } else if (stripos($row['Type'],"char")) {
               $objName = "char_".$row['Field'];
            } else if (stripos($row['Type'],"date")) {
               $objName = "date_".$row['Field'];
               $initValue = "";
            }

            if ($objName!="") {
?>
               <div class="row">
                  <div class="col-sm-1"></div>
                  <div class="col-sm-10 txt-left">
                     <?php
                        if ($p_dbtable == "employeespdsq") {
                           echo
                           '<div class="txt-left">
                              <span><b>'.$row['Comment'].'</b></span>
                           </div>';
                        }
                     ?>
                     <!-- Label -->
                     <div class='txt-left'><b>
                     <?php
                        if (isset($fldLabel[$row['Field']])) {
                           if ($fldLabel[$row['Field']] == "") {
                              echo $row['Field'];
                           } else {
                              echo $fldLabel[$row['Field']];
                           }
                        } else {
                           echo $row['Field'];
                        }
                     ?>:</b>
                     </div>
                     <?php
                        if ($isFKey) {
                           $tblused = explode("RefId",$row['Field'])[0];
                           switch (explode("RefId",$row['Field'])[0]) {
                              case "CountryCitizen":
                              case "ResiCountry":
                              case "PermanentCountry":
                                 $tblused = "Country";
                              break;
                              case "ResiAddCity":
                              case "PermanentAddCity":
                                 $tblused = "City";
                              break;
                              case "ResiAddProvince":
                              case "PermanentAddProvince":
                                 $tblused = "Province";
                              break;
                           }
                           createSelect($tblused,
                                        $objName,
                                        "",100,"Name","","");
                        }
                        else if ($row['Field'] == "Sex" || $row['Field'] == "Gender") {
                           createSelectGender("char_Gender","","");
                        } else if ($row['Field'] == "LevelType" && $p_dbtable == "employeeseduc") {
                           echo $select_educLvl;
                        }
                        else if ($row['Field'] == "PayGroup") {
                           echo
                           '<select class="form-input saveFields--" id="PayGroup" name="char_PayGroup" title="Payment Group">
                              <option value="">Select Pay Group</option>';
                              foreach ($_SESSION["PayGroup"] as $e) {
                                 echo '<option value="'.$e.'">'.$e.'</option>';
                              }
                           echo
                           '</select>';
                        }
                        else if ($row['Field'] == "isGovtService" || 
                                 $row['Field'] == "isServiceRecord" ||
                                 $row['Field'] == "Present") {
                           echo
                           '<select class="form-input saveFields--" style="width:100px;" name="'.$objName.'">
                              <option value=1>YES</option>
                              <option value=0 ';
                              if ($row['Field'] == "Present") echo "selected";
                           echo    
                              '>NO</option>
                           </select>';
                        }
                        else {
                     ?>
                           <input type="<?php echo $type; ?>"
                              class="form-input saveFields-- <?php echo $class; ?>"
                              name="<?php echo $objName; ?>"
                              value="<?php echo $initValue; ?>"
                           >
                     <?php
                        }
                     ?>
                  </div>
                  <div class="col-sm-1"></div>
               </div>
<?php
               $objName = "";
            }
         }
      }
      echo '</div>';
      spacer(5);
      createButton("Save Record Now",
                   "btnSaveRecord",
                   "btn-cls2-silver",
                   "fa-floppy-o",
                   "");
      spacer(20);
   }
?>

