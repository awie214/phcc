<?php
	if (!function_exists('HoursFormat')) {
		function HoursFormat($timeInMin) {
	        if ($timeInMin > 0) {
	           $hr = explode(".",$timeInMin / 60)[0];
	           if ($hr <= 9) {
	              $hr = "0".$hr;
	           }
	           $mod_min = ($timeInMin % 60);
	           if ($mod_min <= 9) {
	              $mod_min = "0".$mod_min;
	           }
	           return $hr.":".$mod_min;   
	        } else {
	           return "&nbsp;";
	        }
	    }
	}

	include 'conn.e2e.php';

   	$curr_date						= date("Y-m-d",time());
	$arr 							= array();
    
	$NewMonth 						= explode("-", $month_start)[1];
	$NewYear 						= explode("-", $month_start)[0];
	$num_of_days 					= cal_days_in_month(CAL_GREGORIAN,$NewMonth,$NewYear);
	$new_week 						= "";
	$week_count 					= 0;
	if (strtotime($curr_date) <= strtotime($month_end)) {
		$month_end = $curr_date;
	} else {
		$month_end = $month_end;
	}
	$num_of_days = date("d",strtotime($month_end));

	for($i=1;$i<=$num_of_days;$i++) {
		$y = $NewYear."-".$NewMonth."-".$i;
		$day = date("D",strtotime($y));	
		if ($i <= 9) {
			$i = "0".$i;
		}
		$y = $NewYear."-".$NewMonth."-".$i;
		$week = date("W",strtotime($y));
		if ($week != $new_week) {
			$week_count++;
		}
		$dtr = [
	   		"AttendanceDate" => "$y",
	   		"AttendanceTime" => "",
	   		"UTC" => "",
	   		"TimeIn" => "",
	   		"LunchOut" => "",
	   		"LunchIn" => "",
	   		"TimeOut" => "",
	   		"OBOut" => "",
	   		"OBIn" => "",
	   		"Day" => "$i",
	   		"Week" => "$week_count",
	   		"KEntry" => "",
	   		"Holiday" => "",
	   		"CTO"=>"",
	   		"Leave"=> "",
	   		"OffSus"=>"",
	   		"OffSusName"=>""
	   	];
	   	$arr["ARR"][$y] = $dtr;
		$new_week = $week;
	}
	$KEntryContent = file_get_contents(json."Settings_".$CompanyID.".json");
  	$KEntry_json   = json_decode($KEntryContent, true);
  	$OTMinTime = $KEntry_json["OTMinTime"];
  	$COCMinTime = $KEntry_json["COCMinTime"];

	$EmpInfo = f_Find("empinformation",$Default_qry." AND EmployeesRefId = ".$emprefid);
	if ($EmpInfo) {
		$fetch = mysqli_fetch_assoc($EmpInfo);
		$worksched 					= $fetch["WorkScheduleRefId"];
	}
	$userID = 0;
	$Emp_WorkSched 		= FindFirst("workschedule"," WHERE RefId = ".$worksched,"*");
	$where_empAtt 		= $Default_qry;
	$where_empAtt 		.= " AND EmployeesRefId = '".$emprefid."'";
	$where_empAtt       .= " AND AttendanceDate BETWEEN '".$month_start."' AND '".$month_end."'";
	$rs_empAtt 			= SelectEach("employeesattendance",$where_empAtt);
	if ($rs_empAtt) {
		while ($row = mysqli_fetch_assoc($rs_empAtt)) {
			$AttendanceDate 	= $row["AttendanceDate"];
			$AttendanceTime 	= $row["AttendanceTime"];
			$UTC 				= $row["CheckTime"];
			$KEntry         	= $row["KindOfEntry"];
			$fld 				= "";
			$val 				= "";
			switch ($KEntry) {
				case 1:
					$fld 		= "TimeIn";
					$val     	= get_today_minute($UTC);
					break;
				case 2:
					$fld 		= "LunchOut";
					$val     	= get_today_minute($UTC);
					break;
				case 3:
					$fld 		= "LunchIn";
					$val     	= get_today_minute($UTC);
					break;
				case 4:
					$fld 		= "TimeOut";
					$val     	= get_today_minute($UTC);
					break;
				case 7:
					$fld 		= "OBOut";
					$val     	= get_today_minute($UTC);
					break;
				case 8:
					$fld 		= "OBIn";
					$val     	= get_today_minute($UTC);
					break;
			}
			$arr["ARR"][$AttendanceDate][$fld] = $val;
			$arr["ARR"][$AttendanceDate]["UTC"] = $UTC;
		}
	}
	$biometricsID = FindFirst("employees","WHERE RefId = $emprefid","BiometricsID");

    switch ($CompanyID) {
		case '2':
			include 'inc/inc_biometrics_2.e2e.php';
			break;
		case '28':
			include 'inc/inc_biometrics_28.e2e.php';
			break;
		default:
			include 'inc/inc_bio_default.e2e.php';
			break;
	}


	$holiday = SelectEach("holiday","");
    if ($holiday) {
        while ($row = mysqli_fetch_assoc($holiday)) {
           	$StartDate = $row["StartDate"];
           	$Name = $row["Name"];
           	$EveryYr = $row["isApplyEveryYr"];
           	$Legal = $row["isLegal"];
           	$temp_arr = explode("-", $StartDate);
           	$temp_date = $NewYear."-".$temp_arr[1]."-".$temp_arr[2];
           	if ($EveryYr == 1) {
              	if(isset($arr["ARR"][$temp_date])) {
                 	$arr["ARR"][$temp_date]["Holiday"] = $Name;
              	}   
           	} else {
              	if(isset($arr["ARR"][$StartDate])) {
                 	$arr["ARR"][$StartDate]["Holiday"] = $Name;
              	}
           	}
        }
    }

    $where_leave 		= $Default_qry;
	$where_leave		.= " AND EmployeesRefId = ".$emprefid;
	$where_leave		.= " AND ApplicationDateFrom BETWEEN '".$month_start."' AND '".$month_end."' AND Status = 'Approved'";
	$rs_leave			= SelectEach("employeesleave",$where_leave);
	if ($rs_leave) {
		while ($row = mysqli_fetch_assoc($rs_leave)) {
			$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
			if ($diff == 0) {
				if(isset($arr["ARR"][$row["ApplicationDateFrom"]])) {
                 	$arr["ARR"][$row["ApplicationDateFrom"]]["Leave"] = $row["LeavesRefId"];
              	}
			} else {
				for ($i=0; $i <= $diff ; $i++) { 
					$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
					if(isset($arr["ARR"][$temp_date])) {
                     	$arr["ARR"][$temp_date]["Leave"] = $row["LeavesRefId"];
                  	}
				}
			}
			
		}
	}
	$where_cto 		= $Default_qry;
	$where_cto		.= " AND EmployeesRefId = ".$emprefid;
	$where_cto		.= " AND ApplicationDateFrom BETWEEN '".$month_start."' AND '".$month_end."' AND Status = 'Approved'";
	$rs_cto			= SelectEach("employeescto",$where_cto);
	if ($rs_cto) {
		while ($row = mysqli_fetch_assoc($rs_cto)) {
			$diff = dateDifference($row["ApplicationDateFrom"],$row["ApplicationDateTo"]);
			if ($diff == 0) {
				if(isset($arr["ARR"][$row["ApplicationDateFrom"]])) {
                 	$arr["ARR"][$row["ApplicationDateFrom"]]["CTO"] = $row["Hours"]*60;
              	}
			} else {
				for ($i=0; $i <= $diff ; $i++) { 
					$temp_date = date('Y-m-d', strtotime($row["ApplicationDateFrom"] . ' +'.$i.' day'));
					if(isset($arr["ARR"][$temp_date])) {
                     	$arr["ARR"][$temp_date]["CTO"] = $row["Hours"]*60;
                  	}
				}
			}
			
		}
	}

	$where_offsus 	= $Default_qry;
	$where_offsus	.= " AND EmployeesRefId = ".$emprefid;
	$where_offsus	.= " AND StartDate BETWEEN '".$month_start."' AND '".$month_end."'";
	$rs_offsus		= SelectEach("officesuspension",$where_offsus);
	if ($rs_offsus) {
		while ($row = mysqli_fetch_assoc($rs_offsus)) {
			$diff = dateDifference($row["StartDate"],$row["EndDate"]);
			if ($diff == 0) {
				if(isset($arr["ARR"][$row["StartDate"]])) {
                 	$arr["ARR"][$row["StartDate"]]["OffSus"] = $row["StartTime"];
              	}
			} else {
				for ($i=0; $i <= $diff ; $i++) { 
					$temp_date = date('Y-m-d', strtotime($row["StartDate"] . ' +'.$i.' day'));
					if(isset($arr["ARR"][$temp_date])) {
                     	$arr["ARR"][$temp_date]["OffSus"] = $row["StartTime"];
                     	$arr["ARR"][$temp_date]["OffSusName"] = $row["Name"];
                     	
                  	}
				}
			}
			
			
		}
	}
	/*==============================================================================================================*/
	//START OF COMPUTATION AND SHOOTING
	/*==============================================================================================================*/
	$day 		 			= "";
	
	$day_count_diff			= 0;
	$OT_Count				= 0;
	$Total_Days				= 0;
	$RG_Time_Per_Week		= Array(0,0,0,0,0,0);
	$Tardy_Time_Per_Week	= Array(0,0,0,0,0,0);
	$UT_Time_Per_Week		= Array(0,0,0,0,0,0);
	$Excess_Time_Per_Week 	= Array(0,0,0,0,0,0);
	$Absent_Time_Per_Week 	= Array(0,0,0,0,0,0);

	$Absent_Count_Per_Week 	= Array(0,0,0,0,0,0);
	$Tardy_Count_Per_Week	= Array(0,0,0,0,0,0);
	$UT_Count_Per_Week		= Array(0,0,0,0,0,0);
	$COC_Time_Per_Week		= Array(0,0,0,0,0,0);
	foreach ($arr as $value) {
		foreach ($value as $key => $row) {
			$day_name = date("D",strtotime($row['AttendanceDate']));
			if ($day_name != "") {
				switch ($day_name) {
					case 'Mon':
						$day_name = "Monday";
						break;
					case 'Tue':
						$day_name = "Tuesday";
						break;
					case 'Wed':
						$day_name = "Wednesday";
						break;
					case 'Thu':
						$day_name = "Thursday";
						break;
					case 'Fri':
						$day_name = "Friday";
						break;
					case 'Sat':
						$day_name = "Saturday";
						break;
					case 'Sun':
						$day_name = "Sunday";
						break;
				}
			}	
			
			
			$day = $row["Day"]; 
			$underTime 	 				= "";
			$late 						= "";
			$remarks     				= "";
			$day_in             	   	= $day_name."In";
			$day_out            	   	= $day_name."Out";
			$day_flexi          	   	= $day_name."FlexiTime";
			$day_LBOut					= $day_name."LBOut";
			$day_LBIn					= $day_name."LBIn";
			$day_RestDay				= $day_name."isRestDay";
			$day_isflexi          	   	= $day_name."isFlexi";

			$data_isflexi             	= $Emp_WorkSched[$day_isflexi];
			$data_flexi             	= $Emp_WorkSched[$day_flexi];
			$data_timein            	= $Emp_WorkSched[$day_in];
			$data_timeout 				= $Emp_WorkSched[$day_out];
			$data_LunchOut 				= $Emp_WorkSched[$day_LBOut];
			$data_LunchIn 				= $Emp_WorkSched[$day_LBIn];
			$data_RestDay      			= $Emp_WorkSched[$day_RestDay];
			$day_work_hours_count   	= ($data_timeout - $data_timein) - ($data_LunchIn - $data_LunchOut);
			$Week 						= $row["Week"];	
			$AutoLB						= $Emp_WorkSched["AutoLB"];
			

			$arr_TI						= $row["TimeIn"];
			$arr_OBO					= $row["OBOut"];
			$arr_TO						= $row["TimeOut"];
			$arr_OBI					= $row["OBIn"];
			$arr_Holiday				= $row["Holiday"];
			$arr_leave 					= $row["Leave"];
			$arr_COC					= $row["CTO"];
			$arr_OffSus					= $row["OffSus"];

			if  (
					(
						$row["Holiday"] != "" || 
						$row["TimeIn"] 	!= "" || 
						$row["Leave"] 	!= "" || 
						$row["CTO"] 	!= "" || 
						$row["OBOut"] 	!= ""
					) && $data_RestDay != 1
			    ) {
					$Total_Days++;	
				
			}

			if ($arr_TI == "" && $arr_leave != "") {
				$arr_TI = $data_timein;
				$arr_TO = $data_timeout;
			}
			if ($arr_TI == "" && $arr_OBO != "") {
				$arr_TI = $arr_OBO;
			}
			if ($arr_TO == "" && $arr_OBI != "") {
				$arr_TO = $arr_OBI;
			}

			if ($arr_Holiday != "" && $arr_TI == "") {
				$arr_TI = $data_timein;
				$arr_TO = $data_timeout;
			}
			
			if ($arr_COC != "") {
				if ($arr_COC >= $day_work_hours_count) {
					$arr_TI = $data_timein;
					$arr_TO = $data_timeout;
				} else {
					if ($arr_TI == "") {
						$arr_TI = $data_timein;
					}
					if ($arr_TO == "") {
						$arr_TO = $data_timein + $arr_COC;
					} else {
						$arr_TO = $arr_TO + $arr_COC;
					}
					/*echo "COC: ".$arr_COC."<br>"; 
					echo "Work Hours: ".$day_work_hours_count."<br>"; 
					echo "Time In : ".$arr_TI."<br>"; 
					echo "Time Out: ".$arr_TO."<br>"; */
				}	
			}

			if ($arr_OffSus != "") {
				if ($arr_TI == "") {
					$arr_TI = $arr_OffSus;
				}
				if ($arr_TO == "") {
					$arr_TO = $data_timeout;
				}
			}
			
			
			/*==============================================================================================================*/
			//MAXIMUM TIME FROM FOR EMPLOYEE TO ENTER WITHOUT LATE
			/*==============================================================================================================*/
			if ($Emp_WorkSched["ScheduleType"] == "Fl" || $Emp_WorkSched["ScheduleType"] == "Co") {
				$data_flexi = $data_flexi;
			} else if ($Emp_WorkSched["ScheduleType"] == "Fi") {
				$data_flexi = $data_timein;
			} else {
				$data_flexi = $data_flexi;
			}
			/*==============================================================================================================*/
			//GETTING THE LATE OF THE EMPLOYEE
			/*==============================================================================================================*/
			if ($data_RestDay != 1) {
					if ($arr_TI != "" || $arr_OBO != "") {
						if ($data_isflexi != 1) {
							if ($arr_TI >= $data_timein) {
								$late = "";
							} else {
								$late = ($arr_TI - $data_flexi);
								switch ($Week) {
									case 1:
										$Tardy_Time_Per_Week[1] = $Tardy_Time_Per_Week[1] + $late;
										$Tardy_Count_Per_Week[1]++;
										break;
									case 2:
										$Tardy_Time_Per_Week[2] = $Tardy_Time_Per_Week[2] + $late;
										$Tardy_Count_Per_Week[2]++;
										break;
									case 3:
										$Tardy_Time_Per_Week[3] = $Tardy_Time_Per_Week[3] + $late;
										$Tardy_Count_Per_Week[3]++;
										break;
									case 4:
										$Tardy_Time_Per_Week[4] = $Tardy_Time_Per_Week[4] + $late;
										$Tardy_Count_Per_Week[4]++;
										break;
									case 5:
										$Tardy_Time_Per_Week[5] = $Tardy_Time_Per_Week[5] + $late;
										$Tardy_Count_Per_Week[5]++;
										break;
								}		
							}
						} else {
							if ($arr_TI <= $data_flexi) {
								$late = "";
							} else {
								$late = ($arr_TI - $data_flexi);
								switch ($Week) {
									case 1:
										$Tardy_Time_Per_Week[1] = $Tardy_Time_Per_Week[1] + $late;
										$Tardy_Count_Per_Week[1]++;
										break;
									case 2:
										$Tardy_Time_Per_Week[2] = $Tardy_Time_Per_Week[2] + $late;
										$Tardy_Count_Per_Week[2]++;
										break;
									case 3:
										$Tardy_Time_Per_Week[3] = $Tardy_Time_Per_Week[3] + $late;
										$Tardy_Count_Per_Week[3]++;
										break;
									case 4:
										$Tardy_Time_Per_Week[4] = $Tardy_Time_Per_Week[4] + $late;
										$Tardy_Count_Per_Week[4]++;
										break;
									case 5:
										$Tardy_Time_Per_Week[5] = $Tardy_Time_Per_Week[5] + $late;
										$Tardy_Count_Per_Week[5]++;
										break;
								}		
							}
						}
						
					}	
				}
			
			/*==============================================================================================================*/
			//GETTING THE WORKING AND EXCESS HOURS OF THE EMPLOYEE
			/*==============================================================================================================*/
			
			if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
				if ($arr_TI <= $data_timein) {
					$arr_TI = $data_timein;
				}
				$consume_time = (($arr_TO - $arr_TI) - ($data_LunchIn - $data_LunchOut));
				$excess_time = $consume_time - $day_work_hours_count; 	
				if ($excess_time > 0) {
					if ($Week == 1) $Excess_Time_Per_Week[1] = $Excess_Time_Per_Week[1] + $excess_time;
					if ($Week == 2) $Excess_Time_Per_Week[2] = $Excess_Time_Per_Week[2] + $excess_time;
					if ($Week == 3) $Excess_Time_Per_Week[3] = $Excess_Time_Per_Week[3] + $excess_time;
					if ($Week == 4) $Excess_Time_Per_Week[4] = $Excess_Time_Per_Week[4] + $excess_time;
					if ($Week == 5) $Excess_Time_Per_Week[5] = $Excess_Time_Per_Week[5] + $excess_time;
				}
			} else {
				$excess_time = "";
				$consume_time = "";
			}
			
			/*==============================================================================================================*/
			//GETTING THE UNDERTIME AND EXCESS HOURS OF THE EMPLOYEE
			/*==============================================================================================================*/
			if ($consume_time >= $day_work_hours_count) {
				if ($day_work_hours_count != "") {
					if ($Week == 1) $RG_Time_Per_Week[1] = $RG_Time_Per_Week[1] + $day_work_hours_count;
					if ($Week == 2) $RG_Time_Per_Week[2] = $RG_Time_Per_Week[2] + $day_work_hours_count;
					if ($Week == 3) $RG_Time_Per_Week[3] = $RG_Time_Per_Week[3] + $day_work_hours_count;
					if ($Week == 4) $RG_Time_Per_Week[4] = $RG_Time_Per_Week[4] + $day_work_hours_count;
					if ($Week == 5) $RG_Time_Per_Week[5] = $RG_Time_Per_Week[5] + $day_work_hours_count;
				}
				$consume_time = $day_work_hours_count;
			} else {
				if ($arr_TO != "" && $arr_TI != "" && $data_RestDay != 1) {
					$excess_time = "";
					$underTime = $day_work_hours_count - $consume_time;
					switch ($Week) {
						case 1:
							$UT_Time_Per_Week[1] = $UT_Time_Per_Week[1] + $underTime;
							$UT_Count_Per_Week[1]++;
							break;
						case 2:
							$UT_Time_Per_Week[2] = $UT_Time_Per_Week[2] + $underTime;
							$UT_Count_Per_Week[2]++;
							break;
						case 3:
							$UT_Time_Per_Week[3] = $UT_Time_Per_Week[3] + $underTime;
							$UT_Count_Per_Week[3]++;
							break;
						case 4:
							$UT_Time_Per_Week[4] = $UT_Time_Per_Week[4] + $underTime;
							$UT_Count_Per_Week[4]++;
							break;
						case 5:
							$UT_Time_Per_Week[5] = $UT_Time_Per_Week[5] + $underTime;
							$UT_Count_Per_Week[5]++;
							break;
					}
				} else {
					$excess_time = "";
				}
				if ($consume_time != "") {
					if ($Week == 1) $RG_Time_Per_Week[1] = $RG_Time_Per_Week[1] + $consume_time;	
					if ($Week == 2) $RG_Time_Per_Week[2] = $RG_Time_Per_Week[2] + $consume_time;
					if ($Week == 3) $RG_Time_Per_Week[3] = $RG_Time_Per_Week[3] + $consume_time;
					if ($Week == 4) $RG_Time_Per_Week[4] = $RG_Time_Per_Week[4] + $consume_time;
					if ($Week == 5) $RG_Time_Per_Week[5] = $RG_Time_Per_Week[5] + $consume_time;
				}
				$consume_time = $consume_time;
			}
			/*==============================================================================================================*/
			//AUTO LUNCH BREAK
			/*==============================================================================================================*/
			if ($AutoLB == 1) {
				if ($arr_TI != "") {
					$Lunch_Out = $data_LunchOut;
					$Lunch_In = $data_LunchIn;
				} else {
					$Lunch_Out = "";
					$Lunch_In = "";
				}
			} else {
				$Lunch_Out = $row["LunchOut"];
				$Lunch_In = $row["LunchIn"];
			}	
			/*==============================================================================================================*/
			//MAPPING
			/*==============================================================================================================*/
			$where = $Default_qry;
			$where .= " AND EmployeesRefId = ".$emprefid;
			$where .= " AND StartDate = '".$row['AttendanceDate']."'";
			$where .= " AND Status = 'Approved'";
			$OTRow = FindFirst("overtime_request",$where,"*");
			if ($OTRow) {
				if ($OTRow["WithPay"] == 1) {
					if ($excess_time != "") {
						if ($excess_time >= $OTMinTime) {
							$OT_Count = $OT_Count + $excess_time;
						}
					}
				} else {
					if ($excess_time >= $COCMinTime) {
						if ($Week == 1) $COC_Time_Per_Week[1] = $COC_Time_Per_Week[1] + $excess_time;
						if ($Week == 2) $COC_Time_Per_Week[2] = $COC_Time_Per_Week[2] + $excess_time;
						if ($Week == 3) $COC_Time_Per_Week[3] = $COC_Time_Per_Week[3] + $excess_time;
						if ($Week == 4) $COC_Time_Per_Week[4] = $COC_Time_Per_Week[4] + $excess_time;
						if ($Week == 5) $COC_Time_Per_Week[5] = $COC_Time_Per_Week[5] + $excess_time;		
					}
				}
			} else {
				$map["[[exh".$day."]]"]   	   = HoursFormat($excess_time);
			}

			/*==============================================================================================================*/
			//GETTING ABSENT
			/*==============================================================================================================*/
			if ($arr_TI == "" && $arr_OBO == "" && $data_RestDay != 1) {
				if ($Week == 1) $Absent_Count_Per_Week[1]++;
				if ($Week == 2) $Absent_Count_Per_Week[2]++;
				if ($Week == 3) $Absent_Count_Per_Week[3]++;
				if ($Week == 4) $Absent_Count_Per_Week[4]++;
				if ($Week == 5) $Absent_Count_Per_Week[5]++;
			}
		}
	}

	$late_count 	= 0;
	$UT_count 		= 0;
	$total_hours 	= 0;
	$tot_reg		= 0;
	$tot_excess 	= 0;
	$absent 		= 0;
	$late 			= 0;
	$UT  			= 0;

	for ($i=1; $i <= 5; $i++) { 
		$map["[[rgwk$i]]"]     = HoursFormat($RG_Time_Per_Week[$i]);		
		$map["[[twk$i]]"]      = HoursFormat($Tardy_Time_Per_Week[$i]);	
		$map["[[ulwk$i]]"]     = HoursFormat($UT_Time_Per_Week[$i]);	
		$map["[[ehwk$i]]"]     = HoursFormat($Excess_Time_Per_Week[$i]);
		$map["[[ttwk$i]]"]     = $Tardy_Count_Per_Week[$i];
		$map["[[tuwk$i]]"]     = $UT_Count_Per_Week[$i];
		$map["[[awk$i]]"]      = $Absent_Count_Per_Week[$i];
		$map["[[cowk$i]]"]     = HoursFormat($COC_Time_Per_Week[$i]);
	}
	$tot_reg   	= $RG_Time_Per_Week[1] + 
				  $RG_Time_Per_Week[2] +
				  $RG_Time_Per_Week[3] + 
				  $RG_Time_Per_Week[4] + 
				  $RG_Time_Per_Week[5];

    $tot_excess = $Excess_Time_Per_Week[1] + 
                  $Excess_Time_Per_Week[2] + 
                  $Excess_Time_Per_Week[3] + 
                  $Excess_Time_Per_Week[4] +
                  $Excess_Time_Per_Week[5];

  	$total_hours = HoursFormat($tot_reg + $tot_excess);
  	$tot_hours_eq = getEquivalent(($tot_reg + $tot_excess),"workinghrsconversion");

	$late_count    = HoursFormat($Tardy_Time_Per_Week[1] + 
									  $Tardy_Time_Per_Week[2] + 
									  $Tardy_Time_Per_Week[3] + 
									  $Tardy_Time_Per_Week[4] + 
									  $Tardy_Time_Per_Week[5]);

	$UT_count   = HoursFormat($UT_Time_Per_Week[1] + 
									  $UT_Time_Per_Week[2] + 
									  $UT_Time_Per_Week[3] + 
									  $UT_Time_Per_Week[4] + 
									  $UT_Time_Per_Week[5]);
	
	

	$map["[[totcc]]"]  	= HoursFormat($COC_Time_Per_Week[1] + 
			                          $COC_Time_Per_Week[2] + 
			                          $COC_Time_Per_Week[3] + 
			                          $COC_Time_Per_Week[4] +
			                          $COC_Time_Per_Week[5]);

	$late   = $Tardy_Count_Per_Week[1] + 
                          $Tardy_Count_Per_Week[2] + 
                          $Tardy_Count_Per_Week[3] + 
                          $Tardy_Count_Per_Week[4] +
                          $Tardy_Count_Per_Week[5];

	$UT   = $UT_Count_Per_Week[1] + 
                          $UT_Count_Per_Week[2] + 
                          $UT_Count_Per_Week[3] + 
                          $UT_Count_Per_Week[4] +
                          $UT_Count_Per_Week[5];

  	$absent   =  $Absent_Count_Per_Week[1] + 
                          $Absent_Count_Per_Week[2] + 
                          $Absent_Count_Per_Week[3] + 
                          $Absent_Count_Per_Week[4] +
                          $Absent_Count_Per_Week[5];

	$map["[[otp]]"] 	= HoursFormat($OT_Count);

	/*==============================================================================================================*/
	//END OF COMPUTATION AND SHOOTING
	/*==============================================================================================================*/
	
	$dbg = false;

	if ($dbg) {
		//print_r(json_encode($arr));
	
		echo '
			<table border=1 style="width:100%;">
				<tr>
					<td>AttendanceDate</td>
					<td>AttendanceTime</td>
					<td>UTC</td>
					<td>TimeIn</td>
					<td>LunchOut</td>
					<td>LunchIn</td>
					<td>TimeOut</td>
					<td>OBOut</td>
					<td>OBIn</td>
					<td>Day</td>
					<td>Week</td>
					<td>KEntry</td>
					<td>Holiday</td>
					<td>Leave</td>
					<td>CTO</td>
					<td>OFFSUS</td>
				</tr>
		';
		foreach ($arr as $value) {
			foreach ($value as $key => $new_val) {
				//print_r(json_encode($new_val));
				
				echo '
					<tr>
						<td>'.$new_val["AttendanceDate"].'</td>
						<td>'.$new_val["AttendanceTime"].'</td>
						<td>'.$new_val["UTC"].'</td>
						<td>'.$new_val["TimeIn"].'</td>
						<td>'.$new_val["LunchOut"].'</td>
						<td>'.$new_val["LunchIn"].'</td>
						<td>'.$new_val["TimeOut"].'</td>
						<td>'.$new_val["OBOut"].'</td>
						<td>'.$new_val["OBIn"].'</td>
						<td>'.$new_val["Day"].'</td>
						<td>'.$new_val["Week"].'</td>
						<td>'.$new_val["KEntry"].'</td>
						<td>'.$new_val["Holiday"].'</td>
						<td>'.$new_val["Leave"].'</td>
						<td>'.$new_val["CTO"].'</td>
						<td>'.$new_val["OffSus"].'</td>
					</tr>
				';
				
			}
			
		}
		echo '</table>';

	}
?>