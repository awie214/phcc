<?php
	include 'conn.e2e.php';
	include 'constant.e2e.php';
	include pathClass.'0620functions.e2e.php';
	echo '
		<html>
	';
	echo '
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.js"></script>
	    
	';
	include 'conn.e2e.php';
	$hris_count = $pms_count = 0;
	echo '<body><table width="100%" style="font-size: 10pt;" class="table">';
	echo '<tr><td>HRIS</td><td>PMS</td></tr>';
	echo '<tr><td valign="top"><table width="95%" style="font-size: 10pt;" border=1>';
	$where = "Where Inactive IS NULL OR Inactive = 0 ORDER BY LastName";
	//$where = " ORDER BY LastName";
	$hris_emp = SelectEach("employees",$where);
	while ($hris_row = mysqli_fetch_assoc($hris_emp)) {
		$hris_count++;
		$LastName = $hris_row["LastName"];
		$FirstName = $hris_row["FirstName"];
		$MiddleName = $hris_row["MiddleName"];
		$AgencyId = $hris_row["AgencyId"];
		echo '<tr style="height:80px;"><td>'.$hris_count.'</td><td>'.$LastName.' '.$FirstName.'</td><td>'.$AgencyId.'</td></tr>';
	}
	echo '</table></td>';
	echo '<td valign="top"><table width="95%" style="font-size: 10pt;" border=1>';
	$pms_emp = SelectEach("pms_employees","WHERE active = 1 ORDER BY lastname");
	while ($pms_row = mysqli_fetch_assoc($pms_emp)) {
		$pms_count++;
		$lastname = $pms_row["lastname"];
		$firstname = $pms_row["firstname"];
		$middlename = $pms_row["middlename"];
		$employee_number = $pms_row["employee_number"];
		$id = $pms_row["id"];
		echo '<tr style="height:80px;" id="tr_'.$id.'">
				<td><input class="form-control" type="text" id="empid_'.$id.'"></td>
				<td class="text-center"><button class="btn btn-danger" type="button" onclick="inactive('.$id.')">INACTIVE</button></td>
				<td class="text-center"><button class="btn btn-info" type="button" onclick="update('.$id.')">UPDATE</button></td>
				<td id="tr_empid_'.$id.'">'.$employee_number.'</td>
				<td>'.$lastname.' '.$firstname.'</td>
			</tr>';
	}
	echo '</table></td><tr>';
	echo '</table></body>';


	echo '
		<script>
			$(document).ready(function () {
				$("td").css("padding","10px");
				$("[id*=\'empid_\']").val("");
			});
			function inactive(id) {
				$.get("testing_ajax.php",
			   	{
			      fn:"setInactive",
			      empid:id
			   	},
			   	function(data,status){
			      	if (status == "success") {
			         	eval(data);
			      	}
			   	});
			}
			function update(id) {
				var new_emp_number = $("#empid_" + id).val();
				$.get("testing_ajax.php",
			   	{
			      fn:"setEmployeeNumber",
			      empid:id,
			      emp_number:new_emp_number
			   	},
			   	function(data,status){
			      	if (status == "success") {
			         	eval(data);
			      	}
			   	});
			}
		</script>
		</html>
	';
?>