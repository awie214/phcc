<?php
   //session_start();
   include_once "constant.e2e.php";
   include_once "conn.e2e.php";
   $_SESSION["path"] = $path;
   $_SESSION["Classes"] = $pathClass;
   require_once pathClass."0620functions.e2e.php";
   require_once pathClass."SysFunctions.e2e.php";
   require_once pathClass."Forms.e2e.php";

   $_SESSION["cssFile"] = "CSS_PIS".f_encode(date("Ymd",time())).".css";
   foreach(glob(pathPublic.'js/*.e2e.*.js') as $file) {
      unlink($file);
   }
   $lgMin = false;
   $header = 1;
   $sys = new SysFunctions();
   $form = new SysForm();
   $sys->destroy_css("CSS_PIS");

   $css = ["armyBlue1","armyBlue2","bgPIS"];

   $_SESSION["cssFilePath"] = $sys->css_create($css);
   $_SESSION["0620utilities"] = $sys->min($sys->js("js1_0620utilities.js"),"0620utilities",["js","js",1],$lgMin);
   $_SESSION["0620functions"] = $sys->min($sys->js("js2_0620functions.js"),"0620functions",["js","js",2],$lgMin);
   $_SESSION["0620SystemRoute"] = $sys->min($sys->js("js3_0620SystemRoute.js"),"0620SystemRoute",["js","js",3],$lgMin);
   $_SESSION["0620sys"] = $sys->min($sys->js("js4_0620sys.js"),"0620sys",["js","js",4],$lgMin);
   $_SESSION["toolTip"] = $sys->min($sys->js("js5_toolTip.js"),"toolTip",["js","js",5],false);
   $_SESSION["jquery_utilities"] = $sys->min($sys->js("js6_jquery_utilities.js"),"jquery_utilities",["js","js",6],$lgMin);
   $_SESSION["login"] = $sys->min($sys->js("js7_login.js"),"login",["js","js",7],$lgMin);
   $_SESSION["tip"] = $sys->min($sys->css("tip.css"),"tip",["css","css",1],$lgMin);

   $_SESSION["PayGroup"] = ["Monthly","Semi-Monthly"];
?>
<html>
	<head>
		<title>HRIS</title>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="<?php echo $sys->publicPath("css/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css">
      <script type="text/javascript" src="<?php echo $path; ?>jquery/jquery.js"></script>
      <link rel="stylesheet" href="<?php echo $path; ?>bs/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
      <link rel="stylesheet" href="<?php echo $path; ?>bs/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
      <script src="<?php echo $path; ?>bs/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

      <!-- DATE PICKER -->
      <link href="<?php echo $sys->publicPath("jqueryui/jquery-ui.css"); ?>" rel="stylesheet">
      <script type="text/javascript" src="<?php echo $sys->publicPath("jqueryui/jquery-ui.js"); ?>"></script>
      <link href="<?php echo $sys->publicPath("datatables/jquery.dataTables.min.css"); ?>" rel="stylesheet">
      <script type="text/javascript" src="<?php echo $sys->publicPath("datatables/jquery.dataTables.min.js"); ?>"></script>

      <script type="text/javascript" src="<?php echo $sys->js($_SESSION["0620utilities"]); ?>"></script>
      <script type="text/javascript" src="<?php echo $sys->js($_SESSION["0620functions"]); ?>"></script>
      <script type="text/javascript" src="<?php echo $sys->js($_SESSION["0620sys"]); ?>"></script>
      <script type="text/javascript" src="<?php echo $sys->js($_SESSION["login"]); ?>"></script>
      <script type="text/javascript" src="<?php echo $path; ?>js/jsSHAver2/src/sha.js"></script>
      <link rel="stylesheet" href="<?php echo $sys->css($_SESSION["cssFilePath"]); ?>">
      <style>
         .midBody {
            height: calc(100vh - 120px);
         }
         .sysNameHolder {
            height: 100px;
            text-align: center;
            align-items: center;
            margin:0;
         }
         .sysName {
            font-family:'Edwardian Script ITC';
            font-size:30pt;
            font-weight:600;
            display:inline-block;
            vertical-align:middle;
            line-height: 100px;
            margin:0;
         }
         .outer {
             display: table;
             position: absolute;
             height: calc(100% - 130px);
             width: 100%;
             background:transparent;
         }
         .middle {
             display: table-cell;
             vertical-align: middle;
             background:transparent;
         }
         .inner {
             margin-left: auto;
             margin-right: auto;
             height:100%;
             background:#ffffff;
             background:url("<?php echo img("PIDSLogoBig.png"); ?>") no-repeat center;
         }
      </style>
      <script type="text/javascript">
         $(document).ready(function(){
            $("#div_login").hide();
            //$("#div_login").show();
            $("#aLogin").click(function () {
               $("#div_login").show();
               $("#div_logo").hide();
            });
            $("#punchMSG").hide();
            $("#txtEmpIDNO").blur(function () {
               if ($(this).val() != "") {
                  $.post("chkPicEmployees.e2e.php",
                  {
                     val1:$("#txtEmpIDNO").val()
                  },
                  function(data,status) {
                     if (status == "success") {
                        try {
                           if (data.indexOf("Not Found") > 0) {
                              $("[name*='btnPunch']").prop("disabled",true);
                              $("#punchMSG").show();
                              $("#txtEmpIDNO").select();
                              $("#txtEmpIDNO").focus();
                           } else {
                              eval(data);
                              $("#punchMSG").hide();
                           }
                        } catch (e) {
                            if (e instanceof SyntaxError) {
                                alert(e.message);
                            }
                        }
                     }
                     else {
                        alert("Ooops Error : " + status + "[event_txtEmpIDNO]");
                     }
                  });
               }
            });
            $("#inputPW").focus(function () {
               $(this).val("");
               //$('#EmpPic').attr("src","../../../public/images/nopic.png");
               //$('#PunchedList').html("");
            });
            $("#txtEmpIDNO").focus(function () {
               $('#SuccessMSG').hide();
            });
         });
         sessionStorage.clear();
      </script>
   </head>
   <body style="padding:0;" class="wmlogo">
		<form name="formlogin" method="post">
         <div style="padding:0;">
            <div class="bgSea sysNameHolder">
               <span class="sysName">Human Resources And Information System</span>
            </div>
            <div class="midBody wmlogo container" style="padding:0;">
               <div class="txt-center">
                  <span style="font-size:14pt;font-weight:900;">Employees Time Clock</span><br>
                  <span style="font-size:20pt;font-weight:900;" id="TimeDate"></span><br>
               </div>
               <div class="row" style="height:100%;margin:0;padding:0;" id="">
                  <div class="col-xs-6 txt-center" style="margin:0;padding:15px;height:100%;">
                        <div>
                           <div style="height:250px;">
                              <img id="EmpPic" src="<?php echo img("nopic.png"); ?>" style="margin:auto;border:1px solid black;height:100%;">
                           </div><br>
                           <div class="form-group txt-center">
                              <div>
                                 <label class="control-label" for="txtEmpIDNO">
                                    <i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;Employees ID No.
                                 </label><br>
                                 <input class="form-input number--" type="text" id="txtEmpIDNO" name="txtEmpIDNO" value="" style="font-size:20pt;width:50%;">
                                 <br><div id="punchMSG" style="padding:5px;color:red;display:none;">Oooops!!! UNAVAILABLE EMPLOYEES ID NO.</div>
                              </div>
                           </div>
                           <div class="form-group txt-center">
                             <label class="control-label" for="inputPW">Password</label><br>
                             <input class="form-input" type="password" id="inputPW" name="token" value="" style="text-align:center;font-size:20pt;width:50%;">
                           </div>
                           <div class="txt-center">
                              <button type="button" class="btn-cls4-sea" onclick="TCNow(1);" name="btnPunch1" disabled>Time In</button>
                              <button type="button" class="btn-cls4-red" onclick="TCNow(2);" name="btnPunch2" disabled>Lunch Out</button>
                              <button type="button" class="btn-cls4-sea" onclick="TCNow(3);" name="btnPunch3" disabled>Lunch In</button>
                              <button type="button" class="btn-cls4-red" onclick="TCNow(4);" name="btnPunch4" disabled>Time Out</button>
                              <button type="button" class="btn-cls4-sea" onclick="TCNow(5);" name="btnPunch5" disabled>OT In</button>
                              <button type="button" class="btn-cls4-red" onclick="TCNow(6);" name="btnPunch6" disabled>OT Out</button>
                           </div>
                           <div class="alert alert-success margin-top" id="SuccessMSG" style="display:none;">
                             <span style="background:font-size:16pt;font-weight:900;" id="SuccessMSG2"></span>
                           </div>
                        </div>
                  </div>
                  <div class="col-xs-6 txt-center" style="margin:0;padding:15px;height:100%;">
                     <div style="height:200px;margin:0;padding:0;">
                        <div id="PunchedList" style="overflow:auto;height:200px;">-- clear --</div>
                     </div>
                     <!--<div style="height:120px;margin-top:50px;padding:0;">
                        <?php
                           $t = time();
                           $date_today = date("Y-m-d",$t);
                           $criteria = "where AttendanceDate = '$date_today' ORDER BY AttendanceTime DESC LIMIT 1";
                           $rsLastPunch = f_Find("employeesattendance",$criteria);
                           $empPicLastPunch = "nopic.png";
                           $empLastPunch = "&nbsp;";
                           if ($rsLastPunch) {
                              $rowLastPunch = $rsLastPunch->fetch_assoc();
                              $empPicLastPunch = "EmployeesPhoto/".FindFirst("employees","where RefId = ".$rowLastPunch["EmployeesRefId"],"PicFilename");
                              $empLastPunch = getLabel_AttendanceEntry($rowLastPunch["KindOfEntry"])." - ".date("H:i:s",$rowLastPunch["AttendanceTime"]);
                           }
                        ?>
                        <img src="<?php echo img($empPicLastPunch); ?>" id="prevPic" style="border:1px solid black;height:100%;">
                        <div class="txt-center" style="margin-top:10px;" id="LastPunch"><?php echo $empLastPunch; ?></div>
                     </div>-->
                  </div>

               </div>
               <?php
                  footer();
                  //include_once ("varHidden.e2e.php");
               ?>
            </div>
         <input type="hidden" name="hToken" value="">
         <input type="hidden" id="hmode"  name="hmode"  value="">
         <input type="hidden" id="hRefId" name="hRefId"  value="">
      </form>
	</body>
</html>