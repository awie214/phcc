<?php
	include 'FnUpload.php';
	mysqli_query($conn,"TRUNCATE employeestraining");
	$EmpLND = fopen("csv/emp_lnd_28.csv", "r");
	while(!feof($EmpLND)) {
		$lnd_row = explode(",", fgets($EmpLND));
		$Fld = "CompanyRefId, BranchRefId, ";
		$Val = "28, 1, ";
		$EmpAgencyID 		= clean($lnd_row[0]);
		$SeminarsRefId 		= clean($lnd_row[1]);
		$StartDate			= clean($lnd_row[2]);
		$EndDate			= clean($lnd_row[3]);
		$NumofHrs			= intval(clean($lnd_row[4]));
		$SeminarClassRefId  = clean($lnd_row[5]);
		$SponsorRefId		= clean($lnd_row[6]);
		
		if (strpos($StartDate, "/") > 1) {
			if ($StartDate != "") {
	        	$StartDate_arr = explode("/",$StartDate);
	        	$day = $StartDate_arr[0];
	        	$month = $StartDate_arr[1];
	        	$year = $StartDate_arr[2];
	        	$day = intval($day);
	        	$month = intval($month);
	        	if ($month < 13) {
	        		if ($day <= 9) $day = "0".$day;
	        		if ($month <= 9) $month = "0".$month;
	        		$StartDate = $year."-".$month."-".$day;
	        	}
	        }	
		} else {
			$StartDate = "";
		}


		if (strpos($EndDate, "/") > 1) {
			if ($EndDate != "") {
	        	$EndDate_arr = explode("/",$EndDate);
	        	$day = $EndDate_arr[0];
	        	$month = $EndDate_arr[1];
	        	$year = $EndDate_arr[2];
	        	$day = intval($day);
	        	$month = intval($month);
	        	if ($month < 13) {
	        		if ($day <= 9) $day = "0".$day;
	        		if ($month <= 9) $month = "0".$month;
	        		$EndDate = $year."-".$month."-".$day;
	        	}
	        }	
		} else {
			$EndDate = "";
		}
		/*if (validateDate($StartDate,"d/m/Y")) {
			$date_arr = explode("/", $StartDate);
			$StartDate = $date_arr[2]."-".$date_arr[1]."-".$date_arr[0];
		} else if (validateDate($StartDate,"m/d/Y")) {
			$date_arr = explode("/", $StartDate);
			$StartDate = $date_arr[2]."-".$date_arr[0]."-".$date_arr[1];
		} else {
			$StartDate = "";
		}


		if (validateDate($EndDate,"d/m/Y")) {
			$date_arr = explode("/", $EndDate);
			$EndDate = $date_arr[2]."-".$date_arr[1]."-".$date_arr[0];
		} else if (validateDate($EndDate,"m/d/Y")) {
			$date_arr = explode("/", $EndDate);
			$EndDate = $date_arr[2]."-".$date_arr[0]."-".$date_arr[1];
		} else {
			if (strtolower($EndDate) == "present") {
				$Fld .= "Present, ";
				$Val .= "1, ";
			}
			$EndDate = "";
		}*/


		$emprefid = FindFirst("employees","WHERE AgencyId = '$EmpAgencyID'","RefId",$conn);
		if (is_numeric($emprefid)) {
			if ($SeminarClassRefId != "") {
				$SeminarClassRefId = saveFM("seminarclass","Name, ","'$SeminarClassRefId', ",$SeminarClassRefId);
				$Fld .= "SeminarClassRefId , ";
				$Val .= "'$SeminarClassRefId',";
			}
			if ($SeminarsRefId != "") {
				$SeminarsRefId = saveFM("seminars","Name, ","'$SeminarsRefId', ",$SeminarsRefId);
				$Fld .= "SeminarsRefId , ";
				$Val .= "'$SeminarsRefId',";
			}
			if ($SponsorRefId != "") {
				$SponsorRefId = saveFM("sponsor","Name, ","'$SponsorRefId', ",$SponsorRefId);
				$Fld .= "SponsorRefId , ";
				$Val .= "'$SponsorRefId',";
			}
			if ($StartDate != "") {
				$Fld .= "StartDate, ";
				$Val .= "'$StartDate',";
			}

			if ($EndDate != "") {
				$Fld .= "EndDate, ";
				$Val .= "'$EndDate',";
			}

			if ($NumofHrs != 0) {
				$Fld .= "NumofHrs, ";
				$Val .= "'$NumofHrs',";
			}
			$Fld .= "EmployeesRefId,";
			$Val .= "$emprefid,";
			$save_training = save("employeestraining",$Fld,$Val);
			if (is_numeric($save_training)) {
				echo "$emprefid Training Saved.<br>";
			}
		}
	}
?>