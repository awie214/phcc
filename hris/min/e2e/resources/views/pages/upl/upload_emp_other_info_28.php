<?php
	include 'FnUpload.php';
	mysqli_query($conn,"TRUNCATE employeesotherinfo");
	$EmpOtherInfo = fopen("csv/emp_otherinfo_28.csv", "r");
	while(!feof($EmpOtherInfo)) {
		$other_info_row = explode(",", fgets($EmpOtherInfo));
		if (count($other_info_row) == 12) {
			$Fld = "CompanyRefId, BranchRefId, Skills, Recognition, Affiliates, ";
			$Val = "28, 1, ";
			$EmpAgencyID = clean($other_info_row[0]);
			$Skills = clean($other_info_row[1]);
			$Recognition = clean($other_info_row[2]);
			$Affiliates = clean($other_info_row[3]);

			$emprefid = FindFirst("employees","WHERE AgencyId = '$EmpAgencyID'","RefId",$conn);
			if (is_numeric($emprefid)) {
				$Val .= "'$Skills', '$Recognition', '$Affiliates', ";
				$save_other_info = save("employeesotherinfo",$Fld,$Val);
				if (is_numeric($save_other_info)) {
					echo "$emprefid Other Info Saved<br>";
				}
			}

		}
	}
?>