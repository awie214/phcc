<?php
	include 'FnUpload.php';
	mysqli_query($conn,"TRUNCATE employeeselegibility");
	$EmpEligibility = fopen("csv/emp_eligibility1_28.csv", "r");
	while(!feof($EmpEligibility)) {
		$Fld = "CompanyRefId, BranchRefId, ";
		$Val = "28, 1, ";

		$eligibility_row = explode(",", fgets($EmpEligibility));
		$EmpAgencyID 		= clean($eligibility_row[0]);
		$CareerServiceRefId = clean($eligibility_row[1]);
		$Rating      		= clean($eligibility_row[2]);
		$ExamDate 	 		= clean($eligibility_row[3]);
		$ExamPlace   		= clean($eligibility_row[4]);
		$LicenseNo			= clean($eligibility_row[5]);
		$LicenseReleasedDate = clean($eligibility_row[6]);

		$Rating 			= floatval(str_replace("%", "", $Rating));


		/*if (validateDate($ExamDate,"d/m/Y")) {
			$date_arr = explode("/", $ExamDate);
			$ExamDate = $date_arr[2]."-".$date_arr[1]."-".$date_arr[0];
		} else if (validateDate($ExamDate,"m/d/Y")) {
			$date_arr = explode("/", $ExamDate);
			$ExamDate = $date_arr[2]."-".$date_arr[0]."-".$date_arr[1];
		} else {
			$ExamDate = "";
		}*/
		if (strpos($ExamDate, "/") > 1) {
			if ($ExamDate != "") {
	        	$ExamDate_arr = explode("/",$ExamDate);
	        	$day = $ExamDate_arr[0];
	        	$month = $ExamDate_arr[1];
	        	$year = $ExamDate_arr[2];
	        	$day = intval($day);
	        	$month = intval($month);
	        	if ($month < 13) {
	        		if ($day <= 9) $day = "0".$day;
	        		if ($month <= 9) $month = "0".$month;
	        		$ExamDate = $year."-".$month."-".$day;
	        	}
	        }	
		} else {
			$ExamDate = "";
		}


		if (strpos($LicenseReleasedDate, "/") > 1) {
			if ($LicenseReleasedDate != "") {
	        	$LicenseReleasedDate_arr = explode("/",$LicenseReleasedDate);
	        	$day = $LicenseReleasedDate_arr[0];
	        	$month = $LicenseReleasedDate_arr[1];
	        	$year = $LicenseReleasedDate_arr[2];
	        	$day = intval($day);
	        	$month = intval($month);
	        	if ($month < 13) {
	        		if ($day <= 9) $day = "0".$day;
	        		if ($month <= 9) $month = "0".$month;
	        		$LicenseReleasedDate = $year."-".$month."-".$day;
	        	}
	        }	
		} else {
			$LicenseReleasedDate = "";
		}
		



		/*if (validateDate($LicenseReleasedDate,"d/m/Y")) {
			$date_arr = explode("/", $LicenseReleasedDate);
			$LicenseReleasedDate = $date_arr[2]."-".$date_arr[1]."-".$date_arr[0];
		} else if (validateDate($LicenseReleasedDate,"m/d/Y")) {
			$date_arr = explode("/", $LicenseReleasedDate);
			$LicenseReleasedDate = $date_arr[2]."-".$date_arr[0]."-".$date_arr[1];
		} else {
			$LicenseReleasedDate = "";
		}*/

		if ($ExamDate != "") {
			$Fld .= "ExamDate, ";
			$Val .= "'$ExamDate', ";
		}

		if ($LicenseReleasedDate != "") {
			$Fld .= "LicenseReleasedDate, ";
			$Val .= "'$LicenseReleasedDate', ";
		}
		$emprefid = FindFirst("employees","WHERE AgencyId = '$EmpAgencyID'","RefId",$conn);
		if (is_numeric($emprefid)) {
			if ($CareerServiceRefId != "") {
				$CareerServiceRefId = saveFM("careerservice","Name, ","'$CareerServiceRefId', ",$CareerServiceRefId);
				$Fld .= "CareerServiceRefId, ";
				$Val .= "'$CareerServiceRefId', ";
			}
			$Fld .= "EmployeesRefId, Rating, ExamPlace, LicenseNo, ";
			$Val .= "$emprefid, '$Rating', '$ExamPlace', '$LicenseNo',";
			$save_eligibility = save("employeeselegibility",$Fld,$Val);
			if (is_numeric($save_eligibility)) {
				echo "$emprefid Eligibility Saved <br>";
			}
		}
	}
?>