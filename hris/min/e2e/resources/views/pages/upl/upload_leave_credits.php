<form action="upload_leave_credits.php" method="get">
	<label>Effectivy Date:</label><input type="text" name="effect_date">
	<br>
	<label>CID:</label><input type="text" name="cid">
	<br>
	<label>As Of Date:</label><input type="text" name="start_date">
	<br>
	<button type="submit" name="submit">SUBMIT</button>
</form>
<?php
	include 'FnUpload.php';
	include '../conn.e2e.php';
	if (isset($_GET["cid"]) && isset($_GET["start_date"]) && isset($_GET["effect_date"])) {
		mysqli_query($conn,"TRUNCATE employeescreditbalance");
		$EffectivityDate	= $_GET["effect_date"];
		$cid   				= $_GET["cid"];
		$BegBalAsOfDate 	= $_GET["start_date"];
		$branch 			= "1";
		$table 				= "employeescreditbalance";
		$EffectivityYear 	= date("Y",time());
		$file 				= "csv/leave_credits_".$_GET["cid"].".csv";
		$credtis 			= fopen($file, "r");
		while(!feof($credtis)) {
			$obj = fgets($credtis);
			if ($obj != "") {
				$obj_arr = explode(",", $obj);
				$empid = $obj_arr[0];
				$VL    = $obj_arr[1];
				$SL    = $obj_arr[2];
				if ($obj_arr[3]) {
					$FL = $obj_arr[3];
				} else {
					$FL = "0.00";
				}
				if ($obj_arr[4]) {
					$SPL = $obj_arr[4];
				} else {
					$SPL = "0.00";
				}

				$emprefid = FindFirst("employees","WHERE AgencyId = '$empid'","RefId",$conn);
				if ($emprefid > 0) {
					$Fld 	= "CompanyRefId, BranchRefId, EmployeesRefId, EffectivityYear, EffectivityDate, BegBalAsOfDate, ";
					$Val    = "'$cid', '$branch', '$emprefid', '$EffectivityYear', '$EffectivityDate', '$BegBalAsOfDate', ";
					$VL_Fld = $Fld." NameCredits, BeginningBalance, ForceLeave,";
					$VL_Val = $Val." 'VL', '$VL', '$FL',";
					$SL_Fld = $Fld." NameCredits, BeginningBalance, ";
					$SL_Val = $Val." 'SL', '$SL', ";
					$OT_Fld = $Fld." NameCredits, BeginningBalance, ";
					$OT_Val = $Val." 'OT', '0.00', ";
					$SPL_Fld = $Fld." NameCredits, BeginningBalance, ";
					$SPL_Val = $Val." 'SPL', '$SPL', ";
					$save_vl = save($table,$VL_Fld,$VL_Val);
					if (is_numeric($save_vl)) {
						echo "Successfully save VL For $emprefid.<br>";
					} else {
						echo "Error in Saving VL.<br>";
					}
					$save_sl = save($table,$SL_Fld,$SL_Val);
					if (is_numeric($save_sl)) {
						echo "Successfully save SL For $emprefid.<br>";
					} else {
						echo "Error in Saving SL.<br>";
					}
					$save_ot = save($table,$OT_Fld,$OT_Val);
					if (is_numeric($save_ot)) {
						echo "Successfully save OT For $emprefid.<br>";
					} else {
						echo "Error in Saving OT.<br>";
					}
					$save_spl = save($table,$SPL_Fld,$SPL_Val);
					if (is_numeric($save_spl)) {
						echo "Successfully save SPL For $emprefid.<br>";
					} else {
						echo "Error in Saving OT.<br>";
					}
				}
			}
		}
	} else {
		echo "Parameters In Complete<br>";
		echo "cid->start_date->effect_date";
		return false;
	}
	
?>