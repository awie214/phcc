<?php
	include 'FnUpload.php';
	include '../conn.e2e.php';
	$file = fopen("csv/coc_phcc.csv", "r");
	$count = 0;
	while(!feof($file)) {
		$str 		= fgets($file);
		$arr 		= explode(",", $str);
		$empid 		= $arr[0];
		$date  		= $arr[1];
		$balance 	= $arr[2];
		$bal_arr 	= explode(".", $balance);
		$hr 		= $bal_arr[0] * 60;
		$min 		= $bal_arr[1];
		$coc 		= intval($hr) + intval($min);
		$date 		= date("Y-m-d",strtotime($date));
		$date 		= "2018-10-31";
		//echo $empid.' has '.$coc.' as of '.$date."<br>";
		$emprefid   = FindFirst("employees","WHERE AgencyId = '$empid'","RefId",$conn);
		if (is_numeric($emprefid)) {
			$fld = "NameCredits, CompanyRefId, BranchRefId, EmployeesRefId, EffectivityYear,";
			$fld .= " BegBalAsOfDate, BeginningBalance,";
			$val = "'OT', 2, 1, '$emprefid', '2018', '2018-10-31', '$coc',";	
			$save = save("employeescreditbalance",$fld,$val);
			if (is_numeric($save)) {
				echo "Added for $empid.<br>";
			} else {
				echo "Error in uploading for $empid.<br>";
			}
		}
		
	}
?>