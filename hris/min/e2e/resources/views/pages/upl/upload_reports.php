<?php
	include '../conn.e2e.php';
	mysqli_query($conn,"TRUNCATE reports");
	function save($table,$flds,$vals) {
		include '../conn.e2e.php';
		$table = strtolower($table);
		$date_today    	= date("Y-m-d",time());
        $curr_time     	= date("H:i:s",time());
        $trackingA_fld 	= "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
        $trackingA_val 	= "'$date_today', '$curr_time', 'Admin', 'A'";
        $flds   	   	= $flds.$trackingA_fld;
        $vals   		= $vals.$trackingA_val;
        $sql 			= "INSERT INTO $table ($flds) VALUES ($vals)";
        $rs 			= mysqli_query($conn,$sql) or die(mysqli_error($conn));
        if ($rs) {
        	return mysqli_insert_id($conn);
        } else {
        	echo $sql;
        }
	}
	function FindFirst($table,$where,$fld) {
		include '../conn.e2e.php';
		$table = strtolower($table);
		if ($fld == "*") {
			$sql	= "SELECT * FROM $table ".$where." LIMIT 1";
		} else {
			$sql	= "SELECT `$fld` FROM $table ".$where." LIMIT 1";
		}
		$rs 		= mysqli_query($conn,$sql) or die(mysqli_error($conn));
		if ($rs) {
			if ($fld == "*") {
				$row = mysqli_fetch_assoc($rs);
				return $row;
			} else {
				$row = mysqli_fetch_assoc($rs);
				return $row[$fld];
			}
		} else {
			echo $sql;
		}
	}

	$ReportJson = file_get_contents("json/report.json");
	$Rpt = json_decode($ReportJson,true);

	$count = 0;
	foreach ($Rpt as $key => $RptCount) {
		$Fld = "CompanyRefId, BranchRefId, ";
		$Val = "0, 0, ";
		foreach ($RptCount as $key => $value) {
			$value = mysqli_real_escape_string($conn,$value);
			$Fld .= "`$key`, ";
			$Val .= "'$value', ";
		}
		$filename = $RptCount["FileName"];
		$name     = $RptCount["Name"];
		$where    = "WHERE FileName = '$filename' AND Name = '$name'";
		$check    = FindFirst("reports",$where,"RefId");
		if (!is_numeric($check)) {
			$save_report = save("reports",$Fld,$Val);
			if (is_numeric($save_report)) {
				$count++;
				echo "$count. $name added to the report module.<br>";
			}
		}
	}
?>