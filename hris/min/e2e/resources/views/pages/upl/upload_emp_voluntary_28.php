<?php
	include 'FnUpload.php';
	mysqli_query($conn,"TRUNCATE employeesvoluntary");
	$EmpVoluntary = fopen("csv/emp_voluntary_28.csv", "r");
	while(!feof($EmpVoluntary)) {
		$Fld = "CompanyRefId, BranchRefId, ";
		$Val = "28, 1, ";
		$voluntary_row 		= explode(",", fgets($EmpVoluntary));
		$EmpAgencyID 		= clean($voluntary_row[0]);
		$OrganizationRefId 	= clean($voluntary_row[1]);
		$StartDate			= clean($voluntary_row[2]);
		$EndDate			= clean($voluntary_row[3]);
		$NumofHrs			= intval(clean($voluntary_row[4]));
		$WorksNature		= clean($voluntary_row[5]);
		
		if (strpos($StartDate, "/") > 1) {
			if ($StartDate != "") {
	        	$StartDate_arr = explode("/",$StartDate);
	        	$day = $StartDate_arr[0];
	        	$month = $StartDate_arr[1];
	        	$year = $StartDate_arr[2];
	        	$day = intval($day);
	        	$month = intval($month);
	        	if ($month < 13) {
	        		if ($day <= 9) $day = "0".$day;
	        		if ($month <= 9) $month = "0".$month;
	        		$StartDate = $year."-".$month."-".$day;
	        	}
	        }	
		} else {
			$StartDate = "";
		}


		if (strpos($EndDate, "/") > 1) {
			if ($EndDate != "") {
	        	$EndDate_arr = explode("/",$EndDate);
	        	$day = $EndDate_arr[0];
	        	$month = $EndDate_arr[1];
	        	$year = $EndDate_arr[2];
	        	$day = intval($day);
	        	$month = intval($month);
	        	if ($month < 13) {
	        		if ($day <= 9) $day = "0".$day;
	        		if ($month <= 9) $month = "0".$month;
	        		$EndDate = $year."-".$month."-".$day;
	        	}
	        }	
		} else {
			$EndDate = "";
		}
		/*if (validateDate($StartDate,"d/m/Y")) {
			$date_arr = explode("/", $StartDate);
			$StartDate = $date_arr[2]."-".$date_arr[1]."-".$date_arr[0];
		} else if (validateDate($StartDate,"m/d/Y")) {
			$date_arr = explode("/", $StartDate);
			$StartDate = $date_arr[2]."-".$date_arr[0]."-".$date_arr[1];
		} else {
			$StartDate = "";
		}


		if (validateDate($EndDate,"d/m/Y")) {
			$date_arr = explode("/", $EndDate);
			$EndDate = $date_arr[2]."-".$date_arr[1]."-".$date_arr[0];
		} else if (validateDate($EndDate,"m/d/Y")) {
			$date_arr = explode("/", $EndDate);
			$EndDate = $date_arr[2]."-".$date_arr[0]."-".$date_arr[1];
		} else {
			if (strtolower($EndDate) == "present") {
				$Fld .= "Present, ";
				$Val .= "1, ";
			}
			$EndDate = "";
		}*/


		$emprefid = FindFirst("employees","WHERE AgencyId = '$EmpAgencyID'","RefId",$conn);
		if (is_numeric($emprefid)) {
			if ($OrganizationRefId != "") {
				$OrganizationRefId = saveFM("Organization","Name, ","'$OrganizationRefId', ",$OrganizationRefId);
				$Fld .= "OrganizationRefId , ";
				$Val .= "'$OrganizationRefId',";
			}
			if ($StartDate != "") {
				$Fld .= "StartDate, ";
				$Val .= "'$StartDate',";
			}

			if ($EndDate != "") {
				$Fld .= "EndDate, ";
				$Val .= "'$EndDate',";
			}

			if ($NumofHrs != 0) {
				$Fld .= "NumofHrs, ";
				$Val .= "'$NumofHrs',";
			}
			$Fld .= "EmployeesRefId, WorksNature, ";
			$Val .= "$emprefid, '$WorksNature',";
			$save_voluntary = save("employeesvoluntary",$Fld,$Val);
			if (is_numeric($save_voluntary)) {
				echo "$emprefid Voluntary Saved.<br>";
			}
		}
	}
?>