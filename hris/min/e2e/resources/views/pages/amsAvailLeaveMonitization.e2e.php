<?php $module = module("AvailmentLeaveMonitization"); ?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_AfterTrn"); ?>"></script>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_leavemonitization"); ?>"></script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="row">
                        <?php
                           if ($UserCode == "COMPEMP") {
                              echo '<div class="col-xs-12">';
                           } else {
                              echo '<div class="col-xs-3">';
                                 employeeSelector();
                              echo '</div>';
                              echo '<div class="col-xs-9">';
                              $templ->doSelectedEmployee();
                           }
                        ?>
                           <div class="row" id="divList">
                              <div class="mypanel">
                                 <div class="panel-top">LIST</div>
                                 <div class="panel-mid">

                                    <span id="spGridTable">
                                       <?php
                                          if ($UserCode == "COMPEMP") {
                                             $gridTableHdr_arr = ["File Date", "Balance(VL)", "Value(VL)", "Amount(VL)", "Balance(SL)", "Value(SL)", "Amount(SL)","Status"];
                                             $gridTableFld_arr = ["FiledDate", "VLBalance", "VLValue", "VLAmount", "SLBalance", "SLValue", "SLAmount", "Status"];
                                             $sql = "SELECT * FROM $table WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = $EmployeesRefId ORDER BY FiledDate";
                                             $Action = [false,false,false,false];
                                          }
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      $Action,
                                                      "gridTable");
                                       ?>
                                    </span>
                                    <?php
                                       btnINRECLO([true,true,false]);
                                    ?>
                                 </div>
                                 <div class="panel-bottom"></div>
                              </div>
                           </div>
                           <div class="row" id="divView">
                              <div class="mypanel">
                                 <div class="panel-top">
                                    <span id="ScreenMode">AVAILING</span> <?php echo strtoupper($ScreenName); ?>
                                 </div>
                                 <div class="panel-mid">
                                    <div id="EntryScrn">
                                       <div class="row" id="badgeRefId">
                                          <div class="col-xs-6">
                                             <ul class="nav nav-pills">
                                                <li class="active" style="font-size:12pt;font-weight:600;">
                                                   <a>REFID : <span class="badge" style="font-size:12pt;font-weight:600;" id="idRefid">
                                                   </span></a>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                       <div class="padd10">
                                          <div class="row">
                                             <div class="col-xs-3">   
                                                <?php label("Employee ID:",""); ?><br>
                                                <input type="text" name="agencyid" id="agencyid" class="form-input" readonly>
                                                <input type="hidden" value="" class="form-input saveFields--" 
                                                name="sint_EmployeesRefId">
                                             </div>
                                          </div>
                                          <br>
                                          <div class="row">
                                             <div class="col-xs-4">
                                                <?php label("Date:",""); ?><br>
                                                <input type="text" class="form-input date-- saveFields--" name="date_FiledDate" value="<?php today(); ?>">
                                             </div>
                                             <div class="col-xs-8">
                                                <div class="row">
                                                   <div class="col-xs-12 text-center">
                                                      <label>
                                                         Leave Credit Balances
                                                      </label>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-6 text-center">
                                                      <input type="text" class="form-disp saveFields--" disabled name="deci_VLBalance">
                                                      <br>
                                                      VL
                                                   </div>
                                                   <div class="col-xs-6 text-center">
                                                      <input type="text" class="form-disp saveFields--" disabled name="deci_SLBalance">
                                                      <br>
                                                      SL
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-4">
                                                <label>
                                                   Type of Monetization
                                                </label>
                                                <br>
                                                <select name="sint_IsHalf" id="sint_IsHalf" class="form-input saveFields--">
                                                   <option value="0">Regular Monetization</option>
                                                   <option value="1">Special Monetization</option>
                                                </select>
                                             </div>
                                          </div>
                                          <div class="row margin-top" id="regular_monetization">
                                             <div class="col-xs-6">
                                                <div class="row">
                                                   <div class="col-xs-12 text-center" style="margin-top: 20px;">
                                                      <label>No. Of Days to Monetize</label>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-6 text-center">
                                                      <input type="text" class="number-- form-input saveFields--"  name="deci_VLValue" id="deci_VLValue">
                                                      <!-- <select name="deci_VLValue" id="deci_VLValue" class="form-input saveFields--">
                                                         <option value=""></option>
                                                         <?php
                                                            for ($a=0; $a <= 30 ; $a++) { 
                                                               echo '<option value="'.number_format($a,3).'">'.$a.'</option>';
                                                            }
                                                         ?>
                                                      </select> -->
                                                      <br>
                                                      <label>VL</label>
                                                   </div>
                                                   <div class="col-xs-6 text-center">
                                                      <input type="text" class="number-- form-input saveFields--"  name="deci_SLValue" id="deci_SLValue">
                                                      <!-- <select name="deci_SLValue" id="deci_SLValue" class="form-input saveFields--">
                                                         <option value=""></option>
                                                         <?php
                                                            for ($b=0; $b <= 30 ; $b++) { 
                                                               echo '<option value="'.number_format($b,3).'">'.$b.'</option>';
                                                            }
                                                         ?>
                                                      </select> -->
                                                      <br>
                                                      <label>SL</label>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-xs-6" style="display: none;">
                                                <div class="row">
                                                   <div class="col-xs-12">
                                                      <label>CHECKING</label>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-6">
                                                      <label>Salary:</label>
                                                      <input type="text" name="hSalary" id="hSalary" class="form-input" readonly>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-6 text-center">
                                                      <input type="text" name="deci_VLAmount" id="deci_VLAmount" class="form-input saveFields--" readonly>
                                                      <br>
                                                      <label>VL Amount</label>
                                                   </div>
                                                   <div class="col-xs-6 text-center">
                                                      <input type="text" name="deci_SLAmount" id="deci_SLAmount" class="form-input saveFields--" readonly>
                                                      <br>
                                                      <label>SL Amount</label>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row margin-top" id="fifty_monetization">
                                             <div class="col-xs-6">
                                                <div class="form-group">
                                                   <label>Reason For Monetization</label>
                                                   <textarea class="form-input saveFields--" rows="5" name="char_Remarks" id="char_Remarks" placeholder="remarks"></textarea>
                                                </div>
                                             </div>   
                                          </div>
                                       </div>
                                    </div>
                                    <?php
                                       spacer(10);
                                       btnSACABA([true,true,true]);
                                    ?>
                                 </div>

                                 <div class="panel-bottom"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>   
               </div>
            </div>
            <?php
               footer();
               include "varJSON.e2e.php";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>