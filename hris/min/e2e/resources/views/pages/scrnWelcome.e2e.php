<?php
   include 'conn.e2e.php';
   if (getvalue("hCompanyID") == "2") {
      $disabled = "disabled";
   } else {
      $disabled = "";
   }
   //$disabled = "";
   $EmpRefId = getvalue("hEmpRefId");
?>
<!DOCTYPE>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo $_SESSION["login"] ?>"></script>
      <script type="text/javascript" src="<?php echo $path; ?>js/jsSHAver2/src/sha.js"></script>
      <link rel="stylesheet" href="<?php echo $path."/css/sideBar.css"; ?>">
      <style type="text/css">
         #PIS_Header, #AMS_Header, #PMS_Header, #myrequest {cursor: pointer;}
      </style>
      <script language="JavaScript">
         $(document).ready(function () {
            

         });
         function clickRpt(file,where){
            $("#rptContent").attr("src","blank.htm");
            var rptFile = file;
            var url = "ReportCaller.e2e.php?file=" + rptFile;
            url += "&where=" + where;  
            url += "&" + $("[name='hgParam']").val();
            $("#prnModal").modal();
            $("#rptContent").attr("src",url);
         }
         function printAvail(refid,file){
            $("#rptContent").attr("src","blank.htm");
            var rptFile = file;
            var url = "ReportCaller.e2e.php?file=" + rptFile;
            url += "&refid=" + refid;
            url += "&" + $("[name='hgParam']").val();
            $("#prnModal").modal();
            $("#rptContent").attr("src",url);
         }
      </script>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script>
      <style type="text/css">
         .request {
            cursor: pointer;
         }
         .request:hover {
            text-decoration: underline;
         }
      </style>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <nav class="navbar navbar-fixed-top">
            <div class="sysNameHolder sysBG" style="border-bottom:3px solid #fff;">
               <?php
                  $TRNBTN = 0;
                  $title = "";
                  $Logout = true;
                  include $files["inc"]["hdr"];
               ?>
               <span class="sysName">
                  <?php
                     echo $settings["Title"];
                  ?>
               </span>
            </div>
         </nav>
         <div style="margin-top:60px;">
            <?php
               if (!$isUser) {
                  doSideBarMain();
            ?>
                  <div class="container-fluid" id="mainScreen">
                     <?php doTitleBar("DASHBOARD / REMINDERS"); ?>
                     <div class="row">
                        <div class="col-xs-6">
                           <?php
                              $newly_hired      = 0;
                              $emp_count        = 0;
                              $emp_bday         = 0;
                              $three            = 0;
                              $fivebelow        = 0;
                              $six_ten          = 0;
                              $eleven_twenty    = 0;
                              $twentyone_thirty = 0;
                              $thirtyup         = 0;
                              $curr_date        = date("Y",time());


                              $emp_count = SelectEach("employees","WHERE (Inactive != 1 OR Inactive IS NULL)");
                              if (mysqli_num_rows($emp_count) > 0) {
                                 $emp_count = mysqli_num_rows($emp_count);
                              }
                              //----------------------------------------------------
                              //----------------------------------------------------
                              //----------------------------------------------------
                              $emp_bday_where = "WHERE MONTH(BirthDate) = ".date("m",time());
                              $rs = SelectEach("employees",$emp_bday_where);
                              if ($rs) {
                                 $emp_bday = mysqli_num_rows($rs);
                              } else {
                                 $emp_bday = 0;
                              }
                              //----------------------------------------------------
                              //----------------------------------------------------
                              //----------------------------------------------------

                              $past_month = date("Y",time())."-01-01";
                              $newly_hired_rs = SelectEach("empinformation","WHERE HiredDate > '$past_month'");
                              if ($newly_hired_rs) {
                                 while ($newly_hired_row = mysqli_fetch_assoc($newly_hired_rs)) {
                                    $newly_hired++;
                                 }
                              }
                              //----------------------------------------------------
                              //----------------------------------------------------
                              //----------------------------------------------------

                              $six_ten_where          = "WHERE YEAR(HiredDate) >= '2008' AND YEAR(HiredDate) < 2013";
                              $eleven_twenty_where    = "WHERE YEAR(HiredDate) >= '1998' AND YEAR(HiredDate) < 2008";
                              $twentyone_thirty_where = "WHERE YEAR(HiredDate) >= '1988' AND YEAR(HiredDate) < 1998";
                              $thirtyup_where         = "WHERE YEAR(HiredDate) <= '1987'";
                              $fivebelow_where        = "WHERE YEAR(HiredDate) >= '2013'";
                              $three_where            = "WHERE YEAR(HiredDate) == '2016'";

                              $years_rs = SelectEach("empinformation","WHERE HiredDate != ''");
                              if ($years_rs) {
                                 while ($year_row = mysqli_fetch_assoc($years_rs)) {
                                    $hired = date("Y",strtotime($year_row["HiredDate"]));
                                    $diff = $curr_date - $hired;
                                    $diff = $diff + 1;
                                    if ($diff == 3) {
                                       $three++;
                                    } else if ($diff <= 5) {
                                       $fivebelow++;
                                    } else if ($diff >= 6 && $diff <= 10) {
                                       $six_ten++;
                                    } else if ($diff >= 11 && $diff <= 20) {
                                       $eleven_twenty++;
                                    } else if ($diff >= 21 && $diff <= 30) {
                                       $twentyone_thirty++;
                                    } else if ($diff >= 30) {
                                       $thirtyup++;
                                    }
                                 }
                              }
                           ?>

                           <?php spacer(5) ?>
                           <div class="panel-group">
                              <div class="panel panel-default">
                                 <div class="panel-heading">TOTAL NUMBER OF EMPLOYMENT</div>
                                 <div class="panel-body">
                                    <ul class="list-group">
                                       <li class="list-group-item counts--" onclick="showEmp('Employees','Name',0);">
                                          TOTAL ACTIVE EMPLOYEES
                                          <span class="badge"><?php echo $emp_count; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" onclick="clickRpt('rpt_Newly_Hired','');">
                                          NEWLY HIRED
                                          <span class="badge"><?php echo $newly_hired; ?></span>
                                       </li>
                                       
                                    </ul>
                                 </div>
                              </div>
                              <div class="panel panel-default">
                                 <div class="panel-heading ">NOTIFICATION FOR</div>
                                 <div class="panel-body">
                                    <ul class="list-group">
                                       <li class="list-group-item counts--" onclick="showEmp('BirthDate','With Birthdays this month',0);">
                                          BIRTHDAY CELEBRANTS FOR THE MONTH
                                          <span class="badge"><?php echo $emp_bday; ?></span>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="panel panel-default">
                                 <div class="panel-heading ">LOYALTY</div>
                                 <div class="panel-body">
                                    <ul class="list-group">
                                       <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_Loyalty','WHERE YEAR(HiredDate) = \'2016\'');"
                                       >
                                          3 YEARS
                                          <span class="badge"><?php echo $three; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_Loyalty','WHERE YEAR(HiredDate) <= \'<?php echo date("Y",time()) - 5; ?>\'');"
                                       >
                                          5 YEARS BELOW
                                          <span class="badge"><?php echo $fivebelow; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_Loyalty','WHERE YEAR(HiredDate) >= \'1998\' AND YEAR(HiredDate) < \'2008\'');"
                                       >
                                          6 - 10 YEARS
                                          <span class="badge"><?php echo $six_ten; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_Loyalty','WHERE YEAR(HiredDate) >= \'1988\' AND YEAR(HiredDate) < \'1998\'');"
                                       >
                                          11 - 20 YEARS
                                          <span class="badge"><?php echo $eleven_twenty; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_Loyalty','WHERE YEAR(HiredDate) >= \'2008\' AND YEAR(HiredDate) < \'2013\'');"
                                       >
                                          21 - 30 YEARS
                                          <span class="badge"><?php echo $twentyone_thirty; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_Loyalty','WHERE YEAR(HiredDate) <= \'1998\'');"
                                       >
                                          30 YEARS UP
                                          <span class="badge"><?php echo $thirtyup; ?></span>
                                       </li>
                                       <li class="list-group-item counts--" 
                                           onclick="clickRpt('rpt_LoyaltyList','');"
                                       >
                                          Loyalty Report
                                          
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-6 padd5">
                           <div class="panel panel-default">
                              <div class="panel-heading ">PENDING AVAILMENTS:</div>
                              <div class="panel-body">
                                 <ul class="list-group">
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvLeaveAvail','');">
                                       Leave Applications:
                                       <?php 
                                          $leave_count = SelectEach("employeesleave","WHERE Status IS NULL");
                                          if ($leave_count) {
                                             $leave_count = mysqli_num_rows($leave_count);
                                          } else {
                                             $leave_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $leave_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvOfficeAuthority','');">
                                       Office Authority Applications:
                                       <?php 
                                          $authority_count = SelectEach("employeesauthority","WHERE Status IS NULL");
                                          if ($authority_count) {
                                             $authority_count = mysqli_num_rows($authority_count);
                                          } else {
                                             $authority_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $authority_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvCTOAvail','');">
                                       CTO Applications:
                                       <?php 
                                          $CTO_count = SelectEach("employeescto","WHERE Status IS NULL");
                                          if ($CTO_count) {
                                             $CTO_count = mysqli_num_rows($CTO_count);
                                          } else {
                                             $CTO_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $CTO_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvMonetization','');">
                                       Leave Monetization Applications:
                                       <?php 
                                          $monetization_count = SelectEach("employeesleavemonetization","WHERE Status IS NULL");
                                          if ($monetization_count) {
                                             $monetization_count = mysqli_num_rows($monetization_count);
                                          } else {
                                             $monetization_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $monetization_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvAttendanceRegistration','');">
                                       Attendance Registration Request:
                                       <?php 
                                          $attendancereq_count = SelectEach("attendance_request","WHERE Status IS NULL");
                                          if ($attendancereq_count) {
                                             $attendancereq_count = mysqli_num_rows($attendancereq_count);
                                          } else {
                                             $attendancereq_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $attendancereq_count; ?></span>
                                    </li>
                                    <li class="list-group-item counts--" onclick="gotoscrn('amsAppvCOCOvertime','');">
                                       Overtime Request:
                                       <?php 
                                          $overtime_count = SelectEach("overtime_request","WHERE Status IS NULL");
                                          if ($overtime_count) {
                                             $overtime_count = mysqli_num_rows($overtime_count);
                                          } else {
                                             $overtime_count = 0;
                                          }
                                       ?>
                                       <span class="badge"><?php echo $overtime_count; ?></span>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- Modal -->
                  <div class="modal fade border0" id="prnModal" role="dialog">
                     <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">

                        <div class="mypanel border0" style="height:100%;">
                           <div class="panel-top bgSilver">
                              <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                                 <i class="fa fa-print" aria-hidden="true"></i>
                              </a>
                              <label class="close" data-dismiss="modal">&times;</button>
                           </div>
                           <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                        </div>
                     </div>
                  </div>
            <?php

               } else {
                  /*
                     USER SIDE SCRN WELCOME
                  */
                  spacer(25);
            ?>
                  
                  <div class="row" style="margin:0px;">
                     <div class="col-xs-6" style="margin:0px;">
                        <?php
                           include 'conn.e2e.php';
                           $sql = "SELECT * FROM `employees` WHERE RefId = ".getvalue("hEmpRefId");
                           $rs = mysqli_query($conn,$sql);
                           if (mysqli_num_rows($rs) > 0)
                           {
                              $row = mysqli_fetch_assoc($rs)

                        ?>
                              <table style="width: 100%;">
                                 <tr> 
                                    <td width="50%" align="center" valign="middle">
                                       <p>
                                       <?php
                                          if ($row['PicFilename'] != "") {
                                             if (file_exists(img($row['CompanyRefId']."/EmployeesPhoto/".$row['PicFilename']))) {
                                                echo '<img src="'.img($row['CompanyRefId']."/EmployeesPhoto/".$row['PicFilename']).'" style="width:150px;">';
                                             } else {
                                                echo '<img src="'.img("nopic.png").'" style="width:150px;">';
                                             }
                                          } else {
                                             echo '<img src="'.img("nopic.png").'" style="width:150px;">';
                                          }
                                       ?>
                                       </p>
                                       <p>
                                       <button
                                          class="btn-cls2-red"
                                          id="btnChangePW" name="btnChangePW">Change Password
                                       </button>
                                       </p>
                                    </td>
                                    <td width="50%" valign="top">
                                       <?php
                                          $result = FindFirst('empinformation',"WHERE CompanyRefId = $CompanyId AND BranchRefId = $BranchId AND EmployeesRefId = ".$row["RefId"],"*");
                                          $info = array_merge($row,$result);
                                          //$templ->doEmployeeInfo($info);
                                          echo '
                                             <div class="row" style="font-size:10pt;">
                                                <div class="col-xs-12">
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <b>Employee Name:</b>
                                                         <br>
                                                         <u>'.$info["LastName"].", ".$info["FirstName"].' '.$info["ExtName"].' '.$info["MiddleName"].'</u>
                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <b>Plantilla Item No:</b>
                                                         <br>
                                                         <u>'.getRecord("positionitem",$info["PositionItemRefId"],"Name").'</u>
                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <b>Position:</b>
                                                         <br>
                                                         <u>'.getRecord("position",$info["PositionRefId"],"Name").'</u>
                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <b>Office:</b>
                                                         <br>
                                                         <u>'.getRecord("office",$info["OfficeRefId"],"Name").'</u>
                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <b>Division:</b>
                                                         <br>
                                                         <u>'.getRecord("division",$info["DivisionRefId"],"Name").'</u>
                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <b>Employee ID:</b>
                                                         <br>
                                                         <u>'.$info["AgencyId"].'</u>
                                                      </div>
                                                   </div>
                                                   <div class="row margin-top">
                                                      <div class="col-xs-12">
                                                         <b>Appt Status:</b>
                                                         <br>
                                                         <u>'.getRecord("empstatus",$info["EmpStatusRefId"],"Name").'</u>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          ';
                                       ?>

                                    </td>
                                 </tr>
                              </table>   
                        <?php
                           }
                        ?>
                     </div>
                     <div class="col-xs-1"></div>
                     <div class="col-xs-4">
                        <div id="panelReminders">
                           <div class="row">
                              <div class="col-xs-12">
                                 <div class="panel-top" for="PIS_menu"id="PIS_Header">PERSONAL INFORMATION SYSTEM (PIS)</div>
                                 <div class="panel-mid" style="border-bottom: 1px solid black;" id="PIS_menu">
                                    <div class="row">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="scrn"
                                               route="201File"
                                                  id="userPDS">
                                             <li><u>PERSONAL DATA SHEET (PDS)</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="scrn"
                                               route="EmpAttach" <?php echo $disabled; ?>>
                                             <li><u>201 FILE ATTACHMENTS</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="scrn"
                                               route="201Update" <?php echo $disabled; ?>>
                                             <li><u>201 FILE UPDATE</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="user"
                                               route="SERVICERECORD_1000" <?php echo $disabled; ?>>
                                             <li><u>SERVICE RECORD</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="user"
                                               route="Work_Exp_Attachment" <?php echo $disabled; ?>>
                                             <li><u>WORK EXPERIENCE ATTACHMENT</u></li>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                              <div class="panel-top" id="AMS_Header">ATTENDANCE MANAGEMENT SYSTEM (AMS)</div>
                                 <div class="panel-mid" style="border-bottom: 1px solid black;" id="AMS_menu">
                                    <?php
                                       if (getvalue("hCompanyID") == "2") {
                                    ?>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button" class="btn-cls4-sea" disabled>
                                             <!-- <a href="forms/2/PCCUSERGUIDEFINAL.pdf" style="color:white !important; text-decoration:none !important;" download>
                                                <li><u>DOWNLOAD DTR MANUAL</u></li>
                                             </a> -->
                                             <li><u>DOWNLOAD - AMS MANUAL</u></li>
                                          </button>
                                       </button>
                                       </div>
                                    </div>
                                    <?php
                                       }
                                    ?>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button" 
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="TrnDTR"
                                               id="userDTR">
                                          <li><u>DAILY TIME RECORD (DTR)</u></li>
                                       </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="LeaveCard" <?php echo $disabled; ?>>
                                             <li><u>LEAVE CARD</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <?php bar();?>
                                    <!-- <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="ReqChangeShift" <?php //echo $disabled; ?>>
                                             <li><u>REQUEST FOR CHANGE SHIFT</u></li>
                                          </button>
                                       </div>
                                    </div> -->
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="ReqOvertime">
                                             <li><u>REQUEST FOR OVERTIME</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="ReqForceLeave">
                                             <li><u>REQUEST FOR CANCELLATION OF LEAVE</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="user"
                                               route="ReqAttendanceRegistration">
                                             <li><u>REQUEST FOR ATTENDANCE REGISTRATION</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <?php bar();?>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="user"
                                               route="AvailLeave">
                                             <li><u>APPLICATION OF LEAVE</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="AvailCTO">
                                             <li><u>APPLICATION OF CTO</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="ams"
                                               route="AvailAuthority">
                                             <li><u>APPLICATION OF OFFICE AUTHORITY</u></li>
                                          </button>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="user"
                                               route="AvailMonetization">
                                             <li><u>APPLICATION OF LEAVE MONETIZATION</u></li>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <div class="panel-top" id="PMS_Header">PAYROLL MANAGEMENT SYSTEM (PMS)</div>
                                 <div class="panel-mid" style="border-bottom: 1px solid black;" id="PMS_menu">
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="user"
                                               route="PAYSLIP_2">
                                             <li><u>PAYSLIP</u></li>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <div class="panel-top" id="SPMS_Header">STRATEGIC PERFORMANCE MANAGEMENT SYSTEM (SPMS)</div>
                                 <div class="panel-mid" style="border-bottom: 1px solid black;" id="SPMS_menu">
                                    <div class="row margin-top">
                                       <div class="col-xs-12">
                                          <button type="button"
                                               class="Menu btn-cls2-tree"
                                               pre="spms_"
                                               route="IPS"
                                              >
                                             <li><u>INDIVIDUAL PERFORMANCE SCORECARD [IPS]</u></li>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <div class="panel-top" id="myrequest">
                                    MY REQUEST(S)
                                 </div>
                                 <div class="panel-mid" id="myrequestView" style="border-bottom: 1px solid black;">
                                    <div class="row" style="margin:0px;">
                                       <div class="col-xs-12">
                                          <?php
                                             include 'inc/inc_employees_req_list.e2e.php';
                                          ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <div class="panel-top" id="Leaves_Header">Summary Of Leaves</div>
                                 <div class="panel-mid" style="border-bottom: 1px solid black;" id="Leaves_menu">
                                    <?php
                                       include 'inc/inc_emp_leave_balances.php';
                                    ?>
                                 </div>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-12">
                                 <?php
                                    $emp_row = FindFirst("employees","WHERE RefId = $EmpRefId","`BiometricsID`, `CompanyRefId`");
                                    if ($emp_row) {
                                       $CompanyRefId = $emp_row["CompanyRefId"];
                                       $BioID = $emp_row["BiometricsID"];
                                       switch ($CompanyRefId) {
                                          case '2':
                                             include 'inc/inc_emp_notif_2.e2e.php';
                                             break;
                                          case '14':
                                             include 'inc/inc_emp_notif_14.e2e.php';
                                             break;
                                       }
                                    }
                                 ?>
                              </div>
                           </div>
                        </div>
                        <div class="mypanel margin-top" id="panelChangePW" style="display:none">
                           <div class="panel-top">Change Password</div>
                           <div class="panel-mid">
                              <div class="row">
                                 <div class="col-xs-6" style="margin-left: 20px;">
                                    <div class="row">
                                       <div class="form-group">
                                          <label>Current Password:</label>
                                          <input type="password" name="currentToken" id="currentToken" class="form-input">
                                       </div>
                                    </div>
                                    <?php entryAlert("cuPW","Wrong Current Password !!!"); ?>
                                    <div class="row">
                                       <div class="form-group">
                                          <label>New Password:</label>
                                          <input type="password" name="newToken" id="newToken" class="form-input">
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group">
                                          <label>Re-Type Password:</label>
                                          <input type="password" name="reToken" id="reToken" class="form-input">
                                       </div>
                                    </div>
                                    <?php entryAlert("rePW","Mismacthed of New Password !!!"); ?>
                                    <div class="row">
                                       <div class="form-group">
                                          <button
                                          class="btn-cls2-sea"
                                          id="btnChangeNow" name="btnChangeNow">Change Now
                                          </button>
                                          <button
                                          class="btn-cls2-red"
                                          id="btnChangeCancel" name="btnChangeCancel">Cancel
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="panel-bottom"></div>
                        </div>
                     </div>   
                  </div>
                  <!-- Modal -->
                  <div class="modal fade" id="prnModal" role="dialog">
                     <div class="modal-dialog" style="height:90%;width:80%">
                        <div class="mypanel" style="height:100%;">
                           <div class="panel-top bgSilver">
                              <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                                 <i class="fa fa-print" aria-hidden="true"></i>
                              </a>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                           </div>
                           <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                        </div>
                     </div>
                  </div>
                  
            <?php
               }
               footer();
               include "varHidden.e2e.php";
               doHidden("hRptFile","DashboardRpt","");
            ?>
         </div>
      </form>
      
   </body>
</html>


