<?php
	include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';
   include_once pathClass.'DTR_Process.e2e.php';

   $emprefid 			= 123;
	$credit_detail    = FindLast("employeescreditbalance","WHERE EmployeesRefId = '$emprefid' AND NameCredits = 'VL'","*");
   if ($credit_detail) {
      $year = $credit_detail["EffectivityYear"]; 
      $balance_date = $credit_detail["BegBalAsOfDate"];
      $from = date("m",strtotime($balance_date));
      $to = "12";
      $arr_credit       = computeCredit($emprefid,$from,$to,$year);
      foreach ($arr_credit as $xkey => $xvalue) {
         if ($xvalue < 0) $xvalue = 0;
         if ($xkey == "OT") {
            if ($xvalue > 0) {
               $xvalue = convertToHoursMins($xvalue);
            } else {
               $xvalue = 0;
            }
            echo '
            <div class="row margin-top">
               <div class="col-xs-6">
                  '.strtoupper($xkey).'
               </div>
               <div class="col-xs-6">
                  <span class="badge">'.$xvalue.'</span>
               </div>
            </div>
            ';
         } else {
            echo '
            <div class="row margin-top">
               <div class="col-xs-6">
                  '.strtoupper($xkey).'
               </div>
               <div class="col-xs-6">
                  <span class="badge">'.$xvalue.'</span>
               </div>
            </div>
            ';   
         }
      }
   }

   $where = "WHERE EmployeesRefId = '$emprefid' AND isForceLeave = 1";
   $rs = SelectEach("employeesleave",$where);
   if ($rs) {
   	while ($row = mysqli_fetch_assoc($rs)) {
   		$from 		= $row["ApplicationDateFrom"];
   		$to 			= $row["ApplicationDateTo"];
   		$status 		= $row["Status"];
   		$leave_days = count_leave($emprefid,$from,$to);
   		echo $leave_days." -> $status<br>";
   	}
   }
   
?>