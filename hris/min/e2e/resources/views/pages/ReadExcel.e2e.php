<?php
   require_once 'constant.e2e.php';
   require_once $_SESSION['path'].'PHPExcel/Classes/PHPExcel.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';

   /*RUNNING*/
   $MasterFile = false;
   $diag = 0;
   $inputFileName = "EmployeesList.xlsx"; //$target_file;
   $objReader = PHPExcel_IOFactory::createReaderForFile($inputFileName);
   $objPHPExcel = $objReader->load($inputFileName);
   $workSheet = $objPHPExcel->setActiveSheetIndex(0);
   function putValue($cellCoor){
      $cellValue = $GLOBALS['workSheet']->getCell($cellCoor);
      if ($cellValue == "")
         return '<span style="font-family:calibri;font-size:9pt;color:#888;">N/A</span>';
      else
         return strtoupper($cellValue);
   }
   function chkItemnSave($table,$value){
      $_SESSION["sysData"] = "DataLibrary";
      if ($value != "") {
         $value = strConv($value);
         $refid = FindFirst($table,"WHERE Name = '$value'","RefId");
         if ($refid) {
            return $refid;
         } else {
            $Code = date("Ymd",time()).date("His",time());
            $flds = "`Code`,`Name`, `Remarks`,";
            $values = "'$Code','$value','Auto Create from PDS Upload',";
            $refid = f_SaveRecord("NEWSAVE",$table,$flds,$values);
            return $refid;
         }
      } else return 0;
   }
   function dateConv($dateval) {
      if ($dateval != "") {
         if (stripos($dateval,"/") > 0) {
            $dob = explode("/",$dateval);
            if (count($dob) == 3) {
               return $dob[2]."-".$dob[0]."-".$dob[1];
            } else return null;
         } else return null;
      } else return null;
   }
   function intConv($value) {
      if ($value != "") {
         if (is_numeric($value)) return $value;
                            else return 0;
      } else return 0;
   }

   function strConv($value) {
      $retValue = trim($value);
      $retValue = str_replace(","," ",$retValue);
      $retValue = addslashes($retValue);
      return $retValue;
   }

   function saveYN($value) {
      if ($value == "NO") return 0;
                     else return 1;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <script type="text/javascript" src="<?php echo path("jquery/jquery.js") ?>"></script>
      <style type="text/css">
         #PDSbody
         {
            font-family:Arial Narrow;
            font-size:8pt;
         }
         td {padding-bottom:1px;padding-top:1px;}
         table {width:100%;border-collapse:collapse;}
         .tabTitle {font-family:Arial Narrow;font-size:11pt;font-weight:bold;}
         .tabTitle_td {border-top:2px solid #000;border-bottom:2px solid #000;}
         .bgGray {background:#bfbfbf;color:white;}
         .bgGrayLabel {background:#bfbfbf;color:black;vertical-align:top;}
         .answer{font-family:calibri;font-size:9pt;font-weight:600;}
         .SignDate {font-family:Arial Narrow;font-size:10pt;font-weight:bold;color:black;}
         .b-left{border-left:1px solid #000}
         .b-right{border-right:1px solid #000}
         .b-top{border-top:1px solid #000}
         .b-bottom{border-bottom:1px solid #000}
         .bgBlueLabel{background:#b3d1ff;color:black;vertical-align:top;}
         .page-- {max-height:13.5in;width:8.5in;border:1px solid #ddd;}
         @media print {
            .ruler, .noPrint, .noPrint *{display: none !important;}
            .nextpage  {page-break-after:always;}
            .lastpage  {page-break-after:avoid;}
            .page-- {max-height:14in;width:8.5in;border:0px solid #ddd;}
         }
      </style>
   </head>
   <body onload="alert('Process Done');">
      <div class="noPrint" style="padding:5px;position:fixed;top:0;z-index:999;opacity:.50;filter:alpha(opacity=50);width:100%;background-color:#000;">
         <input type="button" value="PRINT PDS" onClick="window.print();">
         <input type="button" value="CLOSE" onClick="window.close();">
      </div>
      <div class="noPrint" style="height:25px;"></div>
      <div id="PDSbody">
            <div class="page-- nextpage">
               <?php
                  $workSheet = $objPHPExcel->setActiveSheetIndex(0);
                  require_once("pds_Page1.e2e.php");
               ?>
            </div>
            <hr class="noPrint">
            <div class="page-- nextpage">
               <?php
                  $workSheet = $objPHPExcel->setActiveSheetIndex(1);
                  require_once("pds_Page2.e2e.php");
               ?>
            </div>
            <hr class="noPrint">
            <div id="" class="page-- nextpage">
               <?php
                  $workSheet = $objPHPExcel->setActiveSheetIndex(2);
                  require_once("pds_Page3.e2e.php");
               ?>
            </div>
            <hr class="noPrint">
            <div class="page-- lastpage">
               <?php
                  $workSheet = $objPHPExcel->setActiveSheetIndex(3);
                  require_once("pds_Page4.e2e.php");
               ?>
            </div>
      </div>
      <script type="text/javascript">
         <?php
            $List_ID1 = ["C7","C8","C9","E10","J10","C11","J11","J12","C13","I13","M13","I15","M15","C17"
                        ,"I17","M17","C19","I19","C20","I20","M20","C22","C24","C26","C27","C28","C29"
                        ,"M24","I26","I27","I28","I29","C31","C32","C34","C35","C36","C37","C38","C39","C40","C42","C43","C44","C45","C46"
                        ,"I32","I34","I35","I36","I37","I38","I39","I40","I42","I43","I44","I45"
                        ,"M32","M34","M35","M36","M37","M38","M39","M40","M42","M43","M44","M45"
                        ,"C51","C52","C53","C54","C55","C56","C57","C58","C59","C60","C61"
                        ,"F51","F52","F53","F54","F55","F56","F57","F58","F59","F60","F61"
                        ,"J51","J52","J53","J54","J55","J56","J57","J58","J59","J60","J61"
                        ,"K51","K52","K53","K54","K55","K56","K57","K58","K59","K60","K61"
                        ,"L51","L52","L53","L54","L55","L56","L57","L58","L59","L60","L61"
                        ,"M51","M52","M53","M54","M55","M56","M57","M58","M59","M60","M61"
                        ,"N51","N52","N53","N54","N55","N56","N57","N58","N59","N60","N61"
                        ,"I22","M22","I24","G33","G41"
                        ];
            $List_ID2 = [
               "A4","F4","G4","I4","L4","M4",
               "A5","F5","G5","I5","L5","M5",
               "A6","F6","G6","I6","L6","M6",
               "A7","F7","G7","I7","L7","M7",
               "A8","F8","G8","I8","L8","M8",
               "A9","F9","G9","I9","L9","M9",
               "A10","F10","G10","I10","L10","M10",
               "O6","Q6","R6","U6","X6","Y6","Z6","AA6",
               "O7","Q7","R7","U7","X7","Y7","Z7","AA7",
               "O8","Q8","R8","U8","X8","Y8","Z8","AA8",
               "O9","Q9","R9","U9","X9","Y9","Z9","AA9",
               "O10","Q10","R10","U10","X10","Y10","Z10","AA10",
               "O11","Q11","R11","U11","X11","Y11","Z11","AA11",
               "O12","Q12","R12","U12","X12","Y12","Z12","AA12",
               "O13","Q13","R13","U13","X13","Y13","Z13","AA13",
               "O14","Q14","R14","U14","X14","Y14","Z14","AA14",
               "O15","Q15","R15","U15","X15","Y15","Z15","AA15",
               "O16","Q16","R16","U16","X16","Y16","Z16","AA16",
               "O17","Q17","R17","U17","X17","Y17","Z17","AA17",
               "O18","Q18","R18","U18","X18","Y18","Z18","AA18",
               "O19","Q19","R19","U19","X19","Y19","Z19","AA19",
               "O20","Q20","R20","U20","X20","Y20","Z20","AA20"
            ];
            $List_ID3 = [
               "A5","E5","F5","G5","H5",
               "A6","E6","F6","G6","H6",
               "A7","E7","F7","G7","H7",
               "A8","E8","F8","G8","H8",
               "A9","E9","F9","G9","H9",
               "A10","E10","F10","G10","H10",
               "A11","E11","F11","G11","H11",
               "L7","Q7","R7","S7","T7","U7",
               "L8","Q8","R8","S8","T8","U8",
               "L9","Q9","R9","S9","T9","U9",
               "L10","Q10","R10","S10","T10","U10",
               "L11","Q11","R11","S11","T11","U11",
               "L12","Q12","R12","S12","T12","U12",
               "L13","Q13","R13","S13","T13","U13",
               "L14","Q14","R14","S14","T14","U14",
               "L15","Q15","R15","S15","T15","U15",
               "L16","Q16","R16","S16","T16","U16",
               "L17","Q17","R17","S17","T17","U17",
               "L18","Q18","R18","S18","T18","U18",
               "X7","Y7","Z7",
               "X8","Y8","Z8",
               "X9","Y9","Z9",
               "X10","Y10","Z10",
               "X11","Y11","Z11",
               "X12","Y12","Z12",
               "X13","Y13","Z13"
            ];
            $List_ID4 = [];
            $CellValue = "";
            $page = ["page1_","page2_","page3_","page4_"];
            for ($k=0;$k<count($page);$k++) {
               $workSheet = $objPHPExcel->setActiveSheetIndex($k);
               if ($k==0) {$cellCoor = $List_ID1;}
               else if ($k==1) {$cellCoor = $List_ID2;}
               else if ($k==2) {$cellCoor = $List_ID3;}
               else if ($k==3) {$cellCoor = $List_ID4;}
               for ($s=0;$s<count($cellCoor);$s++) {
                  $cVal = putValue($cellCoor[$s]);
                  $pre = $page[$k];
                  if ($s < count($cellCoor)) {
                     $CellValue .= $pre.$cellCoor[$s].":'".$cVal."',";
                  } else {
                     $CellValue .= $pre.$cellCoor[$s].":'".$cVal."'";
                  }
               }
            }
            echo "\n".'var n = {'.$CellValue.'};'."\n";
         ?>
         $(document).ready(function() {
            $(".answer").each(function (){
               var objID = $(this).attr("id");
               if (objID) {
                  $(this).html(n[objID]);
               }
            });
         });
      </script>
      <?php
         /*SAVE IN DBASE TABLE*/
         $values = "";
         $flds = "";
         $null = "";
         $wRemarks = 'Uploaded via PDS/Excel';
         $workSheet = $objPHPExcel->setActiveSheetIndex(0);
         $wLastName = $workSheet->getCell("C7");
         $wFirstName = $workSheet->getCell("C8");
         $wMiddleName = $workSheet->getCell("C9");
         $wExtName = $workSheet->getCell("N8");
         //$wNickName = $workSheet->getCell("");
         //$wContactNo = $workSheet->getCell("");
         $wBirthPlace = $workSheet->getCell("C11");
         $wBirthDate = dateConv($workSheet->getCell("E10"));
         $wEmailAdd = $workSheet->getCell("I29");
         $wSex = "";
         if ($workSheet->getCell("C12") == "Male")
            $wSex = "M";
         else if ($workSheet->getCell("C12") == "Female")
            $wSex = "F";
         $wCivilStatus = "";
         switch ($workSheet->getCell("C13")) {
            case "Single":
               $wCivilStatus = "Si";
            break;
            case "Married":
               $wCivilStatus = "Ma";
            break;
            case "Annuled":
               $wCivilStatus = "An";
            break;
            case "Widowed":
               $wCivilStatus = "Wi";
            break;
            case "Separated":
               $wCivilStatus = "Se";
            break;
            case "Others":
               $wCivilStatus = "Ot";
            break;
         }
         $wCitizenshipRefId = chkItemnSave("Citizenship",$workSheet->getCell("J11"));
         if ($workSheet->getCell("J10") == "Filipino")
            $wisFilipino = 1;
         else
            $wisFilipino = 0;
         if ($workSheet->getCell("J11") == "By Birth")
            $wisByBirthOrNatural = 1;
         else
            $wisByBirthOrNatural = 0;
         $wCountryCitizenRefId = chkItemnSave("Country",$workSheet->getCell("J12"));
         $wHeight = $workSheet->getCell("C17");
         $wWeight = $workSheet->getCell("C19");
         $wBloodTypeRefId = chkItemnSave("BloodType",$workSheet->getCell("C20"));
         $wPAGIBIG = $workSheet->getCell("C24");
         $wGSIS = $workSheet->getCell("C22");
         $wPHIC = $workSheet->getCell("C26");
         $wTIN = $workSheet->getCell("C28");
         $wSSS = $workSheet->getCell("C27");
         $wAgencyId = $workSheet->getCell("C29");
         $wResiHouseNo = $workSheet->getCell("I13");
         $wResiStreet = $workSheet->getCell("M13");
         $wResiBrgy = $workSheet->getCell("I15")." ".$workSheet->getCell("M15");
         $wResiAddCityRefId = chkItemnSave("City",$workSheet->getCell("I17"));
         $wResiAddProvinceRefId = chkItemnSave("Province",$workSheet->getCell("M17"));
         //$wResiCountryRefId = $workSheet->getCell("");
         $wResidentTelNo = $workSheet->getCell("I27");
         $wPermanentHouseNo = $workSheet->getCell("I20");
         $wPermanentStreet = $workSheet->getCell("M20");
         $wPermanentBrgy = $workSheet->getCell("I22")." ".$workSheet->getCell("M22");
         $wPermanentAddCityRefId = chkItemnSave("City",$workSheet->getCell("I24"));
         $wPermanentAddProvinceRefId = chkItemnSave("Province",$workSheet->getCell("M24"));
         //$wPermanentCountryRefId = $workSheet->getCell("");
         $wTelNo = $workSheet->getCell("I27");
         $wMobileNo = $workSheet->getCell("I28");
         $workSheet = $objPHPExcel->setActiveSheetIndex(3);
         $wGovtIssuedID = $workSheet->getCell("C60");
         $wGovtIssuedIDNo = $workSheet->getCell("C61");
         $wGovtIssuedIDPlace = $workSheet->getCell("C63");
         $values .= "'$wLastName','$wFirstName','$wMiddleName','$wExtName','$null',";
         $values .= "'$null','$wBirthDate','$wBirthPlace','$wEmailAdd','$wSex',";
         $values .= "'$wCivilStatus',$wCitizenshipRefId,'$wisFilipino','$wisByBirthOrNatural',$wCountryCitizenRefId,";
         $values .= "'$wHeight','$wWeight','$wBloodTypeRefId','$wPAGIBIG','$wGSIS',";
         $values .= "'$wPHIC','$wTIN','$wSSS','$wAgencyId','$wResiHouseNo',";
         $values .= "'$wResiStreet','$wResiBrgy',$wResiAddCityRefId,$wResiAddProvinceRefId,'$null',";
         $values .= "'$wResidentTelNo','$wPermanentHouseNo','$wPermanentStreet','$wPermanentBrgy',$wPermanentAddCityRefId,";
         $values .= "$wPermanentAddProvinceRefId,'$null','$wTelNo','$wMobileNo','$wRemarks',";
         $values .= "'$wGovtIssuedID', '$wGovtIssuedIDNo', '$wGovtIssuedIDPlace',";
         $flds .= "`LastName`,`FirstName`,`MiddleName`,`ExtName`,`NickName`,";
         $flds .= "`ContactNo`,`BirthDate`,`BirthPlace`,`EmailAdd`,`Sex`,";
         $flds .= "`CivilStatus`,CitizenshipRefId,isFilipino,isByBirthOrNatural,CountryCitizenRefId,";
         $flds .= "`Height`,`Weight`,`BloodTypeRefId`,`PAGIBIG`,`GSIS`,";
         $flds .= "`PHIC`,`TIN`,`SSS`,`AgencyId`,`ResiHouseNo`,";
         $flds .= "`ResiStreet`,`ResiBrgy`,`ResiAddCityRefId`,`ResiAddProvinceRefId`,`ResiCountryRefId`,";
         $flds .= "`ResidentTelNo`,`PermanentHouseNo`,`PermanentStreet`,`PermanentBrgy`,`PermanentAddCityRefId`,";
         $flds .= "`PermanentAddProvinceRefId`,`PermanentCountryRefId`,`TelNo`,`MobileNo`,`Remarks`,";
         $flds .= "`GovtIssuedID`, `GovtIssuedIDNo`, `GovtIssuedIDPlace`,";
         $empRefId = f_SaveRecord("NEWSAVE","employees",$flds,$values);
         if (!is_numeric($empRefId)) {
            echo "<li class='noPrint'>ERR::$empRefId</li>";
         }

         $workSheet = $objPHPExcel->setActiveSheetIndex(0);
         if ($empRefId) {
            $flds = "";
            $values = "";
            $wSpouseLastName = $workSheet->getCell("C31");
            $wSpouseFirstName = $workSheet->getCell("C32");
            $wSpouseMiddleName = $workSheet->getCell("C34");
            $wSpouseExtName = $workSheet->getCell("G33");
            $wSpouseBirthDate = "";
            $wOccupationRefId = chkItemnSave("Occupations",$workSheet->getCell("C35"));
            $wEmployersName = $workSheet->getCell("C36");
            $wBusinessAddress = $workSheet->getCell("C37");
            $wSpouseMobileNo = $workSheet->getCell("C38");
            $wFatherLastName = $workSheet->getCell("C39");
            $wFatherFirstName = $workSheet->getCell("C40");
            $wFatherMiddleName = $workSheet->getCell("C42");
            $wFatherExtName = $workSheet->getCell("G41");
            // MISSING Mothers Maiden Name Field??
            $wMotherLastName = $workSheet->getCell("C44");
            $wMotherFirstName = $workSheet->getCell("C45");
            $wMotherMiddleName = $workSheet->getCell("C46");
            $wMotherExtName = "";

            $flds .= "`EmployeesRefId`, `SpouseLastName`, `SpouseFirstName`, `SpouseMiddleName`, `SpouseExtName`,";
            $flds .= "`SpouseBirthDate`, `OccupationsRefId`, `EmployersName`, `BusinessAddress`, `SpouseMobileNo`,";
            $flds .= "`FatherLastName`, `FatherFirstName`, `FatherMiddleName`, `FatherExtName`, `MotherLastName`,";
            $flds .= "`MotherFirstName`, `MotherMiddleName`, `MotherExtName`, `Remarks`,";
            $values .= "$empRefId, '$wSpouseLastName', '$wSpouseFirstName', '$wSpouseMiddleName', '$wSpouseExtName',";
            $values .= "'$wSpouseBirthDate', $wOccupationRefId, '$wEmployersName', '$wBusinessAddress', '$wSpouseMobileNo',";
            $values .= "'$wFatherLastName', '$wFatherFirstName', '$wFatherMiddleName', '$wFatherExtName', '$wMotherLastName',";
            $values .= "'$wMotherFirstName', '$wMotherMiddleName', '$wMotherExtName', '$wRemarks',";
            $empFamRefId = f_SaveRecord("NEWSAVE","employeesfamily",$flds,$values);

            if (!is_numeric($empFamRefId)) {
               echo "<li class='noPrint'>ERR::$empFamRefId</li>";
            }


            /*CHILDREN*/
            $countSave = 0;
            $idx = 31;
            for ($j=1;$j<=15;$j++) {
               if (($idx + $j)!=33 && ($idx + $j)!=41 && ($idx + $j) <= 45) {
                  $wFullName = $workSheet->getCell("I".($idx + $j));
                  if ($wFullName != "") {
                     $wBirthDate = $workSheet->getCell("M".($idx + $j));
                     $wBirthDate = dateConv($wBirthDate);
                     $wGender = "";

                     $flds = "";
                     $values = "";
                     $flds .= "`EmployeesRefId`, `FullName`, `BirthDate`,";
                     $flds .= "`Gender`, `Remarks`,";
                     $values .= "$empRefId, '$wFullName', '$wBirthDate',";
                     $values .= "'$wGender', '$wRemarks',";
                     $empChildRefId = f_SaveRecord("NEWSAVE","employeeschild",$flds,$values);
                     if ($empChildRefId > 0) {
                        $countSave++;
                     }
                  }
               }
            }
            if (!is_numeric($empChildRefId)) {
               echo "<li class='noPrint'>ERR::$empChildRefId</li>";
            }

            /*EDUC BG*/
            $countSave = 0;
            $idx = 50;
            for ($j=1;$j<=12;$j++) {
               if (($idx + $j) <= 60) {
                  $educLevel = 0;
                  if (($idx + $j) >= 51 && (($idx + $j) <= 52)) {
                     $educLevel = 1;
                  } else if (($idx + $j) >= 53 && (($idx + $j) <= 54)) {
                     $educLevel = 2;
                  } else if (($idx + $j) >= 55 && (($idx + $j) <= 56)) {
                     $educLevel = 3;
                  } else if (($idx + $j) >= 57 && (($idx + $j) <= 58)) {
                     $educLevel = 4;
                  } else if (($idx + $j) >= 59 && (($idx + $j) <= 60)) {
                     $educLevel = 5;
                  }

                  if (($idx + $j) >= 51 && (($idx + $j) <= 54)) {
                     $wCourseRefId = 0;
                  } else {
                     $wCourseRefId = chkItemnSave("Course",$workSheet->getCell("F".($idx + $j)));
                  }

                  $wSchoolsRefId = chkItemnSave("Schools",$workSheet->getCell("C".($idx + $j)));
                  if ($wSchoolsRefId > 0) {
                     $wYearGraduated = intConv($workSheet->getCell("M".($idx + $j)));
                     $wHighestGrade = $workSheet->getCell("L".($idx + $j));
                     $wDateFrom = dateConv($workSheet->getCell("J".($idx + $j)));
                     $wDateTo = dateConv($workSheet->getCell("K".($idx + $j)));
                     $wHonors = $workSheet->getCell("N".($idx + $j));

                     $flds = "";
                     $values = "";
                     $flds .= "`EmployeesRefId`, `LevelType`, `SchoolsRefId`, `CourseRefId`, `YearGraduated`,";
                     $flds .= "`HighestGrade`, `DateFrom`, `DateTo`, `Honors`, `Remarks`,";
                     $values .= "$empRefId, '$educLevel', $wSchoolsRefId, $wCourseRefId, '$wYearGraduated',";
                     $values .= "'$wHighestGrade', '$wDateFrom', '$wDateTo', '$wHonors', '$wRemarks',";
                     $empEducRefId = f_SaveRecord("NEWSAVE","employeeseduc",$flds,$values);
                     if ($empEducRefId > 0) {
                        $countSave++;
                     }
                  }
               }
            }
            if (!is_numeric($empEducRefId)) {
               echo "<li class='noPrint'>ERR::$empEducRefId</li>";
            }

            /*CIVIL SERVICE ELIGIBILITY */
            $countSave = 0;
            $workSheet = $objPHPExcel->setActiveSheetIndex(1);
            $idx = 3;
            for ($j=1;$j<=8;$j++) {
               if (($idx + $j) <= 10) {

                  $wCareerServiceRefId = chkItemnSave("CareerService",$workSheet->getCell("A".($idx + $j)));
                  if ($wCareerServiceRefId > 0) {
                     $wRating = intConv($workSheet->getCell("F".($idx + $j)));
                     $wExamDate = dateConv($workSheet->getCell("G".($idx + $j)));
                     $wExamPlace = $workSheet->getCell("I".($idx + $j));
                     $wLicenseNo = $workSheet->getCell("L".($idx + $j));
                     $wLicenseReleasedDate = dateConv($workSheet->getCell("M".($idx + $j)));
                     $flds = "";
                     $values = "";
                     $flds .= "`EmployeesRefId`, `CareerServiceRefId`, `Rating`, `ExamDate`, `ExamPlace`,";
                     $flds .= "`LicenseNo`, `LicenseReleasedDate`, `Remarks`,";
                     $values .= "$empRefId, $wCareerServiceRefId, '$wRating', '$wExamDate', '$wExamPlace',";
                     $values .= "'$wLicenseNo', '$wLicenseReleasedDate', '$wRemarks',";
                     $empEligibRefId = f_SaveRecord("NEWSAVE","employeeselegibility",$flds,$values);
                     if ($empEligibRefId) {
                        $countSave++;

                     }
                  }
               }
            }
            if (!is_numeric($empEligibRefId)) {
               echo "<li class='noPrint'>ERR::$empEligibRefId</li>";
            }

            /*V. WORK EXPERIENCE*/
            $idx = 5;
            $countSave = 0;
            for ($j=1;$j<=37;$j++) {
               if (($idx + $j) <= 42) {
                  $wPositionRefId = chkItemnSave("Position",$workSheet->getCell("R".($idx + $j)));
                  if ($wPositionRefId > 0) {
                     $wAgencyRefId = chkItemnSave("Agency",$workSheet->getCell("U".($idx + $j)));
                     $wStepIncrementRefId = chkItemnSave("StepIncrement",$workSheet->getCell("Y".($idx + $j)));
                     $wApptStatusRefId = chkItemnSave("ApptStatus",$workSheet->getCell("Z".($idx + $j)));
                     $wWorkStartDate = dateConv($workSheet->getCell("O".($idx + $j)));
                     $wWorkEndDate = dateConv($workSheet->getCell("Q".($idx + $j)));
                     $wSalaryAmount = $workSheet->getCell("X".($idx + $j));
                     if ($workSheet->getCell("G".($idx + $j)) == "Y") {
                        $wisGovtService = 1;
                     } else {
                        $wisGovtService = 0;
                     }

                     //$wLWOP = $workSheet->getCell("G".($idx + $j))
                     //$wReason = $workSheet->getCell("G".($idx + $j))
                     //$wExtDate = dateConv($workSheet->getCell("G".($idx + $j)));
                     //$wSeparatedDate = dateConv($workSheet->getCell("G".($idx + $j)));
                     //$wEmpStatusRefId = chkItemnSave("EmpStatus",$workSheet->getCell("A".($idx + $j)));
                     //$wSalaryGradeRefId = chkItemnSave("SalaryGrade",$workSheet->getCell("A".($idx + $j)));
                     //$wPayrateRefId = chkItemnSave("Payrate",$workSheet->getCell("A".($idx + $j)));
                     //$wPayGroup = $workSheet->getCell("G".($idx + $j))

                     $flds = "";
                     $values = "";
                     $flds .= "`EmployeesRefId`, `AgencyRefId`, `PositionRefId`,";
                     $flds .= "`StepIncrementRefId`, `WorkStartDate`, `WorkEndDate`, `SalaryAmount`,";
                     $flds .= "`ApptStatusRefId`, `isGovtService`, `Remarks`,";
                     $values .= "$empRefId, '$wAgencyRefId', $wPositionRefId,";
                     $values .= "$wStepIncrementRefId, '$wWorkStartDate', '$wWorkEndDate', $wSalaryAmount,";
                     $values .= "$wApptStatusRefId, $wisGovtService, '$wRemarks',";
                     $empWorkExpRefId = f_SaveRecord("NEWSAVE","employeesworkexperience",$flds,$values);
                     if ($empWorkExpRefId > 0) {
                        $countSave++;
                     }
                  }
               }
            }
            if (!is_numeric($empWorkExpRefId)) {
               echo "<li class='noPrint'>ERR::$empWorkExpRefId</li>";
            }

            $workSheet = $objPHPExcel->setActiveSheetIndex(2);
            $idx = 4;
            $countSave = 0;
            for ($j=1;$j<=8;$j++) {
               if (($idx + $j) <= 11) {
                  $wOrganizationRefId = chkItemnSave("Organization",$workSheet->getCell("A".($idx + $j)));
                  if ($wOrganizationRefId > 0) {
                     $wStartDate = dateConv($workSheet->getCell("E".($idx + $j)));
                     $wEndDate = dateConv($workSheet->getCell("F".($idx + $j)));
                     $wNumofHrs = intConv($workSheet->getCell("G".($idx + $j)));
                     $wWorksNature = $workSheet->getCell("H".($idx + $j));
                     $flds    = "";
                     $values  = "";
                     $flds   .= "`EmployeesRefId`, `OrganizationRefId`, `StartDate`,";
                     $flds   .= "`EndDate`, `NumofHrs`, `WorksNature`, `Remarks`,";
                     $values .= "$empRefId, $wOrganizationRefId, '$wStartDate',";
                     $values .= "'$wEndDate', $wNumofHrs, '$wWorksNature', '$wRemarks',";
                     $empVoluntaryRefId = f_SaveRecord("NEWSAVE","employeesvoluntary",$flds,$values);
                     if ($empVoluntaryRefId > 0) {
                        $countSave++;
                     }
                  }
               }
            }
            if (!is_numeric($empVoluntaryRefId)) {
               echo "<li class='noPrint'>ERR::$empVoluntaryRefId</li>";
            }

            /*VII*/
            $idx = 6;
            $countSave = 0;
            for ($j=1;$j<=12;$j++) {
               if (($idx + $j) <= 18) {
                  $wSeminar = $workSheet->getCell("L".($idx + $j));
                  $wSeminarRefId = chkItemnSave("Seminars",$wSeminar);
                  if ($wSeminarRefId > 0) {
                     $wDescription = strConv($workSheet->getCell("L".($idx + $j)));
                     $wStartDate = dateConv($workSheet->getCell("Q".($idx + $j)));
                     $wEndDate = dateConv($workSheet->getCell("R".($idx + $j)));
                     $wNumofHrs = intConv($workSheet->getCell("S".($idx + $j)));
                     $wSponsor = $workSheet->getCell("U".($idx + $j));
                     $wSponsorRefId = chkItemnSave("Sponsor",$wSponsor);
                     $wSeminarPlaceRefId = 0;
                     $wSeminarType = $workSheet->getCell("T".($idx + $j));
                     $wSeminarTypeRefId = chkItemnSave("SeminarType",$wSeminarType);

                     $flds    = "";
                     $values  = "";
                     $flds .= "`EmployeesRefId`, `SeminarsRefId`, `StartDate`, `EndDate`, `NumofHrs`,";
                     $flds .= "`SponsorRefId`, `SeminarPlaceRefId`, `SeminarTypeRefId`, `Description`, `Remarks`,";
                     $values .= "$empRefId, $wSeminarRefId, '$wStartDate', '$wEndDate', $wNumofHrs,";
                     $values .= "$wSponsorRefId, $wSeminarPlaceRefId, $wSeminarTypeRefId, '$wDescription', '$wRemarks',";
                     $empTrainingRefId = f_SaveRecord("NEWSAVE","employeestraining",$flds,$values);
                     if ($empTrainingRefId > 0) {
                        $countSave++;
                     }
                  }
               }
            }
            if (!is_numeric($empTrainingRefId)) {
               echo "<li class='noPrint'>ERR::$empTrainingRefId</li>";
            }

            /*VIII*/
            $idx = 6;
            $countSave = 0;
            for ($j=1;$j<=7;$j++) {
               if (($idx + $j) <= 13) {
                  $wSkills = strConv($workSheet->getCell("X".($idx + $j)));
                  $wRecognition = strConv($workSheet->getCell("Y".($idx + $j)));
                  $wAffiliates = strConv($workSheet->getCell("Z".($idx + $j)));
                  if ($wSkills!="" || $wRecognition!="" || $wAffiliates!="") {
                     $flds = "`EmployeesRefId`, `Skills`, `Recognition`, `Affiliates`, `Remarks`,";
                     $values = "$empRefId, '$wSkills', '$wRecognition', '$wAffiliates', '$wRemarks',";
                     $empOtherInfoRefId = f_SaveRecord("NEWSAVE","employeesotherinfo",$flds,$values);
                     if ($empOtherInfoRefId > 0) {
                        $countSave++;
                     }
                  }
               }
            }
            if (!is_numeric($empOtherInfoRefId)) {
               echo "<li class='noPrint'>ERR::$empOtherInfoRefId</li>";
            }
            /*PDSQ Page 4*/
            $workSheet = $objPHPExcel->setActiveSheetIndex(3);
            $wQ1a    = saveYN($workSheet->getCell("F4")); // 34
            $wQ1aexp = strConv($workSheet->getCell("G5"));
            $wQ1b    = saveYN($workSheet->getCell("F8")); // 34
            $wQ1bexp = strConv($workSheet->getCell("G9"));
            $wQ2a    = saveYN($workSheet->getCell("F12")); // 35
            $wQ2aexp = strConv($workSheet->getCell("G13"));
            $wQ2b    = saveYN($workSheet->getCell("F16")); // 35
            $wQ2bexp = strConv($workSheet->getCell("G17"));
            $wQ3a    = saveYN($workSheet->getCell("F24")); // 36
            $wQ3aexp = strConv($workSheet->getCell("G25"));
            $wQ4a    = saveYN($workSheet->getCell("F28")); // 37
            $wQ4aexp = strConv($workSheet->getCell("G29"));
            $wQ5a    = saveYN($workSheet->getCell("F33")); // 38
            $wQ5aexp = strConv($workSheet->getCell("G34"));
            $wQ5b    = saveYN($workSheet->getCell("F36")); // 38
            $wQ5bexp = strConv($workSheet->getCell("G37"));
            $wQ6a    = saveYN($workSheet->getCell("F39")); // 39
            $wQ6aexp = strConv($workSheet->getCell("G40"));
            $wQ7a    = saveYN($workSheet->getCell("F43")); // 40
            $wQ7aexp = strConv($workSheet->getCell("G44"));
            $wQ7b    = saveYN($workSheet->getCell("F46")); // 40
            $wQ7bexp = strConv($workSheet->getCell("G47"));
            $wQ7c    = saveYN($workSheet->getCell("F49")); // 40
            $wQ7cexp = strConv($workSheet->getCell("G50"));
            $wQ2DateFiled = $workSheet->getCell("J19");
            $wQ2CaseStatus = $workSheet->getCell("J20");

            $flds    = "";
            $values  = "";
            $flds .= "`EmployeesRefId`, `Q1a`, `Q1aexp`, `Q1b`, `Q1bexp`,";
            $flds .= "`Q2a`, `Q2aexp`, `Q2b`, `Q2bexp`, `Q3a`,";
            $flds .= "`Q3aexp`, `Q4a`, `Q4aexp`, `Q5a`, `Q5aexp`,";
            $flds .= "`Q5b`, `Q5bexp`, `Q6a`, `Q6aexp`, `Q7a`,";
            $flds .= "`Q7aexp`, `Q7b`, `Q7bexp`, `Q7c`, `Q7cexp`,";
            $flds .= "`Q2DateFiled`, `Q2CaseStatus`,";

            $values .= "$empRefId, $wQ1a, '$wQ1aexp', $wQ1b, '$wQ1bexp',";
            $values .= "$wQ2a, '$wQ2aexp', $wQ2b, '$wQ2bexp', $wQ3a,";
            $values .= "'$wQ3aexp', $wQ4a, '$wQ4aexp', $wQ5a, '$wQ5aexp',";
            $values .= "$wQ5b, '$wQ5bexp', $wQ6a, '$wQ6aexp', $wQ7a,";
            $values .= "'$wQ7aexp', $wQ7b, '$wQ7bexp', $wQ7c, '$wQ7cexp',";
            $values .= "'$wQ2DateFiled', '$wQ2CaseStatus',";
            $empPDSQRefId = f_SaveRecord("NEWSAVE","employeespdsq",$flds,$values);
            if (!is_numeric($empPDSQRefId)) {
               echo "<li class='noPrint'>ERR::$empPDSQRefId</li>";
            }


            /* References */
            $idx = 53;
            $countSave = 0;
            for ($j=1;$j<=3;$j++) {
               if (($idx + $j) <= 56) {
                  $wName = strConv($workSheet->getCell("A".($idx + $j)));
                  $wAddress = strConv($workSheet->getCell("E".($idx + $j)));
                  $wContactNo = strConv($workSheet->getCell("F".($idx + $j)));
                  if ($wName!="" || $wAddress!="" || $wContactNo!="") {
                     $flds = "`EmployeesRefId`, `Name`, `Address`, `ContactNo`, `Remarks`,";
                     $values = "$empRefId, '$wName', '$wAddress', '$wContactNo', '$wRemarks',";
                     $empRefRefId = f_SaveRecord("NEWSAVE","employeesreference",$flds,$values);
                     if ($empRefRefId > 0) {
                        $countSave++;
                     }

                  }
               }
            }
            if (!is_numeric($empRefRefId)) {
               echo "<li class='noPrint'>ERR::$empRefRefId</li>";
            }
         } else {
            echo "<li class='noPrint'>ERR::$empRefId</li>";
         }
      ?>
   </body>
</html>

