<?php
session_start();

include "conn.e2e.php";
require_once $_SESSION['Classes'].'0620functions.e2e.php';
include "colors.e2e.php";

$mainFILE  = getvalue("hMainFile");
$table     = getvalue("hTable");
$Session   = getvalue("hSess");
$task      = getvalue("htask");
$studrefid = getvalue("StudentRefId");
$refid     = getvalue("hRefID");
echo "var d = document.xForm;";
switch ($task) {
   case "SAVEA":
      $p = fopen('syslog/ADDEDRECORD_'.$today.'.log', 'a+');
      $fv    = getvalue('hfv');   
      $items = explode(",",$fv);
      $flds      = "";
      $fldname   = "";
      $fieldname = "";
      $fldvalue  = "";
      $fval      = "";
      $StudentRefId = "";
      $newGLVL = "";
      
      for ($j=0;$j<(sizeof($items) - 1);$j++) {
         $flds = explode("|",$items[$j]);
         $obj_TYPE  = $flds[0];
         $obj_NAME  = $flds[1];
         $obj_VALUE = $flds[2];
         
         $fieldname = substr($obj_NAME,13);
         $datatype  = substr($obj_NAME,9,3);
         $fldname   = $fieldname.", ".$fldname;
         $fval      = $obj_VALUE;
         include "incObjs.php";
         if ($fieldname == "Password") $fval = f_encode($fval);
         if ($fieldname == "GradeLevel") $newGLVL = $fval;
         if ($fieldname == "StudentRefId") $StudentRefId = $fval;
         
         
         if ($datatype == "fde" || $datatype == "fin") {
            $fldvalue = $fval.", ".$fldvalue;
         }
         else {
            $fldvalue = "'$fval',".$fldvalue;
            if ($datatype == "ftm") {
               $fldvalue = str_replace(":","",$fldvalue);
            }
            
         }
      }
      $lgSaveSucces = f_SaveRecord("ADD",$table,$fldname,$fldvalue);
      if ($lgSaveSucces) $msg = "Record Successfully Saved!!!";
                    else $msg = "Saving Error!!!";
      echo "alert('$msg');";
      if ($lgSaveSucces) {
         fwrite($p,$datelog." ".$table." - ".$fldname." - ".$fldvalue."\n");
         if (getvalue("updstud") == "yes" || getvalue("updstud") == "true" ) {
            $fieldnvalue = "GradeLevel = $newGLVL,".$trackingE;
            $qry_update = "UPDATE Students SET $fieldnvalue WHERE SchoolId = $SchoolId AND RefId = $StudentRefId";
            if (mysqli_query($conn, $qry_update)) 
               $msg = "Student Record Updated!!!";
            else 
               $msg = "Error updating record >> ".mysqli_error($conn);
            echo "alert('$msg');";
         }
         if (getvalue("updenrll") == "true" && $FeeId == 3 /*Tuition Fee*/) {
            $msg = updateEnrollmentBalance($SchoolId,$EnrollmentRefId,$StudentRefId);
            echo "alert('$msg');";
         }
      }
      fclose($p);
      echo "self.location = '".$mainFILE.".php?".$_SESSION['sess_gParam']."&hRefID=".$refid."&StudentRefId=".$StudentRefId."';";
   break;
   case "SAVEE":
      if ($refid) {
         $fv = getvalue('hfv');   
         $items = explode(",",$fv);
         $flds = "";
         $fldname = "";
         $fieldname = "";
         $fldvalue = "";
         $fieldnvalue = "";
         
         for ($j=0;$j<(sizeof($items) - 1);$j++) {
            $flds = explode("|",$items[$j]);
            $obj_TYPE  = $flds[0];
            $obj_NAME  = $flds[1];
            $obj_VALUE = $flds[2];
            $fieldname = substr($flds[1],13);
            if ($fieldname !== "Password") {
               $datatype  = substr($flds[1],9,3);
               $fldname   = $fieldname.", ".$fldname;
               $fval      = $flds[2];
               if ($datatype=="fde"||$datatype=="fin") $fldvalue = $fval;
               else {
                  $fldvalue = "'".$fval."'";
                  if ($datatype=="ftm") $fldvalue = str_replace(":","",$fldvalue);
               }
               $fieldnvalue = $fieldnvalue . $fieldname." = ".$fldvalue.",";
               include "incObjs.php";
            }
            
         }
         $change = fopen('syslog/DataChanged_'.$today.'.log', 'a+'); 
         $fieldnvalue = $fieldnvalue.$trackingE;
         
            /*DATA LOGS*/         
            $rs       = SelectEach($table,"WHERE RefId = $refid LIMIT 1");
            $numField = mysql_num_fields($rs);
            $row      = mysql_fetch_assoc($rs);
            $data     = "";
            for ( $i = 0; $i < $numField; $i++ ) {
               $fldname = mysql_field_name($rs, $i);
               $fldvalue = $row[$fldname];
               $data .= "$fldname=$fldvalue|";
            }
            fwrite($change,"--- MODIFY --- \n");
            fwrite($change,$datelog."--".str_pad($_SESSION['sess_user'],12," ")."-- FR >> ".$data."\n");
            /*-------------------*/
         
         $qry_update = "UPDATE ".$table." SET ".$fieldnvalue." WHERE RefId = ".$refid;
         if (mysqli_query($conn, $qry_update)) {
            fwrite($change,$datelog."--".str_pad($_SESSION['sess_user'],12," ")."-- TO >> ".$qry_update."\n\n");
            if (getvalue("updenrll") == "true" && $FeeId == 3 /*Tuition Fee*/) {
               $msg = updateEnrollmentBalance($SchoolId,$EnrollmentRefId,$StudentRefId);
               echo "alert('$msg');";
            }
            echo "self.location = '".$mainFILE.".php?hRefID=$refid';";
         }
         else echo "alert('Error updating record >> ".mysqli_error($conn)."');";
         fclose($change);
      }
      else {
         echo "alert('NO SELECTED REF.ID');";  
         echo "self.location = '".$mainFILE.".php?hRefID=".$refid."&StudentRefId=".$studrefid."';";
      }
   break;
   case "setEdit":
   break;
   case "deleteRecord":
      $change = fopen('syslog/DataChanged_'.$today.'.log', 'a+'); 
      $refid = getvalue('hRefID');
      
      /*-------------------*/
            
      if ($refid) {
         /*DATA LOGS*/         
         $rs       = SelectEach($table,"WHERE RefId = $refid LIMIT 1");
         $numField = mysql_num_fields($rs);
         $row      = mysql_fetch_assoc($rs);
         $data     = "";
         for ( $i = 0; $i < $numField; $i++ ) {
            $fldname = mysql_field_name($rs, $i);
            $fldvalue = $row[$fldname];
            $data .= "$fldname=$fldvalue|";
         }
         fwrite($change,$datelog."--".str_pad($_SESSION['sess_user'],12," ")."-- ATTEMPT TO DELETE >> ".$data."\n");
         
         $msg = "";
         $sql =  "DELETE FROM ".$table." WHERE RefId = ".$refid;
         if (mysqli_query($conn, $sql)) {
            fwrite($change,$datelog."--".str_pad($_SESSION['sess_user'],12," ")."-- DELETE >> $sql\n\n");
            echo "alert('RECORD $refid Successfully DELETED!');";
            echo "self.location = '".$mainFILE.".php?StudentRefId=".$studrefid."';";
         }
         else { 
            $msg = "Error deleting record: ".mysqli_error($conn);
            echo "alert('$msg');";
         }
      }
      fclose($change);
   break;
   
   
}

?>