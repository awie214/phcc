<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_IPS"); ?>"></script>
      <script type="text/javascript">
         $(document).ready(function () {
            $("#btnPrintIPS").click(function () {
               var emprefid = $("#sint_EmployeesRefId").val();
               var file = "rpt_DPS_2";
               printIPCR(emprefid,file);
            });
         });
      </script>
      <style type="text/css">
         .td-input {
            padding: 2px;
         }
      </style>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("DIVISION PERFORMANCE SCOREBOARD [DPS]"); ?>
            <div class="container-fluid margin-top10">
               <div class="row" id="spmsList">
                  <?php
                     if (getvalue("hUserGroup") != "COMPEMP") {
                  ?>
                  <div class="col-xs-3">
                     <?php
                        employeeSelector();
                     ?>
                  </div>
                  <div class="col-xs-9">
                  <?php
                     } else {
                        echo '<div class="col-xs-12">';   
                     }
                  ?>
                     <div class="panel-top">
                        LIST OF PERFORMANCE MANAGEMENT OF 
                        <span id="selectedEmployees">&nbsp;</span>
                     </div>
                     <div class="panel-mid">
                        <div id="spGridTable">
                        </div>
                     </div>
                     <div class="panel-bottom">
                        <button type="button"
                             class="btn-cls4-sea trnbtn"
                             id="btnINSERTSPMS" name="btnINSERTSPMS">
                           <i class="fa fa-file" aria-hidden="true"></i>
                           &nbsp;Insert New
                        </button>
                        <button type="button"
                             class="btn-cls4-lemon trnbtn"
                             id="btnPrintIPS" name="btnPrintIPS">
                           <i class="fa fa-print" aria-hidden="true"></i>
                           &nbsp;Print
                        </button>
                     </div>
                  </div>
               </div>
               <div class="row" id="insert_spms">
                  <div class="col-xs-12">
                     <div class="panel-top">ADD NEW</div>
                     <div class="panel-mid">
                        <div class="row">
                           <div class="col-xs-4">
                              <label>Quarter</label>
                              <select class="form-input" name="sint_Quarter" id="sint_Quarter">
                                 <option value="">Select Quarter</option>
                                 <option value="1">1st Quarter</option>
                                 <option value="2">2nd Quarter</option>
                                 <option value="3">3rd Quarter</option>
                                 <option value="4">4th Quarter</option>
                              </select>
                           </div>
                           <div class="col-xs-4">
                              <label>Year</label>
                              <select class="form-input" name="sint_Year" id="sint_Year">
                                 <?php
                                    $past_year = date("Y",time()) - 2;
                                    $curr_year = date("Y",time());
                                    $future_year = date("Y",time()) + 2;
                                    for ($i=$past_year; $i <= $future_year ; $i++) { 
                                       if ($i == $curr_year) {
                                          echo '<option value="'.$i.'" selected>'.$i.'</option>';   
                                       } else {
                                          echo '<option value="'.$i.'">'.$i.'</option>';
                                       }
                                       
                                    }
                                 ?>
                              </select>
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-12">
                              <table style="width: 100%" border="1">
                                 <thead>
                                    <tr>
                                       <th class="text-center" rowspan="2" style="width: 15%; padding: 10px;">
                                          Performance<br>Objective
                                       </th>
                                       <th class="text-center" rowspan="2" style="width: 15%;">
                                          Measure
                                       </th>
                                       <th class="text-center" rowspan="2" style="width: 15%;">
                                          Target
                                       </th>
                                       <th class="text-center" rowspan="2" style="width: 15%;">
                                          Weight
                                       </th>
                                       <th class="text-center" colspan="5">
                                          Rating
                                       </th>
                                       <th class="text-center" rowspan="2" style="width: 15%;">
                                          Actual<br>Accomplishment
                                       </th>
                                    </tr>
                                    <tr>
                                       <th class="text-center" style="width: 5%;">Q</th>
                                       <th class="text-center" style="width: 5%;">E</th>
                                       <th class="text-center" style="width: 5%;">T</th>
                                       <th class="text-center" style="width: 5%;">RS</th>
                                       <th class="text-center" style="width: 5%;">WS</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td colspan="10" class="panel-top">
                                          <b>STRATEGIC OBJECTIVE (_%)</b>
                                       </td>
                                    </tr>
                                    <?php
                                       for ($a=1; $a <= 5; $a++) { 
                                          echo '<tr id="sorow_'.$a.'">';
                                          echo '
                                          <td class="td-input">
                                             <input type="hidden"
                                                    name="so_refid_'.$a.'"
                                                    id="so_refid_'.$a.'"
                                                    value="0">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="so_objectives_'.$a.'" 
                                                    id="so_objectives_'.$a.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="so_measure_'.$a.'" 
                                                    id="so_measure_'.$a.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="so_target_'.$a.'" 
                                                    id="so_target_'.$a.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="so_weight_'.$a.'" 
                                                    id="so_weight_'.$a.'">
                                          </td>
                                          <td align="center">
                                             <input type="checkbox"
                                                    class="td-input" 
                                                    name="so_r1_'.$a.'" 
                                                    id="so_r1_'.$a.'" value="0">
                                          </td>
                                          <td align="center">
                                             <input type="checkbox"
                                                    class="td-input" 
                                                    name="so_r2_'.$a.'" 
                                                    id="so_r2_'.$a.'" value="0">
                                          </td>
                                          <td align="center">
                                             <input type="checkbox"
                                                    class="td-input" 
                                                    name="so_r3_'.$a.'" 
                                                    id="so_r3_'.$a.'" value="0">
                                          </td>
                                          <td align="center">
                                             <input type="checkbox"
                                                    class="td-input" 
                                                    name="so_r4_'.$a.'" 
                                                    id="so_r4_'.$a.'" value="0">
                                          </td>
                                          <td align="center">
                                             <input type="checkbox"
                                                    class="td-input" 
                                                    name="so_r5_'.$a.'" 
                                                    id="so_r5_'.$a.'" value="0">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="so_accomplishment_'.$a.'" 
                                                    id="so_accomplishment_'.$a.'">
                                          </td>
                                          ';
                                          echo '</tr>';
                                       }
                                    ?>
                                    <tr>
                                       <td colspan="10" class="td-input">
                                          <button type="button" class="btn-cls4-tree" id="addrow_so">
                                             Add row
                                          </button>
                                          <button type="button" class="btn-cls4-red" id="delete_so">
                                             Delete last row
                                          </button>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="8" class="td-input">
                                          <b>RATING</b>
                                       </td>
                                       <td class="td-input" colspan="2">
                                          <input type="text" 
                                                 class="form-input" 
                                                 name="" 
                                                 id="">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="10" class="panel-top">
                                          <b>CORE FUNCTIONS (_%)</b>
                                       </td>
                                    </tr>
                                    <?php
                                       for ($b=1; $b <= 5; $b++) { 
                                          echo '<tr id="cfrow_'.$b.'">';
                                          echo '
                                          <td class="td-input">
                                             <input type="hidden"
                                                    name="cf_refid_'.$b.'"
                                                    id="cf_refid_'.$b.'"
                                                    value="0">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="cf_objectives_'.$b.'" 
                                                    id="cf_objectives_'.$b.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="cf_measure_'.$b.'" 
                                                    id="cf_measure_'.$b.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="cf_target_'.$b.'" 
                                                    id="cf_target_'.$b.'">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="cf_weight_'.$b.'" 
                                                    id="cf_weight_'.$b.'">
                                          </td>
                                          <td align="center">
                                             <input type="checkbox"
                                                    class="td-input" 
                                                    name="cf_r1_'.$b.'" 
                                                    id="cf_r1_'.$b.'" value="0">
                                          </td>
                                          <td align="center">
                                             <input type="checkbox"
                                                    class="td-input" 
                                                    name="cf_r2_'.$b.'" 
                                                    id="cf_r2_'.$b.'" value="0">
                                          </td>
                                          <td align="center">
                                             <input type="checkbox"
                                                    class="td-input" 
                                                    name="cf_r3_'.$b.'" 
                                                    id="cf_r3_'.$b.'" value="0">
                                          </td>
                                          <td align="center">
                                             <input type="checkbox"
                                                    class="td-input" 
                                                    name="cf_r4_'.$b.'" 
                                                    id="cf_r4_'.$b.'" value="0">
                                          </td>
                                          <td align="center">
                                             <input type="checkbox"
                                                    class="td-input" 
                                                    name="cf_r5_'.$b.'" 
                                                    id="cf_r5_'.$b.'" value="0">
                                          </td>
                                          <td class="td-input">
                                             <input type="text" 
                                                    class="form-input" 
                                                    name="cf_accomplishment_'.$b.'" 
                                                    id="cf_accomplishment_'.$b.'">
                                          </td>
                                          ';
                                          echo '</tr>';
                                       }
                                    ?>
                                    <tr>
                                       <td colspan="10" class="td-input">
                                          <button type="button" class="btn-cls4-tree" id="addrow_cf">
                                             Add row
                                          </button>
                                          <button type="button" class="btn-cls4-red" id="delete_cf">
                                             Delete last row
                                          </button>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="8" class="td-input">
                                          <b>RATING</b>
                                       </td>
                                       <td class="td-input" colspan="2">
                                          <input type="text" 
                                                 class="form-input" 
                                                 name="" 
                                                 id="">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="8" class="td-input">
                                          <b>TOTAL RATING</b>
                                       </td>
                                       <td class="td-input" colspan="2">
                                          <input type="text" 
                                                 class="form-input" 
                                                 name="" 
                                                 id="">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="8" class="td-input">
                                          <b>PREMIUM POINTS FOR INTERVENING TASKS (if applicable)</b>
                                       </td>
                                       <td class="td-input" colspan="2">
                                          <input type="text" 
                                                 class="form-input" 
                                                 name="" 
                                                 id="">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="8" class="td-input">
                                          <b>OVERALL NUMERICAL RATING</b>
                                       </td>
                                       <td class="td-input" colspan="2">
                                          <input type="text" 
                                                 class="form-input" 
                                                 name="" 
                                                 id="">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td colspan="8" class="td-input">
                                          <b>ADJECTIVAL RATING</b>
                                       </td>
                                       <td colspan="2" class="td-input">
                                          <input type="text" 
                                                 class="form-input" 
                                                 name="" 
                                                 id="">
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>  
                     </div>
                     <div class="panel-bottom">
                        <input type="hidden" name="sint_IPCRRefId" id="sint_IPCRRefId" value="0">
                        <input type="hidden" name="sint_EmployeesRefId" id="sint_EmployeesRefId" value="0">
                        <input type="hidden" name="count_sp" id="count_sp">
                        <input type="hidden" name="count_cf" id="count_cf">
                        <input type="hidden" name="count_sf" id="count_sf">
                        <input type="hidden" name="fn" id="fn" value="saveIPCR">
                        <button type="button" id="btnSAVEIPCR" class="btn-cls4-sea">
                           <i class="fa fa-save"></i>&nbsp;SAVE
                        </button>
                        <button type="button" id="btnCANCELIPCR" class="btn-cls4-red">
                           <i class="fa fa-times"></i>&nbsp;CANCEL
                        </button>
                        <button type="button" id="btnEDITIPCR" class="btn-cls4-sea">
                           <i class="fa fa-save"></i>&nbsp;UPDATE
                        </button>
                        <button type="button" id="btnBACKIPCR" class="btn-cls4-red">
                           <i class="fa fa-backward"></i>&nbsp;BACK
                        </button>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "spms_ipcr";
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
</html>