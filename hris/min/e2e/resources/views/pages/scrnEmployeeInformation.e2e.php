<?php
   //require "ctrl_EmploymentInformation.e2e.php";
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="javascript" src="<?php echo jsCtrl("ctrl_EmploymentInformation"); ?>"></script>
      <style type="text/css">
      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="">
        <?php $sys->SysHdr($sys,"pis"); ?>
        <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($modTitle); ?>
            <div class="container-fluid margin-top">
              <div class="row">
                <div class="col-xs-12" id="div_CONTENT">
                  <div class="row">
                        <div class="col-xs-3">
                           <?php
                              employeeSelector();
                           ?>
                        </div>
                        <div class="col-xs-9">
                           <div class="row" id="DataEntry">
                              <div class="col-xs-12">
                                 <div class="mypanel">
                                    <div class="panel-top">
                                       Employees Name : <span id="spanEmpName"></span>
                                    </div>
                                    <div class="panel-mid-litebg">
                                       <div class="row">
                                          <div class="col-xs-12">
                                             <!-- <button type="button"
                                                     class="btn-cls4-sea"
                                                     id="btnSAVE" name="btnSAVE" disabled
                                                     >
                                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;
                                                UPDATE
                                             </button> -->
                                             <button type="button"
                                                     class="btn-cls4-sea"
                                                     id="btnUpdate" name="btnUpdate">
                                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;
                                                UPDATE EMPLOYEE INFORMATION
                                             </button>
                                             <input type="hidden" name="fn" value="updateEmpInfo">
                                          <!-- 
                                             <button type="button"
                                                     class="btn-cls4-red"
                                                     id="btnCANCEL" name="btnCANCEL" disabled
                                                     >
                                                <i class="fa fa-undo" aria-hidden="true"></i>&nbsp;
                                                CANCEL
                                             </button>
                                          -->
                                          </div>
                                       </div>
                                       <div id="EntryEmpInformation" style="padding:5px;">
                                          <div class="row">
                                             <div class="col-xs-6">
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      First Day of Service:
                                                   </div>
                                                   <div class="col-xs-7" >
                                                      <input type="text" class="form-input saveFields-- date--" name="date_HiredDate" >
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Date of Assumption (PCC):
                                                   </div>
                                                   <div class="col-xs-7" >
                                                   <input type="text" class="form-input saveFields-- date--" name="date_AssumptionDate" >
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Resigned / Retired / Transfer Date:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <input type="text" class="form-input saveFields-- date--" name="date_ResignedDate">
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Rehired Date:
                                                   </div>
                                                   <div class="col-xs-7" >
                                                      <input type="text" class="form-input saveFields-- date--" name="date_RehiredDate" >
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col-xs-6">
                                                <div class="row margin-top">
                                                   <div class="col-xs-12 txt-center">
                                                      <strong>For Contract of Service / Job Order</strong>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Start Date:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <input type="text" class="form-input saveFields-- date--"
                                                      name="date_StartDate"/>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      End Date:
                                                   </div>
                                                   <div class="col-xs-7" >
                                                      <input type="text" class="form-input saveFields-- date--"
                                                      name="date_EndDate" />
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <?php bar(); ?>
                                          <div class="row">
                                             <div class="col-xs-6">
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">Agency:</div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         $table = "Agency";
                                                         createSelect($table,
                                                                      "sint_".$table."RefId",
                                                                      "",100,"Name","Select ".$table,"");

                                                      ?>
                                                   </div>

                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">Plantilla Item:</div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         $table = "PositionItem";
                                                         createSelect($table,
                                                                      "sint_".$table."RefId",
                                                                      "",100,"Name","Select Position Item","");

                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                  <div class="col-xs-5 form-label">
                                                      Position:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         createSelect("Position",
                                                                      "sint_PositionRefId",
                                                                      "",100,"Name","Select Position","");
                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Office
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         createSelect("Office",
                                                                      "sint_OfficeRefId",
                                                                      "",100,"Name","Select Office","");
                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Division
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         createSelect("Division",
                                                                      "sint_DivisionRefId",
                                                                      "",100,"Name","Select Division","");
                                                      ?>
                                                   </div>
                                                </div>
                                                <?php
                                                   if (getvalue("hCompanyID") == "2") {
                                                ?>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Interim Position:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         createSelect("InterimPosition",
                                                                      "InterimPositionRefId",
                                                                      "",100,"Name","Select Interim Position","");
                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Interim Office
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         createSelect("InterimOffice",
                                                                      "InterimOfficeRefId",
                                                                      "",100,"Name","Select Interim Office","");
                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Interim Division:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         createSelect("InterimDivision",
                                                                      "InterimDivisionRefId",
                                                                      "",100,"Name","Select Interim Division","");
                                                      ?>
                                                   </div>
                                                </div>
                                                <?php
                                                   } else {
                                                ?>
                                                
                                                <?php 
                                                   } 
                                                ?>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">Appointment Status:</div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         $table = "ApptStatus";
                                                         createSelect($table,
                                                                      "sint_".$table."RefId",
                                                                      "",100,"Name","Select Appointment Status","");

                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">Employment Status:</div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         $table = "EmpStatus";
                                                         createSelect($table,
                                                                      "sint_".$table."RefId",
                                                                      "",100,"Name","Select Employees Status","");

                                                      ?>
                                                   </div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div class="col-xs-6">
                                                <?php
                                                   if (getvalue("hCompanyID") != "2") {
                                                ?>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Salary Grade:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         $table = "SalaryGrade";
                                                         createSelect($table,
                                                                      "sint_".$table."RefId",
                                                                      "",100,"Name","Select Grade","");

                                                      ?>
                                                   </div>
                                                </div>
                                                <?php
                                                   } else {
                                                ?>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Job Grade:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         $table = "JobGrade";
                                                         createSelect($table,
                                                                      "sint_".$table."RefId",
                                                                      "",100,"Name","Select Job Grade","");

                                                      ?>
                                                   </div>
                                                </div>
                                                <?php
                                                   }
                                                ?>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Salary Step:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         $table = "StepIncrement";
                                                         createSelect($table,
                                                                      "sint_".$table."RefId",
                                                                      "",100,"Name","Select Step Increment","");

                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Salary Amount:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <input type="text" class="form-input saveFields-- number--" name="deci_SalaryAmount" id="deci_SalaryAmount" />
                                                   </div>
                                                </div>
                                                <!-- <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Pay Rate:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         $table = "PayRate";
                                                         createSelect($table,
                                                                      "sint_".$table."RefId",
                                                                      "",100,"Name","Select Pay Rate","");

                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Pay Period:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <select class="form-input saveFields--" id="PayGroup__<?php echo $j; ?>" name="char_PayPeriod"
                                                         title='Payment Group'>
                                                         <option value="">Select Pay Group</option>
                                                         <?php
                                                            foreach ($_SESSION["PayGroup"] as $e) {
                                                               echo '<option value="'.$e.'">'.$e.'</option>';
                                                            }
                                                         ?>
                                                      </select>
                                                   </div>
                                                </div> -->
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Employee Type:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <select class="form-input" name="sint_Inactive"
                                                         title='Employees Status'>
                                                         <option value="0">Active</option>
                                                         <option value="1">In-Active</option>
                                                      </select>
                                                   </div>
                                                </div>
                                                <?php
                                                   if (getvalue("hCompanyID") == "28") {
                                                ?>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Unit:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         $table = "Units";
                                                         createSelect($table,
                                                                      "sint_".$table."RefId",
                                                                      "",100,"Name","Select Unit","");

                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Actual O/D/U:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         $table = "Department";
                                                         createSelect($table,
                                                                      "sint_".$table."RefId",
                                                                      "",100,"Name","Select Department","");

                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Location:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         $table = "Locations";
                                                         createSelect($table,
                                                                      "sint_".$table."RefId",
                                                                      "",100,"Name","Select Location","");

                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-5 form-label">
                                                      Branch:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <?php
                                                         $table = "Sections";
                                                         createSelect($table,
                                                                      "sint_".$table."RefId",
                                                                      "",100,"Name","Select Branch","");

                                                      ?>
                                                   </div>
                                                </div>
                                                <?php
                                                   }
                                                ?>
                                                <div class="row margin-top" id="transfer">
                                                   <div class="col-xs-5 form-label">
                                                      Transfer To:
                                                   </div>
                                                   <div class="col-xs-7">
                                                      <input type="text" name="char_TransferTo" id="char_TransferTo" class="form-input saveFields--">
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel-bottom" id="message">
                                    
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                  </div>
                </div>
              </div>  
              <!-- Modal -->
              <div class="modal fade border0" id="prnModal" role="dialog">
                 <div class="modal-dialog border0" style="padding:0px;width:97%;height:92%;">
                    <div class="mypanel border0" style="height:100%;">
                       <div class="panel-top bgSilver">
                          <a href="#" data-toggle="tooltip" data-placement="top" title="Print Now" id="btnPRINTNOW">
                             <i class="fa fa-print" aria-hidden="true"></i>
                          </a>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                       </div>
                       <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                    </div>
                 </div>
              </div>
              <?php
                 footer();
                 include "varHidden.e2e.php";
                 doHidden("hEmpRefid","","");
              ?>
            </div>
        </div>
      </form>
   </body>
</html>



