<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
   </head>
   <body onload = "indicateActiveModules();" style="background:#fff;">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("DASHBOARD"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="row">
                        <div class="col-xs-9">
                           <div class="row">
                              <?php spacer (50);?>
                              <div class="col-xs-7">
                                 <img src="<?php echo img("Performance_Review.jpg"); ?>" width="80%">
                              </div>
                              <div class="col-xs-4">
                                 <div class="row">
                                    <div class="col-xs-12 txt-center" style="border-radius:20px;
                                    background:var(--lemon);
                                    color:#fff;
                                    padding:10px 10px 10px 10px;">
                                       <div class="counts--" style="color:var(--lemon);">
                                          <h1>25</h1>
                                          <div class="txt-center"><h5>EMPLOYEES SUBMITTED IPCR</h5></div>
                                       </div>
                                    </div>
                                 </div>
                                 <?php spacer(20);?>
                                 <div class="row">
                                    <div class="col-xs-12 txt-center" style="border-radius:20px;
                                    background:var(--lemon);
                                    color:#fff;
                                    padding:10px 10px 10px 10px;">
                                       <div class="counts--" style="color:var(--lemon);">
                                          <h1>35</h1>
                                          <div class="txt-center"><h5>EMPLOYEES NOT SUBMITTED IPCR</h5></div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="mypanel">
                              <div class="panel-top txt-center">
                                 ANNOUNCEMENTS
                              </div>
                              <div class="panel-mid">
                                 &nbsp;
                              </div>
                           </div>
                           <?php spacer(10) ?>
                           <div class="mypanel">
                              <div class="panel-top txt-center">
                                 NOTIFICATIONS
                              </div>
                              <div class="panel-mid">
                                 &nbsp;
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               include_once ("varHidden.e2e.php");
            ?>
         </div>
      </form>
   </body>
</html>




