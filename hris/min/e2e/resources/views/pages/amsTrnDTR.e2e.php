<?php 
   include_once 'constant.e2e.php';
   include_once pathClass.'DTRFunction.e2e.php';
   include_once pathClass.'DTR_Process.e2e.php';
   function EmployeesSearch2($attr) {
      if ($GLOBALS['UserCode']!="COMPEMP") {
         $LName = $FName = $MName = $FullName = "";
         $empRefId = $whereClause = "";
         $Entries = 0;
         if (!empty($attr["empRefId"])) {
            $whereClause .= " WHERE RefId = '".$attr["empRefId"]."'";
            $Entries++;
         } else {
            $whereClause .= " WHERE RefId = 0";
         }


         if (!empty($attr["empLName"])) {
            $whereClause .= " AND LastName LIKE '".$attr["empLName"]."%'";
            $Entries++;
         }
         if (!empty($attr["empFName"])) {
            $whereClause .= " AND FirstName LIKE '".$attr["empFName"]."%'";
            $Entries++;
         }
         if (!empty($attr["empMName"])) {
            $whereClause .= " AND MiddleName LIKE '".$attr["empMName"]."%'";
            $Entries++;
         }
         //echo $whereClause;
         $emp = FindFirst("employees",$whereClause,"RefId, LastName, FirstName, MiddleName");

         if ($emp) {
            $empRefId = $emp["RefId"];
            $LName = $emp["LastName"];
            $FName = $emp["FirstName"];
            $MName = $emp["MiddleName"];
            $MiddleName = substr($MName, 0,1);
            $FullName = $FName." ".$MiddleName." ".$LName;
         } else {
            $LName = $attr["empLName"];
            $FName = $attr["empFName"];
            $MName = $attr["empMName"];
            $empRefId = $attr["empRefId"];
            $FullName = $attr["empFullName"];
            // $MiddleName = substr($MName, 0,1);
            // $FullName = $FName." ".$MiddleName." ".$LName;
         }
         spacer(5);
         echo
         '<div class="mypanel">
            <div class="panel-top bgIntroBlack">Search Employee:</div>
            <div class="panel-mid" style="padding:5px;">
               <div class="row">
                  <div class="col-xs-1" style="display:none;">
                     <label>Ref. Id</label><br>
                     <input type="text" class="form-input rptCriteria-- number--" name="txtRefId" placeholder="Emp.RefId" value="'.$empRefId.'"/>
                  </div>
                  <div class="col-xs-2" style="display:none;">
                     <label>Last Name</label><br>
                     <input type="text" class="form-input rptCriteria--" name="txtLName" placeholder="Last Name" value="'.$LName.'"/>
                  </div>
                  <div class="col-xs-2" style="display:none;">
                     <label>First Name</label><br>
                     <input type="text" class="form-input rptCriteria--" name="txtFName" placeholder="First Name" value="'.$FName.'"/>
                  </div>
                  <div class="col-xs-2" style="display:none;">
                     <label>Middle Name</label><br>
                     <input type="text" class="form-input rptCriteria--" name="txtMidName" placeholder="Middle Name" value="'.$MName.'"/>
                  </div>
                  <div class="col-xs-1"></div>
                  <div class="col-xs-6">
                     <label>Employee Name Name</label><br>
                     <input type="text" class="form-input" id="txtFullName" name="txtFullName" placeholder="Employee Name" value="'.$FullName.'"/>
                  </div>
                  <div class="col-xs-5">
                     <div style="margin-top:20px;">
                        <button type="submit" value="submit" class="btn-cls4-sea" name="submit" style="margin-left:5px;">
                           <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;SUBMIT
                        </button>
                     </div>
                  </div>
               </div>
            </div>
            <div class="panel-bottom"></div>
         </div>';
         /*echo
         '<script language="javascript">
            remIconDL();
         </script>';*/
         modalEmpLkUp();
         return $empRefId;
      }
      return 0;
   }
   function btndtr($KindOfEntry) {
      switch ($KindOfEntry) {
         case "TI":
            $idxEntry = 1;
         break;
         case "LO":
            $idxEntry = 2;
         break;
         case "LI":
            $idxEntry = 3;
         break;
         case "TO":
            $idxEntry = 4;
         break;
         case "OTI":
            $idxEntry = 5;
         break;
         case "OTO":
            $idxEntry = 6;
         break;
         case "OBO":
            $idxEntry = 7;
         break;
         case "OBI":
            $idxEntry = 8;
         break;
      }
      echo
      '<button type="button" class="btn-cls4-tree" punch="'.$KindOfEntry.'" idx="'.$idxEntry.'" id="btnEdit_'.$KindOfEntry.'">
         <i class="fa fa-pencil" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn-cls4-sea" punch="'.$KindOfEntry.'" idx="'.$idxEntry.'" id="btnSave_'.$KindOfEntry.'">
         <i class="fa fa-floppy-o" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn-cls4-red" punch="'.$KindOfEntry.'" idx="'.$idxEntry.'" id="btnCancel_'.$KindOfEntry.'">
         <i class="fa fa-times" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn-cls4-red" punch="'.$KindOfEntry.'" idx="'.$idxEntry.'" id="btnDelete_'.$KindOfEntry.'">
         <i class="fa fa-trash" aria-hidden="true"></i>
      </button>';
   }
   function punchField($KindOfEntry,$idx) {
      echo '<input type="time" class="form-input details-- time--" kEntry="'.$idx.'" checktime="" name="entry_'.$KindOfEntry.'" disabled>';
   }
   //dtr_process();
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once "pageHEAD.e2e.php"; ?>
      <script language="JavaScript">
         $(document).ready(function () {
            /*var name = "";
            $("[class*='form-input']").each(function () {
               name += $(this).attr("name") + ", ";
            });
            alert(name);*/
            EmployeeAutoComplete("employees","txtFullName");
            $("#txtFullName").blur(function () {
               var value = $(this).val();
               arr = value.split("-");
               $("[name='txtRefId']").val(arr[0]);
               $("[name='hEmpRefId']").val(arr[0]);
               $("#SelectedEMP").val(arr[0]);
               $(this).val(arr[1]);
            });
            if ($("#SelectedEMP").val() != "") {
               displayDatePicker('dummy');   
            }
            
            $("#pop_dtr").click(function () {
               var NewMonth = getValueByName("hNewMonth");
               var EmpRefId = getValueByName("txtRefId");
               var NewYear = getValueByName("hNewYear");
               var param = "&form=FORM_DTR&htm=1";

               if (NewMonth == 0) {
                  NewMonth = $("#hDummyMonth").val();
                  NewYear  = $("#hDummyYear").val();
               }
               param += "&hNewMonth="+NewMonth+"&emprefid="+EmpRefId+"&hNewYear="+NewYear;
               param += "&json=fieldmap";
               param += "&map=FieldMapping";
               popPDS("FormViewer",param);
            });
            <?php
               if ($EmpRefId || $EmployeesRefId) {
                  echo "displayDatePicker('dummy');";
               }
            ?>
         });
      </script>
      <style type="text/css">
         .dpDiv              {}
         .dpTable            {background:#fefefe;width:100%;font-family:Tahoma;font-size:11px;text-align:center;color: #000000;}
         .dpTD               {border: 1px solid gray;}
         .dpDayHighlightTD   {border: 1px solid gray;background-color: var(--bgGray1);}
         .visitedTD          {background-color: var(--bgIntroBlack2);color:white;}
         .dpTodayTD          {background-color: var(--MainColor1);color:white;}
         .dpTDHover          {background-color: var(--highLight);border: 1px solid #000000;cursor:pointer;color:red;}
         .dpTitleText        {padding:10px;font-size:10pt;color:black;font-weight: bold;}
         .dpDayTD            {padding:10px;font-size:10pt;font-weight:600;background-color:var(--MainColor1);color:#fff;}
         .dpDayHighlight     {color:#0000FF;font-weight: bold;}
         .dayNum             {font-size:11pt;font-weight:600;color:blue;}
         .NOENTRY            {color:red;}

         .daycontent--:hover {
            background-color: var(--highLight);color:white;
            cursor:pointer;
         }
         #overlay {
            position: fixed; /* Sit on top of the page content */
            display: none; /* Hidden by default */
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5); /* Black background with opacity */
            z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
            cursor: pointer; /* Add a pointer on hover */
         }
         img {
            position: center;
         }
      </style>
      <link rel="stylesheet" href="<?php echo path("js/autocomplete/css/jquery-ui.css"); ?>" type="text/css" />
      <script type="text/javascript" src="<?php echo path("js/autocomplete/jquery-ui.js") ?>"></script>
      <script src="<?php echo jsPath("dtr_calendar")."?".time();?>"></script>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($paramTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div>
                        <?php if ($UserCode != "COMPEMP") {
                              $txtrefid = $_SESSION["SelectedEMP"];
                              if (isset($_POST["submit"])) {
                                 $_SESSION["SelectedEMP"] = getvalue("txtRefId");
                                 $txtrefid = getvalue("txtRefId");
                              }
                           $attr =  [
                                       "empRefId"=>getvalue("txtRefId"),
                                       "empLName"=>getvalue("txtLName"),
                                       "empFName"=>getvalue("txtFName"),
                                       "empMName"=>getvalue("txtMidName")
                                       //"empFullName"=>getvalue("txtFullName")
                                    ];
                                    $EmpRefId = EmployeesSearch($attr);
                                    
                                    spacer(10); 
                        } else {
                           $EmpRefId = $EmployeesRefId;
                           //echo '<label class="fontB10">My Daily Time Record (DTR)</label>';
                           $workschedule = FindFirst("empinformation","WHERE EmployeesRefId = '$EmpRefId'","WorkScheduleRefId");
                           if ($workschedule) {
                              saveDTRSummary($EmpRefId,date("m",time()),date("Y",time()),$workschedule);   
                           }
                        }
                        ?>

                        <?php if ($EmpRefId) { ?>
                        <div class="panel-mid border">
                           <div class="row margin-top">
                              <div class="col-xs-4">
                                 <!--
                                 <div class="row">
                                    <div class="col-xs-8">
                                       <div>
                                          <input type="text" class="form-input date--" name="tDateFrom" title="FROM">
                                          <input type="text" class="form-input date--" name="tDateTo" title="TO">
                                       </div>
                                    </div>
                                 </div>
                                 -->
                              </div>
                              <div class="col-xs-8">
                                 <div class="row txt-right">
                                    <div class="col-xs-12">
                                       <?php if ($UserCode != "COMPEMP") { ?>
                                       <button type="button" class="btn-cls4-sea" id="btnReProcess">PROCESS</button>
                                       <?php } ?>
                                       <button type="button" class="btn-cls4-tree" id="pop_dtr">PREVIEW DTR</button>
                                    </div>
                                    <!--<button type="button" class="btn-cls4-tree Menu" pre="ams" id="print_dtr" route="PrintDtr">&nbsp;PRINT DTR</button>-->
                                    
                                    <?php
                                    /*
                                    if ($UserCode != "COMPEMP") {
                                       echo
                                       '<button type="button" class="btn-cls4-sea" id="dtr">DETAILS</button>';
                                    }
                                    */
                                    ?>
                                 </div>
                              </div>
                           </div>
                           <div class="row margin-top padd5">
                              <div class="col-xs-12" id="CalHolder">Calendar Holder</div>
                           </div>
                        </div>
                        <?php } ?>

                     </div>
                  </div>
               </div>
            </div>
            <div id="overlay">
               <div class="col-xs-4"></div>
               <div class="col-xs-4 txt-center" style="margin-top: 10%;">
                  <?php
                     echo '<img src="'.path.'images/loading.gif">';
                  ?>   
               </div>
            </div>

            
            <div class="modal fade border0" id="dtr_modal" role="dialog">
               <div class="modal-dialog border0" style="padding:10px;width:95%;">
                  <div class="mypanel border0" style="height:100%;">
                     <div id="overlay">
                        <div class="col-xs-4"></div>
                        <div class="col-xs-4" style="margin-top: 10%;">
                           <?php
                              echo '<img src="'.path.'images/loading.gif">';
                           ?>   
                        </div>
                        
                     </div> 
                     <div class="panel-top">
                        DETAIL SUMMARY
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <div class="panel-mid">
                        <div class="row" style="padding:10px;">
                           <div class="col-xs-7" style="padding:10px;">
                              <div class="row">
                                 <div class="col-xs-6"><label>Date Selected : <span id="DateSelected"></span></label></div>
                                 <div class="col-xs-6 txt-right">
                                    <button type="button" class="btn-cls4-red" id="btnCancelDetails">CLOSE</button>
                                 </div>
                              </div>
                              <div class="row">

                              </div>
                              <div class="row margin-top" style="padding:10px;border:1px solid black">
                                 <div class="col-xs-12">
                                    <div class="row">
                                       <div class="col-xs-3 txt-right">
                                          <span class="label">TIME IN:</span>
                                       </div>
                                       <div class="col-xs-9">
                                          <?php punchField("TI",1); ?>
                                          <?php btndtr("TI"); ?>
                                       </div>
                                       
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-3 txt-right">
                                          <span class="label">LUNCH OUT:</span>
                                       </div>
                                       <div class="col-xs-9">
                                          <?php punchField("LO",2); ?>
                                          <?php btndtr("LO"); ?>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-3 txt-right">
                                          <span class="label">LUNCH IN:</span>
                                       </div>
                                       <div class="col-xs-9">
                                          <?php punchField("LI",3); ?>
                                          <?php btndtr("LI"); ?>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-3 txt-right">
                                          <span class="label">TIME OUT:</span>
                                       </div>
                                       <div class="col-xs-9">
                                          <?php punchField("TO",4); ?>
                                          <?php btndtr("TO"); ?>
                                       </div>
                                    </div>
                                 </div>
                                 <?php /*
                                 <div class="col-xs-6">
                                    <div class="row">
                                       <div class="col-xs-3 txt-right">
                                          <span class="label">O.T IN:</span>
                                       </div>
                                       <div class="col-xs-4">
                                          <?php punchField("OTI",5); ?>
                                       </div>
                                       <div class="col-xs-5">
                                          <?php //btndtr("OTI"); ?>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-3 txt-right">
                                          <span class="label">O.T OUT:</span>
                                       </div>
                                       <div class="col-xs-4">
                                          <?php punchField("OTO",6); ?>
                                       </div>
                                       <div class="col-xs-5">
                                          <?php //btndtr("OTO"); ?>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-3 txt-right">
                                          <span class="label">O.B OUT:</span>
                                       </div>
                                       <div class="col-xs-4">
                                          <?php punchField("OBO",7); ?>
                                       </div>
                                       <div class="col-xs-5">
                                          <?php //btndtr("OBO"); ?>
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-3 txt-right">
                                          <span class="label">O.B IN:</span>
                                       </div>
                                       <div class="col-xs-4">
                                          <?php punchField("OBI",8); ?>
                                       </div>
                                       <div class="col-xs-5">
                                          <?php //btndtr("OBI"); ?>
                                       </div>
                                    </div>
                                 </div>
                                  */ ?>
                              </div>
                              <div class="row margin-top">
                                 <button type="button" class="btn-cls4-sea" id="btnProcess">PROCESS</button>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-3 txt-right">
                                    <span class="label">Schedule:</span>
                                 </div>
                                 <div class="col-xs-4">
                                    <input type="text" class="form-input" name="worksched" disabled>
                                 </div>
                              </div>
                              <div class="row margin-top">
                                 <div class="col-xs-3 txt-right">
                                    <span class="label">DTR Remarks:</span>
                                 </div>
                                 <div class="col-xs-9">
                                    <input type="text" class="form-input" disabled name="DTRRemarks" id="DTRRemarks">
                                 </div>
                              </div>
                              <?php spacer(10);?>
                              <div class="row margin-top" style="border:1px solid black;padding:5px;">
                                 <div class="col-xs-12">
                                    <div class="row">
                                       <div class="col-xs-2">
                                          <span class="label txt-center">Hours</span><br>
                                          <input type="text" class="form-input" disabled id="tdyHours" name="tdyHours">
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label txt-center">Absent</span><br>
                                          <input type="text" class="form-input" disabled id="tdyAbsent" name="tdyAbsent">
                                       </div>
                                    </div>
                                    <div class="row margin-top">
                                       <div class="col-xs-2">
                                          <span class="label txt-center">Late 1</span><br>
                                          <input type="text" class="form-input" disabled id="tdyLate1" name="tdyLate1">
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label txt-center">UT 1</span><br>
                                          <input type="text" class="form-input" disabled id="tdyUT1" name="tdyUT1">
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label txt-center">Late 2</span><br>
                                          <input type="text" class="form-input" disabled id="tdyLate2" name="tdyLate2">
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label txt-center">UT 2</span><br>
                                          <input type="text" class="form-input" disabled id="tdyUT2" name="tdyUT2">
                                       </div>
                                       <div class="col-xs-2">
                                          <span class="label txt-center">OT</span><br>
                                          <input type="text" class="form-input" disabled id="tdyOT" name="tdyOT">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xs-5">
                              <div class="row" style="border:1px solid black;padding:10px;">
                                 <div class="col-xs-12">
                                    <div class="row">
                                       <div class="col-xs-6">
                                          <div class="row">
                                             <div class="col-xs-3">
                                             </div>
                                             <div class="col-xs-6">
                                                <span class="label">TOTALS</span>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <span class="label">Days:</span>
                                             </div>
                                             <div class="col-xs-6">
                                                <input type="text" class="form-input" disabled name="TOTDays">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <span class="label">Hours:</span>
                                             </div>
                                             <div class="col-xs-6">
                                                <input type="text" class="form-input" disabled name="TOTHours">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <span class="label">Absent:</span>
                                             </div>
                                             <div class="col-xs-6">
                                                <input type="text" class="form-input" disabled name="TOTAbsent">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <span class="label">Late:</span>
                                             </div>
                                             <div class="col-xs-6">
                                                <input type="text" class="form-input" disabled name="TOTLate">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <span class="label">UT:</span>
                                             </div>
                                             <div class="col-xs-6">
                                                <input type="text" class="form-input" disabled name="TOTUT">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <span class="label">OT:</span>
                                             </div>
                                             <div class="col-xs-6">
                                                <input type="text" class="form-input" disabled name="TOTOT">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-xs-6">
                                          <div class="row">
                                             <div class="col-xs-3">
                                             </div>
                                             <div class="col-xs-6">
                                                <span class="label">EQUIVALENT</span>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <span class="label">Days:</span>
                                             </div>
                                             <div class="col-xs-6">
                                                <input type="text" class="form-input" disabled name="DayEQ">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <span class="label">Hours:</span>
                                             </div>
                                             <div class="col-xs-6">
                                                <input type="text" class="form-input" disabled name="HoursEQ">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <span class="label">Absent:</span>
                                             </div>
                                             <div class="col-xs-6">
                                                <input type="text" class="form-input" disabled name="AbsentEQ">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <span class="label">Late:</span>
                                             </div>
                                             <div class="col-xs-6">
                                                <input type="text" class="form-input" disabled name="LateEQ">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <span class="label">UT:</span>
                                             </div>
                                             <div class="col-xs-6">
                                                <input type="text" class="form-input" disabled name="UTEQ">
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-3 txt-right">
                                                <span class="label">OT:</span>
                                             </div>
                                             <div class="col-xs-6">
                                                <input type="text" class="form-input" disabled name="OTEQ">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <?php spacer(10);?>
                              <div>
                                 <div class="row margin-top">
                                    <span class="label">Leave Earnings</span>
                                 </div>
                                 <div class="row margin-top" style="border:1px solid black;padding:10px;">
                                    <div class="col-xs-12">
                                       <div class="row">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Vacation Leave:</span>
                                          </div>
                                          <div class="col-xs-6">
                                             <input type="text" class="form-input" disabled name="VLEarned">
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Sick Leave:</span>
                                          </div>
                                          <div class="col-xs-6">
                                             <input type="text" class="form-input" disabled name="SLEarned">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <?php spacer(10);?>
                                 <div class="row">
                                    <span class="label">Leave Balances</span>
                                 </div>
                                 <div class="row margin-top" style="border:1px solid black;padding:10px;">
                                    <div class="col-xs-12">
                                       <div class="row">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Vacation Leave:</span>
                                          </div>
                                          <div class="col-xs-6">
                                             <input type="text" class="form-input" disabled name="VLBal">
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-3 txt-right">
                                             <span class="label">Sick Leave:</span>
                                          </div>
                                          <div class="col-xs-6">
                                             <input type="text" class="form-input" disabled name="SLBal">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <?php spacer(10);?>
                                 <div class="row" style="border:1px solid black;padding:10px;">
                                    <div class="col-xs-6 txt-right">
                                       <span class="label">Compensatory O.T Credits:</span>
                                    </div>
                                    <div class="col-xs-6">
                                       <input type="text" class="form-input" disabled name="COC">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <input type="hidden" name="hDummyMonth" id="hDummyMonth" value="<?php echo date("m",time()); ?>">
            <input type="hidden" name="hDummyYear" id="hDummyYear" value="<?php echo date("Y",time()); ?>">
            <?php
               footer();
               doHidden("paramTitle",$paramTitle,"");
               doHidden("dummy","","");
               doHidden("hDate","",date("Y-m-d",time()));
               doHidden("hCurrentTimeValue","","");
               doHidden("hNewMonth","","");
               doHidden("hNewYear","","");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>

   </body>
</html>