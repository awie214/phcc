<?php

?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("ACCESS EMPLOYEES"); ?>

                           <div class="row" id="div_CONTENT">
                              <div class="col-xs-12">
                                 <div>
                                    <span id="spSAVECAN">
                                       <!--
                                       <button type="button" class="btn-cls4-red trnbtn"
                                                   name="btnCANCEL" id="btnCANCEL">
                                             <i class="fa fa-undo" aria-hidden="true"></i>
                                             &nbsp;&nbsp;CANCEL
                                       </button>
                                       -->
                                    </span>
                                 </div>
                                 <?php spacer(5) ?>
                                 <div class="row">
                                    <div class="col-xs-3">
                                       <div class="mypanel">
                                          <div class="panel-top bgTree">SYSTEM USER</div>
                                          <div class="panel-mid-litebg" style="max-height:600px;overflow:auto;">
                                             <div class="list-group">
                                                   <?php
                                                      $rs = f_Find("sysuser","order by UserName");
                                                      while ($row = mysqli_fetch_assoc($rs)) {

                                                            if ($row["UserName"]!= "") {
                                                               echo
                                                               '<a href="#" onclick="selectSysUser(\''.$row["UserName"].'\','.$row["RefId"].');" id="'.$row["UserName"].'" class="list-group-item">
                                                                  ['.$row["RefId"].'] - '.$row["UserName"].'
                                                               </a>';
                                                            }
                                                      }
                                                   ?>
                                             </div>
                                          </div>
                                          <div class="panel-bottom"></div>
                                       </div>
                                    </div>
                                    <div class="col-xs-9">
                                       <div class="mypanel">
                                          <div class="panel-top">
                                             <div class="row">
                                                <div class="col-xs-6">
                                                      <a href="javascript:void(0);" class="clsUPDATE">
                                                         <i class="fa fa-floppy-o" aria-hidden="true"
                                                         title="Update / Save Access"
                                                         style="color:white"></i>&nbsp;
                                                      </a>
                                                      LIST OF EMPLOYEES ACCESSED BY THE USER
                                                </div>
                                                <div class="col-xs-6 txt-center" id="idSysUserName">No System User Selected</div>
                                             </div>
                                          </div>
                                          <div class="panel-mid">
                                             <div class="row">
                                                <div class="col-xs-12" style="overflow:auto;height:550px;">
                                                   <span id="spEmployeesAccess">
                                                   </span>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="panel-bottom"></div>
                                       </div>
                                    </div>
                                 </div>

                              </div>
                           </div>
            <script language="JavaScript" src="<?php echo jsCtrl("ctrl_SysUserAccess"); ?>"></script>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



