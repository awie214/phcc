<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $from = getvalue("txtAttendanceDateFrom");
   $to   = getvalue("txtAttendanceDateTo");
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
   $num_days = cal_days_in_month(CAL_GREGORIAN,$month,$year);
   function checkDateDTR($emprefid,$month,$year,$day){
      include 'conn.e2e.php';
      $date = $year."-".$month."-"."$day";
      $BiometricsID = FindFirst("employees","WHERE RefId = $emprefid","BiometricsID");
      $new_day = date("D",strtotime($year."-".$month."-".$day));
      if ($new_day == "Sun" || $new_day == "Sat") {
         $str = '<td style="width:30px;"></td><td style="width:30px;"></td>';
      } else {
         $str = '<td class="no-entry"></td><td class="no-entry"></td>';      
      }
      
      if ($BiometricsID != "") {
         $biometricsID = strtolower($BiometricsID);
         $sql = "SELECT * FROM USERINFO where NAME = '$biometricsID'";
         $rs = mysqli_query($bio_conn,$sql);
         if ($rs) {
            while ($row = mysqli_fetch_assoc($rs)) {
               $count = 0;
               $userID = $row["USERID"];
               $where_zk = "WHERE USERID = $userID";
               $where_zk .= " AND VERIFYCODE = 1";
               $where_zk .= " AND CHECKTIME >= '$date 00:00:01' AND CHECKTIME <= '$date 23:59:59'";
               $where_zk .= " ORDER BY CHECKTIME"; 
               $new_sql = "SELECT * FROM CHECKINOUT ".$where_zk;
               $new_rs = mysqli_query($bio_conn,$new_sql);
               $new_count = mysqli_num_rows($new_rs);
               if ($new_rs) {
                  if ($new_count == "1") {
                     while ($n_row = mysqli_fetch_assoc($new_rs)) {
                        $CHECKTIME = strtotime($n_row["CHECKTIME"]);
                        if (get_today_minute($CHECKTIME) <= 720) {
                           $str = '<td clas="text-center">'.rpt_HoursFormat(get_today_minute($CHECKTIME)).'</td><td class="no-entry"></td>';
                        } else {
                           $str = '<td class="no-entry"></td><td clas="text-center">'.rpt_HoursFormat(get_today_minute($CHECKTIME)).'</td>';
                        }
                     }
                  } else if ($new_count > 1) {
                     $curr_arr = array();
                     $new_arr = array();
                     $new_arr = ["TimeIn"=>"","TimeOut"=>""];
                     while ($n_row = mysqli_fetch_assoc($new_rs)) {
                        $CHECKTIME = strtotime($n_row["CHECKTIME"]);
                        if (get_today_minute($CHECKTIME) <= 720) {
                           if ($new_arr["TimeIn"] == "") {
                              $new_arr["TimeIn"] = $CHECKTIME;
                           }
                        } else {
                           if ($new_arr["TimeOut"] == "") {
                              $new_arr["TimeOut"] = $CHECKTIME;
                           }
                        }
                     }
                     if ($new_arr["TimeIn"] != "" && $new_arr["TimeIn"] == "") {
                        $str = '<td clas="text-center">'.rpt_HoursFormat(get_today_minute($new_arr["TimeIn"])).'</td><td class="no-entry"></td>';
                     } else if ($new_arr["TimeOut"] != "" && $new_arr["TimeIn"] == "") {
                        $str = '<td class="no-entry"></td><td clas="text-center">'.rpt_HoursFormat(get_today_minute($new_arr["TimeOut"])).'</td>';
                     } else if ($new_arr["TimeOut"] != "" && $new_arr["TimeIn"] != "") {
                        $str = '<td>'.rpt_HoursFormat(get_today_minute($new_arr["TimeIn"])).'</td><td clas="text-center">'.rpt_HoursFormat(get_today_minute($new_arr["TimeOut"])).'</td>';
                     }
                  }   
               }
            }
         }
      }
      return $str;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         .no-entry {
            background: pink;
            width: 30px;
         }
         @media print {
            body {
               font-size: 8pt;
            }
         }
      </style>
   </head>
   <body>
      <div class="container-fluid">
         <table border="1" style="width: 100%"> 
            <tr>
               <th colspan="<?php echo ($num_days + 2 + $num_days); ?>">
                  <?php
                     $errmsg = "";
                     rptHeader(getvalue("RptName"));
                  ?>
                  <p class="txt-center">
                     <?php
                        echo "For the Month of ";
                        echo date("m/d/Y",strtotime($from))." - ".date("m/d/Y",strtotime($to));
                     ?>
                  </p>
               </th>
            </tr>
            <?php
               $division = SelectEach("division","");
               if ($division) {
                  while ($div_row = mysqli_fetch_assoc($division)) {
                     $DivisionRefId = $div_row["RefId"];
                     //if ($div_row["Name"] == "Information and Communications Technology Division") {
                        echo '<tr>';
                           echo '
                              <th colspan="'.($num_days + 2 + $num_days).'">'.$div_row["Name"].'</th>
                           ';
                        echo '</tr>';

                        echo '<tr>';
                           echo '<td rowspan="2" class="text-center" style="width:10%;"><b>Employee Name</b></td>';
                           for ($b=1; $b <= $num_days; $b++) { 
                              if ($b <= 9) $b = "0".$b;
                              $curr_day = date("D",strtotime($year."-".$month."-".$b));
                              echo '<td class="text-center" colspan="2" style="width: 60px;"><b>'.$curr_day." ".$b.' </b></td>';
                           }
                        echo '</tr>';


                        echo '<tr>';
                           for ($c=1; $c <= $num_days; $c++) { 
                              echo '<td class="text-center" style="width: 30px;"><b>IN</b></td>';
                              echo '<td class="text-center" style="width: 30px;"><b>OUT</b></td>';
                           }
                        echo '</tr>';
                        $sql = "SELECT `LastName`,`FirstName`,`MiddleName`,`EmployeesRefId` FROM employees INNER JOIN empinformation WHERE empinformation.DivisionRefId = '$DivisionRefId' AND employees.RefId = empinformation.EmployeesRefId AND (employees.Inactive != 1 OR employees.Inactive IS NULL)  ORDER BY employees.LastName";
                        $rs = mysqli_query($conn,$sql);
                        if (mysqli_num_rows($rs) > 0) {
                           while ($row = mysqli_fetch_assoc($rs)) {
                              $emprefid = $row["EmployeesRefId"];
                              $FirstName = $row["FirstName"];
                              $LastName = $row["LastName"];
                              $FullName = $LastName.", ".$FirstName;
                              echo '<tr>';
                                 echo '<td>'.$FullName.'</td>';
                                 for ($i=1; $i <= $num_days; $i++) { 
                                    if ($i <= 9) $i = "0".$i;
                                    $str = checkDateDTR($emprefid,$month,$year,$i);
                                    echo $str;
                                 }
                              echo '</tr>';
                           }
                        } else {
                           echo '<tr>';
                              echo '
                                 <td colspan="'.($num_days + 2 + $num_days).'">NO EMPLOYEES</td>
                              ';
                           echo '</tr>';
                        }
                        /*$empinfo = SelectEach("empinformation","WHERE DivisionRefId = '$DivisionRefId'");
                        if ($empinfo) {
                           while ($empinfo_row = mysqli_fetch_assoc($empinfo)) {
                              $emprefid = $empinfo_row["EmployeesRefId"];
                              $emp = FindFirst("employees","WHERE RefId = $emprefid","`LastName`,`FirstName`,`MiddleName`");
                              if ($emp) {
                                 $FirstName = $emp["FirstName"];
                                 $LastName = $emp["LastName"];
                                 $MiddleName = $emp["MiddleName"];
                                 $FullName = $LastName.", ".$FirstName;
                                 echo '<tr>';
                                    echo '<td>'.$FullName.'</td>';
                                    for ($i=1; $i <= $num_days; $i++) { 
                                       if ($i <= 9) $i = "0".$i;
                                       $str = checkDateDTR($emprefid,$month,$year,$i);
                                       echo $str;
                                    }
                                 echo '</tr>';
                              }
                           }
                        } else {
                           echo '<tr>';
                              echo '
                                 <td colspan="'.($num_days + 2 + $num_days).'">NO EMPLOYEES</td>
                              ';
                           echo '</tr>';
                        }*/
                     //}
                  }
               }
            ?>
         </table>
      </div>
   </body>
</html>
