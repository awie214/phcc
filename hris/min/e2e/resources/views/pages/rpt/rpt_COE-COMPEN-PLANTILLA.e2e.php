<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
          <div class="row">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p style="text-indent: 30px;">
                This is to certify that (EMPLOYEE NAME, ALL CAPS, BOLD) is a permanent employee of the Philippine Competition Commission (PCC). He has been with the Commission from  August 1, 2016 to present, and is currently appointed as Director IV. As such, he receives the corresponding monthly compensation, as follows:
               </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <table border="1">
                <tr align="center">
                  <td><label>ITEM</label></td>
                  <td><label>AMOUNT (in PhP)</label></td>
                </tr>
                <tr>
                  <td>Basic Salary</td>
                  <td align="right">169 300.00</td>
                </tr>
                <tr>
                  <td>Personal Economic Relief Allowance (PERA)</td>
                  <td align="right">2 000.00</td>
                </tr>
                <tr>
                  <td>Representation Allowance (RA)</td>
                  <td align="right">9 000.00</td>
                </tr>
                <tr>
                  <td>Extraordinary and Miscellaneous Expenses</td>
                  <td align="right">9 800.00</td>
                </tr>
                <tr>
                  <td>Communication Allowance</td>
                  <td align="right">3 000.00</td>
                </tr>
                <tr>
                  <td><label>TOTAL COMPENSATION</label></td>
                  <td align="right">193 100.00</td>
                </tr>
              </table>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p>
                This certification is being issued upon the request of <b>Ms. Milan</b> for whatever legal purpose it may serve.
              </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-1"></div>
            <div class="col-xs-10">
              <p>
                Issued this 16th day of November 2017, in Pasig City, Philippines.
              </p>
            </div>
          </div>
          <div class="row margin">
            <div class="col-xs-8"></div>
            <div class="col-xs-4">
              <p>
                <label>KENNETH V. TANATE</label><br>
                Director IV<br>
                Administrative Office
              </p>
            </div>
          </div>



      </div>
      <?php rptFooter(); ?>
   </body>
</html>