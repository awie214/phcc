<?php
	include_once 'constant.e2e.php';
	require_once pathClass.'0620functions.e2e.php';
	$refid 		= getvalue("refid");
	$row 		= FindFirst("spms_ops","WHERE RefId = '$refid'","*");
	if ($row) {
		$emprefid 	= $row["EmployeesRefId"];
		$emp_row 	= FindFirst("employees","WHERE RefId = '$emprefid'","*");
		$LastName 	= $emp_row["LastName"];
		$FirstName 	= $emp_row["FirstName"];
		$MiddleName = $emp_row["MiddleName"];
		$MiddleName = substr($MiddleName, 0, 1);
		$ExtName 	= $emp_row["ExtName"];
		$FullName 	= $FirstName." ".$MiddleName.". ".$LastName." ".$ExtName; 
		$FullName 	= strtoupper($FullName);
		$empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
		if ($empinfo_row) {
			$Division 	= strtoupper(getRecord("division",$empinfo_row["DivisionRefId"],"Name"));
			$Position 	= strtoupper(getRecord("Position",$empinfo_row["PositionRefId"],"Name"));
			$Office 		= strtoupper(getRecord("Office",$empinfo_row["OfficeRefId"],"Name"));
		} else {
			$Division = $Position = $Office = "";
		}


		$semester 			= $row["semester"];
		$year 				= $row["year"];
		$rating 			= $row["rating"];
		$total_rating 		= $row["total_rating"];
		$premium_point 		= $row["premium_point"];
		$overall_rating 	= $row["overall_rating"];
		$adjectival_rating 	= $row["adjectival_rating"];
		if ($semester == 1) {
			$semester = "JANUARY TO JUNE";
		} else if ($semester == 2) {
			$semester = "JULY TO DECEMBER";
		}
	} else {
		echo "Record Not Found";
		return false;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<?php
		include_once 'pageHEAD.e2e.php';
	?>
	<title></title>
	<style type="text/css">
		body {
			font-family: arial;
		}
		thead {
			font-weight: 300;
			text-align: center;
			padding: 2px;
		}
		td {
			padding: 2px;
		}
		.company {
			font-family: Copperplate, "Copperplate Gothic Light" !important;
			font-size: 15pt;
		}
		.border {border: 1px solid black;}
		@page {
		  	size: A4;
		}
		@media print {
		  	@page {size: landscape}
		}
	</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<div class="row" style="page-break-after: always;">
					<div class="col-xs-12" style="border: 2px solid black;">
						<div class="row">
							<div class="col-xs-12 text-center company" style="border-bottom: 2px solid black; padding: 5px;">
								<b>Philippine Competition Commission</b>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12" style="border-bottom: 2px solid black; padding: 5px; padding-top: 30px; padding-bottom: 30px;">
								<div class="row">
									<div class="col-xs-12 text-center">
										<b>
											OFFICE PERFORMANCE SCORECARD (OPS)
											<br>
											<?php echo $Office; ?>
										</b>
										<br><br><br><br>
									</div>
								</div>
								<div class="row margin-top">
									<div class="col-xs-12">
										I, <b><u><?php echo $FullName; ?></u></b>, Head of <b><u><?php echo $Office; ?></u></b>, commit to deliver and agree to be rated on the attainment of the following targets in accordance with the indicated measure for the period <b><u><?php echo $semester." ".$year; ?></u></b>.
										<br><br><br><br><br>
									</div>
								</div>
								<div class="row margin-top">
									<div class="col-xs-8"></div>
									<div class="col-xs-4 text-center">
										<u><b><?php echo $FullName; ?></b></u>
										<br>
										<b><?php echo $Position; ?></b>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12" style="padding: 5px;">
								<table width="100%" border="2" class="margin-top">
									<tr>
										<td class="border" style="width: 80%;">
											APPROVED BY:
										</td>
										<td class="border" rowspan="2" style="width: 20%;" valign="top">
											DATE:
										</td>
									</tr>
									<tr>
										<td class="border text-center" valign="bottom" style="height: 100px;">
											<u><b>(SIGNATURE OVER PRINTED NAME OF OFFICE HEAD)</b></u>
											<br>
											<b>DIRECTOR III/IV</b>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="row margin-top" style="page-break-after: always;">
					<div class="col=col-xs-12" style="border: 2px solid black;">
						<div class="row">
							<div class="col-xs-12" style="border-bottom: 2px solid black; padding: 5px;">
								<table style="width: 100%" border="1">
                           <thead>
                              <tr>
                                 <th class="text-center" rowspan="2" style="">
                                    Performance Objective
                                 </th>
                                 <th class="text-center" rowspan="2" style="">
                                    Measure
                                 </th>
                                 <th class="text-center" rowspan="2" style="">
                                    Target
                                 </th>
                                 <th class="text-center" rowspan="2" style="">
                                    Weight
                                 </th>
                                 <th class="text-center" colspan="5">
                                    Rating
                                 </th>
                                 <th class="text-center" rowspan="2" style="">
                                    Alloted<br>Budget
                                 </th>
                                 <th class="text-center" rowspan="2" style="">
                                    Accountable<br>Division
                                 </th>
                                 <th class="text-center" rowspan="2" style="">
                                    Actual Accomplishment
                                 </th>
                              </tr>
                              <tr>
                                 <th class="text-center" style="">Quality</th>
                                 <th class="text-center" style="">Efficiency</th>
                                 <th class="text-center" style="">Timeliness</th>
                                 <th class="text-center" style="">Raw Score</th>
                                 <th class="text-center" style="">Weighted<br>Score</th>
                              </tr>
                           </thead>
                           <tbody>
                           	<tr>
	                              <td colspan="12" style="background: #999999;">
	                                 <b>STRATEGIC OBEJCTIVES (_%)</b>
	                              </td>
	                           </tr>
	                           <?php
	                           	$strategic_where = "WHERE ops_id = '$refid' AND type = 'Strategic Function'";
	                           	$s_rs = SelectEach("ops_details",$strategic_where);
	                           	if ($s_rs) {
	                           		while ($s_row = mysqli_fetch_assoc($s_rs)) {
	                           				$objectives_id 	= $s_row["objectives_id"];
	                           				$measure 			= $s_row["measure"];
	                           				$target 				= $s_row["target"];
	                           				$weight 				= $s_row["weight"];
	                           				$weightedscore 	= $s_row["weightedscore"];
	                           				$rawscore 			= $s_row["rawscore"];
	                           				$accomplishment 	= $s_row["accomplishment"];
	                           				$quality 			= $s_row["quality"];
	                           				$effectiveness 	= $s_row["effectiveness"];
	                           				$timeliness 		= $s_row["timeliness"];
	                           				$accountable 		= $s_row["accountable"];
	                           				$budget 				= $s_row["budget"];
	                           				$objective 			= getRecord("objectives",$objectives_id,"Name");
	                           				echo '
	                           					<tr>
					                          			<td>'.$objective.'</td>
					                          			<td>'.$measure.'</td>
					                          			<td>'.$target.'</td>
					                          			<td class="text-center">'.$weight.'</td>
					                          			<td class="text-center">'.$quality.'</td>
					                          			<td class="text-center">'.$effectiveness.'</td>
					                          			<td class="text-center">'.$timeliness.'</td>
					                          			<td class="text-center">'.$rawscore.'</td>
					                          			<td class="text-center">'.$weightedscore.'</td>
					                          			<td class="text-center">'.$accountable.'</td>
					                          			<td class="text-center">'.$budget.'</td>
					                          			<td>'.$accomplishment.'</td>
					                          		</tr>
	                           				';
	                           			}	
	                           	} else {
	                           		for ($a=1; $a <= 3; $a++) { 
	                          	?>
	                          			<tr>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          		</tr>
	                          	<?php
	                           		}
	                           	}
	                           ?>
	                           <tr>
	                           	<td colspan="3" style="background: #cccccc;">
	                           		<b>RATING</b>
	                           	</td>
	                           	<td colspan="5"></td>
	                           	<td style="background: #cccccc;"></td>
	                           	<td></td>
	                           </tr>
	                           <tr>
	                              <td colspan="12" style="background: #999999;">
	                                 <b>CORE FUNCTIONS (_%)</b>
	                              </td>
	                           </tr>
	                           <?php
	                           	$core_where = "WHERE ops_id = '$refid' AND type = 'Core Function'";
	                           	$s_rs = SelectEach("ops_details",$core_where);
	                           	if ($s_rs) {
	                           		while ($s_row = mysqli_fetch_assoc($s_rs)) {
	                           				$objectives_id 	= $s_row["objectives_id"];
	                           				$measure 			= $s_row["measure"];
	                           				$target 				= $s_row["target"];
	                           				$weight 				= $s_row["weight"];
	                           				$weightedscore 	= $s_row["weightedscore"];
	                           				$rawscore 			= $s_row["rawscore"];
	                           				$accomplishment 	= $s_row["accomplishment"];
	                           				$quality 			= $s_row["quality"];
	                           				$effectiveness 	= $s_row["effectiveness"];
	                           				$timeliness 		= $s_row["timeliness"];
	                           				$accountable 		= $s_row["accountable"];
	                           				$budget 				= $s_row["budget"];
	                           				$objective 			= getRecord("objectives",$objectives_id,"Name");
	                           				echo '
	                           					<tr>
					                          			<td>'.$objective.'</td>
					                          			<td>'.$measure.'</td>
					                          			<td>'.$target.'</td>
					                          			<td class="text-center">'.$weight.'</td>
					                          			<td class="text-center">'.$quality.'</td>
					                          			<td class="text-center">'.$effectiveness.'</td>
					                          			<td class="text-center">'.$timeliness.'</td>
					                          			<td class="text-center">'.$rawscore.'</td>
					                          			<td class="text-center">'.$weightedscore.'</td>
					                          			<td class="text-center">'.$accountable.'</td>
					                          			<td class="text-center">'.$budget.'</td>
					                          			<td>'.$accomplishment.'</td>
					                          		</tr>
	                           				';
	                           			}	
	                           	} else {
	                           		for ($a=1; $a <= 3; $a++) { 
	                          	?>
	                          			<tr>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          			<td>&nbsp;</td>
		                          		</tr>
	                          	<?php
	                           		}
	                           	}
	                           ?>
	                           <tr>
	                           	<td colspan="3" style="background: #cccccc;">
	                           		<b>RATING</b>
	                           	</td>
	                           	<td colspan="5"></td>
	                           	<td style="background: #cccccc;"></td>
	                           	<td colspan="3"></td>
	                           </tr>
	                           <tr>
	                           	<td colspan="3" style="background: #cccccc;">
	                           		<b>TOTAL RATING</b>
	                           	</td>
	                           	<td colspan="5"></td>
	                           	<td style="background: #cccccc;"></td>
	                           	<<td colspan="3"></td>
	                           </tr>
	                           <tr>
	                           	<td colspan="3" style="background: #cccccc;">
	                           		<b>PREMIUM POINTS FOR INTERVENING TASKS (if applicable)</b>
	                           	</td>
	                           	<td colspan="5"></td>
	                           	<td style="background: #cccccc;"></td>
	                           	<<td colspan="3"></td>
	                           </tr>
	                           <tr>
	                           	<td colspan="3" style="background: #cccccc;">
	                           		<b>OVERALL NUMERICAL RATING</b>
	                           	</td>
	                           	<td colspan="5"></td>
	                           	<td style="background: #cccccc;"></td>
	                           	<<td colspan="3"></td>
	                           </tr>
	                           <tr>
	                           	<td colspan="3" style="background: #cccccc;">
	                           		<b>ADJECTIVAL RATING</b>
	                           	</td>
	                           	<td colspan="9"></td>
	                           </tr>
                           </tbody>
                        </table>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 text-center" style="border-bottom: 2px solid black; padding: 20px;">
								&nbsp;
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12" style="padding: 5px;">
								<table width="100%" border="2" class="margin-top">
									<tr>
										<td class="border" style="width: 50%;">
											DISCUSSED WITH:
										</td>
										<td class="border" style="width: 50%;">
											FINAL RATING BY:
										</td>
									</tr>
									<tr>
										<td class="border" style="height: 70px;">&nbsp;</td>
										<td class="border">&nbsp;</td>
									</tr>
									<tr>
										<td class="border text-center">
											HEAD OF DIVISION
										</td>
										<td class="border text-center">
											HEAD OF OFFICE
										</td>
									</tr>
									<tr>
										<td class="border text-center">
											Date: ____________________
										</td>
										<td class="border text-center">
											Date: ____________________
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<?php
					spacer(100);
					rptHeader("Form 1A: Rating Matrix");
				?>
				<div class="row margin-top">
					<div class="col-xs-12">
						<table width="100%" border="1">
							<thead>
								<tr>
									<th class="text-center">No.</th>
									<th class="text-center">Measure</th>
									<th class="text-center">Category</th>
									<th class="text-center">Target</th>
									<th class="text-center" colspan="2">Rating Scale</th>
								</tr>	
							</thead>
							<tbody>
								<?php
	                           	$strategic_where = "WHERE ops_id = '$refid'";
	                           	$s_rs = SelectEach("ops_details",$strategic_where);
	                           	$count = 0;
	                           	if ($s_rs) {
	                           		while ($s_row = mysqli_fetch_assoc($s_rs)) {
	                           			$count++;
                           				$measure 			= $s_row["measure"];
                           				$target 			= $s_row["target"];
                           				$quality 			= $s_row["quality"];
                           				$effectiveness 		= $s_row["effectiveness"];
                           				$timeliness 		= $s_row["timeliness"];
                           				if ($quality != "") {
                           					echo '
                           						<tr>
	                           						<td class="text-center" rowspan="6">'.$count.'</td>
	                           						<td rowspan="6">'.$measure.'</td>
	                           						<td class="text-center" rowspan="6">Quality</td>
	                           						<td class="text-center" rowspan="6">'.$target.'</td>
	                           					</tr>	
                           					';
                           					for ($i=5; $i > 0; $i-=1) { 
		                     					echo '
			                     					<tr>
		                     							<td class="text-center">'.$i.'</td>
		                     							<td>'.$s_row["q".$i].'</td>
			                     					</tr>
			                     				';
		                     				}
                           				}
	                           		}	
	                           	}
	                          	?>
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>