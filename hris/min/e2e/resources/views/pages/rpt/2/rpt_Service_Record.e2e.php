<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         table {
            page-break-after: always;
         }
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            while ($row = mysqli_fetch_assoc($rsEmployees)) {
         ?>
         <table>
            <thead>
               <tr>
                  <th colspan="11" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                     <?php spacer(30); ?>
                     <div class="row">
                        <div class="col-sm-3 text-right">
                           Employee Name: 
                        </div>
                        <div class="col-sm-9 text-left">
                           <?php echo $row["LastName"].", ".$row["FirstName"]." ".$row["LastName"]?>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-3 text-right">
                           BIRTH:
                        </div>
                        <div class="col-sm-3 text-center">
                           <?php echo $row["BirthDate"]; ?>
                           <br>
                           (DATE)
                        </div>
                        <div class="col-sm-1"></div>
                        <div class="col-sm-3 text-center">
                           <?php echo $row["BirthPlace"]; ?>
                           <br>
                           (PLACE)
                        </div>
                     </div>
                  </th>
               </tr>  
               <tr>
                  <td colspan="11">
                     This is to certify that the employee named hereinabove actually rendered services in this Office as shown by the service record below, each line of which is supported by appointment and other papers actually issued by the Office and approved by the authorities concerned unless otherwise indicated.                                                
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th colspan="2">SERVICE</th>
                  <th colspan="3">RECORD OF APPOINTMENT</th>
                  <th colspan="2">OFFICE ENTITY/DIVISION</th>
                  <th rowspan="3"> LEAVE OF <br>Absence <br>w/o pay</th>
                  <th colspan="2">SEPARATION</th>
                  <th rowspan="3">REMARKS</th>
               </tr>
               <tr class="colHEADER">
                  <th colspan="2">Inclusive Dates</th>
                  <th rowspan="2">Designation</th>
                  <th rowspan="2">Status</th>
                  <th rowspan="2">
                     (Pesos)<br>Annual Salary
                  </th>
                  <th rowspan="2">
                     STATION / <br>PLACE OF <br>ASSIGNMENT
                  </th>
                  <th rowspan="2">Branch</th>
                  <th rowspan="2">Date</th>
                  <th rowspan="2">Cause</th>
               </tr>
               <tr class="colHEADER">
                  <th>From</th>
                  <th>To</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  for ($i=1; $i < 5; $i++) { 
               ?>
                  <tr>
                     <td>&nbsp;</td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                  </tr>
               <?php } ?>
            </tbody>
            <tr>
               <td colspan="11">
                  <div class="row">
                     <div class="col-xs-12">
                        Issued in compliance with E.O. 54 dated August 10, 1954, and in accordance with GSIS Circular No. 58, dated August 10, 1954.
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-xs-8">
                     </div> 
                     <div class="col-xs-4">
                        Certified Correct:
                     </div>
                  </div>
                  <?php spacer(20); ?>
                  <div class="row">
                     <div class="col-xs-1"></div>
                     <div class="col-xs-7">
                        <?php echo date("d-M-Y",time()); ?>
                        <br>
                        DATE
                     </div>
                     <div class="col-xs-4 text-center">
                        KENNETH V. TANATE
                        <br>
                        Director IV, Administrative Office        
                     </div>
                  </div>
               </td>
            </tr>
         </table>
         <?php } ?>
      </div>
   </body>
</html>
