<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <!--<table>
            <thead>
               <tr>
                  <th>
                  </th>
               </tr>   
            </thead>
            <tbody>
               <tr>
                  <td></td>
               </tr>   
            </tbody>
            <tfoot>
               <tr>
                  <td></td>
               </tr>   
            </tfoot>
         </table>-->   
         <table>
            <thead>
               <tr>
                  <th colspan="9" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                     <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
                  </th>
                  
               </tr>
               <tr class="colHEADER" align="center">
                  <th rowspan="2">ID</th>
                  <th rowspan="2">EMPLOYEE NAME</th>
                  <th rowspan="2">POSITION</th>
                  <th colspan="5">SALARY</th>
                  <th rowspan="2">NET PAY</th>
               </tr>
               <tr class="colHEADER" align="center">
                  <th>BASIC</th>
                  <th>MID-YEAR</th>
                  <th>1/2 CASH GIFT</th>
                  <th>GROSS PAY</th>
                  <th>W/ TAX</th>
               </tr>

            </thead>
            <tbody>
                  <?php for ($j=1;$j<=60;$j++) 
                  { ?>
                     <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                     </tr>
                  <?php 
                  } ?>
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="9">
                     <?php rptFooter(); ?>
                  </td>
               </tr>   
            </tfoot>
         </table>   
      </div>
   </body>
</html>