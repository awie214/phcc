<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <!--<table>
            <thead>
               <tr>
                  <th>
                  </th>
               </tr>   
            </thead>
            <tbody>
               <tr>
                  <td></td>
               </tr>   
            </tbody>
            <tfoot>
               <tr>
                  <td></td>
               </tr>   
            </tfoot>
         </table>-->   
         <table>
            <thead>
               <tr>
                  <th colspan="13" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getRptName(getvalue("drpReportKind")));
                     ?>
                     <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
                  </th>
               </tr>   
               <tr class="colHEADER">
                  <th>ID</th>
                  <th>EMPLOYEE NAME</th>
                  <th>POSITION/DESIGNATION</th>
                  <th>OFFICE</th>
                  <th>EXTRAORDINARY EXPENSES</th>
                  <th>TOTAL AMOUNT</th>
                  <th>&nbsp;</th>
                  <th>SIGNATURE</th>
                  <th>REMARKS</th>
               </tr>
               
            </thead>
            <tbody>
              <?php 
                 $rs = SelectEach("empinformation","GROUP BY RefId LIMIT 60");
                 $i = 0;
                 if ($rs){
                    while($row = mysqli_fetch_assoc($rs)){
                    $i++;
              ?>
              <tr>
                 <td><?php echo $row["EmployeesRefId"]; ?></td>
                 <td><?php echo getRecord("employees",$row["EmployeesRefId"],"FirstName"); ?></td>
                 <td><?php echo getRecord("position",$row["PositionRefId"],"Name"); ?></td>
                 <td><?php echo getRecord("office",$row["OfficeRefId"],"Name"); ?></td>
                 <td align="right">5</td>
                 <td align="right">6</td>
                 <td align="center"><?php echo $i;?></td>
                 <td align="center"></td>
                 <td></td>
              </tr>
              <?php
                    }
                 }
              ?>
              <tr>
                <td colspan="4" align="right">GRAND TOTAL</td>
                <td></td>
                <td></td>
                <td colspan="3"></td>
              </tr>
              <tr><td colspan="9">&nbsp;</td></tr>
               <tr>
                  <td colspan="3">Certified Correct:</td>
                  <td colspan="6">Approved for Payment:</td>
               </tr>
               <tr>
                  <td colspan="3">
                     ANTONIA LYNNELY L. BAUTISTA
                  </td>
                  <td colspan="6">
                     GWEN GRECIA-DE VERA
                  </td>
               </tr>
               <tr>
                  <td colspan="3">Chief Admin Officer, HRDD</td>
                  <td colspan="6">Executive Director</td>
               </tr>
               <tr>
                  <td colspan="3">
                     Certified: Supporting documents complete and<br>
                     proper, and cash available in the amount of<br>
                     Php _____________________.
                  </td>
                  <td colspan="6">
                     Certified: Each employee whose name appears on the payroll has been<br>
                     paid the amount as indicated opposite his/her name.
                  </td>
               </tr>
               <tr>
                  <td colspan="3">
                     CAROLYN V. AQUINO<br>
                     Accountant III, FPMO
                  </td>
                  <td colspan="6">
                     FLERIDA J. GIRON<br>
                     Cashier III, ALO
                  </td>
               </tr>         
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="13">
                     <?php rptFooter(); ?>
                  </td>
               </tr>   
            </tfoot>
         </table>

      </div>
   </body>
</html>