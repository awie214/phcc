<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <table>
            <thead>
               <tr>
                  <th colspan="9" align="center" style="text-align:center;">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                     <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
                  </th>
               </tr>
               <tr class="colHEADER" align="center">
                  <th>ID</th>
                  <th>EMPLOYEE NAME</th>
                  <th>Personal Share (PS)</th>
                  <th>GOVERNMENT SHARE (GS)</th>
                  <th>TOTAL</th>
               </tr>
            </thead>
            <tbody>
                  <?php for ($j=1;$j<=60;$j++) 
                  { ?>
                     <tr>
                        <td>&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                     </tr>
                  <?php 
                  } ?>
                  <tr>
                     <td colspan="5">
                        <div class="row">
                           <div class="col-sm-6">
                              Certified Correct:
                           </div>
                           <div class="col-sm-6">
                              Approved for Payment:
                           </div>
                        </div>
                        <?php spacer(20); ?>
                        <div class="row">
                           <div class="col-sm-6">
                              <u>ANTONIA LYNNELY L. BAUTISTA</u>
                              <br>
                              Chief Admin Officer, HRDD
                           </div>
                           <div class="col-sm-6">
                              <u>KENNETH V. TANATE, PhD</u>
                              <br>
                              Director IV, ALO
                           </div>
                        </div>
                     </td>
                  </tr>
            </tbody>
            <tfoot>
               <tr>
                  <td colspan="9">
                     <?php rptFooter(); ?>
                  </td>
               </tr>   
            </tfoot>
         </table>
      </div>
   </body>
</html>