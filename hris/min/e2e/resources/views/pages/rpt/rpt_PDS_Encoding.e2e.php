<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         
      </style>
   </head>
   <body>
      <div class="container-fluid">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
         
         <table style="width: 100%;">
            <thead>
               <tr>
                  <td colspan="19">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th>#</th>
                  <th>Employee Name</th>
                  <th>Position</th>
                  <th>Personal Information (=>16 - required)</th>
                  <th>PI:Address (=>8 fields- required)</th>
                  <th>PI:Contact (=>2 fields- required)</th>
                  <th>PI:ID's (=>4 fields- required)</th>
                  <th>Family Background (=>6 fields- required)</th>
                  <th>Education: Elementary (=>4 fields required)</th>
                  <th>Education:Secondary (=>4 fields required) </th>
                  <th>Education:Vacational (=>0 fields required)</th>
                  <th>Education:College (=>5 fields required)</th>
                  <th>Education:Masteral (=>0 fields required)</th>
                  <th>Eligibility(=>4 fields required)</th>
                  <th>Work Experience(=>7fields required)</th>
                  <th>Voluntary Work(=>5fields required)</th>
                  <th>Learning And Development(=>7fields required)</th>
                  <th>Other Information (=>0 fields required)</th>
                  <th>PDS Questions (=>12 fields required)</th>
                  <th>Reference (=>9 fields required)</th>
                  <th>No. of Accomplished field (=>93 required)</th>
                  <th>PDS Status</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                     $emprefid      = $row_emp["RefId"];
                     $where         = "WHERE EmployeesRefId = '$emprefid'";
                     $family        = FindFirst("employeesfamily",$where,"*");
                     $elementary    = FindFirst("employeeseduc",$where." AND LevelType = 1","*");
                     $secondary     = FindFirst("employeeseduc",$where." AND LevelType = 2","*");
                     $vocational    = SelectEach("employeeseduc",$where." AND LevelType = 3");
                     $college       = SelectEach("employeeseduc",$where." AND LevelType = 4");
                     $masteral      = SelectEach("employeeseduc",$where." AND LevelType = 5");
                     $eligibility   = SelectEach("employeeselegibility",$where);
                     $workexp       = SelectEach("employeesworkexperience",$where);
                     $voluntary     = SelectEach("employeesvoluntary",$where);
                     $lnd           = SelectEach("employeestraining",$where);
                     $other         = SelectEach("employeesotherinfo",$where);
                     $reference     = SelectEach("employeesreference",$where);
                     $pdsq          = FindFirst("employeespdsq",$where,"*");
                     $Position      = FindFirst("empinformation",$where,"PositionRefId");
                     $Position      = getRecord("position",$Position,"Name");
                     

                     $count++;
                     $total         = 0;
                     $status        = "";
                     for ($i=1; $i <= 19; $i++) { 
                        ${"col_".$i} = 0;
                     }
                     if ($row_emp["LastName"] != "") $col_1++;
                     if ($row_emp["FirstName"] != "") $col_1++; 
                     if ($row_emp["MiddleName"] != "") $col_1++;
                     if ($row_emp["ExtName"] != "") $col_1++;
                     if ($row_emp["BirthPlace"] != "") $col_1++;
                     if ($row_emp["BirthDate"] != "") $col_1++;
                     if ($row_emp["Sex"] != "") $col_1++;
                     if ($row_emp["CivilStatus"] != "") $col_1++;
                     if ($row_emp["isFilipino"] != "") $col_1++;
                     if ($row_emp["Height"] != "") $col_1++;
                     if ($row_emp["Weight"] != "") $col_1++;
                     if ($row_emp["BloodTypeRefId"] != "") $col_1++;
                     if ($row_emp["AgencyId"] != "") $col_1++;
                     if ($row_emp["GovtIssuedID"] != "") $col_1++;
                     if ($row_emp["GovtIssuedIDNo"] != "") $col_1++;
                     if ($row_emp["GovtIssuedIDDate"] != "") $col_1++;
                     if ($row_emp["GovtIssuedIDPlace"] != "") $col_1++;


                     if ($row_emp["ResiHouseNo"] != "") $col_2++;
                     if ($row_emp["ResiStreet"] != "") $col_2++;
                     if ($row_emp["ResiSubd"] != "") $col_2++;
                     if ($row_emp["ResiBrgy"] != "") $col_2++;
                     if ($row_emp["ResiAddCityRefId"] != "") $col_2++;
                     if ($row_emp["ResiAddProvinceRefId"] != "") $col_2++;
                     if ($row_emp["ResiCountryRefId"] != "") $col_2++;
                     if ($row_emp["PermanentHouseNo"] != "") $col_2++;
                     if ($row_emp["PermanentStreet"] != "") $col_2++;
                     if ($row_emp["PermanentSubd"] != "") $col_2++;
                     if ($row_emp["PermanentBrgy"] != "") $col_2++;
                     if ($row_emp["PermanentAddCityRefId"] != "") $col_2++;
                     if ($row_emp["PermanentAddProvinceRefId"] != "") $col_2++;
                     if ($row_emp["PermanentCountryRefId"] != "") $col_2++;

                     if ($row_emp["TelNo"] != "") $col_3++;
                     if ($row_emp["MobileNo"] != "") $col_3++;                     
                     if ($row_emp["EmailAdd"] != "") $col_3++;

                     if ($row_emp["GSIS"] != "") $col_4++;
                     if ($row_emp["PAGIBIG"] != "") $col_4++;
                     if ($row_emp["SSS"] != "") $col_4++;
                     if ($row_emp["TIN"] != "") $col_4++;
                     if ($row_emp["PHIC"] != "") $col_4++;

                     if ($family) {
                        if ($family["FatherLastName"] != "") $col_5++;
                        if ($family["FatherFirstName"] != "") $col_5++;
                        if ($family["FatherMiddleName"] != "") $col_5++;
                        if ($family["FatherExtName"] != "") $col_5++;
                        if ($family["MotherLastName"] != "") $col_5++;
                        if ($family["MotherFirstName"] != "") $col_5++;
                        if ($family["MotherMiddleName"] != "") $col_5++;   
                     }

                     if ($elementary) {
                        if ($elementary["SchoolsRefId"] != "") $col_6++;
                        if ($elementary["YearGraduated"] != "") $col_6++;
                        if ($elementary["HighestGrade"] != "") $col_6++;
                        if ($elementary["DateFrom"] != "") $col_6++;
                        if ($elementary["DateTo"] != "") $col_6++;
                        if ($elementary["Honors"] != "") $col_6++;
                     }

                     if ($secondary) {
                        if ($secondary["SchoolsRefId"] != "") $col_7++;
                        if ($secondary["YearGraduated"] != "") $col_7++;
                        if ($secondary["HighestGrade"] != "") $col_7++;
                        if ($secondary["DateFrom"] != "") $col_7++;
                        if ($secondary["DateTo"] != "") $col_7++;
                        if ($secondary["Honors"] != "") $col_7++;
                     }

                     if ($vocational) {
                        while ($vocational_row = mysqli_fetch_assoc($vocational)) {
                           if ($vocational_row["SchoolsRefId"] != "") $col_8++;
                           if ($vocational_row["CourseRefId"] != "") $col_8++;
                           if ($vocational_row["YearGraduated"] != "") $col_8++;
                           if ($vocational_row["HighestGrade"] != "") $col_8++;
                           if ($vocational_row["DateFrom"] != "") $col_8++;
                           if ($vocational_row["DateTo"] != "") $col_8++;
                           if ($vocational_row["Honors"] != "") $col_8++;
                        }
                     }

                     if ($college) {
                        while ($college_row = mysqli_fetch_assoc($college)) {
                           if ($college_row["SchoolsRefId"] != "") $col_9++;
                           if ($college_row["CourseRefId"] != "") $col_9++;
                           if ($college_row["YearGraduated"] != "") $col_9++;
                           if ($college_row["HighestGrade"] != "") $col_9++;
                           if ($college_row["DateFrom"] != "") $col_9++;
                           if ($college_row["DateTo"] != "") $col_9++;
                           if ($college_row["Honors"] != "") $col_9++;
                        }
                     }

                     if ($masteral) {
                        while ($masteral_row = mysqli_fetch_assoc($masteral)) {
                           if ($masteral_row["SchoolsRefId"] != "") $col_10++;
                           if ($masteral_row["CourseRefId"] != "") $col_10++;
                           if ($masteral_row["YearGraduated"] != "") $col_10++;
                           if ($masteral_row["HighestGrade"] != "") $col_10++;
                           if ($masteral_row["DateFrom"] != "") $col_10++;
                           if ($masteral_row["DateTo"] != "") $col_10++;
                           if ($masteral_row["Honors"] != "") $col_10++;
                        }
                     }

                     if ($eligibility) {
                        while ($eligibility_row = mysqli_fetch_assoc($eligibility)) {
                           if ($eligibility_row["CareerServiceRefId"] != "") $col_11++;
                           if ($eligibility_row["Rating"] != "") $col_11++;
                           if ($eligibility_row["ExamDate"] != "") $col_11++;
                           if ($eligibility_row["ExamPlace"] != "") $col_11++;
                           if ($eligibility_row["LicenseNo"] != "") $col_11++;
                           if ($eligibility_row["LicenseReleasedDate"] != "") $col_11++;
                        }
                     }

                     if ($workexp) {
                        while ($workexp_row = mysqli_fetch_assoc($workexp)) {
                           if ($workexp_row["WorkStartDate"] != "") $col_12++;
                           if ($workexp_row["WorkEndDate"] != "") $col_12++;
                           if ($workexp_row["SalaryAmount"] != "") $col_12++;
                           if ($workexp_row["AgencyRefId"] != "") $col_12++;
                           if ($workexp_row["PositionRefId"] != "") $col_12++;
                           if ($workexp_row["EmpStatusRefId"] != "") $col_12++;
                           if ($workexp_row["SalaryGradeRefId"] != "") $col_12++;
                           if ($workexp_row["StepIncrementRefId"] != "") $col_12++;
                           if ($workexp_row["JobGradeRefId"] != "") $col_12++;
                        }
                     }

                     if ($voluntary) {
                        while ($voluntary_row = mysqli_fetch_assoc($voluntary)) {
                           if ($voluntary_row["OrganizationRefId"] != "") $col_13++;
                           if ($voluntary_row["StartDate"] != "") $col_13++;
                           if ($voluntary_row["EndDate"] != "") $col_13++;
                           if ($voluntary_row["NumofHrs"] != "") $col_13++;
                           if ($voluntary_row["WorksNature"] != "") $col_13++;
                        }
                     }

                     if ($lnd) {
                        while ($lnd_row = mysqli_fetch_assoc($lnd)) {
                           if ($lnd_row["SeminarsRefId"] != "") $col_14++;
                           if ($lnd_row["StartDate"] != "") $col_14++;
                           if ($lnd_row["EndDate"] != "") $col_14++;
                           if ($lnd_row["NumofHrs"] != "") $col_14++;
                           if ($lnd_row["SponsorRefId"] != "") $col_14++;
                           if ($lnd_row["SeminarPlace"] != "") $col_14++;
                           if ($lnd_row["SeminarTypeRefId"] != "") $col_14++;
                           if ($lnd_row["SeminarClassRefId"] != "") $col_14++;
                           if ($lnd_row["Description"] != "") $col_14++;
                        }
                     }

                     if ($other) {
                        while ($other_row = mysqli_fetch_assoc($other)) {
                           if ($other_row["Skills"] != "") $col_15++;
                           if ($other_row["Recognition"] != "") $col_15++;
                           if ($other_row["Affiliates"] != "") $col_15++;
                        }
                     }

                     if ($pdsq) {
                        if ($pdsq["Q1a"] != "") $col_16++;
                        if ($pdsq["Q1aexp"] != "") $col_16++;
                        if ($pdsq["Q1b"] != "") $col_16++;
                        if ($pdsq["Q1bexp"] != "") $col_16++;
                        if ($pdsq["Q2a"] != "") $col_16++;
                        if ($pdsq["Q2aexp"] != "") $col_16++;
                        if ($pdsq["Q2b"] != "") $col_16++;   
                        if ($pdsq["Q2bexp"] != "") $col_16++;
                        if ($pdsq["Q2DateFiled"] != "") $col_16++;
                        if ($pdsq["Q2CaseStatus"] != "") $col_16++;
                        if ($pdsq["Q3a"] != "") $col_16++;
                        if ($pdsq["Q3aexp"] != "") $col_16++;
                        if ($pdsq["Q4a"] != "") $col_16++;
                        if ($pdsq["Q4aexp"] != "") $col_16++;   
                        if ($pdsq["Q5a"] != "") $col_16++;
                        if ($pdsq["Q5aexp"] != "") $col_16++;
                        if ($pdsq["Q5b"] != "") $col_16++;
                        if ($pdsq["Q5bexp"] != "") $col_16++;
                        if ($pdsq["Q6a"] != "") $col_16++;
                        if ($pdsq["Q6aexp"] != "") $col_16++;
                        if ($pdsq["Q7a"] != "") $col_16++;   
                        if ($pdsq["Q7aexp"] != "") $col_16++;   
                        if ($pdsq["Q7b"] != "") $col_16++;   
                        if ($pdsq["Q7bexp"] != "") $col_16++;   
                        if ($pdsq["Q7c"] != "") $col_16++;   
                        if ($pdsq["Q7cexp"] != "") $col_16++;   
                     } else {
                        $col_16 = 12;   
                     }

                     if ($reference) {
                        while ($reference_row = mysqli_fetch_assoc($reference)) {
                           if ($reference_row["Name"] != "") $col_17++;
                           if ($reference_row["Address"] != "") $col_17++;
                           if ($reference_row["ContactNo"] != "") $col_17++;
                        }
                     }   
                     for ($a=1; $a <= 19; $a++) { 
                        $total += ${"col_".$a};
                     }                  
                     if ($total >= 93) {
                        $status = "Complete";
                     } else {
                        $status = "Incomplete";
                     }
                     $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];

                     echo '
                        <tr>
                           <td class="text-center">'.$count.'</td>
                           <td>'.strtoupper($FullName).'</td>
                           <td>'.$Position.'</td>
                           <td class="text-center">'.$col_1.'</td>
                           <td class="text-center">'.$col_2.'</td>
                           <td class="text-center">'.$col_3.'</td>
                           <td class="text-center">'.$col_4.'</td>
                           <td class="text-center">'.$col_5.'</td>
                           <td class="text-center">'.$col_6.'</td>
                           <td class="text-center">'.$col_7.'</td>
                           <td class="text-center">'.$col_8.'</td>
                           <td class="text-center">'.$col_9.'</td>
                           <td class="text-center">'.$col_10.'</td>
                           <td class="text-center">'.$col_11.'</td>
                           <td class="text-center">'.$col_12.'</td>
                           <td class="text-center">'.$col_13.'</td>
                           <td class="text-center">'.$col_14.'</td>
                           <td class="text-center">'.$col_15.'</td>
                           <td class="text-center">'.$col_16.'</td>
                           <td class="text-center">'.$col_17.'</td>
                           <td class="text-center">'.$total.'</td>
                           <td class="text-center">'.$status.'</td>
                        </tr>
                     ';
                  }
               ?>
               
            </tbody>
         </table>
         <?php
            }
         ?>
      </div>
   </body>
</html>