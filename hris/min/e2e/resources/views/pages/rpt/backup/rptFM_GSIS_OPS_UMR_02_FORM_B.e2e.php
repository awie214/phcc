<?php
   //session_start();
   //include 'colors.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   
   include "incRptSortBy.e2e.php";
   $rs = SelectEach($table,$whereClause);
   if ($rs) $rowcount = mysqli_num_rows($rs);

   if ($dbg) {
      echo $whereClause;
   }
?>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   </head>
   <body>
      <div class="container-fluid rptBody">
         <div class="row">
            <label>Agency Name:&nbsp;<?php //echo $row['']; ?></label>
         </div>
         <div class="row margin-top">
            <label>Agency BP Number:&nbsp;<?php //echo $row['']; ?></label>
         </div>
         <div class="row margin-top">
            <label>FORM B. List of Transferees.</label>
         </div>
         <?php
            $errmsg = "";
            rptHeader("FOR AGENCY REMITTANCE ADVICE");
            if ($rs && $errmsg == "")
            {
         ?>
         <table border="1" width="80%">
            <tr>
               <td nowrap class="center--">Ref Id</td>
               <td nowrap class="center--">Member BP Number</td>
               <td nowrap class="center--">Employee Name</td>
               <td nowrap class="center--">Salary</td>
               <td nowrap class="center--">Effectivity Date</td>
               <td nowrap class="center--">Position</td>
               <td nowrap class="center--">Employment Status</td>
            </tr>
            <?php
               while ($row = mysqli_fetch_assoc($rs)) {
            ?>
               <tr>
                  <td nowrap class="center--"><?php echo $row['RefId'];?></td>
                  <td nowrap class="center--">BP NUMBER</td>
                  <td nowrap><?php echo $row['LastName'].",".$row['FirstName']." ".$row['MiddleName']." ".$row['ExtName']; ?></td>
                  <td nowrap class="center--"><?php echo number_format(getRecord("employeesworkexperience",$row['RefId'],"SalaryAmount"),2); ?></td>
                  <td nowrap class="center--">Effectivity Date</td>
                  <td nowrap>
                     <?php 
                        $Position = getRecord("employeesworkexperience",$row['RefId'],"PositionRefId");
                        echo getRecord("position","$Position","Name");
                     ?>
                  </td>
                  <td nowrap class="center--">
                     <?php 
                        if ($row['Inactive'] = 1) {
                           echo "Active";
                        }else {
                           echo "Inactive";
                        } 
                     ?>
                  </td>
               </tr>   
            <?php
               }
               echo "RECORD COUNT : ".mysqli_num_rows($rs);
            }else {
               echo '<div>NO RECORD QUERIED base on your criteria!!!</div>';
               echo '<div>'.$errmsg.'</div>';
            }
            ?>
         </table>
         <p class="MsoNormal margin-top" style='margin-bottom:0cm;margin-bottom:.0001pt;
            text-align:center;line-height:normal'><span style='font-size:10.0pt;
            font-family:"Arial",sans-serif'>Issue No. 01, Rev No. 0, (16 August 2016),
            FM-GSIS-OPS-UMR-02</span></p>
      </div>
      <?php rptFooter();?>
   </body>
</html>