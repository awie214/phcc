<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   $whereClause = "LIMIT 10";
   $table = "employees";
   $rs = SelectEach($table,$whereClause);
   if ($rs) $rowcount = mysqli_num_rows($rs);

   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $errmsg = "";
            rptHeader(getRptName(getvalue("drpReportKind")));
            if ($rs && $errmsg == "")
            {
         ?>
         <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1) ?></u> 2017</p>
         <table border="1">
            <tr>
               <th colspan="2">Division:</th>
               <th colspan="2">Late</th>
               <th colspan="2">Undertime</th>
               <th rowspan="2">Total Hours</th>
               <th rowspan="2">Total Hours Eqv.</th>
               <th rowspan="2">No. Of Abs.</th>
               <th rowspan="2">Total Total No. of Work Days</th>
               <th rowspan="2">Remarks</th>
            </tr>
            <tr>
               <th>No.</th>
               <th>Name of Employee</th>
               <th>No. of Late</th>
               <th>Total Min/Hours Late</th>
               <th>No. of UT</th>
               <th>Total Mins/Hours UT</th>
            </tr>
            <?php while ($row = mysqli_fetch_assoc($rs) ) {?>
               <tr>
                  <td class="txt-center"><?php echo $row['RefId'];?></td>
                  <td class="pad-left"><?php echo $row['LastName'].', '.$row['FirstName'].', '.$row['MiddleName'];?></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
               <tr>
            <?php
               }
               echo "RECORD COUNT : ".mysqli_num_rows($rs);
            }else {
               echo '<div>NO RECORD QUERIED base on your criteria!!!</div>';
               echo '<div>'.$errmsg.'</div>';
            }
            ?>
         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>