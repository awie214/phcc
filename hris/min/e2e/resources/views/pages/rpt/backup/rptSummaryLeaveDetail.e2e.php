<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">YEAR : <u><?php echo monthName(date("m",time()),1).", ".date("Y",time()) ?></u></p>
         <p>Name of Employee : <u>DELA CRUZ, JUAN</u></p>

         <table border="1">
            <tr>
               <th></th>
               <th>Date</th>
               <th>Used Leave</th>
               <th>FL</th>
               <th>Earnings</th>
               <th>Balance</th>
            </tr>
            <tr>
               <th>BEGINNING BALANCE</th>
               <th>01/01/2017</th>
               <th></th>
               <th></th>
               <th>64,304</th>
               <th></th>
            </tr>
            <?php for($j=1;$j<=3;$j++) {?>
            <tr>
               <td class="txt-center"></td>
               <td class="txt-center">01/31/2017</td>
               <td class="txt-center">3</td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>

            </tr>
            <?php } ?>
            <tr>
               <td class="txt-center">TOTAL (VL)</td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
            </tr>

            <tr>
               <td class="txt-center" colspan=6>&nbsp;</td>
            </tr>

            <tr>
               <th>BEGINNING BALANCE</th>
               <th>01/01/2017</th>
               <th></th>
               <th></th>
               <th>64,304</th>
               <th></th>
            </tr>
            <?php for($j=1;$j<=3;$j++) {?>
            <tr>
               <td class="txt-center"></td>
               <td class="txt-center">01/31/2017</td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>

            </tr>
            <?php } ?>
            <tr>
               <td class="txt-center">TOTAL (SL)</td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
               <td class="txt-center"></td>
            </tr>


         </table>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>