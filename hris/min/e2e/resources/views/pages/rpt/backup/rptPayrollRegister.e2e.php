<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include "pageHEAD.e2e.php"; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader(getRptName(getvalue("drpReportKind")));
         ?>
         <p class="txt-center">For the Month of <u><?php echo monthName(date("m",time()),1) ?></u> 2017</p>
         <div style="text-align: center;">
            <table border="1">
               <tr>
                  <th rowspan="2">No.</th>
                  <th rowspan="2">Name of Employee</th>
                  <th rowspan="2">Tax Status</th>
                  <th rowspan="2">Basic Salary</th>
                  <th colspan="2">Absences</th>
                  <th colspan="2">Late/UT</th>
                  <th rowspan="2">Taxable Income</th>
                  <th colspan="2">Allowances</th>
                  <th rowspan="2">Gross</th>
                  <th rowspan="2">Witholding Tax</th>
                  <th rowspan="2">GSIS Contr.</th>
                  <th rowspan="2">Pagibig Contr.</th>
                  <th rowspan="2">Philhealth</th>
                  <th rowspan="2">Loans</th>
                  <th rowspan="2">Total Deduction</th>
                  <th rowspan="2">Net Pay</th>
                  <th rowspan="2">Signature</th>
               </tr>
               <tr>
                  <td>days</td>
                  <td>Amount</td>
                  <td>Hrs/Min</td>
                  <td>Amount</td>
                  <td>PERA</td>
                  <td>RATA</td>
               </tr>
               <?php for ($j=1;$j<=15;$j++) {?>
               <tr style="text-align:center;">
                  <td><?php echo $j ?></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
               </tr>
            <?php }?>
            </table>
         </div>
         <p>
            <div class="row">
               <div class="col-xs-2 txt-right">Prepared By:</div>
               <div class="col-xs-4"></div>
               <div class="col-xs-2 txt-right">Approved By:</div>
               <div class="col-xs-4"></div>
            </div>
            <div class="row">
               <div class="col-xs-2"></div>
               <div class="col-xs-4">________________________</div>
               <div class="col-xs-2"></div>
               <div class="col-xs-3">________________________</div>
               <div class="col-xs-1"></div>
            </div>
         </p>

      </div>
      <?php rptFooter(); ?>
   </body>
</html>