<?php
   include_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   $leave_refid   = getvalue("refid");
   $spl_type      = "";
   
   $leave_row     = FindFirst("employeesleave","WHERE RefId = $leave_refid","*");
   if ($leave_row) {
      $emprefid   = $leave_row["EmployeesRefId"];
      $leave_type = $leave_row["LeavesRefId"];
      $leave_name = FindFirst("leaves","WHERE RefId = $leave_type","`Code`,`Name`");
      $name       = $leave_name["Name"];
      $code       = $leave_name["Code"];
      $is_fl      = $leave_row["isForceLeave"];
      $FiledDate  = date("F d, Y",strtotime($leave_row["FiledDate"]));
      $to         = date("F d, Y",strtotime($leave_row["ApplicationDateTo"]));
      $from       = date("F d, Y",strtotime($leave_row["ApplicationDateFrom"]));
      $VLPlace    = $leave_row["VLPlace"];
      if ($from == $to) {
         $Inclusive_Date = $from;
      } else {
         $Inclusive_Date = $from." to ".$to;
      }
      
      $Status     = $leave_row["Status"];
      $spl_type   = $leave_row["SPLType"];
      $day_count  = count_leave($emprefid,$leave_row["ApplicationDateFrom"],$leave_row["ApplicationDateTo"]); 
      //if ($day_count == 0) $day_count = 1;
      $emp_row       = FindFirst("employees","WHERE RefId = $emprefid","`LastName`,`FirstName`,`MiddleName`,`ExtName`");
      if ($emp_row) {
         $LastName      = $emp_row["LastName"];
         $FirstName     = $emp_row["FirstName"];
         $MiddleName    = $emp_row["MiddleName"];   
         $ExtName       = $emp_row["ExtName"];
         $FullName      = $LastName.", ".$FirstName." ".$ExtName." ".$MiddleName;
      } else {
         $FullName = $ExtName = $LastName = $FirstName = $MiddleName = "&nbsp;";
      }
      
      $empinfo_row   = FindFirst("empinformation","WHERE EmployeesRefId = $emprefid","*");

      if ($empinfo_row) {
         $Office        = getRecord("office",$empinfo_row["OfficeRefId"],"Name");
         $Agency        = getRecord("agency",$empinfo_row["AgencyRefId"],"Name");
         $Position      = getRecord("position",$empinfo_row["PositionRefId"],"Name");
         $SalaryAmount  = "₱ ".number_format($empinfo_row["SalaryAmount"],2);
      } else {
         $Office        = "&nbsp;";
         $Agency        = "&nbsp;";
         $Position      = "&nbsp;";
         $SalaryAmount  = "&nbsp;";
      }
      

      $where_credit     = "WHERE EmployeesRefId = '$emprefid' AND EffectivityYear = '".date("Y",strtotime($leave_row["FiledDate"]))."'";
      $credit_detail    = FindLast("employeescreditbalance",$where_credit,"*");
      //$credit_detail    = FindLast("employeescreditbalance","WHERE EmployeesRefId = '$emprefid' AND NameCredits = 'VL'","*");
      if ($credit_detail) {
         $year          = $credit_detail["EffectivityYear"]; 
         $balance_date  = $credit_detail["BegBalAsOfDate"];
         $from          = date("m",strtotime($balance_date." + 1 Day"));
         if (intval(date("m",strtotime($leave_row["FiledDate"]))) > 1) {
            $to            = date("m",strtotime($leave_row["FiledDate"])) - 1;   
         } else {
            $to            = 12;
         }
         $credit_arr    = computeCredit($emprefid,$from,$to,$year);
         if ($to <= 9) {
            $selected_month = "0".$to;   
         } else {
            $selected_month = $to;
         }
         $num_of_days   = cal_days_in_month(CAL_GREGORIAN,$selected_month,$year);
         $BegBalDate    = "$year-$selected_month-$num_of_days";
         $BegBalDate    = date("F d, Y",strtotime($BegBalDate));
         $VL            = $credit_arr["VL"];
         $SL            = $credit_arr["SL"];
         $total_leave   = $VL + $SL;
      } else {
         $VL = $SL = $total_leave = 0;
         $BegBalDate = "";
      }

   }
   

?>
<!DOCTYPE HTML>
<html>
   <head>
      <?php 
         $file = "Availment of Leave";
         include_once 'pageHEAD.e2e.php';
         
      ?>
      <style type="text/css">
         @media print {
            body {
               font-size: 9pt;
            }
            label {
               font-size: 9pt;
            }
         }
      </style>
      <script type="text/javascript">
         $(document).ready(function () {
            $("input").prop("disabled",true);
            <?php
               switch ($code) {
                  case 'VL':
                     if ($VLPlace != "") {
                        echo '$("#inAbroad").prop("checked",true);';
                        echo '$("#VLPlace").html("'.$VLPlace.'");';
                     } else {
                        echo '$("#inPhil").prop("checked",true);';   
                     }
                     echo '$("#VL_chk").prop("checked",true);';
                     if ($is_fl == 1) echo '$("#is_forceleave").html("<u>FORCE LEAVE</u>");';
                     $name = "";
                     break;
                  case 'SL':
                     echo '$("#SL_chk").prop("checked",true);';
                     $name = "";
                     break;
                  case 'ML':
                     echo '$("#ML_chk").prop("checked",true);';
                     $name = "";
                     break;
                  default:
                     echo '$("#OT_chk").prop("checked",true);';
                     break;
               }
               if ($Status == "Approved") {
                  echo '$("#is_approve").prop("checked",true);';
               }
               if ($spl_type != "") {
                  switch ($spl_type) {
                     case 'PM':
                        $name = "SPL - Personal Milestone";
                        break;
                     case 'PO':
                        $name = "SPL - Parental Obligation";
                        break;
                     case 'Fil':
                        $name = "SPL - Fillial";
                        break;
                     case 'DomE':
                        $name = "SPL - Domestic Emergencies";
                        break;
                     case 'PTrn':
                        $name = "SPL - Personal Transactions";
                        break;
                     case 'C':
                        $name = "SPL - Calamity";
                        break;
                  }
               }
               echo '$("#other_leave").html("'.$name.'");';
            ?>

         });
      </script>
   </head>
   <body>
      <div class="container-fluid">
         <div class="row">
            <div class="col-xs-12">
               CSC Form 6
               <br>
               Revised 1998
            </div>
         </div>
         <div class="row text-center" style="border:1px solid black; border-bottom: 1px solid white;">
            <div class="col-xs-12">
               <h3>APPLICATION FOR LEAVE</h3>
            </div>
         </div>
         <div class="row" style="border:1px solid black;">
            <div class="col-xs-12">
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-3" style="border-right:1px solid black;">
                     <label>1. Office/Agency</label>
                     <br>
                     <?php echo $Office; ?>
                  </div>
                  <div class="col-xs-9">
                     <div class="row">
                        <div class="col-xs-2">
                           <label>2. Name</label>
                           <br>
                           &nbsp;
                        </div>
                        <div class="col-xs-10">
                           <div class="row">
                              <div class="col-xs-4">
                                 <label>(Last)</label>
                                 <br>
                                 <?php echo $LastName; ?>
                              </div>
                              <div class="col-xs-4">
                                 <label>(First)</label>
                                 <br>
                                 <?php echo $FirstName." ".$ExtName; ?>
                              </div>
                              <div class="col-xs-4">
                                 <label>(Middle)</label>
                                 <br>
                                 <?php echo $MiddleName; ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-3" style="border-right:1px solid black;">
                     <label>3. Date of Filing</label>
                     <br>
                     <?php echo $FiledDate; ?>
                  </div>
                  <div class="col-xs-9">
                     <div class="row">
                        <div class="col-xs-6" style="border-right:1px solid black;">
                           <label>4. Position</label>
                           <br>
                           <?php echo $Position; ?>
                        </div>
                        <div class="col-xs-6">
                           <label>5. Salary</label>
                           <br>
                           <?php echo $SalaryAmount; ?>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row text-center" style="border-bottom:1px solid black;">
                  <label>DETAILS OF APPLICATION</label>
               </div>
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-6">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>6. a) Type of Leave:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <input type="checkbox" id="VL_chk">&nbsp;<label>Vacation</label>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-1"></div>
                              <div class="col-xs-11">
                                 <div class="row">
                                    <input type="checkbox">&nbsp;<label>To seek employment</label>
                                 </div>
                                 <div class="row margin-top">
                                    <input type="checkbox">&nbsp;<label>Others (Specify)</label>
                                    <label id="is_forceleave"><u></u></label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row margin-top">
                              <input type="checkbox" id="SL_chk">&nbsp;<label>Sick</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox" id="ML_chk">&nbsp;<label>Maternity</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox" id="OT_chk">&nbsp;<label>Others (Specify)</label>
                              <label><u id="other_leave"></u></label>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label>6. c) # of Working Days Applied for:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <label>
                              <?php
                                 echo '<u>'.$day_count.' day(s)</u>';
                              ?>
                           </label>
                           <br>
                           <label>Inclusive Dates:</label>
                           <br>
                           <label><u><?php echo $Inclusive_Date; ?></u></label>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6" style="border-left:1px solid black;">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>6. b) Where Leave will be Spent:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row margin-top">
                              <label>(1)&nbsp;In case of Vacation Leave:</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox" id="inPhil">&nbsp;<label>Within the Philippines</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox" id="inAbroad">&nbsp;<label>Abroad (Specify)</label>
                              <br>
                              <label id="VLPlace" style="color: black;">__________________________________________</label>
                           </div>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row margin-top">
                              <label>(2)&nbsp;In case of Sick Leave:</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox">&nbsp;<label>In hospital (Specify)</label>
                              <br>
                              <label>__________________________________________</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox">&nbsp;<label>Out-Patient (Specify)</label>
                              <br>
                              <label>__________________________________________</label>
                           </div>
                        </div>
                     </div>
                     <?php spacer(6); ?>
                     <div class="row">
                        <div class="col-xs-12">
                           <label>6. d) Commutation:</label>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <div class="col-xs-6">
                                 <input type="checkbox">&nbsp;<label>Requested</label>
                              </div>
                              <div class="col-xs-6">
                                 <input type="checkbox">&nbsp;<label>Not Requested</label>
                              </div>
                           </div>
                           <div class="row margin-top text-center">
                              <br><br>
                              <label><u><?php echo $FullName; ?></u></label>
                              <br>
                              <label>(Signature of Applicant)</label>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row text-center" style="border-bottom:1px solid black;">
                  <label>DETAILS OF ACTION ON APPLICATION</label>
               </div>
               <div class="row" style="border-bottom:1px solid black;">
                  <div class="col-xs-6" style="border-right:1px solid black;">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. a) Certification of Leave Credits as of:</label>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <?php
                              if ($BegBalDate != "") {
                                 echo '<label><u>'.$BegBalDate.'</u></label>';
                              } else {
                                 echo '<label>__________________________________________</label>';      
                              }
                           ?>
                           <br>
                           <div class="row margin-top" style="border:1px solid black;">
                              <div class="col-xs-4 text-center" style="border-right:1px solid black;">
                                 <div class="row" style="border-bottom:1px solid black;">
                                    <label>Vacation</label>
                                 </div>
                                 <div class="row margin-top" style="border-bottom:1px solid black;">
                                    <label>
                                       <?php echo $VL; ?>
                                    </label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Days</label>
                                 </div>
                              </div>
                              <div class="col-xs-4 text-center" style="border-right:1px solid black;">
                                 <div class="row" style="border-bottom:1px solid black;">
                                    <label>Sick</label>
                                 </div>
                                 <div class="row margin-top" style="border-bottom:1px solid black;">
                                    <label>
                                       <?php echo $SL; ?>
                                    </label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Days</label>
                                 </div>
                              </div>
                              <div class="col-xs-4 text-center">
                                 <div class="row" style="border-bottom:1px solid black;">
                                    <label>Total</label>
                                 </div>
                                 <div class="row margin-top" style="border-bottom:1px solid black;">
                                    <label>
                                       <?php echo $total_leave; ?>
                                    </label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Days</label>
                                 </div>
                              </div>
                           </div>
                           <?php spacer(40);?>
                           <div class="row text-center">
                              <label>__________________________________________</label>
                              <br>
                              <label>(Personnel Officer)</label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. b) Recommendation</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <input type="checkbox" id="is_approve"><label>&nbsp;Approval</label>
                           </div>
                           <div class="row margin-top">
                              <input type="checkbox"><label>&nbsp;Disapproval due to</label>
                              <br>
                              <label>__________________________________________</label>
                              <br>
                              <label>__________________________________________</label>
                           </div>
                        </div>
                     </div>
                     <?php spacer(40);?>
                     <div class="row text-center">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <label>__________________________________________</label>
                           <br>
                           <label>(Authorized Official)</label>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6" style="border-right:1px solid black;">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. c) APPROVED FOR:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10">
                           <div class="row">
                              <div class="col-xs-8">
                                 <div class="row">
                                    <label>____________________</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>____________________</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>&nbsp;</label>
                                 </div>
                              </div>
                              <div class="col-xs-4">
                                 <div class="row">
                                    <label>days with pay</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>days without pay</label>
                                 </div>
                                 <div class="row margin-top">
                                    <label>Others (Specify)</label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>7. d) DISAPPROVED DUE TO:</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10 text-center">
                           <label>_____________________________________</label>
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-10 text-center">
                           <label>_____________________________________</label>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row" style="border-bottom:1px solid black; border-left: 1px solid black; border-right: 1px solid black; padding-top: 40px;">
            <div class="col-xs-12 text-center">
               <label>_________________________________________</label>
               <br>
               <label>(Name & Signature of Authorized Official)</label>
            </div>
         </div>
      </div>
   </body>
</html>