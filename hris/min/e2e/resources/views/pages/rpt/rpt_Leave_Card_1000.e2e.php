<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $from = getvalue("txtAttendanceDateFrom");
   $to   = getvalue("txtAttendanceDateTo");
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
   $WorkDayConversion   = file_get_contents(json."WorkDayConversion.json");
   $WorkDayEQ           = json_decode($WorkDayConversion, true);
   $PerDayHours         = "10";
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
      <?php
         $rsEmployees = SelectEach("employees",$whereClause);
         if ($rsEmployees) {
            while ($row = mysqli_fetch_assoc($rsEmployees)) { 
               $new_VL = 0;
               $new_SL = 0;

               $EmployeesRefId = $row["RefId"];
               $CompanyRefId   = $row["CompanyRefId"];
               $BranchRefId    = $row["BranchRefId"];
               $emp_row = FindFirst("empinformation","WHERE EmployeesRefId = ".$row["RefId"],"*");
               if ($emp_row) {
                  $appt    = $emp_row["ApptStatusRefId"];
                  $appt    = getRecord("apptstatus",$appt,"Name");
                  $div     = getRecord("division",$emp_row["DivisionRefId"],"Name");
                  $hired   = date("d F Y",strtotime($emp_row["HiredDate"]));
                  $worksched = $emp_row["WorkScheduleRefId"];
               } else {
                  $appt    = "";
                  $div     = "";
                  $hired   = "";
                  $worksched = "";
               }
               if ($worksched != "") {
                  rptHeader(getRptName("rptLeave_Card"));
      ?>               
               <div class="row" style="padding:10px;">
                  <div class="col-xs-6">
                     <?php 
                        echo "NAME : ".$row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];  
                     ?>
                  </div>
                  <div class="col-xs-6 text-right">
                     <?php echo "1st Day of Service : ".$hired; ?>
                  </div>
               </div>
               <table border="1" width="100%">
                  <thead>
                     <tr align="center">
                        <th rowspan="2" style="width: 15%;">Period Covered</th>
                        <th rowspan="2" style="width: 15%;">Particular</th>
                        <th colspan="4">Vacation Leave</th>
                        <th colspan="4">Sick Leave</th>
                        <th rowspan="2">Remarks</th>
                     </tr>
                     <tr align="center">
                        <th>Earned</th>
                        <th>Absences<br>Undertime<br>With Pay</th>
                        <th>Balance</th>
                        <th>Absences<br>Undertime<br>W/O Pay</th>

                        <th>Earned</th>
                        <th>Absences<br>Undertime<br>With Pay</th>
                        <th>Balance</th>
                        <th>Absences<br>Undertime<br>W/O Pay</th>
                     </tr>
                  </thead>   
                  <tbody>
                     <tr>
                        <?php
                           $whereVL         = "WHERE CompanyRefId = ".$row['CompanyRefId']; 
                           $whereVL         .= " AND BranchRefId = ".$row["BranchRefId"];
                           $whereVL         .= " AND EmployeesRefId = ".$row['RefId']." AND NameCredits = 'VL'";
                           $row_vl          = FindOrderBy("employeescreditbalance",$whereVL,"*","BegBalAsOfDate");
                           if ($row_vl) {
                              $vl = $row_vl["BeginningBalance"];
                              $AsOf = $row_vl["BegBalAsOfDate"];
                              $new_VL = $vl;
                              $start_date = date("Y-m-d",strtotime( "$AsOf + 1 day" ));
                           } else {
                              $vl = 0;
                              $new_VL = 0;
                              $AsOf = date("Y-m",time())."-01";
                              $start_date = date("Y-m-d",strtotime( "$AsOf - 1 day" ));
                           }
                        ?>
                        <td class="text-center">
                           <?php echo date("F d, Y",strtotime($AsOf)); ?>
                        </td>
                        <td>Balance Previous Year</td>
                        <td></td>
                        <td></td>
                        <td class="text-center">
                           <?php
                              ${"VLBal_".$EmployeesRefId} = $vl;
                              echo $vl;
                           ?>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="text-center">
                           <?php
                              $whereSL         = "WHERE CompanyRefId = ".$row['CompanyRefId']; 
                              $whereSL         .= " AND BranchRefId = ".$row["BranchRefId"];
                              $whereSL         .= " AND EmployeesRefId = ".$row['RefId']." AND NameCredits = 'SL'";
                              $row_sl          = FindOrderBy("employeescreditbalance",$whereSL,"*","BegBalAsOfDate");  
                              if ($row_sl) {
                                 $sl = $row_sl["BeginningBalance"];
                                 $new_SL = $sl;
                              } else {
                                 $sl = 0;
                                 $new_SL = 0;
                              }
                              ${"SLBal_".$EmployeesRefId} = $sl;
                              echo $sl;
                           ?>
                        </td>
                        <td></td>
                        <td></td>
                     </tr>
                     <?php
                        ${"arr_leave_".$EmployeesRefId} = [
                           "01"=>"",
                           "02"=>"",
                           "03"=>"",
                           "04"=>"",
                           "05"=>"",
                           "06"=>"",
                           "07"=>"",
                           "08"=>"",
                           "09"=>"",
                           "10"=>"",
                           "11"=>"",
                           "12"=>"",
                        ];
                        $where = "where EmployeesRefId = ".$EmployeesRefId." AND Status = 'Approved'";
                        $where .= " AND ApplicationDateFrom BETWEEN '".$year."-01-01"."' AND '".date("Y-m-d",time())."'";
                        $where .= " ORDER BY ApplicationDateFrom";
                        $rsLeave = SelectEach("employeesleave",$where);
                        if ($rsLeave) {
                           while ($row = mysqli_fetch_assoc($rsLeave)) {
                              $dfrom   = date("d",strtotime($row["ApplicationDateFrom"]));
                              $dto     = date("d",strtotime($row["ApplicationDateTo"]));
                              $type    = getRecord("leaves",$row["LeavesRefId"],"Code");
                              //echo $type;
                              $month   = date("m",strtotime($row["ApplicationDateFrom"]));
                              $date    = $row["ApplicationDateFrom"];
                              $arr     = [$type,$row["ApplicationDateFrom"],$row["ApplicationDateTo"]];
                              ${"arr_leave_".$EmployeesRefId}[intval($month)][$date] = $arr;
                           }
                        }
                        
                        for ($a=1; $a <=12 ; $a++) { 
                           if ($a <= 9) $a = "0".$a;
                           ${"VL_count_".$a} = 0;
                           ${"SL_count_".$a} = 0;
                        }


                        $from = date("m",strtotime($start_date));
                        for ($i=intval($from)-1; $i <= date("m",time()) - 2 ; $i++) {
                           $new_VL_Balance   = 0;
                           $new_SL_Balance   = 0;
                           $new_VL_Deduction = 0;
                           $new_SL_Deduction = 0;
                           $new_VL_wop       = 0;
                           $new_SL_wop       = 0;
                           $idx              = $i + 1;
                           if ($idx <= 9) $idx = "0".$idx;
                           $month         = $idx;
                           $where_dtr  = "WHERE EmployeesRefId = $EmployeesRefId AND Month = '$month' AND Year = '$year'";
                           $arr_empDTR = FindFirst("dtr_process",$where_dtr,"*");
                           $deduction  = $arr_empDTR["Tardy_Deduction_EQ"] + 
                                         $arr_empDTR["Undertime_Deduction_EQ"] + 
                                         $arr_empDTR["Total_Absent_EQ"];
                           /*-------------------------------*/


                           $leave_name = "";
                           

                           $num_of_days   = cal_days_in_month(CAL_GREGORIAN,$month,$year);
                           $where_from    = $year."-".$month."-01";
                           $where_to      = $year."-".$month."-".$num_of_days;
                           $where         = "where EmployeesRefId = ".$EmployeesRefId." AND Status = 'Approved'";
                           $where         .= " AND ApplicationDateFrom >= '".$where_from."' AND ApplicationDateTo <= '".$where_to."'";
                           $where         .= " ORDER BY ApplicationDateFrom";
                           $rsLeave = SelectEach("employeesleave",$where);
                           if ($rsLeave) {
                              while ($row = mysqli_fetch_assoc($rsLeave)) {
                                 $leave_from    = $row["ApplicationDateFrom"];
                                 $leave_to      = $row["ApplicationDateTo"];
                                 $leave_code    = getRecord("leaves",$row["LeavesRefId"],"Code");
                                 
                                 $leave_date    = "";
                                 $sl_leave_eq   = 0;
                                 $vl_leave_eq   = 0;
                                 $leave_eq      = 0;
                                 if ($leave_from == $leave_to) {
                                    $leave_date    = date("M d, Y",strtotime($leave_from));
                                    $leave_name    = $leave_code." (1 day) ";
                                    $dummy_count   = 1;
                                 } else {
                                    $dummy_from    = date("d",strtotime($leave_from));
                                    $dummy_to      = date("d",strtotime($leave_to));
                                    $dummy_count   = dateDifference($leave_from,$leave_to);
                                    $dummy_month   = date("M",strtotime($leave_from));
                                    $dummy_year    = date("Y",strtotime($leave_to));
                                    $leave_date    = $dummy_month." ".$dummy_from."-".$dummy_to." ".$dummy_year;
                                    $leave_name    = $leave_code." ($dummy_count days) ";
                                 }
                                 if (isset($PerDayHours)) {
                                    for ($x=1; $x <= $dummy_count; $x++) { 
                                       $leave_eq = $WorkDayEQ["WorkDayConversion"][$PerDayHours] + $leave_eq;
                                    }
                                 }
                                 switch ($leave_code) {
                                    case 'VL':
                                       $vl_leave_eq = $leave_eq;
                                       $new_VL_Deduction = $leave_eq;
                                       break;
                                    case 'SL':
                                       $sl_leave_eq = $leave_eq;
                                       $new_SL_Deduction = $leave_eq;
                                       break;
                                 }
                                 if ($new_VL_Deduction > $new_VL) {
                                    $new_VL_Balance   = $new_VL - $new_VL_Deduction;
                                    $new_VL_wop       = FindLast("leavecreditsearnedwopay","WHERE NoOfDaysLeaveWOP <= '$new_VL_Balance'","LeaveCreditsEarned");
                                    $new_VL_Balance   = 0;
                                    $new_VL           = 0;
                                 } else {
                                    $new_VL_Balance   = $new_VL - $new_VL_Deduction;
                                    $new_VL           = $new_VL_Balance;
                                 }
                                 if ($vl_leave_eq > 0) {
                                    $dummy_vl_bal = '<td class="text-center">'.$vl_leave_eq.'</td>';
                                    $dummy_vl_bal .= '<td class="text-center">'.(${"VLBal_".$EmployeesRefId}).'</td>';
                                 } else {
                                    $dummy_vl_bal = '<td></td><td></td>';
                                 }
                                 if ($sl_leave_eq > 0) {
                                    $dummy_sl_bal = '<td class="text-center">'.$sl_leave_eq.'</td>';
                                    $dummy_sl_bal .= '<td class="text-center">'.(${"SLBal_".$EmployeesRefId}).'</td>';
                                 } else {
                                    $dummy_sl_bal = '<td></td><td></td>';
                                 }
                                 echo '
                                    <tr>
                                       <td>'.$leave_date.'</td>
                                       <td>'.$leave_name.'</td>
                                       <td></td>
                                       '.$dummy_vl_bal.'
                                       <td></td>
                                       <td></td>
                                       '.$dummy_sl_bal.'
                                       <td></td>
                                       <td>'.$leave_name.'</td>
                                    </tr>

                                 ';
                              }
                           }
                           if (isset(${"arr_leave_".$EmployeesRefId}[intval($idx)])) {
                              $sl_type = "";
                              $vl_type = "";
                              foreach (${"arr_leave_".$EmployeesRefId}[intval($idx)] as $key => $value) {
                                 $type = $value[0];
                                 $from = intval(date("d",strtotime($value[1])));
                                 $to   = intval(date("d",strtotime($value[2])));
                                 if ($from == $to) {
                                    switch ($type) {
                                       case 'VL':
                                          ${"VL_count_".$idx}++;
                                          $vl_type .= $from.","; 
                                          break;
                                       case 'SL':
                                          ${"SL_count_".$idx}++;
                                          $sl_type .= $from.","; 
                                          break;
                                    }
                                 } else {
                                    for ($x=$from; $x <= $to; $x++) { 
                                       switch ($type) {
                                          case 'VL':
                                             ${"VL_count_".$idx}++;
                                             $vl_type .= $x.",";
                                             break;
                                          case 'SL':
                                             ${"SL_count_".$idx}++;
                                             $sl_type .= $x.","; 
                                             break;
                                       }
                                    }
                                    //echo $count." (c ".$type.") ";
                                 }
                              }
                              /*if ($vl_type != "") echo $vl_type."(VL)";
                              if ($sl_type != "") echo $sl_type."(SL)";*/
                           }
                           $sl_earned     = $arr_empDTR["SL_Earned"];
                           $sl_deduction  = ${"SL_count_".$idx};
                           $newSLBal = ${"SLBal_".$EmployeesRefId} + $sl_earned - $sl_deduction;
                           if ($newSLBal < 0) {
                              $sl_wop     = str_replace("-", "", $newSLBal);
                              $newSLBal   = 0;
                              $sl_balance = 0;
                              ${"SLBal_".$EmployeesRefId} = 0;
                           } else {
                              $sl_wop     = 0; 
                              $sl_balance = $newSLBal;
                           }
                           




                           $vl_earned     = $arr_empDTR["VL_Earned"];
                           $deduction     = $arr_empDTR["Tardy_Deduction_EQ"] + 
                                            $arr_empDTR["Undertime_Deduction_EQ"] 
                                            + $arr_empDTR["Total_Absent_EQ"];

                           $vl_deduction  = $deduction + ${"VL_count_".$idx} + $sl_wop;
                           $newVLBal      = ${"VLBal_".$EmployeesRefId} + $vl_earned - $vl_deduction;
                           $where_wop     = "WHERE NoOfDaysLeaveWOP <= '$vl_deduction'";
                           $get_vl_wop    = FindLast("leavecreditsearnedwopay",$where_wop,"LeaveCreditsEarned");
                           
                           if ($newVLBal < 0) {
                              $vl_wop     = $get_vl_wop;
                              $newVLBal   = 0;
                              $vl_balance = 0;
                              $new_VL     = 0;
                              ${"VLBal_".$EmployeesRefId} = 0;
                           } else {
                              $vl_wop     = 0; 
                              $vl_balance = $newVLBal;  
                              $new_VL     = $vl_balance;
                           }
                           /*------------------------------------------------------*/
                           echo '
                              <tr>
                                 <td>'.date("M d, Y",strtotime($where_to)).'</td>
                                 <td>UT '.ToHours($arr_empDTR["Total_Undertime_Hr"]).'</td>
                                 <td></td>
                                 <td class="text-center">'.$vl_deduction.'</td>
                                 <td class="text-center">'.(${"VLBal_".$EmployeesRefId} - $vl_deduction).'</td>
                                 <td class="text-center">'.$vl_wop.'</td>
                                 <td></td>
                                 <td></td>
                                 <td class="text-center">'.(${"SLBal_".$EmployeesRefId} - $sl_deduction).'</td>
                                 <td class="text-center">'.$sl_wop.'</td>
                                 <td>'.$leave_name.'</td>
                              </tr>
                           ';
                           ${"VLBal_".$EmployeesRefId} = $newVLBal;
                           ${"SLBal_".$EmployeesRefId} = $newSLBal;
                           echo '
                              <tr class="text-center">
                                 <td class="text-left">'.date("M d, Y",strtotime($where_to)).'</td>
                                 <td></td>
                                 <td>'.$arr_empDTR["VL_Earned"].'</td>
                                 <td></td>
                                 <td>'.$vl_balance.'</td>
                                 <td></td>
                                 <td>'.$arr_empDTR["VL_Earned"].'</td>
                                 <td></td>
                                 <td>'.$sl_balance.'</td>
                                 <td></td>
                                 <td></td>
                              </tr>
                              <tr>
                                 <td colspan="11" style="background: gray;">&nbsp;</td>
                              </tr>
                           ';

                        }
                        
                     ?>
                  </tbody>   
               </table>
               <p>
                  This is a system generated report. Signature is not required.
               </p>
         <?php 
               }
            }
         }
         ?>
      </div>
   </body>
</html>