<?php
	$where = " ORDER BY HiredDate";
   if (isset($_GET["loyalty_years"])) {
      $loyalty_years = $_GET["loyalty_years"];
   } else {
      $loyalty_years = "";
   }
?>
<!DOCTYPE html>
<html>
   	<head>
      	<?php include_once $files["inc"]["pageHEAD"]; ?>
      	<link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      	<style>
         	td {vertical-align:top;}
      	</style>
   	</head>
   	<body>
      	<div class="container-fluid rptBody">
      		<div class="row">
      			<div class="col-xs-12">
      				<?php
			            rptHeader("EMPLOYEES LOYALTY");
			         ?>
      				<table style="width: 100%" border="1">
      					<thead>
      						<tr class="colHEADER">
      							<th>#</th>
	      						<th>Employee ID</th>
	      						<th>Employee Name</th>
                           <th>Annual Salary</th>
	      						<th>Date Hired</th>	
                           <th>Years</th>
      						</tr>
      					</thead>
      					<tbody>
      						<?php
      							$count = 0;
	                          $newly_hired_rs = SelectEach("empinformation",$where);
	                           if ($newly_hired_rs) {
	                              while ($newly_hired_row = mysqli_fetch_assoc($newly_hired_rs)) {
                                    $emprefid      = $newly_hired_row["EmployeesRefId"];
                                    $hired_date    = $newly_hired_row["HiredDate"];
                                    $Salary        = $newly_hired_row["SalaryAmount"];
                                    $Salary        = intval($Salary);
                                    $years         = computeAge($hired_date);
                                    if ($years > 0) {
                                       if ($years > 1) {
                                          $years = $years." Years"; 
                                       } else {
                                          $years = $years." Year"; 
                                       }
                                    } else {
                                       $years = "";
                                    }
                                    if ($Salary > 0) {
                                       $Salary = "P ".number_format(($Salary * 12),2);
                                    } else {
                                       $Salary = "";
                                    }
                                    if ($loyalty_years != "") {
                                       if (intval($loyalty_years) == computeAge($hired_date)) {
                                          $fld           = "`LastName`,`FirstName`,`MiddleName`,`AgencyId`";
                                          $row_emp = FindFirst("employees","WHERE RefId = '$emprefid'",$fld);
                                          if ($row_emp) {
                                             $count++;
                                             $AgencyId = $row_emp["AgencyId"];
                                             $FullName = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                                             echo '<tr>';
                                                echo '
                                                   <td class="text-center">'.$count.'</td>
                                                   <td class="text-center">'.$AgencyId.'</td>
                                                   <td>'.$FullName.'</td>
                                                      <td class="text-right">'.$Salary.'</td>
                                                   <td class="text-center">'.date("d M Y",strtotime($hired_date)).'</td>
                                                   <td class="text-center">'.$years.'</td>
                                                ';
                                             echo '</tr>';
                                          }      
                                       }
                                    } else {
                                       $fld           = "`LastName`,`FirstName`,`MiddleName`,`AgencyId`";
                                       $row_emp = FindFirst("employees","WHERE RefId = '$emprefid'",$fld);
                                       if ($row_emp) {
                                          $count++;
                                          $AgencyId = $row_emp["AgencyId"];
                                          $FullName = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                                          echo '<tr>';
                                             echo '
                                                <td class="text-center">'.$count.'</td>
                                                <td class="text-center">'.$AgencyId.'</td>
                                                <td>'.$FullName.'</td>
                                                   <td class="text-right">'.$Salary.'</td>
                                                <td class="text-center">'.date("d M Y",strtotime($hired_date)).'</td>
                                                <td class="text-center">'.$years.'</td>
                                             ';
                                          echo '</tr>';
                                       }   
                                    }
	                                 
	                              }
	                           }
      						?>
      					</tbody>
      				</table>
      			</div>
      		</div>
      	</div>
    </body>
</html>