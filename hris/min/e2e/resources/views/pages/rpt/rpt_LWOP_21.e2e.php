<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            $count = 0;
            if ($rsEmployees) {
         ?>
         <table style="width: 100%;">
            <thead>
               <tr>
                  <td colspan="14">
                     <?php
                        rptHeader(getvalue("RptName"));
                     ?>
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th rowspan="2">NAME</th>
                  <th rowspan="2">BASIC</th>
                  <th rowspan="2">PERA</th>
                  <th rowspan="2">LWOP<br>days-hours-mins</th>
                  <th rowspan="2">NO. OF<br>Minutes</th>
                  <th rowspan="2">NO. OF MIN<br>FOR Jan <?php echo date("Y",time()); ?></th>
                  <th colspan="2">Leave Without Pay</th>
                  <th colspan="2">Other Months Ded</th>
                  <th colspan="2">TOTAL</th>
                  <th>GRAND</th>
                  <th rowspan="2">REMARKS</th>
               </tr>
               <tr class="colHEADER">
                  <th>BASIC</th>
                  <th>PERA</th>
                  <th>BASIC</th>
                  <th>PERA</th>
                  <th>BASIC</th>
                  <th>PERA</th>
                  <th>TOTAL</th>
               </tr>
            </thead>
            <tbody>
               <?php
                  while ($row_emp = mysqli_fetch_assoc($rsEmployees)) {
                     $count++;
                     $FullName   = $row_emp["LastName"].", ".$row_emp["FirstName"]." ".$row_emp["MiddleName"];
                     echo '<tr>';
                        echo '<td>'.$FullName.'</td>';
                        for ($i=1; $i <=13 ; $i++) { 
                           echo '<td>&nbsp;</td>';
                        }
                     echo '</tr>';
                  }
                  echo '
                     <tr>
                        <td colspan="6" class="text-center">TOTAL</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                     </tr>
                  ';
               ?>
            </tbody>
         </table>
         <div class="row margin-top">
            <div class="col-xs-12">
               Pursuant to CSC Resolution No. 1400454, dated 3/21/14, the Commission adopts he new formula in the computation of salaries of employees who incur LWOP.               
               <br>
               SALARY   =  No. of Days Paid Status     x   Monthly Salary        
               22 days        

            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-1 text-center">
               SALARY
            </div>
            <div class="col-xs-1 text-center">
               =
            </div>
            <div class="col-xs-2 text-center">
               <u>No. of Days Paid Status</u>
            </div>
            <div class="col-xs-1 text-center">
               X
            </div>
            <div class="col-xs-1 text-center">
               Monthly Salary
            </div>
         </div>
         <div class="row margin-top">
            <div class="col-xs-2">&nbsp;</div>
            <div class="col-xs-2 text-center">22 days</div>
         </div>
         <br>
         <br>
         <br>
         <div class="row margin-top">
            <div class="col-xs-6">
               Prepared by:
               <br>
               <b>LAILA R. PORLUCAS</b>
               <br>
               Administrative Officer IV
            </div>
            <div class="col-xs-6 text-center">
               Certified Correct:
               <br>
               <b>JELLY N. ORTIZ, DPA</b>
               <br>
               Supervising Administrative Officer
            </div>
         </div>
         <?php
            }
         ?>
      </div>
   </body>
</html>