<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include_once 'incRptQryString.e2e.php';
   $whereClause .= " LIMIT 10";
   $whereClause = "WHERE RefId = ".getvalue("selected_emp");
   $rsEmployees = SelectEach("employees",$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) { echo "DBG >> ".$whereClause; }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style type="text/css">
         .rptBody {
            font-size: 9pt;
         }
         @media print {
            .rptBody {
               font-size: 8pt !important;
            }
            label {
               font-size: 8pt !important;
            }
         }
      </style>
   </head>
   <body>

      <?php
         while ($row = mysqli_fetch_assoc($rsEmployees)) {
            $EmployeesRefId = $row["RefId"];
            $CompanyRefId   = $row["CompanyRefId"];
            $BranchRefId    = $row["BranchRefId"];
            $where  = "WHERE CompanyRefId = $CompanyRefId";
            $where .= " AND BranchRefId = $BranchRefId";
            $where .= " AND EmployeesRefId = $EmployeesRefId";
               $empinfo_row = FindFirst("empinformation",$where,"*");
               if ($empinfo_row) {
                  $Position      = getRecord("position",$empinfo_row["PositionRefId"],"Name");
                  $Agency        = getRecord("Agency",$empinfo_row["AgencyRefId"],"Name");
                  $PositionItem  = getRecord("PositionItem",$empinfo_row["PositionItemRefId"],"Name");
                  $Division      = getRecord("Division",$empinfo_row["DivisionRefId"],"Name");
                  $StepIncrement = getRecord("StepIncrement",$empinfo_row["StepIncrementRefId"],"Name");
               } else {
                  $Position = "";
                  $Agency   = "";
                  $PositionItem = "";  
                  $Division = "";
                  $StepIncrement = "";
               }

      ?>

      <div class="container-fluid rptBody" style="page-break-after: always;">
         <div class="row">
            <div class="col-xs-12">
               <div class="row">
                  <div class="col-xs-6">
                     <label>CSC Form No. : _____________________</label>
                  </div>
                  <div class="col-xs-6 text-right">
                     <label>Action Notice No. : _____________________</label>
                  </div>
               </div>
               <?php rptHeader("HR ACTION NOTICE"); ?>
               <div class="row">
                  <div class="col-xs-2">
                     <label>ACTION TYPE:</label>
                  </div>
                  <div class="col-xs-2">
                     <input type="checkbox"> &nbsp;Appointment
                  </div>
                  <div class="col-xs-3">
                     <input type="checkbox"> &nbsp;Other Personnel Action
                  </div>
                  <div class="col-xs-2">
                     <input type="checkbox"> &nbsp;Separation
                  </div>
                  <div class="col-xs-3 text-right">
                     <label>CSID No. : _____________________</label>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-12">
                     <label>EMPLOYEE NAME: <?php echo $row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"]; ?></label>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-8">
                     <label>NATURE OF APPOINTMENT/ACTION : _____________________</label>
                  </div>
                  <div class="col-xs-4 text-right">
                     <label>EFFECTIVITY DATE : _____________________</label>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-8">
                     <label>DETAILS / REMARKS : _____________________</label>
                  </div>
                  <div class="col-xs-4 text-right">
                     <label>EXPIRY DATE : _____________________</label>
                  </div>
               </div>
               <div class="row" style="padding: 5px; border: 1px solid black;">
                  <div class="col-xs-12">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>VICE / FORMER INCUBMENT :</label>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-4">
                           <label>CSID : _____________________</label>
                        </div>
                        <div class="col-xs-8">
                           <label>REASON OF SEPARATION : _____________________</label>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12">
                           <label>NAME :</label>
                        </div>
                     </div>
                  </div>
               </div>
               <?php spacer(10); ?>
               <div class="row">
                  <div class="col-xs-12 text-center" style="color:white; background: gray;">
                     <h4>ACTION DETAILS</h4>
                  </div>
               </div>
               <div class="row" style="border: 1px solid black; padding: 5px;">
                  <div class="col-xs-12">
                     <div class="row">
                        <div class="col-xs-3">
                           <label>POSITION TITLE</label>
                        </div>
                        <div class="col-xs-3">: 
                           <u>
                           <?php
                              echo $Position;
                           ?>
                           </u>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-3">
                           <label>POSITION ITEM NUMBER</label>
                        </div>
                        <div class="col-xs-3">:
                           <u>
                           <?php
                              echo $PositionItem;
                           ?>
                           </u>
                        </div>
                        <div class="col-xs-3">
                           <label>Page No.</label>
                        </div>
                        <div class="col-xs-3">:_______________________</div>
                     </div>
                     <div class="row">
                        <div class="col-xs-3">
                           <label>ACTUAL DESIGNATION</label>
                        </div>
                        <div class="col-xs-3">:_______________________</div>
                     </div>
                     <div class="row">
                        <div class="col-xs-3">
                           <b>SALARY / JOB / PAY GRADE / LEVEL</b>
                        </div>
                        <div class="col-xs-3">:_______________________</div>
                        <div class="col-xs-3">
                           <label>STEP INCREMENT.</label>
                        </div>
                        <div class="col-xs-3">:
                        <u>
                        <?php
                           echo $StepIncrement;
                        ?>
                        </u>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-3">
                           <label>AGENCY NAME</label>
                        </div>
                        <div class="col-xs-9">:
                        <u>
                        <?php
                           echo $Agency;
                        ?>
                        </u>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-3">
                           <label>CO/ RO</label>
                        </div>
                        <div class="col-xs-9">:_______________________</div>
                     </div>
                     <div class="row">
                        <div class="col-xs-3">
                           <label>DISTRICT / DIVISION</label>
                        </div>
                        <div class="col-xs-9">:
                        <u>
                        <?php
                           echo $Division;
                        ?>
                        </div>
                        </u>
                     </div>
                     <div class="row">
                        <div class="col-xs-3">
                           <b>OFFICE / UNIT / PROGRAM / PROJECT</b>
                        </div>
                        <div class="col-xs-9">:_______________________</div>
                     </div>
                     <div class="row">
                        <div class="col-xs-3">
                           <label>EMPLOYMENT STATUS</label>
                        </div>
                        <div class="col-xs-9">
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="row">
                                    <div class="col-xs-4">
                                       <input type="checkbox">&nbsp;Perm
                                    </div>
                                    <div class="col-xs-4">
                                       <input type="checkbox">&nbsp;Temp
                                    </div>
                                    <div class="col-xs-4">
                                       <input type="checkbox">&nbsp;Cas
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-6">
                                 <div class="row">
                                    <div class="col-xs-4">
                                       <input type="checkbox">&nbsp;Cont
                                    </div>
                                    <div class="col-xs-4">
                                       <input type="checkbox">&nbsp;Co-t
                                    </div>
                                    <div class="col-xs-4">
                                       <input type="checkbox">&nbsp;Elect
                                    </div>
                                 </div>
                              </div>
                              
                           </div>
                           <div class="row">
                              <div class="col-xs-6">
                                 <div class="row">
                                    <div class="col-xs-4">
                                       <input type="checkbox">&nbsp;Prob
                                    </div>
                                    <div class="col-xs-4">
                                       <input type="checkbox">&nbsp;Subst
                                    </div>
                                    <div class="col-xs-4">
                                       <input type="checkbox">&nbsp;Reg/Perm
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-6">
                                 <div class="row">
                                    <div class="col-xs-4">
                                       <input type="checkbox">&nbsp;Part-Time
                                    </div>
                                    <div class="col-xs-4">
                                       <input type="checkbox">&nbsp;Non Career Executive
                                    </div>
                                    <div class="col-xs-4">
                                       <input type="checkbox">&nbsp;Fixed Term
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-3">
                           <label>SALARY</label>
                        </div>
                        <div class="col-xs-3">:_______________________</div>
                        <div class="col-xs-1">
                           <label>Mode</label>
                        </div>
                        <div class="col-xs-1">
                           <input type="checkbox"> &nbsp; Annual
                        </div>
                        <div class="col-xs-1">
                           <input type="checkbox"> &nbsp; Daily
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-3">
                           <label>SCHEDULE OF PAYROLL</label>
                        </div>
                        <div class="col-xs-2">
                           <input type="checkbox"> &nbsp;Weekly
                        </div>
                        <div class="col-xs-2">
                           <input type="checkbox"> &nbsp;Semi-Monthly
                        </div>
                        <div class="col-xs-2">
                           <input type="checkbox"> &nbsp;Monthly
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-3">
                           <label>FORMER AGENCY (in case of transfer)</label>
                        </div>
                        <div class="col-xs-9">:_______________________</div>
                     </div>
                  </div>
               </div>
               <div class="row margin-top">
                  <div class="col-xs-6">
                     <div class="row">
                        <div class="col-xs-12">
                           <label>Prepared By:</label>
                           <?php spacer(10); ?>
                        </div>
                     </div>
                     <div class="row text-center">
                        <div class="col-xs-12">
                           ____________________________________
                           <br>
                           AUTHORIZED SIGNATORY
                           <br>
                           (Signature over Printed Name)
                           <?php spacer(20); ?>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12">
                           <label>Date Signed : _______________________</label>
                           <?php spacer(20); ?>
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="row" style="border: 1px solid black; padding: 5px; text-align: center;">
                        <div class="col-xs-12">
                           <label>FOR CSC ACTION. Please don't write anything beyond this point</label>
                        </div>
                     </div>
                     <div class="row" style="border: 1px solid black; padding: 5px; border-top: none;">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-6">
                                 <label>CSC ACTION</label>
                              </div>
                              <div class="col-xs-3">
                                 <input type="checkbox"> &nbsp; Approved
                              </div>
                              <div class="col-xs-3">
                                 <input type="checkbox"> &nbsp; Disapproved
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-3">
                                 <label>REASONS / REMARKS</label>
                              </div>
                              <div class="col-xs-9">
                                 <label>: _______________________</label>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row" style="border: 1px solid black; padding: 5px; border-top: none;">
                        <div class="col-xs-12">
                           <div class="row">
                              <div class="col-xs-12 text-center">
                                 <label><u>DOCUMENT TRACKING</u></label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-3">
                                 <label>Received by :</label>
                              </div>
                              <div class="col-xs-6">
                                 <label>_______________________</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-3">
                                 <label>Encoded by :</label>
                              </div>
                              <div class="col-xs-6">
                                 <label>_______________________</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-xs-3">
                                 <label>Posted by :</label>
                              </div>
                              <div class="col-xs-5">
                                 <label>_______________________</label>
                              </div>
                              <div class="col-xs-4 text-right">
                                 <label>Date: ____________</label>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row margin-top" style="border: 1px solid black;">
                  <div class="col-xs-4" style="border-right: 1px solid black;">
                     <div class="row" style="padding: 5px; text-align: center; border-bottom: 1px solid black;">
                        <div class="col-xs-12">
                           <label>EMPLOYMENT STATUS</label>
                        </div>
                     </div>
                     <div class="row" style="padding: 15px;">
                        <div class="col-xs-4">
                           <div class="row">
                              Perm
                           </div>
                           <div class="row">
                              Temp
                           </div>
                           <div class="row">
                              Cas
                           </div>
                           <div class="row">
                              Cont
                           </div>
                           <div class="row">
                              Co-t
                           </div>
                           <div class="row">
                              Elect
                           </div>
                           <div class="row">
                              Prob
                           </div>
                           <div class="row">
                              Subst
                           </div>
                           <div class="row">
                              Reg / Perm
                           </div>
                           <div class="row">
                              Exec
                           </div>
                           <div class="row">
                              Fixed
                           </div>
                        </div>
                        <div class="col-xs-8">
                           <div class="row">
                              = Permanent
                           </div>
                           <div class="row">
                              = Temporary
                           </div>
                           <div class="row">
                              = Casual
                           </div>
                           <div class="row">
                              = Contractual
                           </div>
                           <div class="row">
                              = Co-Terminus
                           </div>
                           <div class="row">
                              = Elective
                           </div>
                           <div class="row">
                              = Probationary
                           </div>
                           <div class="row">
                              = Substitute
                           </div>
                           <div class="row">
                              = Regular / Permanent
                           </div>
                           <div class="row">
                              = Non Career Executive
                           </div>
                           <div class="row">
                              = Fixed Term
                           </div>

                        </div>
                     </div>
                  </div>
                  <div class="col-xs-8">
                     <div class="row" style="padding: 5px; text-align: center; border-bottom: 1px solid black;">
                        <div class="col-xs-12">
                           <label>NATURE OF APPOINTMENT & ACTION TYPE</label>
                        </div>
                     </div>
                     <div class="row" style="padding: 10px;">
                        <div class="col-xs-3">
                           <div class="row">
                              <label>APPOINTMENT:</label>
                           </div>
                           <div class="row">
                              New Hire (Orig/Initial)
                           </div>
                           <div class="row">
                              Promotion
                           </div>
                           <div class="row">
                              Transfer
                           </div>
                           <div class="row">
                              Re-employment
                           </div>
                           <div class="row">
                              Re-Appointment
                           </div>
                           <div class="row">
                              Renewal
                           </div>
                           <div class="row">
                              Demotion
                           </div>
                           <div class="row">
                              Reinstatement
                           </div>
                           <div class="row">
                              Change of Status
                           </div>
                           <div class="row">
                              Reclassification
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="row">
                              <label>NON-APPOINTMENT:</label>
                           </div>
                           <div class="row">
                              (Other Personnel Action)
                           </div>
                           <div class="row">
                              Step Increment
                           </div>
                           <div class="row">
                              Reassignment
                           </div>
                           <div class="row">
                              Job Rotation
                           </div>
                           <div class="row">
                              Secondment
                           </div>
                           <div class="row">
                              Presidential Appointment
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="row">
                              &nbsp;
                           </div>
                           <div class="row">
                              Change in term #
                           </div>
                           <div class="row">
                              Elected
                           </div>
                           <div class="row">
                              Re-elected                              
                           </div>
                           <div class="row">
                              Recalled
                           </div>
                           <div class="row">
                              Salary Adjustment
                           </div>
                           <div class="row">
                              Details/Designation
                           </div>
                           <div class="row">
                              Suspension
                           </div>
                        </div>
                        <div class="col-xs-3">
                           <div class="row">
                              <label>SEPARATION:</label>
                           </div>
                           <div class="row">
                              Dismissal
                           </div>
                           <div class="row">
                              Resignation
                           </div>
                           <div class="row">
                              Retirement
                           </div>
                           <div class="row">
                              Death
                           </div>
                           <div class="row">
                              Dropped from the rolls
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <?php
         }
      ?>
   </body>
</html>