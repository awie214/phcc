<?php
   require_once 'constant.e2e.php';
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'0620RptFunctions.e2e.php';
   require_once pathClass.'DTRFunction.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   //$whereClause .= " LIMIT 10";
   $table = "employees";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   $from = getvalue("txtAttendanceDateFrom");
   $to   = getvalue("txtAttendanceDateTo");
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   $from    = $year."-".$month."-01";
   $to      = $year."-".$month."-".cal_days_in_month(CAL_GREGORIAN,$month,$year);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
      <?php
         $rsEmployees = SelectEach("employees",$whereClause);
         if ($rsEmployees) {
            while ($row = mysqli_fetch_assoc($rsEmployees)) { 
               $EmployeesRefId = $row["RefId"];
               $CompanyRefId   = $row["CompanyRefId"];
               $BranchRefId    = $row["BranchRefId"];
               $emp_row = FindFirst("empinformation","WHERE EmployeesRefId = ".$row["RefId"],"*");
               if ($emp_row) {
                  $appt    = $emp_row["ApptStatusRefId"];
                  $appt    = getRecord("apptstatus",$appt,"Name");
                  $div     = getRecord("division",$emp_row["DivisionRefId"],"Name");
                  $hired   = date("d F Y",strtotime($emp_row["HiredDate"]));
                  $worksched = $emp_row["WorkScheduleRefId"];
               } else {
                  $appt    = "";
                  $div     = "";
                  $hired   = "";
                  $worksched = "";
               }
               //if ($worksched != "") {
                  rptHeader(getRptName("rptLeave_Card"));
      ?>               
               <div class="row" style="padding:10px;">
                  <div class="col-sm-4">
                     <?php 
                        echo "NAME : ".$row["LastName"].", ".$row["FirstName"]." ".$row["MiddleName"];  
                     ?>
                     <br>
                     Appointment Status: <?php echo $appt; ?>

                  </div>
                  <div class="col-sm-4 text-center">
                     
                  </div>
                  <div class="col-sm-4 text-right">
                     <u><?php echo $hired; ?></u>
                     <br>
                     1st Day of Service
                  </div>
               </div>
               <table border="1" width="100%">
                  <thead>
                     <tr align="center">
                        <th rowspan="2" style="width: 15%;">Period Covered</th>
                        <th rowspan="2" style="width: 15%;">Particular</th>
                        <th colspan="4">Vacation Leave</th>
                        <th colspan="4">Sick Leave</th>
                        <th rowspan="2">Remarks</th>
                     </tr>
                     <tr align="center">
                        <th>Earned</th>
                        <th>Absences<br>Undertime<br>With Pay</th>
                        <th>Balance</th>
                        <th>Absences<br>Undertime<br>W/O Pay</th>

                        <th>Earned</th>
                        <th>Absences<br>Undertime<br>With Pay</th>
                        <th>Balance</th>
                        <th>Absences<br>Undertime<br>W/O Pay</th>
                     </tr>
                  </thead>   
                  <tbody>
                     <tr>
                        <?php
                           $whereVL         = "WHERE CompanyRefId = ".$row['CompanyRefId']; 
                           $whereVL         .= " AND BranchRefId = ".$row["BranchRefId"];
                           $whereVL         .= " AND EmployeesRefId = ".$row['RefId']." AND NameCredits = 'VL' AND EffectivityYear = '$year'";
                           $row_vl          = FindOrderBy("employeescreditbalance",$whereVL,"*","BegBalAsOfDate");
                           if ($row_vl) {
                              $vl = $row_vl["BeginningBalance"];
                              $AsOf = $row_vl["BegBalAsOfDate"];
                              $start_date = date("Y-m-d",strtotime( "$AsOf + 1 day" ));
                           } else {
                              $vl = 0;
                              $AsOf = date("Y-m",time())."-01";
                              $start_date = date("Y-m-d",strtotime( "$AsOf - 1 day" ));
                           }
                        ?>
                        <td colspan="2">ACCUMULATED LEAVE CREDITS AS OF <?php echo date("F d, Y",strtotime($AsOf)); ?></td>
                        <td></td>
                        <td></td>
                        <td class="text-center">
                           <?php
                              ${"VLBal_".$EmployeesRefId} = $vl;
                              echo $vl;
                           ?>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="text-center">
                           <?php
                              $whereSL         = "WHERE CompanyRefId = ".$row['CompanyRefId']; 
                              $whereSL         .= " AND BranchRefId = ".$row["BranchRefId"];
                              $whereSL         .= " AND EmployeesRefId = ".$row['RefId']." AND NameCredits = 'SL' AND EffectivityYear = '$year'";
                              $row_sl          = FindOrderBy("employeescreditbalance",$whereSL,"*","BegBalAsOfDate");  
                              if ($row_sl) {
                                 $sl = $row_sl["BeginningBalance"];
                              } else {
                                 $sl = 0;
                              }
                              ${"SLBal_".$EmployeesRefId} = $sl;
                              echo $sl;
                           ?>
                        </td>
                        <td></td>
                        <td></td>
                     </tr>
                     <?php
                        ${"arr_leave_".$EmployeesRefId} = [
                           "01"=>array(),
                           "02"=>array(),
                           "03"=>array(),
                           "04"=>array(),
                           "05"=>array(),
                           "06"=>array(),
                           "07"=>array(),
                           "08"=>array(),
                           "09"=>array(),
                           "10"=>array(),
                           "11"=>array(),
                           "12"=>array(),
                        ];
                        $where = "where EmployeesRefId = ".$EmployeesRefId." AND Status = 'Approved'";
                        $where .= " AND ApplicationDateFrom BETWEEN '".$year."-01-01"."' AND '".date("Y-m-d",time())."'";
                        $where .= " ORDER BY ApplicationDateFrom";
                        $rsLeave = SelectEach("employeesleave",$where);
                        if ($rsLeave) {
                           while ($row = mysqli_fetch_assoc($rsLeave)) {
                              $dfrom   = date("d",strtotime($row["ApplicationDateFrom"]));
                              $dto     = date("d",strtotime($row["ApplicationDateTo"]));
                              $type    = getRecord("leaves",$row["LeavesRefId"],"Code");
                              
                              $month   = date("m",strtotime($row["ApplicationDateFrom"]));
                              $date    = $row["ApplicationDateFrom"];
                              $arr     = [$type,$row["ApplicationDateFrom"],$row["ApplicationDateTo"]];

                              if (isset(${"arr_leave_".$EmployeesRefId}[$month])) {
                                 array_push(${"arr_leave_".$EmployeesRefId}[$month], $arr);
                                 //${"arr_leave_".$EmployeesRefId}[$month] = $arr;   
                              }
                              
                           }
                        }
                        $arr_month =[
                          "January",
                          "February",
                          "March",
                          "April",
                          "May",
                          "June",
                          "July",
                          "August",
                          "September",
                          "October",
                          "November",
                          "December"
                        ];
                        for ($a=1; $a <=12 ; $a++) { 
                           if ($a <= 9) $a = "0".$a;
                           ${"VL_count_".$a} = 0;
                           ${"SL_count_".$a} = 0;
                           ${"Def_count_".$a} = 0;
                        }
                        $from          = date("m",strtotime($start_date));
                        for ($i=intval($from)-1; $i <= 12 - 1; $i++) {
                           $idx = $i+1;
                           if ($idx <= 9) $idx = "0".$idx;
                           $month      = $idx;
                           //$arr_empDTR = getDTRSummary($EmployeesRefId,intval($month),$year);
                           $where_dtr  = "WHERE EmployeesRefId = $EmployeesRefId AND Month = '$month' AND Year = '$year'";
                           $arr_empDTR = FindFirst("dtr_process",$where_dtr,"*");
                           if ($arr_empDTR) {
                              $where_monetization = "WHERE EmployeesRefId = '$EmployeesRefId'";
                              $where_monetization .= " AND MONTH(FiledDate) = '".$month."'";
                              $where_monetization .= " AND YEAR(FiledDate) = '".$year."'";
                              $where_monetization .= " AND Status = 'Approved'";
                              $monetize   = FindFirst("employeesleavemonetization",$where_monetization,"*");
                              if ($monetize) {
                                 $vl_monetize = $monetize["VLValue"];
                                 $sl_monetize = $monetize["SLValue"];
                                 $remarks_monetize = "Monetized ".floatval($vl_monetize)." (VL) AND ".floatval($sl_monetize)." (SL)";
                              } else {
                                 $vl_monetize = $sl_monetize = 0;
                                 $remarks_monetize = "";
                              }
                              $VL_Days       = $arr_empDTR["VL_Days"];
                              $VL_Used       = $arr_empDTR["VL_Used"];
                              $SL_Days       = $arr_empDTR["SL_Days"];
                              $SL_Used       = $arr_empDTR["SL_Used"];
                              $vl_days_obj   = "";
                              $sl_days_obj   = "";
                              ${"VL_count_".$arr_empDTR["Month"]} = $VL_Used;
                              ${"SL_count_".$arr_empDTR["Month"]} = $SL_Used; 
                              echo '
                                 <tr>
                                    <td class="text-center">'.$arr_month[$i].'</td>';
                              echo '<td>';
                                    if ($VL_Days != "") {
                                       $VL_Days_arr = explode("|", $VL_Days);
                                       foreach ($VL_Days_arr as $key => $value) {
                                          if ($value != "") {
                                             $vl_days_obj .= $value.", ";   
                                          }
                                       }
                                    }
                                    if ($SL_Days != "") {
                                       $SL_Days_arr = explode("|", $SL_Days);
                                       foreach ($SL_Days_arr as $xkey => $xvalue) {
                                          if ($xvalue != "") $sl_days_obj .= $xvalue.", ";
                                       }
                                    }
                                    if ($vl_days_obj != "") {
                                       echo $vl_days_obj." (VL); ";
                                    }
                                    if ($sl_days_obj != "") {
                                       echo $sl_days_obj." (SL); ";
                                    }
                                    if (isset(${"arr_leave_".$EmployeesRefId}[$idx])) {
                                       $def_type   = "";
                                       if (!empty(${"arr_leave_".$EmployeesRefId}[$idx])) {
                                          foreach (${"arr_leave_".$EmployeesRefId}[$idx] as $xkey => $xvalue) {
                                             $def_type   = "";
                                             $other_type = "";
                                             $type = $xvalue[0];
                                             $from = intval(date("d",strtotime($xvalue[1])));
                                             $to   = intval(date("d",strtotime($xvalue[2])));
                                             if ($type != "VL" && $type != "SL") {
                                                if ($from == $to) {
                                                   ${"Def_count_".$idx}++;
                                                   $def_type .= $from;
                                                   $other_type = $type;
                                                } else {
                                                   ${"Def_count_".$idx}++;
                                                   $def_type .= $from." - ".$to;
                                                   $other_type = $type;
                                                }
                                             }
                                          }
                                       }
                                       if ($def_type != "") echo $def_type." ($other_type);";
                                    }

                                    $sl_earned     = $arr_empDTR["SL_Earned"];
                                    $sl_deduction  = $SL_Used + $sl_monetize;
                                    $newSLBal      = ${"SLBal_".$EmployeesRefId} + $sl_earned - $sl_deduction;
                                    
                                    ${"SLBal_".$EmployeesRefId} = $newSLBal;
                                    if ($newSLBal < 0) {
                                       $sl_wop     = str_replace("-", "", $newSLBal);
                                       $newSLBal   = 0;
                                       $sl_balance = 0;
                                       ${"SLBal_".$EmployeesRefId} = 0;
                                    } else {
                                       $sl_wop     = 0; 
                                       $sl_balance = $newSLBal;
                                    }
                                    




                                    $vl_earned     = $arr_empDTR["VL_Earned"];
                                    $deduction     = $arr_empDTR["Tardy_Deduction_EQ"] + 
                                                     $arr_empDTR["Undertime_Deduction_EQ"] 
                                                     + $arr_empDTR["Total_Absent_EQ"];
                                    $vl_deduction  = $deduction + ${"VL_count_".$idx} + $sl_wop + $vl_monetize;
                                    $newVLBal      = ${"VLBal_".$EmployeesRefId} + $vl_earned - $vl_deduction;
                                    $where_wop     = "WHERE NoOfDaysLeaveWOP <= '$vl_deduction'";
                                    $get_vl_wop    = FindLast("leavecreditsearnedwopay",$where_wop,"LeaveCreditsEarned");
                                    ${"VLBal_".$EmployeesRefId} = $newVLBal;
                                    if ($newVLBal < 0) {
                                       $vl_wop     = $get_vl_wop;
                                       $newVLBal   = 0;
                                       $vl_balance = 0;
                                       ${"VLBal_".$EmployeesRefId} = 0;
                                    } else {
                                       $vl_wop     = 0; 
                                       $vl_balance = $newVLBal;  
                                    }


                              echo '</td>';      
                              echo '<td class="text-center">';
                                 echo $vl_earned;
                              echo '</td>';      
                              echo '<td class="text-center">';
                                 echo $vl_deduction;
                              echo '</td>';      
                              echo '<td class=  "text-center">';
                                 echo $vl_balance;
                              echo '</td>';      
                              echo '<td class="text-center">';
                                 echo $vl_wop;
                              echo '</td>';      
                              echo '<td class="text-center">';
                                 echo $sl_earned;
                              echo '</td>';      
                              echo '<td class="text-center">';
                                 echo $sl_deduction;
                              echo '</td>';      
                              echo '<td class="text-center">';
                                 echo $sl_balance;
                              echo '</td>';      
                              echo '<td class="text-center">';
                                 //echo $sl_wop;
                              echo '</td>';      
                              echo '<td style="padding-left:5px;">';
                                    $remAbsent = "";
                                    $Absent_arr = explode("|", $arr_empDTR["Days_Absent"]);
                                    foreach ($Absent_arr as $key => $value) {
                                       if ($value != "") {
                                          $remAbsent = $remAbsent.$value.", ";
                                       }
                                    }
                                    if ($remAbsent != "") {
                                       echo $remAbsent." No File";
                                    }
                                    if ($sl_wop != "") {
                                       echo "( ".$sl_wop." charged to VL)";
                                    }
                                    if ($remarks_monetize != "") {
                                       echo $remarks_monetize;
                                    }
                              echo '</td>';      
                              echo '
                                 </tr>
                              ';
                           }
                        }
                     ?>
                  </tbody>   
               </table>
               <p>
                  This is a system generated report. Signature is not required.
               </p>
         <?php 
               //}
            }
         }
         ?>
      </div>
   </body>
</html>