<?php
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $table = "employees";
   $whereClause .= " ORDER BY LastName";
   $rsEmployees = SelectEach($table,$whereClause);
   if ($rsEmployees) $rowcount = mysqli_num_rows($rsEmployees);
   if ($dbg) {
      echo $whereClause;
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <table style="width: 100%;">
            <thead>
               <tr>
                  <td colspan="15">
                     <?php
                        rptHeader("IPS & DPS Submission Report");
                     ?>
                  </td>
               </tr>
               <tr class="colHEADER">
                  <th rowspan="3">#</th>
                  <th rowspan="3">Employee Name</th>
                  <th rowspan="3">Office</th>
                  <th rowspan="3">Interim Office</th>
                  <th rowspan="3">#</th>
                  <th rowspan="3">Assumption to Duty</th>
                  <th colspan="4">January to June</th>
                  <th colspan="5">July to December</th>
               </tr>
               <tr class="colHEADER">
                  <th>Targets</th>
                  <th colspan="2">With Accomplishments and Final Rating</th>
                  <th>Remarks</th>
                  <th>Targets</th>
                  <th colspan="3">With Accomplishments and Final Rating</th>
                  <th>Remarks</th>
               </tr>
               <tr class="colHEADER">
                  <th>Date of Submission</th>
                  <th>Numerical</th>
                  <th>Adjectival</th>
                  <th></th>
                  <th>Date of Submission</th>
                  <th>Date of Submission</th>
                  <th>Numerical</th>
                  <th>Adjectival</th>
                  <th></th>
               </tr>
            </thead>
            <tbody>
               <?php
                  if ($rsEmployees) {
                     $count = 0;
                     while ($row = mysqli_fetch_assoc($rsEmployees)) {
                        $count++;
                        $emprefid   = $row["RefId"];
                        $LastName   = $row["LastName"];
                        $MiddleName = $row["MiddleName"];
                        $FirstName  = $row["FirstName"];
                        $ExtName    = $row["ExtName"];
                        $FullName   = $LastName.", ".$FirstName." ".substr($MiddleName, 0, 1).". ".$ExtName;
                        echo '<tr>';
                        echo '<td class="text-center">'.$count.'</td>';
                        echo '<td>'.$FullName.'</td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '</tr>';
                     }
                  } else {
                     echo '<tr><td colspan="15">No Record Found.</td></tr>';
                  }
               ?>
            </tbody>
         </table>
      </div>
   </body>
</html>