<?php
	require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   include 'incRptParam.e2e.php';
   include 'incRptQryString.e2e.php';
   $month = getvalue("txtAttendanceMonth");
   $year  = getvalue("txtAttendanceYear");
   $rsEmployees = SelectEach("employees",$whereClause);
?>
<html>
   <head>
   	<?php include_once $files["inc"]["pageHEAD"]; ?>
   	<link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
   	<script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
   </head>
   <body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12">
					<?php
		            rptHeader("LWOP");
		         ?>
		         <div class="row">
		         	<div class="col-xs-12 text-center">
		         		<?php
		         			echo date("F Y",strtotime($year."-".$month."-01"));
		         		?>
		         	</div>
		         </div>
		         <br>
		         <div class="row">
		         	<div class="col-xs-12">
		         		<table width="100%">
		         			<thead>
		         				<tr class="colHEADER">
		         					<th style="width: 10%;">#</th>
		         					<th style="width: 30%;">Employee Name</th>
		         					<th style="width: 30%;">Date/s</th>
		         					<th style="width: 20%;">Absent/Tardy/UT</th>
		         					<th style="width: 10%;">LWOP</th>
		         				</tr>
		         			</thead>
		         			<tbody>
		         				<?php
		         					if ($rsEmployees) {
		         						$count = 0;
		         						while ($emp_row = mysqli_fetch_assoc($rsEmployees)) {
		         							$emprefid 		= $emp_row["RefId"];
		         							$LastName 		= $emp_row["LastName"];
		         							$MiddleName 	= $emp_row["MiddleName"];
		         							$FirstName 		= $emp_row["FirstName"];
		         							$ExtName 		= $emp_row["ExtName"];
		         							$MiddleInitial = substr($MiddleName, 0,1);
		         							$FullName 		= $LastName.", ".$FirstName." ".$ExtName." ".$MiddleInitial.".";

		         							$where_process = "WHERE EmployeesRefId = '$emprefid'";
									      	$where_process .= " AND Year = '$year'";
									      	$where_process .= " AND Month = '$month'";
									      	$row_process 	= FindFirst("dtr_process",$where_process,"*");
									      	if ($row_process) {
									      		$Days_Absent 			= $row_process["Days_Absent"];
									      		$Days_Tardy 			= $row_process["Days_Tardy"];
									      		$Days_Undertime 		= $row_process["Days_Undertime"];
									      		$Tardy_Deduction_EQ 			= $row_process["Tardy_Deduction_EQ"];
									      		$Undertime_Deduction_EQ 	= $row_process["Undertime_Deduction_EQ"];
									      		$Total_Absent_EQ 				= $row_process["Total_Absent_EQ"];
									      		$Days_Absent_arr 		= explode("|", $Days_Absent);
									      		$Days_Tardy_arr 		= explode("|", $Days_Tardy);
									      		$Days_Undertime_arr 	= explode("|", $Days_Undertime);
									      		$Absent_Remarks 		= "";
									      		$Undertime_Remarks 	= "";
									      		$Tardy_Remarks			= "";
									      		$Total_Deduction 		= $Tardy_Deduction_EQ + $Undertime_Deduction_EQ + $Total_Absent_EQ;
									      		foreach ($Days_Absent_arr as $Absent_key => $Absent_value) {
									      			if ($Absent_value != "") {
									      				$Absent_Remarks .= $Absent_value.", ";
									      			}
									      		}
									      		if ($Absent_Remarks != "") {
									      			$Absent_Remarks .= " (A)";
									      		}

									      		foreach ($Days_Undertime_arr as $Undertime_key => $Undertime_value) {
									      			if ($Undertime_value != "") {
									      				$Undertime_Remarks .= $Undertime_value.", ";
									      			}
									      		}
									      		if ($Undertime_Remarks != "") {
									      			$Undertime_Remarks .= " (U)";
									      		}

									      		foreach ($Days_Tardy_arr as $Tardy_key => $Tardy_value) {
									      			if ($Tardy_value != "") {
									      				$Tardy_Remarks .= $Tardy_value.", ";
									      			}
									      		}
									      		if ($Tardy_Remarks != "") {
									      			$Tardy_Remarks .= " (T)";
									      		}


									      		$where_credit  = "WHERE EmployeesRefId = '$emprefid'";
			         							$where_credit 	.= " AND EffectivityYear = '".$year."'";
	      										$credit_detail = FindLast("employeescreditbalance",$where_credit,"*");
	      										if ($credit_detail) {
											         $balance_date  = $credit_detail["BegBalAsOfDate"];
											         $from          = date("m",strtotime($balance_date." + 1 Day"));
											         $to 			= $month;
											         $credit_arr    = computeCredit($emprefid,$from,$to,$year);
											         $LWOP 			= $credit_arr["LWOP"];
											      } else {
											         $LWOP 			= 0;
											      }

											      if ($LWOP > 0) {
											      	$count++;
										      		echo '<tr>';
				         							echo '
				         								<td class="text-center">'.$count.'</td>
				         								<td>'.$FullName.'</td>
				         								<td>'.$Absent_Remarks.' '.$Tardy_Remarks.' '.$Undertime_Remarks.'</td>
				         								<td class="text-center">'.number_format($Total_Deduction,3).'</td>
				         								<td class="text-center">'.number_format($LWOP,3).'</td>
				         							';
				         							echo '</tr>';
										      	}	
									      	}
		         						}
		         					}
		         				?>
		         			</tbody>
		         		</table>
		         	</div>
		         </div>
				</div>
			</div>		
		</div>   	
   </body>
</html>
