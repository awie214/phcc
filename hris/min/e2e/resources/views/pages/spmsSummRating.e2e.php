<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_SummRating"); ?>"></script>
      <script type="text/javascript">
         $(document).ready(function () {



         });
      </script>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"spms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($modTitle); ?>
            <div class="container-fluid margin-top10">
               <div class="row">
                  <div class="col-xs-3">
                     <?php
                     /*
                        $EmpRefId = getvalue("txtRefId");
                        $attr = ["empRefId"=>getvalue("txtRefId"),
                                 "empLName"=>getvalue("txtLName"),
                                 "empFName"=>getvalue("txtFName"),
                                 "empMName"=>getvalue("txtMidName")];
                        $EmpRefId = EmployeesSearch($attr);
                     */
                        echo
                        '<div class="mypanel">
                           <div class="panel-top">
                              Search Select Employees:<br>
                              <input type="text" class="form-input" name="SearchEmployees" style="width:100px;">&nbsp;';
                              createButton("Search","btnSearchEmployees","btn-cls4-lemon","fa-search","");
                           echo
                           '</div>
                           <div class="panel-mid" style="height:500px;overflow:auto;" id="divEmployeesList">';
                           $rsEmployees = SelectEach("employees","order by LastName, FirstName");
                           if ($rsEmployees) {
                              if (mysqli_num_rows($rsEmployees) > 0) {
                                 while ($rows = mysqli_fetch_array($rsEmployees)) {

                                    if ($rows["LastName"] != "" &&
                                       $rows["FirstName"] != "") {
                                       echo '<div class="Employees--" refid="'.$rows["RefId"].'" style="border-bottom:1px solid gray">';
                                       echo '<a href="javascript:void(0);">['.$rows["RefId"].'] '.strtoupper($rows["LastName"]).", ".strtoupper($rows["FirstName"]).'</a>';
                                       echo '</div>';
                                    }
                                 }
                              }
                           }
                        echo
                        '</div>
                           <div class="panel-bottom"></div>
                        </div>';
                     ?>

                  </div>
                  <div class="col-xs-9">
                     <div id="divList">
                        <div class="panel-top">
                           LIST OF PERFORMANCE MANAGEMENT
                        </div>
                        <div class="panel-mid-litebg">
                           <div class="row">
                              <div class="col-xs-12">
                                 <div style="overflow:auto;max-height:500px;">
                                    <div id="spGridTable">
                                       <?php
                                          $table = "employeesperformance";
                                          $gridTableHdr = "Employee Name|Semester|Year|Overall Score|Adjectival";
                                          $gridTableFld = "EmployeesRefId|Semester|YearPerformed|OverallScore|Adjectival";
                                          $gridTableHdr_arr = explode("|",$gridTableHdr);
                                          $gridTableFld_arr = explode("|",$gridTableFld);
                                          $sql = "SELECT * FROM `$table` WHERE RefId = 0 ORDER BY RefId Desc LIMIT 100";
                                          $_SESSION["module_table"] = $table;
                                          $_SESSION["module_gridTableHdr_arr"] = $gridTableHdr_arr;
                                          $_SESSION["module_gridTableFld_arr"] = $gridTableFld_arr;
                                          $_SESSION["module_sql"] = $sql;
                                          $_SESSION["module_gridTable_ID"] = "gridTable";
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      [true,true,true,false],
                                                      $_SESSION["module_gridTable_ID"]);
                                       ?>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="panel-bottom">
                           <?php btnINRECLO(['true','true','true']);?>
                        </div>
                     </div>
                     <div class="row" id="divView">
                        <div class="col-xs-8" style="padding-left:15px;" id="EntryScrn">
                           <input type="hidden" class="form-input saveFields--" name="sint_EmployeesRefId"/>
                           <div class="row margin-top">
                              <div class="col-xs-3">
                                 <label>Semester:</label>
                              </div>
                              <div class="col-xs-3">
                                 <select class="form-input saveFields--" name="char_Semester">
                                    <option value="January - June">January - June</option>
                                    <option value="July - December">July - December</option>
                                 </select>
                              </div>
                              <div class="col-xs-3">
                                 <label>Year:</label>
                              </div>
                              <div class="col-xs-3">
                                 <input type="text" class="form-input number-- saveFields--" name="date_YearPerformed" maxlength="4">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-3">
                                 <label>Overall Score:</label>
                              </div>
                              <div class="col-xs-3">
                                 <input type="text" class="form-input saveFields-- number--" name="bint_OverallScore" placeholder="LIMIT IS 5">
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-3">
                                 <label>Rating:</label>
                              </div>
                              <div class="col-xs-3">
                                 <input type="text" class="form-input saveFields-- number--" name="bint_NumericalRating" readonly>
                              </div>
                           </div>
                           <div class="row margin-top">
                              <div class="col-xs-3">
                                 <label>Adjectival:</label>
                              </div>
                              <div class="col-xs-3">
                                 <select class="form-input saveFields-- mandatory" name="char_Adjectival" readonly>
                                    <option value="5">Outstanding</option>
                                    <option value="4">Very Satisfactory</option>
                                    <option value="3">Satisfactory</option>
                                    <option value="2">Unsatisfactory</option>
                                    <option value="1">Poor</option>
                                 </select>
                              </div>
                           </div>

                           <?php
                              spacer(10);
                              btnSACABA(['true','true','true']);
                           ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("EmpRefIdRequire","YES","");
               $table = "employeesperformance";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>