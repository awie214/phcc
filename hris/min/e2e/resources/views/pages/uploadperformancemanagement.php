<!DOCTYPE html>
<html>
<body>

<?php
require "constant.e2e.php";
require "conn.e2e.php";
require_once pathClass."0620functions.e2e.php";
   $conn->query("TRUNCATE TABLE `employeesperformance`;");
   $t = time();
   $date_today    = date("Y-m-d",$t);
   $curr_time     = date("H:i:s",$t);
   $trackingA_fld = "`LastUpdateDate`, `LastUpdateTime`, `LastUpdateBy`, `Data`";
   $trackingA_val = "'$date_today', '$curr_time', 'PHP', 'M'";

   
   
   
   
   $sql = "SELECT * FROM `employee_pm`";
   $rs = mysqli_query($conn,$sql) or die(mysqli_error($conn));
   echo "Number Records : ".mysqli_num_rows($rs)."<br>";
   if ($rs) {
      if (mysqli_num_rows($rs) > 0) {
         while ($row = mysqli_fetch_array($rs)) {
            $RefId = $row['id'];
            $AgencyId = $row['idno'];
            $EmpRefId = FindFirst("employees","WHERE AgencyId = $AgencyId","RefId");
            $OverallScore = $row['overall_point_score'];
            $Rating = $row['numerical_rating'];
            $Adjectival = $row['adjectival'];
            $Time = $row['timestamp'];
            $date = $row['date'];
            $year = substr($date,strlen($date)-4,4);
            $semester = substr($date,0,strlen($date)-4);
            
            $vals = "'$year','$semester',";
            $vals .= "'$RefId',1000,1,'$AgencyId','$EmpRefId','$OverallScore','$Rating','$Adjectival','$Time',".$trackingA_val;
            $flds = "`YearPerformed`,`Semester`,";
            $flds .= "`RefId`,`CompanyRefId`,`BranchRefId`,`EmployeesId`,`EmployeesRefId`,`OverallScore`,`NumericalRating`,";
            $flds .= "`Adjectival`,`TimePerformed`,".$trackingA_fld;
            
            $conn->query("INSERT INTO `employeesperformance` ($flds) VALUES ($vals)");
            
            
         }
      }
   }
?>
</body>
</html>