<?php
	include_once "constant.e2e.php";
   	include_once pathClass.'0620functions.e2e.php';
   	$rs = SelectEach("employees","WHERE Inactive = 0 OR Inactive IS NULL");
   	if ($rs) {
   		while ($row = mysqli_fetch_assoc($rs)) {
   			$emprefid 		   = $row["RefId"];
   			$credit_arr    	= computeCredit($emprefid,"10","12","2018","2018");
            $credit_name      = "";
            $OT_Flds          = "`NameCredits`,`EmployeesRefId`,`EffectivityYear`,`BegBalAsOfDate`,`BeginningBalance`,";

            $OT_Vals          = "'OT', '$emprefid', '2019', '2018-12-31', '".$credit_arr["OT"]."',";
            
            // echo $VL_Flds."<br>".$VL_Vals."<br>===============================<br>";
            // echo $SL_Flds."<br>".$SL_Vals."<br>===============================<br>";
            // echo $SPL_Flds."<br>".$SPL_Vals."<br>===============================<br>";

            $SAVE_OT    = f_SaveRecord("NEWSAVE","employeescreditbalance",$OT_Flds,$OT_Vals,"SYSTEM");
            
            if (is_numeric($SAVE_OT)) {
               echo "Successfully Save New OT Value for $emprefid.<br>";
            } else {
               echo "Error in Saving OT Value for $emprefid.<br>";
               return false;
            }
            echo "<br>=========================<br>";
   			// echo "VL: ".$credit_arr["VL"]." -> ";
   			// echo "SL: ".$credit_arr["SL"]." -> ";
   			// echo "FL: ".$credit_arr["FL"]." -> ";
   			// echo "SPL: ".$credit_arr["SPL"]." -> ";
   			// echo "OT: ".$credit_arr["OT"]." <br>";
   		}
   	}
?>