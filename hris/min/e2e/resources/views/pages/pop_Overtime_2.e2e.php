<?php
   $file = "Request for Authority To Render Overtime Work";
   include_once 'pageHEAD.e2e.php';
   require_once $_SESSION['Classes'].'0620functions.e2e.php';
   require_once $_SESSION['Classes'].'0620RptFunctions.e2e.php';
   require_once "conn.e2e.php";
   $dbg = false;
   include 'incRptParam.e2e.php';
?>
<!DOCTYPE html>
<html>
   <head>
      <link rel="stylesheet" href="<?php echo path("css/rpt.css"); ?>">
      <script src="<?php echo jsCtrl("ctrl_Report"); ?>"></script>
      <style>
         /*.bottomline {border-bottom:2px solid black;}*/
         .bold {font-size:600;}
      </style>
   </head>
   <body>
      <div class="container-fluid rptBody">
         <?php
            rptHeader("");
         ?>
         <div class="txt-center bold">
            <b>
               <h4>AUTHORITY TO RENDER OVERTIME SERVICES</h4>
            </b>
         </div>
         <div class="row">
            <div class="col-xs-8"></div>
            <div class="col-xs-4 text-center">
               ______________________
               <br>
               Date
            </div>
         </div>
         <br>
         <table border="1">
            <thead>
               <tr>
                  <th>
                     Name
                  </th>
                  <th>
                     Office
                  </th>
                  <th>
                     Position
                  </th>
                  <th>
                     Preferred<br>Remuneration<br>(OT Pay or CTO)
                  </th>
               </tr>
            </thead>
            <tbody>
               <?php
                  $rs = SelectEach("overtime_request","WHERE Status = 'Approved'");
                  if ($rs) {
                     while ($row = mysqli_fetch_assoc($rs)) {
                        $emprefid = $row["EmployeesRefId"];
                        $emp_row = FindFirst("employees","WHERE RefId = '$emprefid'","`FirstName`,`LastName`,`MiddleName`");
                        if ($emp_row) {
                           $LastName = $emp_row["LastName"];
                           $FirstName = $emp_row["FirstName"];
                           $MiddleName = $emp_row["MiddleName"];
                           $FullName = $LastName.", ".$FirstName." ".$MiddleName;
                        } else {
                           $FullName = "";
                        }
                        $empinfo_row = FindFirst("empinformation","WHERE EmployeesRefId = '$emprefid'","*");
                        if ($empinfo_row) {
                           $Office = getRecord("office",$empinfo_row["OfficeRefId"],"Name");
                           $Position = getRecord("position",$empinfo_row["PositionRefId"],"Name");
                        } else {
                           $Office = "";
                           $Position = "";
                        }
                        $stat = $row["WithPay"];
                        if (intval($stat) > 0) {
                           $stat = "Overtime With Pay";
                        } else {
                           $stat = "Overtime Without Pay CTO";
                        }
                        echo '
                           <tr>
                              <td>'.$FullName.'</td>
                              <td class="text-center">'.$Office.'</td>
                              <td class="text-center">'.$Position.'</td>
                              <td class="text-center">'.$stat.'</td>
                           </tr>
                        ';
                     }
                  }
               ?>
            </tbody>
         </table>
         <br>
         <div class="row margin-top">
            <div class="col-xs-8">
               To render overtime job/services on:
            </div>
            <div class="col-xs-4 text-center">
               ______________________
               <br>
               Date
            </div>
         </div>
      </div>
   </body>
</html>