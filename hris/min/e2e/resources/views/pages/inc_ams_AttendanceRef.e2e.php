<div class="row" id="attRef">
   <div class="col-xs-12">
      <div class="row">
         <div class="col-xs-12">
            <input type="checkbox" name="chkAttRef" id="chkAttRef" class="enabler--">
            &nbsp;<label for="chkAttRef">EDIT ATTENDANCE REFERENCE</label>
         </div>
      </div>
      <div class="row margin-top">
         <div class="col-xs-6">
            <div class="row margin-top">
               <div class="col-xs-9">
                  <label class="control-label" for="inputs">MACHINE ID:</label><br>
                  <input class="form-input saveFields--" type="text" id="char_BiometricsID" name="char_BiometricsID">
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-9">
                  <label>Cut Off</label>
                  <br>   
                  <?php
                     /*
                     $attr = array(
                        "row"=>true,
                        "col"=>"12",
                        "label"=>"CUT-OFF",
                        "table"=>"cutoff",
                        "name"=>"sint_CutOffRefId"
                     );
                     createFormFK($attr);
                     */
                     createSelect("cutoff","sint_CutOffRefId","",100,"Name","Select Cut Off","");
                  ?>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-9">
                  <label>Work Schedule</label>
                  <br>
                  <?php
                     /*
                     $attr = array(
                        "row"=>true,
                        "col"=>"12",
                        "label"=>"Work Schedule",
                        "table"=>"workschedule",
                        "name"=>"sint_WorkScheduleRefId"
                     );
                     createFormFK($attr);
                     */
                     createSelect("workschedule","sint_WorkScheduleRefId","",100,"Name","Select Work Schedule","");
                  ?>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-9">
                  <label>Leave Policy</label>
                  <br>
                  <?php
                     /*
                     $attr = array(
                        "row"=>true,
                        "col"=>"12",
                        "label"=>"Leave Policy",
                        "table"=>"LeavePolicy",
                        "name"=>"sint_LeavePolicyRefId"
                     );
                     createFormFK($attr);
                     */
                     createSelect("LeavePolicyGroup","sint_LeavePolicyGroupRefId","",100,"Name","Select Leave Policy","");
                  ?>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-9">
                  <label>Overtime Policy</label>
                  <br>
                  <?php
                     /*
                     $attr = array(
                        "row"=>true,
                        "col"=>"12",
                        "label"=>"Overtime Policy",
                        "table"=>"OvertimePolicy",
                        "name"=>"sint_OvertimePolicyRefId"
                     );
                     createFormFK($attr);
                     */
                     createSelect("OverTimePolicyGroup","sint_OverTimePolicyGroupRefId","",100,"Name","Select Overtime Policy","");
                  ?>
               </div>
            </div>
         </div>
         <div class="col-xs-6">
            <div class="row">
               <div class="col-xs-6">
                  <div class="row">
                     <label class="control-label" for="inputs"><u>SUSPENSION DATE</u></label>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label class="control-label" for="inputs">START DATE:</label><br>
                           <input class="form-input date-- saveFields--" type="text" id="" name="date_StartDate">
                        </div>
                     </div>
                     <div class="row margin-top">
                        <div class="col-xs-12">
                           <label class="control-label" for="inputs">END DATE:</label><br>
                           <input class="form-input date-- saveFields--" type="text" id="" name="date_EndDate">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row margin-top10">
               <div class="col-xs-6">
                  <label>Is Automatic DTR Schedule</label>
                  <select name="sint_IsAutoDTR" class="saveFields-- form-input" id="sint_IsAutoDTR">
                     <option value=""></option>
                     <option value="0">NO</option>
                     <option value="1">YES</option>
                  </select>
               </div>
            </div>
            <div class="row margin-top">
               <div class="col-xs-6">
                  <label>Driver Schedule</label>
                  <select name="sint_IsDriver" class="saveFields-- form-input" id="sint_IsDriver">
                     <option value=""></option>
                     <option value="0">NO</option>
                     <option value="1">YES</option>
                  </select>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
