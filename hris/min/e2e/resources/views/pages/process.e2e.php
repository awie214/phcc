<?php
	error_reporting(E_ALL);
   ini_set('display_errors', 1);
	session_start();
	include_once "constant.e2e.php";
	include_once pathClass.'0620functions.e2e.php';
	$user 		= getvalue("hUser");

	$funcname 	= getvalue("fn");
	$params   	= getvalue("params");
	if (!empty($funcname)) {
   	$funcname($params);
	} else {
   	echo 'alert("Error... No Function defined");';
	}
	function UpdateUserManagement() {
		$obj = getvalue("obj");
		$emprefid = getvalue("emprefid");
		$division = getvalue("division");
		$refid = FindFirst("usermanagement","WHERE EmployeesRefId = '$emprefid'","RefId");
		if ($refid) {
			$fldnval = "SystemAccess = '$obj', DivisionRefId = '$division', ";
			$result = f_SaveRecord("EDITSAVE","usermanagement",$fldnval,$refid);
			if ($result == "") {
				echo 'alert("Successfully Update User Management");';
			} else {
				echo 'alert("Error in Updating");';
			}
		} else {
			$fld = "EmployeesRefId, SystemAccess, DivisionRefId, ";
			$val = "'$emprefid', '$obj', '$division',";
			$result = f_SaveRecord("NEWSAVE","usermanagement",$fld,$val);
			if (is_numeric($result)) {
				echo 'alert("Successfully Added User Management");';
			} else {
				echo 'alert("Error");';
			}
		}
	}

	function getUserManagement() {
		$emprefid 		= getvalue("emprefid");
		$row 			= FindFirst("usermanagement","WHERE EmployeesRefId = '$emprefid'","*");
		echo '$("#sint_DivisionRefId").val("'.$row["DivisionRefId"].'");';
		if ($row) {
			$access_arr = explode("|", $row["SystemAccess"]);
			foreach ($access_arr as $key => $value) {
				if ($value != "") {
					echo '$("#idx_" + '.$value.').attr("checked",true);';
				}
			}
		}
	}
?>