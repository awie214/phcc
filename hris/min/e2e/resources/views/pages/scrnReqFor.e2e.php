<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
   </head>
   <body>
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"pis"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar(getvalue("paramTitle")); ?>
            <div class="container-fluid margin-top" id="div_CONTENT">
               <div class="row">
                  <div class="col-xs-3">
                     <input type="checkbox" id="">
                     <label for="" >PDS</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="checkbox" id="">
                     <label for="">SERVICE RECORDS</label>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6">
                     <h3>REQUEST COPY FOR</h3>
                     <?php bar(); ?>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-3">
                     <input type="checkbox" id="">
                     <label for="" >TOR</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="checkbox" id="">
                     <label for="">DOCUMENT 1</label>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-3">
                     <input type="checkbox" id="" name="chkTrainCert">
                     <label for="" >TRAINING CERTIFICATE</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="checkbox" id="">
                     <label for="">DOCUMENT 2</label>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-3">
                     <input type="checkbox" id="">
                     <label for="" >BIRTH CERTIFICATE</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="checkbox" id="">
                     <label for="">DOCUMENT 3</label>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-3">
                     <input type="checkbox" id="">
                     <label for="" >MARRIAGE CERTIFICATE</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="checkbox" id="">
                     <label for="">DOCUMENT 4</label>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-3">
                     <input type="checkbox" id="">
                     <label for="" >CERTIFICATE OF EMPLOYMENT</label>
                  </div>
                  <div class="col-xs-3">
                     <input type="checkbox" id="">
                     <label for="">DOCUMENT 5</label>
                  </div>
               </div>

               <div>
                  <button type="button"
                          class="btn-cls4-sea trnbtn"
                          id="btnREQUEST" name="btnREQUEST">
                     REQUEST
                  </button>
               </div >
            </div>
            <?php
               footer();
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
      <script language="JavaScript" src="<?php echo jsCtrl("ctrl_Dashboard"); ?>"></script>
   </body>
</html>



