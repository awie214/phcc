<?php
	//ALTER TABLE `course` ADD COLUMN `Level` INT(1) NULL DEFAULT NULL AFTER `Name`;
	include 'conn.e2e.php';
	include 'constant.e2e.php';
	include pathClass.'0620functions.e2e.php';


	$voc_course = fopen(textFile."voc_course.csv","r");
	$date_today    = date("Y-m-d",time());
	$curr_time     = date("H:i:s",time());
	$trackingflds = "`LastUpdateBy`, `LastUpdateDate`, `LastUpdateTime`, `Data`";
	$trackingvals = "'SYSTEM', '$date_today', '$curr_time', 'A'";
	while(! feof($voc_course)) {
	  	$obj = fgets($voc_course);
	  	if ($obj != "") {
		  	$code = "";
		  	$course_name = $obj;
			if ($course_name != "") {
				$flds = "`Code`,`Name`,`Level`,`Remarks`,";
				$values = "'$code', '$course_name', 3, 'Uploaded',";
				$result = fileManager($course_name,"course",$flds,$values);		
				if (!is_numeric($result)) {
					echo "<li>This course is not inserted: ".$result;
				}
			}	
	  	}
	  	
	}


	
	$coll_course = fopen(textFile."coll_course.csv","r");
	$date_today    = date("Y-m-d",time());
	$curr_time     = date("H:i:s",time());
	$trackingflds = "`LastUpdateBy`, `LastUpdateDate`, `LastUpdateTime`, `Data`";
	$trackingvals = "'SYSTEM', '$date_today', '$curr_time', 'A'";
	while(! feof($coll_course)) {
	  	$obj = fgets($coll_course);
  		if ($obj != "") {
  			$obj_arr = explode(",", $obj);
		  	$code = $obj_arr[1];
		  	$course_name = $obj_arr[0];
			if ($course_name != "") {
				$flds = "`Code`,`Name`,`Level`,`Remarks`,";
				$values = "'$code', '$course_name', 4, 'Uploaded',";
				$result = fileManager($course_name,"course",$flds,$values);		
				if (!is_numeric($result)) {
					echo "<li>This course is not inserted: ".$result;
				}
			}
	  	}
	  	
	}



	


	$grad_course = fopen(textFile."course.csv","r");
	$date_today    = date("Y-m-d",time());
	$curr_time     = date("H:i:s",time());
	$trackingflds = "`LastUpdateBy`, `LastUpdateDate`, `LastUpdateTime`, `Data`";
	$trackingvals = "'SYSTEM', '$date_today', '$curr_time', 'A'";
	while(! feof($grad_course)) {
	  	$obj = fgets($grad_course);
	  	if ($obj != "") {
		  	$code = "";
		  	$course_name = $obj;
			if ($course_name != "") {
				$flds = "`Code`,`Name`,`Level`,`Remarks`,";
				$values = "'$code', '$course_name', 5, 'Uploaded',";
				$result = fileManager($course_name,"course",$flds,$values);		
				if (!is_numeric($result)) {
					echo "<li>This course is not inserted: ".$result;
				}
			}	
	  	}
	}
	
?>