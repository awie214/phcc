<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <style type="text/css">
         th {
            vertical-align: top;
            text-align: center;
            font-size: 8pt;
            padding: 5px;
            background: #d9d9d9;
         }
         .td-input {
            padding: 2px;
         }
      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ldms"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar("RECORD UPDATE"); ?>
            <div class="container-fluid margin-top">  
               <div class="mypanel">
                  <div class="row">
                     <div class="col-sm-3">
                        <?php
                           employeeSelector();
                        ?>
                     </div>
                     <div class="col-xs-9" style="padding: 5px;">
                        <div class="row">
                           <div class="col-xs-12">
                              <button type="button" id="ldms_edit" name="ldms_edit" class="btn-cls4-sea">
                                 <i class="fa fa-edit"></i>&nbsp;EDIT
                              </button>
                              <button type="button" id="ldms_print" name="ldms_print" class="btn-cls4-lemon">
                                 <i class="fa fa-print"></i>&nbsp;PRINT
                              </button>
                           </div>
                        </div>
                        <div class="row margin-top">
                           <div class="col-xs-12">
                              <div class="panel-top margin-top">
                                 RECORD UPDATE
                              </div>
                              <div class="panel-mid" style="padding: 10px;">
                                 <div class="row">
                                    <div class="col-xs-8">
                                       <label>Name of Official/Employee:</label>
                                       <input class="form-input" type="text" name="employee_name" id="employee_name" readonly>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-8">
                                       <label>OFFICE/DIVISION:</label>
                                       <input class="form-input" type="text" name="office_division" id="office_division" readonly>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-4">
                                       <label>DATE:</label>
                                       <input class="form-input" type="text" name="date_update" id="date_update">
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <?php bar(); ?>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <div class="panel-top">
                                          A. EDUCATION (starting with the most recent)
                                       </div>
                                       <div class="panel-mid" style="padding: 10px;">
                                          <?php
                                             for ($a=1; $a <= 3; $a++) { 
                                          ?>
                                          <div class="row margin-top">
                                             <div class="col-xs-12" style="padding: 5px; background: #d9d9d9; border: 1px solid black;">
                                                <div class="row">
                                                   <div class="col-xs-4">
                                                      <label>Course/Title Degree</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                   <div class="col-xs-4">
                                                      <label>Date Finished/Graduated</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                   <div class="col-xs-4">
                                                      <label>School Institution</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <?php bar(); ?>
                                             </div>
                                          </div>
                                          <?php
                                             }
                                          ?>
                                       </div>
                                       <div class="panel-bottom"></div>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <div class="panel-top">
                                          B. ELIGIBILITY (starting with the most recent)
                                       </div>
                                       <div class="panel-mid" style="padding: 10px;">
                                          <?php
                                             for ($b=1; $b <= 3; $b++) { 
                                          ?>
                                          <div class="row margin-top">
                                             <div class="col-xs-12" style="padding: 5px; background: #d9d9d9; border: 1px solid black;">
                                                <div class="row">
                                                   <div class="col-xs-4">
                                                      <label>Type of Eligibility Obtained</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                   <div class="col-xs-4">
                                                      <label>Date Obtained</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                   <div class="col-xs-4">
                                                      <label>Issuing Office</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <?php bar(); ?>
                                             </div>
                                          </div>
                                          <?php
                                             }
                                          ?>
                                       </div>
                                       <div class="panel-bottom"></div>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <div class="panel-top">
                                          C. WORK EXPERIENCE (starting with the most recent)
                                       </div>
                                       <div class="panel-mid" style="padding: 10px;">
                                          <?php
                                             for ($c=1; $c <= 3; $c++) { 
                                          ?>
                                          <div class="row margin-top">
                                             <div class="col-xs-12" style="padding: 5px; background: #d9d9d9; border: 1px solid black;">
                                                <div class="row">
                                                   <div class="col-xs-4">
                                                      <label>Position</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                   <div class="col-xs-4">
                                                      <label>Agency/Company</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                   <div class="col-xs-4">
                                                      <label>Department</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-4">
                                                      <label>Division</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                   <div class="col-xs-4">
                                                      <label>Date From</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                   <div class="col-xs-4">
                                                      <label>Date To</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <?php bar(); ?>
                                             </div>
                                          </div>
                                          <?php
                                             }
                                          ?>
                                       </div>
                                       <div class="panel-bottom"></div>
                                    </div>
                                 </div>
                                 <div class="row margin-top">
                                    <div class="col-xs-12">
                                       <div class="panel-top">
                                          D. TRAINING (starting with the most recent)
                                       </div>
                                       <div class="panel-mid" style="padding: 10px;">
                                          <?php
                                             for ($d=1; $d <= 3; $d++) { 
                                          ?>
                                          <div class="row margin-top">
                                             <div class="col-xs-12" style="padding: 5px; background: #d9d9d9; border: 1px solid black;">
                                                <div class="row">
                                                   <div class="col-xs-4">
                                                      <label>Program Training Course</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                   <div class="col-xs-2">
                                                      <label>Date Attended</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                   <div class="col-xs-2">
                                                      <label>No. Of Hours</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                   <div class="col-xs-4">
                                                      <label>Sponsored By</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                </div>
                                                <div class="row margin-top">
                                                   <div class="col-xs-12">
                                                      <label>Competencies Acquired</label>
                                                      <input type="text" name="" id="" class="form-input">
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="row margin-top">
                                             <div class="col-xs-12">
                                                <?php bar(); ?>
                                             </div>
                                          </div>
                                          <?php
                                             }
                                          ?>
                                       </div>
                                       <div class="panel-bottom"></div>
                                    </div>
                                 </div>
                              </div>
                              <div class="panel-bottom">
                                 <input type="hidden" name="emprefid" id="emprefid">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("paramTitle",getvalue("paramTitle"),"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      $(document).ready(function () {
         $("#ldms_print").click(function () {
            var refid = $("#emprefid").val();
            if (refid == "") {
               $.notify("No Employee Selected");
               return false;
            }
            $("#rptContent").attr("src","blank.htm");
            var rptFile = "rpt_ldms_record_update";
            var url = "ReportCaller.e2e.php?file=" + rptFile;
            url += "&refid=" + refid;
            url += "&" + $("[name='hgParam']").val();
            $("#prnModal").modal();
            $("#rptContent").attr("src",url);
         });
      });
      function selectMe(emprefid) {
         $("#emprefid").val(emprefid);
      }
   </script>
</html>



