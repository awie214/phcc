<?php
   session_start();
   clearstatcache();
   require_once "constant.e2e.php";
   require_once "colors.e2e.php";
   require_once "conn.e2e.php";
   require_once pathClass.'0620functions.e2e.php';
   require_once pathClass.'SysTemplate.e2e.php';
   require_once pathClass.'SysFunctions.e2e.php';
   require_once pathClass.'Forms.e2e.php';
   require_once pathClass.'Parameters.e2e.php';
   
   $settingsContent = file_get_contents(json."Settings_".getvalue("hCompanyID").".json");
   $settings = json_decode($settingsContent, true);
   $_SESSION["settings"] = $settings;
   $fContent = file_get_contents(json."file.json");
   $files = json_decode($fContent, true);
   

   /*if (!isset($_SERVER['PHP_AUTH_USER'])) {
      header('WWW-Authenticate: Basic realm="My Realm"');
      header('HTTP/1.0 401 Unauthorized');
      echo 'Text to send if user hits Cancel button';
      exit;
   }*/
   /*
   $pageSess = getvalue("hUser") + "_" + date("Ymdhi");
   if (getvalue("auth") !== "init") {
      if ($pageSess !== getvalue("auth")) {
         $c = $pageSess." --- ".getvalue("auth");
         header ("Location:idx.e2e.php?sessExpired=yes&msg=".$c);
         exit;
      }
   }*/
   if (!isset($_SESSION['pageSession'.getvalue("hUser")])) {
     header ("Location:idx.e2e.php?");
     exit;
   } else {
      if ($_SESSION['pageSession'.getvalue("hUser")] == "") {
         header ("Location:idx.e2e.php?sessExpired=yes&msg=Blank Page Session");
         exit;
      } 
   }
   if(isset($_SESSION['user']) && isset($_SESSION['isLoggedIn'])) {
   } else {
      header ("Location:idx.e2e.php?sessExpired=yes&msg=Directed Page");
      exit;
   }
   if (!isset($_SESSION["SelectedEMP"])) {
      $_SESSION["SelectedEMP"] = "";
   }
   $form          = new SysForm();
   $sys           = new SysFunctions();
   $param         = new sysParam();
   $templ         = new Templates();
   $sysContent   = file_get_contents(json.$_SESSION["jsonFile"]["side"]);
   $mods         = json_decode($sysContent, true);
   $session      = "";
   $LastName     = "";
   $FirstName    = "";
   $MiddleName   = "";
   $user         = "";
   $user_refid   = "";
   $program_used = "";
   $errmsg       = "";
   $modTitle     = "";
   $userlvl      = 0;

   include "params.e2e.php";
   $menuID        = getvalue("id");
   $ModId         = getvalue("hModId");
   $gridTableHdr  = getvalue("hGridTblHdr");
   $gridTableFld  = getvalue("hGridTblFld");
   $table         = getvalue("hTable");
   $mode          = getvalue("hmode");
   $SubMenuID     = getvalue("mid");
   $ScreenName    = getvalue("n");
    
   if (isset($mods["modules"][$menuID]["Title"])) {
      $modTitle      = $mods["modules"][$menuID]["Title"];
   }  
   $rs = mysqli_query($conn,"SELECT * FROM `company`");
   $rsCompany = $rs;
   $today = f_encode(date("Ymd",time()));
   if ($rs) {
      $row = mysqli_fetch_assoc($rs);
      $BigLogo = $row["BigLogo"];
      $SmallLogo = $row["SmallLogo"];
      $Logo = $row["Logo"];
      $color1 = $row["Color1"];
      $color2 = $row["Color2"];
      $cid = $row["RefId"];   

      $dirTarget = path."css";
      $myfile = fopen(path."css/arc/az.css", 'r') or die("Unable to open file!");
      $mynewfile = path."css/~~az".$today.".css";

      $newFile = fopen($mynewfile, 'w');
      while(!feof($myfile)) {
         $str = fgets($myfile);
         $str = str_replace("var(--armyBlue1)","#".$color1,$str);
         $str = str_replace("var(--armyBlue2)","#".$color2,$str);
         fwrite($newFile,$str);
      }
      fclose($newFile);
      fclose($myfile);
   } else {
      balloonMsg("No Company Profile !!!","error");
      return false;
   }

   //var_dump($_POST);

   if ($UserCode == "") {
      $UserCode = getvalue("hUserGroup");
   }

   if ($UserCode == "COMPEMP") {
      $rowEmployees = FindFirst("employees","WHERE RefId=".getvalue("hEmpRefId"),"*");
      $session = $rowEmployees["Session"];
      $EmployeesPic = $rowEmployees["PicFilename"];
      if ($session == "") {
         $session = FindFirst("sysuser","WHERE EmployeesRefId=".getvalue("hEmpRefId"),"Session");
      }
   } else {
      if (getvalue("hUserRefId") != "") {
         $session = FindFirst("sysuser","WHERE RefId=".getvalue("hUserRefId"),"Session");
      } else {
         if (getvalue("hEmpRefId") != "") {
            $session = FindFirst("sysuser","WHERE EmployeesRefId=".getvalue("hEmpRefId"),"Session");
         }
      }

      if ($session == "") {
         if (getvalue("hEmpRefId") != "") {
            $rowEmployees = FindFirst("employees","WHERE RefId=".getvalue("hEmpRefId"),"Session");
            $session = $rowEmployees["Session"];
            $EmployeesPic = $rowEmployees["PicFilename"];
         }
      }
   }

   if ($session == "" && (getvalue("hEmpRefId") != "" || getvalue("hUserRefId") != "")) {
      $sessExpired = fopen($sys->publicPath("syslog/sessExpired_".date("Ymd",time()).".log"), "a+");
      fwrite($sessExpired, "Session: ".$session." ----- hEmpRefId: ".getvalue("hEmpRefId")."----- hUserRefId: ".getvalue("hUserRefId"));
      header ("Location:idx.e2e.php?sessExpired=yes&msg=No Session");
   } else {
      if ($session == "" || getvalue("hEmpRefId") != "" || getvalue("hUserRefId") != "") {
         $sessExpired = fopen($sys->publicPath("syslog/sessExpired_".date("Ymd",time()).".log"), "a+");
         if ($session == "") {
            fwrite($sessExpired, "Blank Session");
            header ("Location:idx.e2e.php?sessExpired=yes&msg=Blank Session");
            return false;
         }
         if (getvalue("hEmpRefId") == "") {
            fwrite($sessExpired, "Blank Emp RefId");
            header ("Location:idx.e2e.php?sessExpired=yes&msg=Blank hEmpRefId");
            return false;
         }
         if (getvalue("hUserRefId") == "") {
            fwrite($sessExpired, "Blank User RefId");
            header ("Location:idx.e2e.php?sessExpired=yes&msg=Blank hUserRefId");
            return false;
         }
      }
   }

   if (isset($rowEmployees)) {
      $EmployeesFullName = $rowEmployees["LastName"].", ".$rowEmployees["FirstName"];
   } else {
      $EmployeesPic = "nopicx.png";
      $EmployeesFullName = "";
   }

   $rs = mysqli_query($conn,"SELECT * FROM `company`");
   $SmallLogo   = "";
   $CompanyCode = "";
   if ($rs) {
      $company = mysqli_fetch_assoc($rs);
      $SmallLogo   = $company["SmallLogo"];
      $CompanyCode = $company["Code"];
   }
   $_SESSION["sysData"] = getvalue("hSysData");
   $isUser = false;
   if ($UserCode == "COMPEMP") $isUser = true;
   $gridtable     = "gridTable";
   $Action        = [true,true,true,false];
   $fileAction    = "GlobalCaller.e2e.php";

   $params = "";
   $params = "&hSess=".$userSESSION;
   $params = "&hUser=".$user;
   $params = "&id=".$menuID;
   $params = "&hTable=".$table;
   $file   = "";

   if (getvalue("file") != "") {
      $progFile = getvalue("file").".e2e.php";
      $file   = getvalue("file");
   } else {
      $progFile = getvalue("hProg").".e2e.php";
      $file     = getvalue("hProg");
   }
   //var_dump($progFile);
   if (!$mode) {
      if ($ModId == 1001) {
         $mode = "VIEW";
      } else {
         $mode = "ADD";
      }
   }

   $refid = getvalue("hRefId");
   $EmpRefId = getvalue("txtRefId");
   if ($gridTableHdr != "") {
      $gridTableHdr_arr = explode("|",$gridTableHdr);
      $gridTableFld_arr = explode("|",$gridTableFld);
   }
   /*if (getvalue("file") == "scrnIssues") {
      header ("Location:scrnIssues.e2e.php");
      return;
   }*/
   $paramTitle = "";
   switch ($file) {
      case "amsTrnDTR":
         $paramTitle       = "Daily Time Record";
      break;
      case "amsTrnWorkSchedule":
         $paramTitle       = "Employees Work Schedules";
      break;
      case "amsTrnOfficeSuspension":
         $paramTitle       = "Office Suspension";
         $ScreenName       = $paramTitle;
         $table            = "officesuspension";
         $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
         $gridTableHdr_arr = ["Employees Name","Name","Start Date","End Date","Start Time","Remarks"];
         $gridTableFld_arr = ["EmployeesRefId","Name","StartDate","EndDate","StartTime","Remarks"];
      break;
      case "amsTrnEmpCreditBal":
         $paramTitle       = "Credit Balance";
         $ScreenName       = $paramTitle;
         $table            = "employeescreditbalance";
         if (getvalue("txtRefId")) {
            $sql = "SELECT * FROM `$table` WHERE EmployeesRefId = ".getvalue("txtRefId")." ORDER BY RefId Desc";
         } else {
            $sql = "SELECT * FROM `$table` WHERE EmployeesRefId = 0 ORDER BY RefId Desc";
         }
         $gridTableHdr_arr = ["Credit Name","Effectivity Year","Beg. Balance","Expiry Date","Bal. As Of Date"];
         $gridTableFld_arr = ["NameCredits","EffectivityYear","BeginningBalance","ExpiryDate","BegBalAsOfDate"];
         $Action           = [true,true,true,false];
      break;
      case "amsReqChangeShift":
         $paramTitle       = "Change Shift";
         $ScreenName       = $paramTitle;
         $table            = "changeshift_request";
         $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
         $gridTableHdr_arr = ["Employees Name","Start Date","End Date","Status"];
         $gridTableFld_arr = ["EmployeesRefId","StartDate","EndDate","Status"];
      break;
      case "amsReqOvertime":
         $paramTitle       = "Overtime";
         $ScreenName       = $paramTitle;
         $table            = "overtime_request";
         $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc LIMIT 10";
         $gridTableHdr_arr = ["Employees Name","Start Date","End Date","Start Time","End Time","Status"];
         $gridTableFld_arr = ["EmployeesRefId","StartDate","EndDate","FromTime","ToTime","Status"];
         $Action           = [false,true,true,true];
      break;
      case "amsReqForceLeave":
         $paramTitle       = "Leave Cancellation";
         $ScreenName       = $paramTitle;
         $table            = "employeesleave";
         $sql              = "SELECT * FROM `$table` WHERE Status = 'Approved' ORDER BY RefId Desc";
         $gridTableHdr_arr = ["Emp. Name","Leaves","Date Filed","App. Date From","App. Date To","Status"];
         $gridTableFld_arr = ["EmployeesRefId","LeavesRefId","FiledDate","ApplicationDateFrom","ApplicationDateTo","Status"];
         $Action           = [true,false,false,false];
      break;
      case "amsAvailLeave":
         $paramTitle       = "Leave";
         $ScreenName       = $paramTitle;
         $table            = "employeesleave";
         $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc LIMIT 10";
         $gridTableHdr_arr = ["Emp. Name","Leaves","Date Filed","App. Date From","App. Date To","Status"];
         $gridTableFld_arr = ["EmployeesRefId","LeavesRefId","FiledDate","ApplicationDateFrom","ApplicationDateTo","Status"];
         $Action           = [false,true,true,true];
      break;
      case "amsAvailCTO":
         $paramTitle       = "CTO";
         $ScreenName       = $paramTitle;
         $table            = "employeescto";
         $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
         $gridTableHdr_arr = ["Emp. Name","Date Filed","App. Date From","App. Date To","Hours","Status"];
         $gridTableFld_arr = ["EmployeesRefId","FiledDate","ApplicationDateFrom","ApplicationDateTo","Hours","Status"];
         $Action           = [false,true,true,true];
      break;
      case "amsAvailAuthority":
         $paramTitle       = "Office Authority";
         $ScreenName       = $paramTitle;
         $table            = "employeesauthority";
         $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc LIMIT 10";
         $gridTableHdr_arr = ["Emp. Name","Authority","Date From","Date To","Time From","Time To","Status"];
         $gridTableFld_arr = ["EmployeesRefId","AbsencesRefId","ApplicationDateFrom","ApplicationDateTo","FromTime","ToTime","Status"];
         $Action           = [false,true,true,true];
      break;
      case "amsAvailLeaveMonitization":
         $paramTitle       = "Leave Monetization";
         $ScreenName       = $paramTitle;
         $table            = "employeesleavemonetization";
         $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
         $gridTableHdr_arr = ["Emp. Name","Date Filed","Status","Type of Monetization"];
         $gridTableFld_arr = ["EmployeesRefId","FiledDate","Status","IsHalf"];
         $Action           = [true,true,true,false];
      break;

      case "amsRptAttendance":
         $paramTitle       = "Attendance Reports";
      break;
      case "amsRptLeave":
         $paramTitle       = "Leave Reports";
      break;
      case "amsRptAuthority":
         $paramTitle       = "Office Authority Reports";
      break;
      case "amsRptCTO":
         $paramTitle       = "CTO Reports";
      break;

      case "amsDashboard":
         $paramTitle = "Dashboard";
      break;
      case "amsPolicy":
         $paramTitle = "Policy";
         $gridTableHdr_arr = array("Name");
         $gridTableFld_arr = array("Name");
         $sql = "SELECT * FROM `$table` ORDER BY RefId Desc LIMIT 100";
      break;
      case "amsEmployee":
         $paramTitle = "Employee";
         $table = "employees";
         $gridTableHdr = "Name|Gender|Employee Status|Position|Work Schedule|Machine Id";
         $gridTableFld = "RefId|FirstName|PositionItemRefId";
         $gridTableHdr_arr = explode("|",$gridTableHdr);
         $gridTableFld_arr = explode("|",$gridTableFld);
         $sql = "SELECT * FROM `$table` WHERE RefId = 0 ORDER BY RefId Desc LIMIT 100";
      break;
      case "pmsFileManager":
         $paramTitle = "File SetUp";
         $_SESSION["sysData"] = "DataLibrary";
         $ScreenName = "";
         switch ($menuID) {
            case "mBenefits":
               $table            = "benefits";
               $ScreenName       = "Benefits";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelBenefits";
               $gridTableHdr_arr = ["Code","Name","Amount","Include Computation"];
               $gridTableFld_arr = ["Code","Name","Amount","IncludeComputation"];

            break;
            case "mAdjustments":
               $table            = "adjustments";
               $ScreenName       = "Adjustments";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelAdjustments";
               $gridTableHdr_arr = ["Code","Name","Amount"];
               $gridTableFld_arr = ["Code","Name","Amount"];
            break;
            case "mDeductions":
               $table            = "deductions";
               $ScreenName       = "Deductions";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelDeductions";
               $gridTableHdr_arr = ["Code","Name","Amount"];
               $gridTableFld_arr = ["Code","Name","Amount"];
            break;

            case "mLoans":
               $table            = "loans";
               $ScreenName       = "Loans";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelLoans";
               $gridTableHdr_arr = ["Code","Name","Remarks"];
               $gridTableFld_arr = ["Code","Name","Remarks"];
            break;
            case "mResponsibility":
               $table            = "responsibilitycenter";
               $ScreenName       = "Resposibility Center";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelResponsibility";
               $gridTableHdr_arr = ["Code","Name","Remarks"];
               $gridTableFld_arr = ["Code","Name","Remarks"];
            break;
            case "mBank":
               $table            = "bank";
               $ScreenName       = "Bank";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelBank";
               $gridTableHdr_arr = ["Code","Name","Remarks"];
               $gridTableFld_arr = ["Code","Name","Remarks"];
            break;
            case "mBankBranch":
               $table            = "bankbranch";
               $ScreenName       = "BankBranch";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelBankBranch";
               $gridTableHdr_arr = ["Code","Name","Address","Remarks"];
               $gridTableFld_arr = ["Code","Name","Address","Remarks"];
            break;
            case "mPHILPolicy":
               $table            = "philhealthpolicy";
               $ScreenName       = "PhilHealth Policies";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelPHILPolicy";
               $gridTableHdr_arr = ["Policy Name","Pay Period","Deduction Period","Type"];
               $gridTableFld_arr = ["Name","PayPeriod","DeductionPeriod","PolicyType"];
            break;
            case "mPHILTable":
               $table            = "philhealthtable";
               $ScreenName       = "PhilHealth Table";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelPHILTable";
               $gridTableHdr_arr = ["Monthly Salary Bracket","Monthly Salary Range From","Monthly Salary Range To","Salary Base","Monthly Contribution","Employee Share","Employer Share"];
               $gridTableFld_arr = ["SalaryBracket","SalaryRangeFrom","SalaryRangeTo","SalaryBase","MonthlyContribution","EmployeeShare","EmployerShare"];
            break;
            case "mPAGIBIGTable":
               $table            = "pagibigtable";
               $ScreenName       = "Pag-Ibig Table";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelPAGIBIGTable";
               $gridTableHdr_arr = ["Pagibig Below","Pagibig Above","Employee Share","Employer Share","Max Employee Share","Max Employer Share"];
               $gridTableFld_arr = ["Below","Above","EmployeeShare","EmployerShare","MaxEmployeeShare","MaxEmployerShare"];
            break;
            case "mPAGIBIGPolicy":
               $table            = "pagibigpolicy";
               $ScreenName       = "Pag-Ibig Policy";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelPAGIBIGPolicy";
               $gridTableHdr_arr = ["Policy Name","Pay Period","Deduction Period","Type"];
               $gridTableFld_arr = ["Name","PayPeriod","DeductionPeriod","PolicyType"];
            break;
            case "mGSIS":
               $table            = "gsispolicy";
               $ScreenName       = "GSIS Policies";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelGSIS";
               $gridTableHdr_arr = ["Policy Name","Pay Period","Deduction Period","Type"];
               $gridTableFld_arr = ["Name","PayPeriod","DeductionPeriod","PolicyType"];
            break;
            case "mTaxTable":
               $table            = "taxtable";
               $ScreenName       = "TAX Table";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelTaxTable";
               $gridTableHdr_arr = ["Status","Exemption","Remarks"];
               $gridTableFld_arr = ["Status","Exemption","Remarks"];
            break;

            case "mTaxStatus":
               $table            = "taxstatustable";
               $ScreenName       = "TAX Status";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelTaxStatus";
               $gridTableHdr_arr = ["Tax Status","Description","Exemption"];
               $gridTableFld_arr = ["TaxStatus","Description","Exemption"];
            break;


            case "mAnnualTaxTable":
               $table            = "annualtaxtable";
               $ScreenName       = "Annual Tax Table";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelAnnualTaxTable";
               $gridTableHdr_arr = ["Annual Tax Below","Annual Tax Above","Rate"];
               $gridTableFld_arr = ["Below","Above","Rate"];
            break;


            case "mTaxPolicy":
               $table            = "taxpolicy";
               $ScreenName       = "TAX Policy";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelTaxPolicy";
               $gridTableHdr_arr = ["Policy Name","Pay Period","Deduction Period","Type"];
               $gridTableFld_arr = ["Name","PayPeriod","DeductionPeriod","PolicyType"];
            break;
            case "mWAGERATE":
               $table            = "wagerate";
               $ScreenName       = "Wage Rate";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelWAGERATE";
               $gridTableHdr_arr = ["Wage Region","Wage Rate"];
               $gridTableFld_arr = ["WageRegion","WageRate"];
            break;

         }
      break;

      case "amsFileSetup":
         $paramTitle = "File SetUp";
         $_SESSION["sysData"] = "DataLibrary";
         $ScreenName = "";
         switch ($menuID) {
            case "mCutOff":
               $table            = "cutoff";
               $ScreenName       = "Cut-Off";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelCutOff";
               $gridTableHdr_arr = ["Code","Name","First CutOff","Second CutOff","Remarks"];
               $gridTableFld_arr = ["Code","Name","FirstCutOff","SecondCutOff","Remarks"];
               break;
            case "mLeaves":
               $table            = "leaves";
               $ScreenName       = "Leave";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelLeave";
               $gridTableHdr_arr = ["Code","Name","Remarks"];
               $gridTableFld_arr = ["Code","Name","Remarks"];
               break;
            break;
            case "mAbsences":
               $table            = "absences";
               $ScreenName       = "Absences";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelAbsences";
               $gridTableHdr_arr = ["Code","Name","Type","Remarks"];
               $gridTableFld_arr = ["Code","Name","Type","Remarks"];
            break;
            case "mHolidays":
               $table            = "holiday";
               $ScreenName       = "Holiday";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId";
               $childFile        = "modelHoliday";
               $gridTableHdr_arr = ["Name","Start Date","End Date","Legal","Apply Every Yr."];
               $gridTableFld_arr = ["Name","StartDate","EndDate","isLegal","isApplyEveryYr"];
            break;
            case "mWorkSched":
               $table            = "workschedule";
               $ScreenName       = "Work Schedules";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $childFile        = "modelWorkSched";
               $gridTableHdr_arr = ["Name","Schedule Type","Remarks"];
               $gridTableFld_arr = ["Name","ScheduleType","Remarks"];
            break;
            case "mOfficeSuspension":
               $paramTitle       = "Office Suspension";
               $ScreenName       = "Office Suspension";
               $table            = "officesuspension";
               $childFile        = "modelOfficeSuspension";
               $sql              = "SELECT * FROM `$table` ORDER BY RefId Desc";
               $gridTableHdr_arr = ["Name","Start Date","End Date","Start Time","Remarks"];
               $gridTableFld_arr = ["Name","StartDate","EndDate","StartTime","Remarks"];
            break;
            default:
               switch ($menuID) {
                  case "mTable1":
                     $ScreenName = "Table I";
                  break;
                  case "mTable2":
                     $ScreenName = "Table II";
                  break;
                  case "mTable3":
                     $ScreenName = "Table III";
                  break;
                  case "mTable4":
                     $ScreenName = "Table IV";
                  break;
               }
               $table            = "";
               $sql              = "";
               $childFile        = "modelTable";
               $gridTableHdr_arr = "";
               $gridTableFld_arr = "";
            break;
         }
      break;
      case "amsUtilities":
         $paramTitle = "Utilities";
      break;
      case "scrnAdministration":
         $paramTitle = "Administration";
      break;
      case "scrnUsers":
         switch ($menuID){
            case "mUserProfile":
               $paramTitle = "User Profile";
               $mode = "VIEW";
               $ModId = "1001";
            break;
            case "mCreateUser":
               $paramTitle = "Create New User";
               $mode = "ADD";
               $ModId = "1002";
            break;
         }
      break;
      case "scrnSysAccount":
         $paramTitle = "";
      break;
      case "scrnAccessEmp":
         $paramTitle = "";
      break;
      case "scrnUploadPDS":
         $paramTitle = "PDS UPLOAD";
      break;
      case "scrnSysConfig":
         $paramTitle = "SYSTEM CONFIG";
      break;
      case "scrnCompanyProfile":
         $paramTitle = "COMPANY PROFILE";
      break;
      case "scrnAuditTrails":
         $paramTitle = "AUDIT TRAILS";
      break;
      case "scrnLogOut":
         $paramTitle = "";
      break;
      case "scrnWelcome":
         $paramTitle = "";
      break;
      case "amsPrintDtr":
         $paramTitle = "PRINT DTR";
      break;
      case "pmsDashboard":
         $paramTitle = "DASHBOARD";
      break;
      case "pmsTrns":
         $paramTitle = "TRANSACTION";
      break;
   }
   if ($paramTitle == "") $paramTitle = getvalue("paramTitle");
   $_SESSION["module_table"]            = strtolower($table);
   $_SESSION["module_gridTable_ID"]     = $gridtable;
   $_SESSION["list_ShowAction"]         = $Action;
   if (isset($gridTableHdr_arr)) $_SESSION["module_gridTableHdr_arr"] = $gridTableHdr_arr;
   if (isset($gridTableFld_arr)) $_SESSION["module_gridTableFld_arr"] = $gridTableFld_arr;

   /*set_error_handler("logTheError");  */

   if ($CompanyId == "") {
      $errmsg = "Param Error No CompanyId<br>";
   }
   if ($BranchId == "") {
      $errmsg .= "Param Error No BranchId<br>";
   }
   if ($user == "") {
      $errmsg .= "Param Error No User<br>";
   }

   $p = fopen($_SESSION['path'].'syslog/'.$user.'_ModulesUsed_'.$today.'.log', 'a+');

   if ($errmsg != "") {
      echo "<h4 style='color:red'>";
      echo $errmsg;
      echo "</h4>";
   }
   fwrite($p,$datelog."->URL:".$_SERVER['REQUEST_URI']."\n");
   include_once $progFile;
   unset($conn);
   unset($form);
   unset($sys);
   unset($param);
   fclose($p);
?>


