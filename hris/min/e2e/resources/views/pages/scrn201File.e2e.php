<?php
   $tabTitle = ["Personal Information","Children Information","Family Background","Educational Background",
       "Eligibility","Work Experience","Voluntary Work",
       "Learning And Development","Other Information","PDS Questions",
       "Reference"];
   $tabFile = ["incPersonalInfo","incChildrenInfo","incFamilyBG","incEducBG",
      "incEligibility","incWorkExperience","incVoluntaryWork",
      "incTrainingProgram","incOtherInfo","incPDSQ",
      "incReference"];
   $sysScreen = getvalue("sysScreen");
   if ($sysScreen == "RMS") {
      $sys = new SysFunctions();
      $table = 'applicant';
      $tableTitle = "APPLICANT LIST";
   } else {
      $table = 'employees';
      $tableTitle = "EMPLOYEES LIST";
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_201File"); ?>"></script>
      <script type="text/javascript">
         $(document).ready(function () {
         });
      </script>
      <style type="text/css">
         #overlay {
            position: fixed; /* Sit on top of the page content */
            display: none; /* Hidden by default */
            width: 100%; /* Full width (cover the whole page) */
            height: 100%; /* Full height (cover the whole page) */
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5); /* Black background with opacity */
            z-index: 2; /* Specify a stack order in case you're using a different order for other elements */
            cursor: pointer; /* Add a pointer on hover */
         }
         .btn-cls4-sea, .btn-cls2-sea {
            color: white !important;
            text-decoration: none;
         }


      </style>
   </head>
   <body>
      <form name="xForm" id="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php
            if ($sysScreen == "RMS") {
               $sys->SysHdr($sys,"rms");
            } else {
               $sys->SysHdr($sys,"pis");
            }
         ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar($modTitle); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div id="overlay">
                     <div class="col-xs-4"></div>
                     <div class="col-xs-4" style="margin-top: 10%;">
                        <?php
                           //echo '<img src="'.path.'images/ajax-loader.gif">';
                        ?>   
                     </div>
                  </div> 
                  <div class="col-xs-12" id="div_CONTENT">

                     <?php
                        if ($UserCode!="COMPEMP") {
                     ?>
                           <div class="row" id="201List">
                              <div class="col-xs-12">
                                 <div class="row">
                                    <div class="col-xs-6">
                                       <button type="button"
                                            class="btn-cls4-sea trnbtn"
                                            id="btnCREATE" name="btnCREATE">
                                          <i class="fa fa-file" aria-hidden="true"></i>&nbsp;
                                          CREATE
                                       </button>
                                    </div>
                                 </div>
                                 <?php 
                                    spacer(5); 

                                    $post = $_POST;
                                    if (isset($post["btnNew201"])) {
                                       echo $post["btnNew201"];
      
                                    }
                                 ?>
                                 <div class="mypanel">
                                    <div class="panel-top" id="setList">
                                       <?php //echo $tableTitle; ?>
                                    </div>
                                    <div class="panel-mid">
                                       <div class="row">
                                          <div class="col-xs-12">

                                             <div id="spGridTablec">
                                                <?php
                                                   if (getvalue("hCompanyID") == "1000") {
                                                      $action = [true,false,true,false];
                                                   } else {
                                                      $action = [true,false,false,false];
                                                   }
                                                      $sql = "SELECT * FROM `".strtolower($table)."` ORDER BY RefId Desc";
                                                      $gridTableHdr_arr = ["Employees ID#","Last Name", "First Name"];
                                                      $gridTableFld_arr = ["AgencyId","LastName", "FirstName"];
                                                      doGridTable($table,
                                                                  $gridTableHdr_arr,
                                                                  $gridTableFld_arr,
                                                                  $sql,
                                                                  $action,
                                                                  $_SESSION["module_gridTable_ID"]);
                                                ?>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel-bottom"></div>
                                 </div>
                              </div>
                           </div>
                        <?php
                        }
                           include 'inc201File.e2e.php';
                        ?>
                  </div>
               </div>
            </div>
            <?php
               footer();
               doHidden("sysScreen",$sysScreen,"");
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
</html>



