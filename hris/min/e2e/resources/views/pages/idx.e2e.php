<?php
   session_start();
   unset($_SESSION['cid']);
   include 'conn.e2e.php';
   include 'constant.e2e.php';
   $_SESSION["path"] = $path;
   $_SESSION["Classes"] = $pathClass;
   require_once pathClass."0620functions.e2e.php";
   require_once pathClass."SysFunctions.e2e.php";
   require_once pathClass."Forms.e2e.php";
   $filesContent = file_get_contents(json.'file.json');
   $files        = json_decode($filesContent, true);
   $_SESSION["jsonFile"] = $files;
   $sys = new SysFunctions();
   $form = new SysForm();

   $rs = mysqli_query($conn,"SELECT * FROM `company`");
   $today = f_encode(date("Ymd",time()));
   $mynewfile = path."css/~~az".$today.".css";
   foreach(glob(pathPublic.'css/~~az*.css') as $file) {
      if ($mynewfile != $file) {
         unlink($file);
      }
   }
   copy(pathPublic.'css/arc/tip.css',pathPublic.'css/~~az1'.$today.".css");
   if ($rs) {
      $row       = mysqli_fetch_assoc($rs);
      $BigLogo   = $row["BigLogo"];
      $SmallLogo = $row["SmallLogo"];
      $Logo      = $row["Logo"];
      $cid       = $row["RefId"];
      $color1    = $row["Color1"];
      $color2    = $row["Color2"];
      $settingsContent = file_get_contents(json."Settings_".$row["RefId"].".json");
      $settings = json_decode($settingsContent, true);
      $_SESSION["settings"] = $settings;
      $dirTarget = path."css";
      $myfile = fopen(path."css/arc/az.css", 'r') or die("Unable to open file!");
      $newFile = fopen($mynewfile, 'w');
      while(!feof($myfile)) {
         $str = fgets($myfile);
         $str = str_replace("var(--armyBlue1)","#".$color1,$str);
         $str = str_replace("var(--armyBlue2)","#".$color2,$str);
         fwrite($newFile,$str);
      }
      fclose($newFile);
      fclose($myfile);

   } else {
      balloonMsg("No Company Profile !!!","error");
      return false;
   }

   foreach(glob(pathPublic.'js/*.e2e.*.js') as $file) {
      unlink($file);
   }
   $lgMin = false;
   $header = 1;
   $sys->destroy_css("CSS_PIS");
   $css = ["armyBlue1","armyBlue2","bgPIS"];

   $_SESSION["0620utilities"]    = jsPath("js1_0620utilities");
   $_SESSION["0620functions"]    = jsPath("js2_0620functions");
   $_SESSION["0620SystemRoute"]  = jsPath("js3_0620SystemRoute");
   $_SESSION["0620sys"]          = jsPath("js4_0620sys");
   $_SESSION["load"]             = jsPath("js0_load");
   $_SESSION["toolTip"]          = jsPath("js5_toolTip");
   $_SESSION["jquery_utilities"] = jsPath("js6_jquery_utilities");
   $_SESSION["login"]            = jsPath("js7_login");
   $_SESSION["tip"]              = path.'css/~~az1'.$today.".css";
   $_SESSION["cssFilePath"]      = $mynewfile;
   $_SESSION["cidx"]             = $cid;
   $_SESSION["PayGroup"]         = ["Monthly","Semi-Monthly"];

   function doSignIn() {
      echo  
      '<div class="signInDiv">
         <div class="form-group">
            <input class="form-control mandatory--" type="text" id="inputUser" name="txtUser" placeholder="Username">
         </div>
         <div class="form-group">
            <input class="form-control mandatory--" type="password" id="inputPW" name="token" placeholder="Password">
         </div>
         <div class="txt-left">
            <button type="button" name="btnLogin" class="btn-cls4-sea" style="width:100%;border: 1px solid #999999;">
               Sign Me In
            </button>
         </div>
         <br>

         <p align="center" style="font-size:18px; color:#00477e;"> Supported Browser for <b>HRIS</b> <img src="'.img("Chrome.png").'" alt="Google Chrome" title="Google Chrome" style="width:35px;></p>
      </div>';
      /*<div class="txt-center margin-top">
         <hr>
         <span style="font-family:tahoma;font-size:10px;font-weight:600;">WELCOME TO HRIS</span>
      </div>*/
   }
?>
<html>
   <head>
      <title>HRIS</title>
      <link rel="icon" href="../../../public/images/2/pcclogo_circle.png" type="image/gif" sizes="16x16">
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="<?php echo $sys->publicPath("css/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css">
      <script type="text/javascript" src="<?php echo $path; ?>jquery/jquery.js"></script>
      <link rel="stylesheet" href="<?php echo $path; ?>bs/css/bootstrap.min.css" crossorigin="anonymous">
      <link rel="stylesheet" href="<?php echo $path; ?>bs/css/bootstrap-theme.min.css" crossorigin="anonymous">
      <script src="<?php echo $path; ?>bs/js/bootstrap.min.js" crossorigin="anonymous"></script>
      <link href="<?php echo $sys->publicPath("datatables/jquery.dataTables.min.css"); ?>" rel="stylesheet">
      <script type="text/javascript" src="<?php echo $sys->publicPath("datatables/jquery.dataTables.min.js"); ?>"></script>
      <script type="text/javascript" src="<?php echo $_SESSION["0620utilities"] ?>"></script>
      <script type="text/javascript" src="<?php echo $_SESSION["0620functions"] ?>"></script>
      <script type="text/javascript" src="<?php echo $_SESSION["0620sys"] ?>"></script>
      <script type="text/javascript" src="<?php echo $_SESSION["load"] ?>"></script>
      <script type="text/javascript" src="<?php echo $_SESSION["login"] ?>"></script>
      <script type="text/javascript" src="<?php echo $path; ?>js/jsSHAver2/src/sha.js"></script>
      <script type="text/javascript" src="<?php echo $path; ?>js/notify.js"></script>
      <script type="text/javascript" src="<?php echo jsCtrl("ctrl_idx"); ?>"></script>
      <link rel="stylesheet" href="<?php echo $_SESSION["cssFilePath"]; ?>">
      <link rel="stylesheet" href="<?php echo $_SESSION["tip"]; ?>">
      <link href="<?php echo $sys->publicPath("datepicker/css/datepicker.css"); ?>" rel="stylesheet">
      <script type="text/javascript" src="<?php echo $sys->publicPath("datepicker/js/bootstrap-datepicker.js"); ?>"></script>
      <style>
         .signInDiv {
            padding:20px;
            max-height:250px;
            max-width:320px;
            /*background-color:#d9d9d9;*/
            /*border: 1px solid #999999;*/
            color: black;
         }
         .form-control {
            border: 1px solid #999999;
            border-radius: 0;
         }
         .midBody {
            height: calc(100vh - 85px);
         }
         .sysNameHolder {
            height: 60px;
            text-align: center;
            align-items: center;
            margin:0;
         }
         .sysName {
            font-family:Arial;
            font-size:16pt;
            font-weight:500;
            display:inline-block;
            vertical-align:middle;
            line-height:60px;
            margin:0;
         }
         .outer {
             display: table;
             position: absolute;
             height: calc(100% - 130px);
             width: 100%;
             background:transparent;
         }
         .middle {
             display: table-cell;
             vertical-align: middle;
             background:transparent;
         }
         .inner {
             margin-left: auto;
             margin-right: auto;
             height:100%;
             background:#ffffff;
             background:url("<?php echo img($cid.'/'.$BigLogo); ?>") no-repeat center;
         }
         .tdBG {
            width:100%;
            height:100%;
            background-image: url("<?php echo img($cid.'/'.$Logo); ?>");
            background-repeat:no-repeat;
            background-size:100% 100%;
         }
         .center {
            display: block;
            position:absolute;
            top:0;
            bottom:0;
            left:0;
            right:0;
            margin:auto;
         }
         #imgBG {
            width:100%;
            height:100%;
         }
      </style>
      <script type="text/javascript">
         $(document).ready(function(){
            $("#aLogin").click(function () {
               $("#div_login").show();
               $("#div_logo").hide();
               $("#inputUser").focus();
            });
         });
         sessionStorage.clear();
      </script>
   </head>
   <body style="padding:0;" class="wmlogo">
      <form name="formlogin" method="post" action="login.e2e.php">
         <div style="padding:0;">
            <div class="sysNameHolder sysBG">
               <span class="sysName">
                  <?php
                     echo $settings["Title"];
                  ?>
               </span>
            </div>
            <div class="midBody wmlogo" style="padding:0;">
               <?php
                  if (getvalue("login_error") == "yes") {
                     balloonMsg(getvalue("msg"),"warn");
                  }
                  if (getvalue("cpwsuccess") == "yes") {
                     balloonMsg("User Password Succesfully Changed...","success");
                  }
                  if (getvalue("sessExpired") == "yes"){
                     balloonMsg("Session Has Been Expired... ".getvalue("msg"),"info");
                  }
                  if (getvalue("pageAuth") == "no"){
                     balloonMsg("Page Not Auth...","info");
                  }
                  if (getvalue("wrongLoginInfo") == "yes"){
                     balloonMsg("No Specific Login Info ... ","error");
                  }
                  if (getvalue("inactive") == "yes") {
                    balloonMsg("Your Login info is inactive ... ","error");
                  }

                  if ($settings["SignInMode"] == 2) {
               ?>
                     <div class="row" style="margin:0;padding:0;" id="div_logo">
                        <div class="outer">
                           <div class="middle">
                              <div class="inner">
                                 <br> 
                                 <span class="tip--" style="float:right;padding-right:20px;">
                                    <a href="javascript:void(0);" id="aLogin">
                                       SIGN IN
                                    </a>
                                    <span class="tipText">Click To Login in the HRIS SYSTEM</span> 
                                 </span>
                              </div>
                           </div>
                        </div>
                     </div>
               <?php 
                     $signin = "none";
                     $imgID = "imgBG"; 
                  } else {
                     $signin = "block";
                     $imgID = "";
                  } 
               ?>
               <div class="row" style="height:100%;margin:0;display:<?php echo $signin; ?>;padding:0;" id="div_login">
                  <div class="col-xs-12" style="height:100%;margin:0;padding:0;">
                     <div class="login scrns" id="login" style="position:relative;width:100%;height:100%;padding:0;vertical-align:middle;">
                        <table style="width:100%;height:calc(100vh - 60px);">
                           <?php 
                              switch ($settings["SignInLayout"]) {
                                 case 1:
                           ?>
                                    <tr>
                                       <td width="5%"></td>
                                       <td width="45%" valign="middle" align="center">
                                          <img src="<?php echo path."images/".$cid."/".$Logo; ?>" id="<?php echo $imgID; ?>" style="width:100%">
                                       </td>
                                       <td width="40%" align="center" valign="middle">
                                          <?php doSignIn(); ?>
                                       </td>
                                       <td width="5%"></td>
                                    <tr>
                           <?php         
                                 break;
                                 case 2:
                           ?>
                                    <tr>
                                       <td width="5%"></td>
                                       <td width="45%" valign="middle" align=center>
                                          <?php doSignIn(); ?> 
                                       </td>
                                       <td width="50%" align="center">
                                          <img src="<?php echo path."images/".$cid."/".$Logo; ?>" id="<?php echo $imgID; ?>" style="width:100%">
                                       </td>
                                    <tr>
                           <?php      
                                 break;
                                 case 3:
                           ?>
                                    <tr>
                                       <td width="100%" align="center" valign="center" class="tdBG">
                                          <?php doSignIn(); ?> 
                                       </td>
                                    <tr>
                           <?php      
                                 break;
                                 case 4:
                           ?>
                                       <td width="5%"> 
                                       <td width="*%" valign="middle" style="position:relative">
                                          <img src="<?php echo path."images/".$cid."/".$Logo; ?>" id="<?php echo $imgID; ?>" style="width:100%">
                                       </td>
                                       <td width="20%">
                                          <span style="position:absolute;top:0;right:0;">
                                             <?php doSignIn(); ?> 
                                          </span>
                                       </td>
                           <?php      
                                 break;
                                 case 5:
                           ?>
                                       <td width="20%">
                                          <span style="position:absolute;top:0;left:0;">
                                             <?php doSignIn(); ?> 
                                          </span>
                                       </td>
                                       <td width="*%" valign="middle" style="position:relative">
                                          <img src="<?php echo path."images/".$cid."/".$Logo; ?>" id="<?php echo $imgID; ?>" style="width:100%">
                                       </td>
                                       <td width="5%"> 
                                       
                           <?php      
                                 break;
                              }
                           ?>
                        </table>    
                     </div>
                     <div id="EntryScrn"></div>
                     <?php
                        if (getvalue("expired") == "yes") {
                           $msg = "";
                           $msg .= "<p>sess : ".getvalue("hSess")."</p>";
                           $msg .= "<p>user : ".getvalue("hUser")."</p>";
                           $msg .= "<p>mid : ".getvalue("id")."</p>";
                           $msg .= "<p>tbl : ".getvalue("hTable")."</p>";
                           msg("warn",$msg);
                        }
                     ?>
                  </div>
               </div>

               <?php
                  footer();
                  //include_once ("varHidden.e2e.php");
               ?>
            </div>
         <input type="hidden" name="hToken" value="">
         <input type="hidden" id="hmode"  name="hmode"  value="">
         <input type="hidden" id="hRefId" name="hRefId"  value="">
      </form>
   </body>
</html>