<?php
   require_once "conn.e2e.php";
   require_once "constant.e2e.php";
   require_once pathClass.'0620RptFunctions.e2e.php';

?>
<!DOCTYPE html>
<html>
   <head>
      <?php include_once $files["inc"]["pageHEAD"]; ?>
      
      <style type="text/css">

      </style>
   </head>
   <body onload = "indicateActiveModules();">
      <form name="xForm" method="post" action="<?php echo $fileAction; ?>">
         <?php $sys->SysHdr($sys,"ams"); ?>
         <div class="container-fluid" id="mainScreen">
            <?php doTitleBar ("REQUEST FOR ATTENDANCE REGISTRATION"); ?>
            <div class="container-fluid margin-top">
               <div class="row">
                  <div class="col-xs-12" id="div_CONTENT">
                     <div class="container-fluid rptBody">
                        <div>
                           <div class="row" id="divList">
                              <div class="col-xs-12">
                                 <div class="panel-top">
                                    List of Attendance Registration Request
                                 </div>
                                 <div class="panel-mid">
                                    <span id="spGridTable">
                                       <?php
                                          $table = "attendance_request";
                                          $gridTableHdr_arr = ["Employee Name","Reason","File Date","Date Applied For", "Status"];
                                          $gridTableFld_arr = ["EmployeesRefId","Type","FiledDate","AppliedDateFor", "Status"];
                                          $sql = "SELECT * FROM $table ORDER BY FiledDate";
                                          $Action = [false,true,true,true];
                                          doGridTable($table,
                                                      $gridTableHdr_arr,
                                                      $gridTableFld_arr,
                                                      $sql,
                                                      $Action,
                                                      "gridTable");
                                       ?>
                                    </span>
                                 </div>
                                 <div class="panel-bottom">
                                    <?php
                                       btnINRECLO([true,true,false]);
                                    ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div id="divView">
                           <div id="">
                              <div class="row margin-top" id="EntryScrn">
                                 <div class="col-xs-6">
                                    <div class="panel-top">
                                       ATTENDANCE REGISTRATION
                                    </div>
                                    <input type="hidden" 
                                           class="saveFields--"
                                           name="sint_EmployeesRefId" 
                                           id="sint_EmployeesRefId" 
                                           value="<?php echo $emprefid; ?>">
                                    <div class="panel-mid" style="padding: 10px;">
                                       <div class="row">
                                          <div class="col-xs-6">
                                             <label>DATE OF FILING:</label>
                                             <br>
                                             <input type="text"
                                                    name="date_FiledDate" 
                                                    id="date_FiledDate" 
                                                    class="form-input saveFields-- date--" 
                                                    value="<?php echo date("Y-m-d",time()); ?>" 
                                                    readonly>
                                          </div>
                                          <div class="col-xs-6">
                                             <label>APPLICATION DATE FOR</label>
                                             <br>
                                             <input type="text" 
                                                    class="form-input saveFields-- date-- mandatory" 
                                                    name="date_AppliedDateFor" 
                                                    id="date_AppliedDateFor">
                                          </div>
                                       </div>
                                       <div class="row margin-top">
                                          <div class="col-xs-12">
                                             <label>REASON</label>
                                             <br>
                                             <select class="form-input saveFields-- mandatory" name="sint_Type" id="sint_Type">
                                                <option value="">Select Reason</option>
                                                <option value="FORGOT TO FINGER SCAN">FORGOT TO FINGER SCAN</option>
                                                <option value="BIOMETRIC FINGERSCAN MALFUNCTION">BIOMETRIC FINGERSCAN MALFUNCTION</option>
                                             </select>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel-bottom">
                                       <?php
                                          btnSACABA([false,true,true]);
                                       ?>
                                    </div>
                                 </div>
                              </div>   
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="prnModal" role="dialog">
               <div class="modal-dialog" style="height:90%;width:80%">
                  <div class="mypanel" style="height:100%;">
                     <div class="panel-top bgSilver">
                        <a href="#" data-toggle="tooltip" data-placement="top" id="btnPRINTNOW">
                           <i class="fa fa-print" aria-hidden="true"></i>
                        </a>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                     </div>
                     <iframe id="rptContent" src="blank.e2e.php" class="iframes"></iframe>
                  </div>
               </div>
            </div>
            <?php
               footer();
               $table = "attendance_request";
               include "varHidden.e2e.php";
            ?>
         </div>
      </form>
   </body>
   <script type="text/javascript">
      function selectMe(refid) {
         printAttachment(refid)
      }
      function printAttachment(refid){
         $("#rptContent").attr("src","blank.htm");
         var rptFile = "rpt_Attendance_Request";
         var url = "ReportCaller.e2e.php?file=" + rptFile;
         url += "&refid=" + refid;
         url += "&" + $("[name='hgParam']").val();
         $("#prnModal").modal();
         $("#rptContent").attr("src",url);
      }
   </script>
</html>



