var newwindow = null;
$(document).ready(function () {
   if ($("#hUserGroup").val() == "COMPEMP") {
      openNav();
   }
   $("[class*='date--']").each(function () {
      $(this).dblclick(function () {
         $(this).val("").datepicker("update");
      });
   });
   $("[name='btnClearSplit']").click(function () {
      $(".split--").val("");
   }); 
   $("#btnPrintMF").click(function () {
      var split = {
         //"Educ": $("[name='splitEduc'] option:selected").val(),
         "Educ": "." + $("[name='splitEduc']").val(),
         "Eligibility": "." + $("[name='splitEligibility']").val(),
         "WorkExp": "." + $("[name='splitWorkExp']").val(),
         "VolWork": "." + $("[name='splitVolWork']").val(),
         "Learning": "." + $("[name='splitLearning']").val(),
         "OtherInfo": "." + $("[name='splitOtherInfo']").val()
      };
      var rptFile = "pop_MasterFile";
      var url = "ReportCaller.e2e.php?" + $("#hgParam").val() + "&file=" + rptFile + "&empRefId=" + $("[name='hEmpRefId']").val() + "&split=" + JSON.stringify(split);
      $("#pds_modal").modal();
      $("#rptContent").attr("src", url);
   });
   $(".overlay").click(function () {
      $(this).hide();
   });
   /*TRN BTN*/
   $("[name~='bADD']").click(function () {
      $(".form-input").attr('readonly', false);
      $(".form-input").attr('disabled', false);
      $(".form-input").val('');
      $("[name~='hRefID']").val('');
      $("[name~='trefid']").val('');
      setModeTrn("ADD", "ADD");
      setBHV("disable", true, "bADD,bEDIT,bDELETE,bBROWSE");
      setBHV("disable", false, "bCANCEL,bSAVE,bEXIT");
   });
   $("[name~='bEDIT']").click(function () {
      if (getvalue("hRefID") == false) {
         alert("ooops No RefId Assigned !!!");
         return false;
      }
      $(".form-input").attr('readonly', false);
      $(".form-input").attr('disabled', false);
      setModeTrn("EDIT", "EDIT");
      setBHV("disable", true, "bADD,bEDIT,bDELETE,bBROWSE");
      setBHV("disable", false, "bCANCEL,bSAVE,bEXIT");
   });
   $("[name~='bDELETE']").click(function () {
      if (getvalue("hRefID") == false) {
         alert("ooops No RefId Assigned !!!");
         return false;
      }
      if (confirm("Delete This Record " + getvalue("hRefID") + "?")) {
         setModeTrn("DELETE", "DELETE");
         $("[name='xForm']").submit();
      } else {
         return false;
      }

   });
   $("[name~='bSAVE']").click(function () {
      if (getvalue("hMODE") == "EDIT" && getvalue("hRefID") == false) {
         alert("ooops No RefId Assigned !!!");
         return false;
      }
      $("[name~='hOBJnVALUE']").val(getObjnValue());
      setModeTrn("SAVE", getvalue("hMODE"));
      $("[name='xForm']").submit();
   });
   $("[name~='bCANCEL']").click(function () {
      setModeTrn("CANCEL", "CANCEL");
      $("[name='xForm']").submit();
   });

   $("#btn_id_UPDATE").click(function () {
      $("[name='hOBJnVALUE']").val(getObjnValue());
      $("[name='hBTN']").val("SAVE");
      $("[name='hMODE']").val("EDIT");
      $("[name='xForm']").submit();
   });
   $("#btn_id_INSERTNEW").click(function () {
      $("[name='hOBJnVALUE']").val(getObjnValue());
      $("[name='hBTN']").val("SAVE");
      $("[name='hMODE']").val("ADD");
      $("[name='xForm']").submit();
   });

   /* table list */
   $("#btnINSERT").click(function () {
      var empRefId = $("[name='sint_EmployeesRefId']").val();
      var agencyId = $('[name="sint_AgencyId"]').val();
      var Agency = $('[name="sint_EmployeesId"]').val();
      if ($("#EmpRefIdRequire").val() == "YES") {
         if (empRefId == "") {
            alert("Ooops!!! You need to select first employee\nThank you.");
            return;
         }
      }
      $('#EmpSelected').empty();
      setValueByName("hEmpSelected","");
      $("#hmode").val("ADD");
      $("#ScreenMode").html($("#hmode").val() + "&nbsp;");
      $("#hRefId").val(0);
      $("#divView, #spSAVECANCEL").show();
      $("#divList, #badgeRefId, #spBACK, #btnTransfer").hide();
      $("#EntryScrn .saveFields--").not(".form-disp").prop("disabled", false);
      $("#EntryScrn .saveFields--").not("input[type='hidden'], input[name='date_FiledDate'], .tmPicker--").val("");
      $("#EntryScrn .saveFields--").prop("checked", false);
      $("#EntryScrn .fkName--").val("");
      $("[name='sint_EmployeesRefId']").val(empRefId);
      $('[name="sint_AgencyId"]').val(agencyId);
      $('[name="sint_EmployeesId"]').val(Agency);
      $('[name="sint_EmployeesId"]').prop("disabled", true);
      $(".time--").prop("selectedIndex", 0);
      // hide object while in Add Mode
      $("#EntryScrn .HideinAdd--").hide();
      if ($(".modalFieldEntry--").length > 0) {
         $("#modalFieldEntry").modal();
      }
      $(".row > .nav-pills").hide();
   });

   $("#btnCLOSE").click(function () {
      $("#divList").slideUp(500);
   });

   $(".Employees--").each(function () {
      $(this).click(function () {
         $(".Employees--").removeClass("elActive");
         selectMe($(this).attr("refid"));
         $(this).addClass("elActive");
      });
   });


   $("#btnCANCEL").click(function () {
      $("#divView").hide();
      $("#divList").show();
   });

   $("#btnSAVE").click(function () {
      fnbtnSAVE();
   });

   $("#btnBACK").click(function () {
      $("#divView").hide();
      $("#divList").show();
   });

   $("#mADMINISTRATION").click(function () {
      $("#submenuADMINISTRATION").slideToggle(300);
   });
   /*=============================================*/

   $(".floatAlert").click(function () {
      $(this).hide();
   });

   $("#btnPRINTNOW").click(function () {
      document.getElementById('rptContent').contentWindow.print();
   });

   $("#tableHdr").click(function () {
      $("#tableDtl").slideToggle(300);
   });

   $("[id*='btnEmpLkUp']").each(function () {
      $(this).click(function () {
         var url = "EmpLookUp.e2e.php?";
         var objName = $(this).attr("name");
         url += "refid=" + $("[name='txtRefId']").val();
         url += "&lname=" + $("[name='txtLName']").val();
         url += "&fname=" + $("[name='txtFName']").val();
         url += "&mname=" + $("[name='txtMidName']").val();
         url += "&agency=" + $("[name='srchAgencyRefId']").val();
         url += "&position=" + $("[name='srchPositionRefId']").val();
         url += "&office=" + $("[name='srchOfficeRefId']").val();
         url += "&division=" + $("[name='srchDivisionRefId']").val();
         url += "&dept=" + $("[name='srchDepartmentRefId']").val();
         url += "&empstatus=" + $("[name='srchEmpStatusRefId']").val();
         url += "&hCompanyID=" + $("[name='hCompanyID']").val();
         url += "&hBranchID=" + $("[name='hBranchID']").val();
         $("#objToFillUp").val(objName);
         $("#EmpListLkUp").html("");
         $("#EmpListLkUp").load(url, function (responseTxt, statusTxt, xhr) {
            if (statusTxt == "error") {
               alert("Ooops Error: " + xhr.status + " : " + xhr.statusText + "[err 002]");
               return false;
            }
            if (statusTxt == "success") {
               $("[name='EmpSearchTaskModal']").val(1);
               $("#modalEmpLookUp").modal();
            }
         });
      });
   });
   $("#filter_button").click(function () {
      var month = $("#month_").val();
      var year  = $("#year_").val();
      var table = $("#hTable").val();
      refreshApplication(month,year,table);
   });

   $("[class*='EmpLkUp--']").each(function () {
      $(this).click(function () {
         var url = "EmpLookUp.e2e.php?";
         url += "hCompanyID=" + $("[name='hCompanyID']").val();
         url += "&hBranchID=" + $("[name='hBranchID']").val();
         $("#objEmpRefIdFill").val($(this).attr("name"));
         $("#objEmpFullNameFill").val($(this).attr("for"));
         $("#EmpListLkUp").html("");
         $("#EmpListLkUp").load(url, function (responseTxt, statusTxt, xhr) {
            if (statusTxt == "error") {
               alert("Ooops Error: " + xhr.status + " : " + xhr.statusText + "[err 003]");
               return false;
            }
            if (statusTxt == "success") {
               $("[name='EmpSearchTaskModal']").val(2);
               $("#modalEmpLookUp").modal();
            }
         });
      });
   });

   //rmsEmpApp
   var tSlideUp = 200;
   var tSlideDw = 300;
   $(".imgMenu, .wmain--").hover(function () {
      $(this).css("cursor", "pointer");
   });
   $("#wmainPIS").hover(function () {
      $("#wsysPIS").slideDown(tSlideDw + 100);
      $("#wsysPMS, #wsysAMS, #wsysSPMS, #wsysRMS, #wsysLDMS").slideUp(tSlideUp);
   });
   $("#wmainAMS").hover(function () {
      $("#wsysAMS").slideDown(tSlideDw + 100);
      $("#wsysPMS, #wsysPIS, #wsysSPMS, #wsysRMS, #wsysLDMS").slideUp(tSlideUp);
   });
   $("#wmainPMS").hover(function () {
      $("#wsysPMS").slideDown(tSlideDw + 100);
      $("#wsysPIS, #wsysAMS, #wsysSPMS, #wsysRMS, #wsysLDMS").slideUp(tSlideUp);
   });
   $("#wmainRMS").hover(function () {
      $("#wsysRMS").slideDown(tSlideDw + 100);
      $("#wsysPMS, #wsysAMS, #wsysSPMS, #wsysPIS, #wsysLDMS").slideUp(tSlideUp);
   });
   $("#wmainSPMS").hover(function () {
      $("#wsysSPMS").slideDown(tSlideDw + 100);
      $("#wsysPMS, #wsysPIS, #wsysAMS, #wsysRMS, #wsysLDMS").slideUp(tSlideUp);
   });
   $("#wmainLDMS").hover(function () {
      $("#wsysLDMS").slideDown(tSlideDw + 100);
      $("#wsysPIS, #wsysAMS, #wsysSPMS, #wsysRMS, #wsysPMS").slideUp(tSlideUp);
   });


   var memOf = $("#hmemof").val();

   /*$("#menuPIS").click(function(){
      $("#submenuPIS").slideToggle(tSlideDw);
      $('#submenuRMS, #submenuSPMS').slideUp(tSlideUp);
   });
   $("#menuRMS").click(function(){
      $("#submenuRMS").slideToggle(tSlideDw);
      $('#submenuPIS, #submenuSPMS').slideUp(tSlideUp);
   });
   $("#menuSPMS").click(function(){
      $("#submenuSPMS").slideToggle(tSlideDw);
      $('#submenuPIS, #submenuRMS').slideUp(tSlideUp);
   });*/

   if (memOf == "pis") {
      $("#submenuPIS").slideDown(tSlideDw);
      $('#submenuRMS, #submenuSPMS').slideUp(tSlideUp);
      $('#sysTitle').html("Personnel Information System");
   } else if (memOf == "rms") {
      $("#submenuRMS").slideDown(tSlideDw);
      $('#submenuPIS, #submenuSPMS').slideUp(tSlideUp);
      $('#sysTitle').html("Recruitment Management System");
   } else if (memOf == "spms") {
      $("#submenuSPMS").slideDown(tSlideDw);
      $('#submenuPIS, #submenuRMS').slideUp(tSlideUp);
      $('#sysTitle').html("Strategic Performance Management System");
   } else if (memOf == "ldms") {
      $('#sysTitle').html("Learning Development Management System");
   } else if (memOf == "amsTrns") {
      $("#submenuTrn").slideDown(tSlideDw);
      $('#submenuReq, #submenuAvail, #submenuAppv, #submenuRpt').slideUp(tSlideUp);
   } else if (memOf == "amsReqs") {
      $("#submenuReq").slideDown(tSlideDw);
      $('#submenuTrn, #submenuAvail, #submenuAppv, #submenuRpt').slideUp(tSlideUp);
   } else if (memOf == "amsAppv") {
      $("#submenuAppv").slideDown(tSlideDw);
      $('#submenuReq, #submenuAvail, #submenuTrn, #submenuRpt').slideUp(tSlideUp);
   } else if (memOf == "amsAvail") {
      $("#submenuAvail").slideDown(tSlideDw);
      $('#submenuReq, #submenuTrn, #submenuAppv, #submenuRpt').slideUp(tSlideUp);
   } else if (memOf == "amsRpts") {
      $("#submenuRpt").slideDown(tSlideDw);
      $('#submenuReq, #submenuAvail, #submenuAppv, #submenuTrn').slideUp(tSlideUp);
   } else {
      /*PIS*/
      $("#submenuPIS").slideDown(tSlideDw);
      $('#submenuRMS, #submenuSPMS').slideUp(tSlideUp);
      $('#sysTitle').html("Personnel Information System");
      /*AMS*/
      $('#submenuTrn, #submenuReq, #submenuAvail, #submenuAppv, #submenuRpt').slideUp(tSlideUp);
      /*PMS*/
   }

   $("#amsTrns").click(function () {
      $("#submenuTrn").slideToggle(tSlideDw);
      $('#submenuReq, #submenuAvail, #submenuAppv, #submenuRpt').slideUp(tSlideUp);
   });
   $("#amsReqs").click(function () {
      $("#submenuReq").slideToggle(tSlideDw);
      $('#submenuTrn, #submenuAvail, #submenuAppv, #submenuRpt').slideUp(tSlideUp);
   });
   $("#amsAppv").click(function () {
      $("#submenuAppv").slideToggle(tSlideDw);
      $('#submenuReq, #submenuAvail, #submenuTrn, #submenuRpt').slideUp(tSlideUp);
   });
   $("#amsAvail").click(function () {
      $("#submenuAvail").slideToggle(tSlideDw);
      $('#submenuReq, #submenuTrn, #submenuAppv, #submenuRpt').slideUp(tSlideUp);
   });
   $("#amsRpts").click(function () {
      $("#submenuRpt").slideToggle(tSlideDw);
      $('#submenuReq, #submenuAvail, #submenuAppv, #submenuTrn').slideUp(tSlideUp);
   });
   $(".srch--").each(function () {
      $(this).keyup(function () {
         UpdateEmpList();
      });
   });

   $("input[type='checkbox']").each(function () {
      $(this).click(function () {
         if ($(this).is(":checked")) {
            $(this).val(1);
         } else {
            $(this).val(0);
         }
      });
   });

});
function loadCity(objName, provinceRefId) {
   $.get("trn.e2e.php",
   {
      fn: "loadCity",
      targetObjName: objName,
      provinceRefId: provinceRefId
   },
   function (data, status) {
      if (status == "success") {
         try {
            eval(data);
         } catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
      }
      else {
         alert("Ooops Error : " + status + "[005]");
      }
   });

}

function saveProceed() {
   /*
      before we save check first all mandatory fields, if had an entered value.
   */
   var count = 0;
   $(".mandatory--, .mandatory, .nonzero").each(function () {
      var tbname = "";
      $(this).removeClass("innerRedShadow");
      if (!$(this).is(":disabled")) {
         if ($(this).val() == "" || $(this).val() <= 0) {
            if ($(this).attr("type") == "hidden") {
               if ($("[name='" + $(this).attr("for") + "']").lenght > 0) {
                  $("[name='" + $(this).attr("for") + "']").focus();
                  $("[name='" + $(this).attr("for") + "']").select();
               } else {
                  $("#" + $(this).attr("for") + "").focus();
                  $("#" + $(this).attr("for") + "").select();
               }
               $.notify("Blank or Zero this hidden field " + $(this).attr("name"));
            } else {
               if ($(this).attr("tabname") != undefined) {
                  tbname = $(this).attr("tabname");
                  $.notify("Please input all required fields for " + tbname + " Information !!!");
               } else {
                  $.notify("Please input all the required fields!!!")
               }
               //alert($(this).attr("name"));
               $(this).addClass("innerRedShadow");
               $(this).focus();
               $(this).select();
            }
            count++;
            return false;
         }
      }
   });
   return count;
}


function fnbtnSAVE() {
   $.ajax({
      url: "chkFManager.e2e.php",
      type: "POST",
      data: new FormData($("[name='xForm']")[0]),
      success: function (responseTxt) {
         if (responseTxt == "Proceed") {
            if (saveProceed() == 0) {
               if ($("#hTable").val() != "") {
                  var ObjValue = getFieldEntry("EntryScrn", "ADD");
                  /*alert(ObjValue);
                  return false;*/
                  gSaveRecord(ObjValue, $("#hTable").val());
               } else {
                  alert("Table not assigned");
               }
            }
         } else {
            alert("Ooops!!! Record Already Exist.")
         }
      },
      enctype: 'multipart/form-data',
      processData: false,
      contentType: false,
      cache: false
   });
}

function UpdateEmpList() {
   $.ajax({ // Send request
      type: "GET",
      url: 'EmpList.e2e.php',
      data: {
         LastName: $("#srchLastName").val(),
         FirstName: $("#srchFirstName").val(),
         Sex: $("#srchSex").val(),
         hCompanyID: $("#hCompanyID").val(),
         hBranchID: $("#hBranchID").val()
      },
      cache: false,
      success: function (x) {
         $("#empList").html(x);
      },
   });
}

function popAddDataLibrary(objName, table) {
   if (!($("select[name='" + objName + "']").is(":disabled"))) {
      url = "&f=" + table + "&hScrnMode=1";
      url += "&objid=" + objName;
      return popitup("FMCaller", url);
   }
}

function popAddSchools(leveltype, chkbox, objName) {
   var idx = objName.split("_")[3];
   if (($("[name='" + chkbox + "']").is(":checked"))) {
      url = "&f=Schools&hScrnMode=1";
      url += "&levelType=" + leveltype;
      url += "&objid=" + objName;
      url += "&idx=" + idx;
      return popitup("FMCaller", url);
   }
}


function setMenuClick(mID) {
   if (typeof (Storage) !== "undefined") {
      sessionStorage.setItem("mIDClick", mID);
   } else {
      alert("Sorry, your browser does not support web storage...");
      return false;
   }
}

function setSession(n, i) {
   if (typeof (Storage) !== "undefined") {
      sessionStorage.setItem(n, i);
      
   } else {
      alert("Sorry, your browser does not support web storage...");
      return false;
   }
}
function refreshApplication(month,year,table) {
   $("#spGridTable").html("");
   $("#spGridTable").load("ReloadApplications.e2e.php",
   {
      month: month,
      year: year,
      table: table
   },
   function(responseTxt, statusTxt, xhr){
      if(statusTxt == "error")
         alert("Ooops Error: " + xhr.status + ": " + xhr.statusText);
         return false;
   });
}

function getMenuClick() {
   return sessionStorage.getItem("mIDClick");
}

function refreshFMDrop(objName, table) {

   if (!($("select[name='" + objName + "']").is(":disabled"))) {
      $("select[name='" + objName + "']").empty();

      $.get("populateFMDrp.e2e.php",
         {
            obj: objName,
            isGovt: $("#sint_isGovtService_" + objName.split("_")[2]).val(),
            tbl: table
         },
         function (data, status) {
            if (status == "success") {
               code = data;
               try {
                  eval(code);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
            else {
               alert("Ooops Error : " + status + "[005]");
            }
         });
   }

}

function popitup(file, p_url) {
   if ($("#isPOP").val() == 'yes') {
      var windowWidth = screen.width * .75;
      var windowHeight = screen.height * .7;
      var centerWidth = (screen.width - windowWidth) / 2;
      var centerHeight = (screen.height - windowHeight) / 2;
      var cSpecs = "directories=0,fullscreen=0" +
         ",height=" + windowHeight +
         ",width=" + windowWidth +
         ",left=" + centerWidth +
         ",top=" + centerHeight +
         ",location=0,menubar=0,resizable=0,scrollbars=yes,status=0,titlebar=0,toolbar=0";
      var cReplace = "replace=true";
      var url = "PopCaller.e2e.php?";
      url += $("#hgParam").val();
      url += "&file=" + file;
      url += p_url;
      var subnewwindow = window.open(url, '_blank', cSpecs, cReplace);
      if (window.focus) {
         subnewwindow.focus();
      }
   } else {
      var windowWidth = screen.width * .75;
      var windowHeight = screen.height * .8;
      var centerWidth = (screen.width - windowWidth) / 2;
      var centerHeight = (screen.height - windowHeight) / 2;
      var cSpecs = "directories=0,fullscreen=0" +
         ",height=" + windowHeight +
         ",width=" + windowWidth +
         ",left=" + centerWidth +
         ",top=" + centerHeight +
         ",location=0,menubar=0,resizable=0,scrollbars=yes,status=0,titlebar=0,toolbar=0";
      var cReplace = "replace=true";
      var url = "PopCaller.e2e.php?";
      url += $("#hgParam").val();
      //url += $("#hgParam").val();
      url += "&file=" + file;
      url += p_url;
      if (newwindow !== null) newwindow.window.close();
      newwindow = window.open(url, 'newwindow', cSpecs, cReplace);
      newwindow.focus();

   }
   return false;
}

function popPDS(file, p_url) {
   var windowWidth = 1000;
   var windowHeight = screen.height * .85;
   var cSpecs = "directories=0,fullscreen=0" +
      ",height=" + windowHeight +
      ",width=" + windowWidth +
      ",left=150" +
      ",top=50" +
      ",location=0,menubar=0,resizable=1,scrollbars=1,status=0,titlebar=0,toolbar=0";
   var cReplace = "replace=false";
   var url = "PopCaller.e2e.php?";
   url += $("#hgParam").val();
   url += "&file=" + file;
   url += p_url;
   PDSwindow = window.open(url, '_pds', cSpecs, cReplace);
   if (window.focus) {
      PDSwindow.focus();
   }
}

function tr_Click(RecordRefid) {
   if (typeof getEmployeeInfo == 'function') {
      getEmployeeInfo(RecordRefid);
   } else if (typeof local_tr_Click == 'function') {
      local_tr_Click(RecordRefid);
   }
}

function changeTitle(str) {
   $("#ScrnTitle").html("<span>" + str + "</span>");
}

function dateOnlyx() {
   with (document.xForm) {
      for (var i = 0; i < elements.length; i++) {
         var ObjClassName = elements[i].className;
         if (ObjClassName.indexOf("date--") >= 0) {
            var objid = elements[i].id;
            elements[i].focus();
            $("#" + objid).datepicker({
               dateFormat: 'yy-mm-dd'
            });

         }
      }
   }
}

function gSaveRecord(Object_Value, dbtable) {
   /*alert (Object_Value + "--" + dbtable);
   return false;*/
   if (!$("[name='hmode']").length) {
      console.log("Sys Error !!! No hmode created");
      alert("Sys Error !!! No hmode created");
      return false;
   }
   if (!$("[name='hRefId']").length) {
      console.log("Sys Error !!! No hmode hRefId created");
      alert("Sys Error !!! No hmode hRefId created");
      return false;
   }
   var saveSuccess = false;
   var TrnMode = $("[name='hmode']").val();
   if (Object_Value != "") {
      setCookie("cookies_Object_Value", Object_Value, 1);
      if (TrnMode == "ADD") {
         var refid = 0;
      } else {
         var refid = $("[name='hRefId']").val();
         if (refid == 0) {
            alert("Warning!!! No RefId Assigned");
            return false;
         }
         if (TrnMode != "EDIT") {
            alert("Warning!!! Transaction Mode not in EDIT");
            return false;
         }
      }

      $.post("SystemAjax.e2e.php",
         {
            task: "ajxSaveRecord",
            refid: refid,
            hTrnMode: TrnMode,
            obj_n_value: Object_Value,
            hUser: $("[name='hUser']").val(),
            table: dbtable
         },
         function (data, status) {
            if (status == "success") {
               code = data;
               try {
                  eval(code);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
         });
   }
}

function indicateActiveModules() {
   $("#" + getMenuClick()).addClass("elActive", 300);
   checkModule($("#hProg").val(),
      $("#hUserGroup").val(),
      $("#hUser").val());
}

function gDeleteRecord(table, refid) {
   $.post("SystemAjax.e2e.php",
   {
      task: "ajxDeleteRecord",
      refid: refid,
      table: table,
      hUser: $("#hUser").val()
   },
   function (data, status) {
      if (status == "success") {
         code = data;
         try {
            eval(code);
         } catch (e) {
            if (e instanceof SyntaxError) {
               alert(e.message);
            }
         }
      }
   });         
}
function refreshList(param) {
   var url = "listRefresh.e2e.php?t=" + $("#hTable").val();
   $("#divList").show();
   $("#divView").hide();
   $("#spGridTable").html("");
   $("#spGridTable").load(url, function (responseTxt, statusTxt, xhr) {
      if (statusTxt == "error") {
         alert("Ooops Error: " + xhr.status + " : " + xhr.statusText);
      }
   });
}

function setModeTrn(btnvalue, mode) {
   $(".alert").hide();
   setBHV("value", mode, "hMODE");
   setBHV("value", btnvalue, "hBTN");
   setBHV("innerHTML", mode, "mode");
}

function getObjValue(id_name) {
   if (document.xForm[id_name] != null) {
      return document.xForm[id_name].value;
   } else if (document.getElementById(id_name) != null) {
      return document.getElementById(id_name).value;
   }
   return "";
}

function viewRECORD(refid, trnmode, task) {
   $(".alert").hide();
   $("#divList").hide();
   $("#divView").show();
   var url = "";
   url = "SystemAjax.e2e.php?task=" + task;
   url += "&hSchoolID=" + getObjValue("hSchoolID");
   url += "&hCampusID=" + getObjValue("hCampusID");
   url += "&hSess=" + escape(getObjValue("hSess"));
   url += "&hUser=" + getObjValue("hUser");
   url += "&hTrnMode=" + trnmode;
   //url += "&uid=" + getObjValue("huid");
   url += "&refid=" + refid;
   url += parseURLhParam();
   loadajx(url, "exe", "null");
}
function timedate() {
   var today = new Date();
   var daylist = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
   var n = today.toLocaleString();
   var x = daylist[today.getDay()] + " " + n;
   $("#TimeDate").html(x);
   var t = setTimeout(timedate, 1000);
}
function checkModule(module, userGroup, user) {
   if ($.inArray(module, sessionStorage.getItem("modules").split(",")) >= 0 && userGroup == "COMPEMP") {
      $(".adminUse--").hide();
   }
}
function checkTime(i) {
   if (i < 10) { i = "0" + i };  // add zero in front of numbers < 10
   return i;
}
function openNav() {
   if (parseInt($("#mainScreen").css("marginLeft")) == 250) {
      closeNav();
      $("#titleBarIcons").html("");
      if ($("#hUserGroup").val() != "COMPEMP") {
         $("#titleBarIcons").html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
      } else {
         $("#titleBarIcons").append('<span onclick="gotoscrn(\'scrnWelcome\',\'\')"><i class="fa fa-home" aria-hidden="true"> <b>HOME</b></i></span>');   
      }
      //
      
   } else {
      if ($("#hUserGroup").val() != "COMPEMP") {
         $("#mainScreen").animate({ marginLeft: "250px" }, 500, "linear");
         $("#div_RSideBar, #div_amsSideBar").animate({ marginLeft: "0" }, 500, "linear");
         $("#titleBarIcons").html('<i class="fa fa-chevron-left" aria-hidden="true"></i>');
      }
   }
}
function closeNav() {
   var t = 500;
   if ($("#hUserGroup").val() == "COMPEMP") t = 0; 
   $("#mainScreen").animate({ marginLeft: "0" }, t, "linear");
   $("#div_RSideBar, #div_amsSideBar").animate({ marginLeft: "-250px" }, t, "linear");
   $("#titleBarIcons").html('<i class="fa fa-chevron-right" aria-hidden="true"></i>');
}

function getObjFieldSave(parentId) {
   var objvalue = new Array();
   var idx = 0;
   $("#" + parentId + " .saveFields--").each(function () {
      objvalue[idx] = $(this).attr("name");
      idx++;
   });
   return objvalue;
}

function getFieldEntry(parentId, TrnMode) {
   var Elem_Value = "";
   var Elem_Name = "";
   var Elem_Type = "";
   var objvalue = "";
   var ObjClassName = "";
   var nameAllowed = ["char", "sint", "bint", "date", "deci"];
   var isInclude = false;
   $("#" + parentId + " .saveFields--").each(function () {
      ObjClassName = $(this).attr("class");
      Elem_Value = $(this).val();
      Elem_Name = $(this).attr("name");
      isInclude = false;
      if ($(this).is("select")) {
         Elem_Type = "select";
      } else if ($(this).is("textarea")) {
         Elem_Type = "textarea";
      } else {
         Elem_Type = $(this).attr("type");
      }

      /*if (ObjClassName.indexOf("date--") >= 0 && Elem_Value == "") {
         Elem_Value = '1900-01-01';
      } else if (ObjClassName.indexOf("number--") >= 0 && Elem_Value == "") {
         Elem_Value = 0;
      }*/

      if (TrnMode == "ADD") {
         if (
            Elem_Type == "select" && Elem_Value > 0 ||
            Elem_Type == "select" && Elem_Value != ""
         ) isInclude = true;
         else if (Elem_Type != "select" && Elem_Value != "") isInclude = true;
         else isInclude = false;

         //alert(Elem_Name + " - " + Elem_Value + " -- " + isInclude);

         if (isInclude) {

            /*if (Elem_Type == "checkbox" && Elem_Type == "radio") {
               objvalue += Elem_Type + "|" + Elem_Name + "|" + Elem_Value + '!';
            }*/

            objvalue += Elem_Type + "|" + Elem_Name + "|" + Elem_Value + '!';
         }

      } else {

         objvalue += Elem_Type + "|" + Elem_Name + "|" + Elem_Value + '!';

      }
   });
   return objvalue;
}

function getFieldEntryMultiple(chkboxName, TrnMode) {
   var objvalue = "";
   $("input[name*='" + chkboxName + "']:checked").each(function () {
      parentId = $(this).parent().attr("id");
      objvalue += getFieldEntry(parentId, TrnMode) + '»'; /*alt-175*/
   });
   return objvalue;
}

function clearFields(obj) {
   var objArray = obj.split(",");
   var Elem_Type = "";
   for (i = 0; i < objArray.length; i++) {
      if ($("[name='" + objArray[i] + "']").is("select")) {
         Elem_Type = "select-one";
      } else if ($("[name='" + objArray[i] + "']").is("textarea")) {
         Elem_Type = "textarea";
      } else {
         Elem_Type = $("[name='" + objArray[i] + "']").attr("type");
      }
      switch (Elem_Type) {
         case "text":
            $("[name='" + objArray[i] + "']").val("");
            break;
         case "select-one":
            $("[name='" + objArray[i] + "']").val("");
            break;
         case "checkbox":
            $("[name='" + objArray[i] + "']").prop("checked", false);
            break;
      }
   }
}
function submitUploadForm() {
   if ($("[name='txtRefId']").val() == "") {
      alert("No Selected Employees!!!");
      return false;
   } else {
      $("#hSubmit").val("YES");
      document.getElementById('xForm').submit();
   }
}

/*for Grid Table Add Edit Delete*/
function deleteRecord(refid) {
   if (refid > 0) {
      $("[name='hRefId']").val(refid);
      var table = $("[name='hTable']").val();
      var user = $("#hUserGroup").val();
      if (user == "COMPEMP") {
         $.get("trn.e2e.php",
         {
            fn:"checkAppv",
            table:$("#hTable").val(),
            refid:refid
         },
         function(data,status){
            if (status == 'success') {
               var result = data.trim();
               if (result == 1) {
                  $.notify("Approved Application/Request cannot be deleted.");
                  $("[name='hRefId']").val(0);
                  return false;
               } else {
                  if (confirm("Are you sure you want to delete this record " + refid + "?")) {
                     if (table != "") {
                        gDeleteRecord(table, refid);
                     }
                     else {
                        alert("Err : No db Table assigned");
                        return false;
                     }
                  }            
               }
            }
         });   
      } else {
         if (confirm("Are you sure you want to delete this record " + refid + "?")) {
            if (table != "") {
               gDeleteRecord(table, refid);
            }
            else {
               alert("Err : No db Table assigned");
               return false;
            }
         }
      }
      
   }
   else {
      alert("Err : No Ref. Id assigned");
      return false;
   }
}

function calcAge(dateString) {
   var birthday = + new Date(dateString);
   return ~~((Date.now() - birthday) / (31557600000));
}
function checkAppv(table,refid){
   $.get("trn.e2e.php",
   {
      fn:"checkAppv",
      table:table,
      refid:refid
   },
   function(data,status){
      if (status == 'success') {
         eval(data.trim());
      }
   });
}

function viewInfo(refid, mode, dum) {
   $("#hRefId").val(refid);
   var user = $("#hUserGroup").val();
   if ($("#hRefId").length > 1) {
      $.notify("Ooops Multiple RefId !!!");
   }
   if ($("#hProg").val() == "scrn201File" && ($("#hSysGroup").val() >= 0 && $("#hSysGroup").val() <= 2)) { // admin side
      setValueByName("hRefId",refid);
      $(".saveFields--").prop("disabled", true);
      $("#201List").hide();
      tabClick(1);
      $("#DataEntry").show();
      if (mode == 2) {
         setValueByName("hmode", "EDIT");  
         $(".enabler--").prop("disabled", false);
      }
      if (mode == 3) {
         $(".enabler--").prop("disabled", true);
         setValueByName("hmode", "VIEW");
      }
      load201();
   } else {
      if ($("#hTable").val() != "") {
         $.get("trn.e2e.php",
         {
            fn:"checkAppv",
            table:$("#hTable").val(),
            refid:refid
         },
         function(data,status){
            if (status == 'success') {
               var result = data.trim();
               if (result == 1 && mode == 2 && user == "COMPEMP") {
                  $.notify("Approved Application/Request cannot be edited.");
                  $("#hRefId").val(0);
                  return false;
               } else {
                  $.get("SystemAjax.e2e.php",
                  {
                     task: "getRecord",
                     refid: refid,
                     tbl: $("#hTable").val(),
                     hCompanyID: $("#hCompanyID").val(),
                     hBranchID: $("#hBranchID").val(),
                     hEmpRefId: $("#hEmpRefId").val(),
                     hUserRefId: $("#hUserRefId").val()
                  },
                  function (data, status) {
                     if (status == "success") {
                        try {
                           $("#divView, #badgeRefId").show();
                           $("#divList").hide();

                           var entryScrn = "EntryScrn";
                           if ($("#hEntryScreen").length > 0) {
                              entryScrn = $("#hEntryScreen").val();
                              if (
                                 $("#hModalName").length > 0
                              ) {
                                 var modTitleID = "";
                                 if (entryScrn.indexOf("LeavePolicy") > 0) {
                                    modTitleID = "modalTitle_LeavePolicy";
                                 } else if (entryScrn.indexOf("OvertimePolicy") > 0) {
                                    modTitleID = "modalTitle_OvertimePolicy";
                                 } else {
                                    modTitleID = "modalTitle";
                                 }
                                 if (mode == 2) $("#" + modTitleID).html("UPDATING RECORD " + refid);
                                 if (mode == 3) {
                                    $("#" + modTitleID).html("VIEWING RECORD " + refid);
                                 }
                                 $("#" + $("#hModalName").val()).modal();
                              }
                           } 
                           switch (mode) {
                              case 2:
                                 if ($("#TableItem").length > 0) {
                                    $("#modalTitle").html("UPDATING RECORD " + refid);
                                    $("#TableItem").modal();
                                 }
                                 $("#hmode").val("EDIT");
                                 $("#spSAVECANCEL, #btnLocSave, #btnLocCancel, #LocSAVE").show();
                                 $(".row > .nav-pills").show();
                                 $("#spBACK, #btnTransfer, .HideinAdd--").hide();
                                 $("#" + entryScrn + " .saveFields--").prop("disabled", false);
                                 $("[name='sint_EmployeesId']").prop("disabled", true);
                                 break;
                              case 3:
                                 $("#hmode").val("VIEW");
                                 $("#spSAVECANCEL, .HideinAdd--, #btnLocSave, #btnLocCancel, #LocSAVE").hide();
                                 $("#spBACK, #btnTransfer").show();
                                 $(".row > .nav-pills").show();
                                 $("#" + entryScrn + " .saveFields--").prop("disabled", true);
                                 
                                 $("#EmpSelected").prop("disabled",true);
                                 break;
                           }

                           $("#ScreenMode").html($("#hmode").val() + "&nbsp;");
                           var objs = getObjFieldSave(entryScrn);
                           objs += ",disp_CustodianName";

                           var newObjs = objs.split(",");
                           var obj = "";
                           var fieldname = "";

                           if (data != "") {
                              var o = JSON.parse(data);
                              $("#idRefid").html(o["RefId"]);
                              for (k = 0; k < newObjs.length; k++) {
                                 if (newObjs[k].indexOf("_") > 0) {
                                    obj = newObjs[k].split("_");
                                    fieldname = obj[1];
                                 } else {
                                    fieldname = newObjs[k];
                                 }
                                 if ($("[name='" + newObjs[k] + "']").length > 0) {
                                    if (newObjs[k].indexOf("_") > 1) {
                                       var arr_obj = newObjs[k].split("_");   
                                       if (arr_obj[0] == "sint") {
                                          if (o[fieldname] == "" || o[fieldname] == null) o[fieldname] = 0;
                                       }
                                    }
                                    setValueByName(newObjs[k],o[fieldname]);
                                    console.log(newObjs[k] + " === " + o[fieldname]);
                                    if ($("[name='" + newObjs[k] + "']").attr("type") == "checkbox" && o[fieldname] == 1) {
                                       $("[name='" + newObjs[k] + "']").prop("checked", true);
                                    }
                                    if (obj[0] == "disp") {
                                       $("[name='" + newObjs[k] + "']").prop("disabled", true);
                                    }
                                 }

                                 if (newObjs[k] == "StartTime") {
                                    if (o[newObjs[k]] > 720) {
                                       newValue = o[newObjs[k]] - 720;
                                       setValueByName(newObjs[k],newValue);
                                    }
                                 }
                                 if (newObjs[k] == "EndTime") {
                                    if (o[newObjs[k]] > 720) {
                                       newValue = o[newObjs[k]] - 720;
                                       setValueByName(newObjs[k],newValue);
                                    }
                                 }
                                 
                                 if (newObjs[k] == "char_SPLType") { 
                                    if (o[fieldname] != null) {
                                       $("#h_spl").show();
                                    } else {
                                       $("#h_spl").hide();
                                    }
                                 }
                                 if (newObjs[k] == "sint_OTClass") { 
                                    if (o[fieldname] == 2) {
                                       $("#special_box").show();
                                    } else {
                                       $("#special_box").hide();
                                    }
                                 }
                                 
                                 if (newObjs[k] == "sint_Whole") { 
                                    if (o[fieldname] > 0) {
                                       $("#time_canvas").hide();
                                    } else {
                                       $("#time_canvas").show();
                                    }
                                 }
                                 if ($("#hTable").val().toLowerCase() == "employeesleave") {
                                    var leave = $("#sint_LeavesRefId option:selected").text();
                                    leave = leave.toLowerCase();
                                    if (leave.indexOf("privilege leave") >= 0) {
                                       $("#h_spl").show();
                                    } else {
                                       $("#h_spl").hide();
                                    }
                                    if (leave.indexOf("vacation leave") >= 0) {
                                       $("#is_fl ,#vl_detail").show();
                                    } else {
                                       $("#is_fl ,#vl_detail").hide();
                                    }
                                 }
                                 if (newObjs[k] == "sint_AbsencesRefId") { 
                                    var value = $("[name='sint_AbsencesRefId'] option:selected").text().toLowerCase();
                                    if (
                                          value == "official business" ||
                                          value == "travel order" ||
                                          value == "office order" ||
                                          value == "foreign travel order"
                                       ) {
                                       $("#destination").show();
                                    } else {
                                       $("#destination").hide();
                                    }
                                 }
                                 
                                 if (newObjs[k] == "StartTime_mode") {
                                    if (o["StartTime"] < 720) var ampm = "AM";
                                                         else var ampm = "PM";
                                       setValueByName(newObjs[k],ampm);
                                 } else if (newObjs[k] == "EndTime_mode") {
                                    if (o["EndTime"] < 720) var ampm = "AM";
                                                      else var ampm = "PM";
                                       setValueByName(newObjs[k],ampm);
                                 }

                              }
                              var hProg = $("[name='hProg']").val();
                              if ($("[name='hSaveType']").val() == "Multiple") {
                                 $(".srch--").prop("disabled",true);
                                 $("[name*='srch'], #EmpSelected").prop("disabled",true);
                                 $('#EmpSelected').empty();
                                 // dapat employees refid
                                 getEmpName(o["EmployeesRefId"],"EmpSelected");
                                 setValueByName("hEmpSelected",o["EmployeesRefId"]);
                              }
                              if (hProg == "amsTrnEmpCreditBal") {
                                 var drpVal = $("[name='char_NameCredits']").val();
                                 $("#LeaveScreen, #OTScreen").hide();
                                 $("#expiry_div").hide();
                                 if (drpVal == "OT") {
                                    $("#expiry_div").show();
                                 }
                                 if (
                                       drpVal == "SL" ||
                                       drpVal == "VL" ||
                                       drpVal == "SPL"
                                    ) {
                                    $("#LeaveScreen").show(150);
                                    if (drpVal == "VL") {
                                       $("#sint_ForceLeave").prop("disabled",false);
                                    } else {
                                       $("#sint_ForceLeave").prop("disabled",true);
                                    }
                                 }
                                 $("[name='char_NameCredits']").prop("disabled",true);
                              }
                           }

                        } catch (e) {
                           if (e instanceof SyntaxError) {
                              alert(e.message);
                           }
                        }
                     }
                  });
               }
            }
         });
      } else {
         alert("Oooops No Table Assigned !!!");
      }
   }
}

function getEmpName(emprefid,obj) {
   $.get("trn.e2e.php",
   {
      fn:"getEmpName",
      emprefid:emprefid,
      obj:obj
   },
   function(data,status){
      if (status == 'success') {
         eval(data);
      }
   });
}
function chkMultipleObjID() {
   var d = document.xForm;
   var ID = new Array();
   with (d) {
      for (var i = 0; i < elements.length; i++) {
         ID[i] = elements[i].id;
         if (ID.indexOf(elements[i].id) > 0) {
            alert(ID);
            alert("ID Exist : " + elements[i].id);
            break;
            return false;
         }
      }
   }
}

function convTimeInSec(t) {
   if (t != "") {
      var tArr = t.split(":");
      return (tArr[0] * 60 * 60) + (tArr[1] * 60);
   } else {
      return "";
   }
}

function tabClick(tabidx) {
   var prevIDX = $("[name='hTabIdx']").val();
   var elActive = "elActive";
   $("[name='btnTab_" + prevIDX + "']").removeClass(elActive);
   $("[name='hTabIdx']").val(tabidx);
   $("[name='btnTab_" + tabidx + "']").addClass(elActive);
   $("#tab_" + prevIDX).hide();
   $("#tab_" + tabidx).show();
}

function setValue(strValue) {
   if (strValue == null) {
      return 0;
   } else {
      return strValue;
   }
}

function setValueByName(objName, objValue) {
   $("[name='" + objName + "']").val(objValue);
}
function setValueById(objId, objValue) {
   $("#" + objId).val(objValue);
}
function getValueByName(objName) {
   return $("[name='" + objName + "']").val();
}
function getValueById(objId) {
   return $("#" + objId).val();
}
function setHTMLById(objId, html) {
   $("#" + objId).html(html);
}


function printDiv(divID,head) {
   var canvas = "";
   var divToPrint=document.getElementById(divID);
   var newWin=window.open('','Print-Window');
   canvas = '<html><head>'+head+'</head><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>';
   newWin.document.open();
   newWin.document.write(canvas);
   newWin.document.close();
   setTimeout(function(){newWin.close();},10);
}

function populateFilter(table) {
   $("#filter_value").empty();
   $.get("trn.e2e.php",
   {
      fn:"populateFilter",
      table:table
   },
   function(data,status){
      if (status == 'success') {
         eval(data);
      }
   });
}
function getSalary(sg,jg,si) {
   $.post("trn.e2e.php",
   {
      fn:"getSalaryAmount",
      sg:sg,
      jg:jg,
      si:si
   },
   function(data,status){
      if (status == 'success') {
         if (data.trim() != "") {
            eval(data);
         }
      } else {
         alert("Error :" + data);
      }
   });
}
function EmployeeAutoComplete(table,objname) {
   $.get("trn.e2e.php",
   {
      fn:"EmployeeAutoComplete",
      table:table
   },
   function(data,status){
      if (status == 'success') {
         var result = JSON.parse(data);
         $("#"+objname+"").autocomplete({
         source: result
       });
      }
   });
}

function getPosition(emprefid) {
   $.get("trn.e2e.php",
   {
      fn:"getPosition",
      emprefid:emprefid
   },
   function(data,status){
      if (status == 'success') {
         eval(data);
      }
   });
}
function getPlantilla(value,position_obj,salarygrade_obj,jobgrade_obj,salary_obj,step_obj,office_obj,division_obj) {
   $.get("trn.e2e.php",
   {
      fn:"getPlantilla",
      refid:value
   },
   function(data,status){
      if (status == 'success') {
         var arr = JSON.parse(data);
         $("#" + position_obj).val(arr.PositionRefId);   
         $("#" + salary_obj).val(arr.SalaryAmount);
         $("#" + step_obj).val(arr.StepIncrementRefId);
         $("#" + office_obj).val(arr.OfficeRefId);
         $("#" + division_obj).val(arr.DivisionRefId);
         if (salarygrade_obj != "") {
            $("#" + salarygrade_obj).val(arr.SalaryGradeRefId);   
         }
         if (jobgrade_obj != "") {
            $("#" + jobgrade_obj).val(arr.JobGradeRefId);   
         }
      }
   });
}