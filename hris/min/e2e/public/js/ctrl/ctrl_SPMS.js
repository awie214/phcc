$(document).ready (function () {
   remIconDL();
   $("#addSPMS").click(function() {
      AddRows("Entry_","contentDiv","refid_");
   });
   $("#insert_spms, #btnEDITSPMS, #btnBACKSPMS").hide();
   $("#btnINSERTSPMS").click(function () {
      $("#spmsList").hide();
      $("#insert_spms").show();
   });
   $("#btnCANCELSPMS, #btnBACKSPMS").click(function () {
      var prog = $("#hProg").val();
      gotoscrn(prog,"");
   });
   
   $("#btnPRINT").click (function () {
      var rptFile = "prnIPCR";
      var url = "ReportCaller.e2e.php?file=" + rptFile + "&";
      $("#prnModal").modal();
      $("#rptContent").attr("src",url);
   });
   $("#deleterow").click(function () {
      var last = "";
      $("[id*='row_']").each(function () {
         last = $(this).attr("id").split("_")[1];
      });
      if (last > 1) {
         $("#row_" + last).remove();   
      }
   });
   $("#btnSAVESPMS, #btnEDITSPMS").click(function () {
      /*$(this).prop("disabled",true);
      $("#btnBACKSPMS, #btnCANCELSPMS").prop("disabled",true);
      $(this).html("Processing..");*/
   	var count    = 0;
      $("[id*='Entry_']").each(function () {
         count++;
      });
      $("#count").val(count);

      $.ajax({
         url: "ajax.e2e.php",
         type: "POST",
         data: new FormData($("[name='xForm']")[0]),
         success: function (responseTxt) {
            eval(responseTxt);
         },
         enctype: 'multipart/form-data',
         processData: false,
         contentType: false,
         cache: false
      });   
   });
});
function errorSave() {
   $.notify("Error in Saving.");
}
function errorUpdate() {
   $.notify("Error in Updating.");
}
function afterSave(){
	alert("Successfully Save");
	gotoscrn ($("#hProg").val(), setAddURL());
}
function afterEdit() {
   alert("Successfully Update");
   gotoscrn ($("#hProg").val(), setAddURL());   
}
function viewInfo(refid,mode,dum){
   var table = $("#hTable").val();
   //$.notify(table);
   var file = "";
   if (mode == 2) {
      getSPMS_detail(refid,table);   
   } else if (mode == 3) {
      if (table == "spms_ips") {
         file = "rpt_IPS_2";
      } else if (table == "spms_dps") {
         file = "rpt_DPS_2";
      } else if (table == "spms_ops") {
         file = "rpt_OPS_2";
      }
      if (file != "") {
         printSPMS(refid,file);     
      }
   }
}
function printSPMS(refid,file){
   $("#rptContent").attr("src","blank.htm");
   var url = "ReportCaller.e2e.php?file=" + file;
   url += "&refid=" + refid;
   url += "&" + $("[name='hgParam']").val();
   $("#prnModal").modal();
   $("#rptContent").attr("src",url);
}
function compute_q1(idx) {
   var q1 = $("#q1_" + idx).val();
   var e2 = $("#e2_" + idx).val();
   var t3 = $("#t3_" + idx).val();
   var result = Number(q1) + Number(e2) + Number(t3);
   result = Number(result) / 3;
   $("#a4_" + idx).val(result.toFixed(2));
}
function compute_e2(idx) {
   var q1 = $("#q1_" + idx).val();
   var e2 = $("#e2_" + idx).val();
   var t3 = $("#t3_" + idx).val();
   var result = Number(q1) + Number(e2) + Number(t3);
   result = Number(result) / 3;
   $("#a4_" + idx).val(result.toFixed(2));
}
function compute_t3(idx) {
   var q1 = $("#q1_" + idx).val();
   var e2 = $("#e2_" + idx).val();
   var t3 = $("#t3_" + idx).val();
   var result = Number(q1) + Number(e2) + Number(t3);
   result = Number(result) / 3;
   $("#a4_" + idx).val(result.toFixed(2));
}

function getSPMS_detail(refid,table){
   $("#sint_SPMSRefId").val(refid);
   $.get("ajax.e2e.php",
   {
      fn:"getSPMS_detail",
      refid:refid,
      table:table
   },
   function(data,status){
      if (status=='success') {
         $("#canvas").html("");
         $("#canvas").html(data);
         $("[id*='delete_']").prop("disabled",true);
         $("#spmsList, #btnSAVESPMS, #btnCANCELSPMS").hide();
         $("#insert_spms, #btnEDITSPMS, #btnBACKSPMS").show();
         remIconDL();
         $.get("ajax.e2e.php",
         {
            fn:"getSPMS",
            refid:refid,
            table:table
         },
         function(data,status){
            if (status=='success') {
               eval(data);
            }
         });
      }
   });
}
function AddRows(parentId,gparentId,refid) {
   // $.notify(parentId + "1");
   // $.notify(gparentId + "2");
   // $.notify(refid + "3");
   var rowHTML    = "";
   var newAppends = "";
   var appends    = "";
   var rowLength  = $("[id*='"+parentId+"']").length;
   appends        = $("#"+parentId+rowLength).html();

   newAppends = appends.split("_"+rowLength).join("_"+(rowLength+1));
   newAppends = newAppends.split("#"+rowLength).join("#"+(rowLength+1));
   newAppends = newAppends.split('idx="' + rowLength + '"').join('idx="'+ (rowLength + 1) +'"');
   newAppends = newAppends.split('unclick="CancelAddRow"').join('onClick=removeRow("'+parentId+(rowLength+1)+'");');
   newAppends = newAppends.split('focus="validateGrade"').join('onChange=validateGrade2($(this).attr(\'partner\')); onFocus=validateGrade2($(this).attr(\'partner\'));');
   newAppends = newAppends.split('click="HighestGrade_validation"').join('onClick=HighestGrade_validation($(this).attr(\'name\'));');
   rowHTML += '<div id="'+parentId+(rowLength+1)+'" class="entry201">';
   rowHTML += newAppends;
   rowHTML += '</div>';
   $("#"+gparentId).append(rowHTML);
   $("[name='"+refid+(rowLength + 1)+"']").val("");
   if (rowHTML.indexOf("date--") > 0) {
      $("#" + parentId+(rowLength+1) + " input[class*='date--']").each(function () {
         $(this).keypress(fnDateClass($(this).attr("name")));
      });
   }
   if (rowHTML.indexOf("alpha--") > 0) {
      $("#" + parentId+(rowLength+1) + " input[class*='alpha--']").each(function () {
         var e = arguments[0];
         fnAlphaClass($(this).attr("name"),e);
      });
   }
   if (rowHTML.indexOf("number--") > 0) {
      $("#" + parentId+(rowLength+1) + " input[class*='number--']").each(function () {
         var e = arguments[0];
         fnNumberClass($(this).attr("name"),e);
      });
   }
   if (rowHTML.indexOf("valDate--") > 0) {
      $("#" + parentId+(rowLength+1) + " input[class*='valDate--']").each(function () {
         $(this).keypress(fnValDate($(this).attr("name")));
      });
   }
   if (rowHTML.indexOf('rowid="') > 0) {
      $("#" + parentId+(rowLength+1) + " input[class*='enabler--']").each(function () {
         $(this).attr("refid","");
         $("[name='"+$(this).attr("rowid")+"']").val("");
      });
   }
   if (rowHTML.indexOf('presentbox--') > 0) {
      $("#" + parentId+(rowLength+1) + " input[class*='presentbox--']").each(function () {
         $(this).attr("checked",false);
         $(this).click(function () {
            sint_Present_Click($(this).attr("id"));
         });
      });
   }
   $("#refid_"+(rowLength+1)).val("");
   $("#" +parentId+(rowLength+1)+ " .saveFields--").prop("disabled",false);
   $("#" +parentId+(rowLength+1)+ "> .enabler--").prop("checked",true);
   $("#" +parentId+(rowLength+1)+ "> .form-input").val("");

   $("#type_"+(rowLength+1)).val("");
   $("#objective_"+(rowLength+1)).val("");
   $("#other_"+(rowLength+1)).val("");
   $("#measure_"+(rowLength+1)).val("");
   $("#target_"+(rowLength+1)).val("");
   $("#weight_"+(rowLength+1)).val("");
   $("#quality_"+(rowLength+1)).val("0");
   $("#effectiveness_"+(rowLength+1)).val("0");
   $("#timeliness_"+(rowLength+1)).val("0");
   $("#rawscore_"+(rowLength+1)).val("");
   $("#weightedscore_"+(rowLength+1)).val("");
   $("#accomplishment_"+(rowLength+1)).val("");
   $("#q1_"+(rowLength+1)).val("");
   $("#q2_"+(rowLength+1)).val("");
   $("#q3_"+(rowLength+1)).val("");
   $("#q4_"+(rowLength+1)).val("");
   $("#q5_"+(rowLength+1)).val("");
   $("#e1_"+(rowLength+1)).val("");
   $("#e2_"+(rowLength+1)).val("");
   $("#e3_"+(rowLength+1)).val("");
   $("#e4_"+(rowLength+1)).val("");
   $("#e5_"+(rowLength+1)).val("");
   $("#t1_"+(rowLength+1)).val("");
   $("#t2_"+(rowLength+1)).val("");
   $("#t3_"+(rowLength+1)).val("");
   $("#t4_"+(rowLength+1)).val("");
   $("#t5_"+(rowLength+1)).val("");
   showBtnUpd();
}



function enableOther(id){
   var idx = id.split("_")[1];
   if ($("#" + id).is(":checked")) {
      $("#newobjective_" + idx).prop("disabled", false);
   } else {
      $("#newobjective_" + idx).prop("disabled", true);
   }
}