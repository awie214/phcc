$(document).ready(function() {
   t = 100;
   //$("#btnHolder").hide();
   $("#iPerInfo").click(function(){
      $("#panelPerInfo").slideToggle(t);
   });
   $("#iFamBG").click(function(){
      $("#panelFamBG").slideToggle(t);
   });
   $("#iEducBG").click(function(){
      $("#panelEducBG").slideToggle(t);
   });
   $("#iEligib").click(function(){
      $("#panelEligib").slideToggle(t);
   });
   $("#iTrainProg").click(function(){
      $("#panelTrainProg").slideToggle(t);
   });
   $("#iVolWork").click(function(){
      $("#panelVolWork").slideToggle(t);
   });
   $("#iWExp").click(function(){
      $("#panelWExp").slideToggle(t);
   });
   $("#iOtherInfo").click(function(){
      $("#panelOtherInfo").slideToggle(t);
   });
   $("#iPDSQ").click(function(){
      $("#panelPDSQ").slideToggle(t);
   });
   $("#iRef").click(function(){
      $("#panelRef").slideToggle(t);
   });
   $("#btnGENERATE").click(function(){
      $("#rptContent").attr("src","blank.e2e.php");
      var rptFile = $("#hRptFile").val();
      var url = "ReportCaller.e2e.php?file=" + rptFile + "&";
      url += getRPTCriteria("rptCriteria");
      $("#prnModal").modal();
      $("#rptContent").attr("src",url);
   });
   $("#aSplitSettings").click(function(){
      $("#split").modal();
   });
   $(".Employees--").each(function (){
      $(this).click(function () {
         selectMe($(this).attr("refid"));
      });
   });
});

function selectMe(emprefid) {
   var url = "ctrl_201File_1.e2e.php?";
   url += "EmpRefId=" + emprefid;
   $("[name='hEmpRefId']").val(emprefid);
   $("#btnHolder").hide();
   //$("#RecordDetails").html("");
   $("#RecordDetails").load(url, function(responseTxt, statusTxt, xhr) {
      if(statusTxt == "error") {
         console.log("Ooops Error (selectMe): " + xhr.status + " : " + xhr.statusText);
         return;
      }
      if(statusTxt == "success") {
         $("#btnHolder").show();
      }
   });
}
function submitForm() {
   if ($("[name='txtRefId']").val() == "") {
      alert ("No Selected Employees!!!");
      return false;
   }
   else {
      var addParam = "&paramTitle=MASTER FILE";
      addParam += "&txtRefId="+$("[name='txtRefId']").val();
      addParam += "&txtLName="+$("[name='txtLName']").val();
      addParam += "&txtFName="+$("[name='txtFName']").val();
      addParam += "&txtMidName="+$("[name='txtMidName']").val();
      gotoscrn("scrnMasterFile",addParam);
   }
}