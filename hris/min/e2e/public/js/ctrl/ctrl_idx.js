$(document).ready(function () {
   $("#signUp").click(function () {
      loadscrn("signup");
   });
   $("#forgotPW").click(function () {
      loadscrn("setUsername");
   });
   $("#btnNextScrn").click(function () {
      urlParam = "fpw" + "&u=" + $("[name='txtUserName']").val();
      loadscrn(urlParam);
   });
   $(".close").click(function () {
      $("#EntryScrn").html("");
      $("#login").fadeIn(500);
   });
   $("#btnNewPassword").click(function(){

   });
   $("#btnSubmitSignUp").click(function(){
      $("[name='char_AnsQues1']").val(calcHash($("[name='AnsQues1']").val()));
      $("[name='char_AnsQues2']").val(calcHash($("[name='AnsQues2']").val()));
      $("[name='AnsQues1']").val("");
      $("[name='AnsQues2']").val("");
      $("[name='hmode']").val("ADD");
      $("[name='hRefId']").val(0);
      var obj = getFieldEntry("SignUpEntry","ADD");
      gSaveRecord(obj,"newsignup");
   });

   $("[name='btnLogin']").click(function () {
      if (saveProceed() == 0) {
         $.get("trn.e2e.php",
         {
            fn:"ChkUName",
            t:calcHash($("[name='token").val()),
            u:$("[name='txtUser']").val()
         },
         function(data,status) {
            if (status == "success") {
               try {
                  eval(data);
               } catch (e) {
                  if (e instanceof SyntaxError) {
                     alert(e.message);
                  }
               }
            }
            else {
               alert("Ooops Error : " + status + "[005]");
            }
         });
      }  
   });

   $("[name='token']").keypress(function (ev) {
      if (ev.which == 13) {
         if (saveProceed() == 0) {
            $.get("trn.e2e.php",
            {
               fn:"ChkUName",
               t:calcHash($("[name='token").val()),
               u:$("[name='txtUser']").val()
            },
            function(data,status) {
               if (status == "success") {
                  try {
                     eval(data);
                  } catch (e) {
                     if (e instanceof SyntaxError) {
                        alert(e.message);
                     }
                  }
               }
               else {
                  alert("Ooops Error : " + status + "[005]");
               }
            });
         }  
         
      } 
   });   

});

function loadscrn(scrn) {
   var url = "scrnSignUp.e2e.php?scrn=" + scrn;
   $("#EntryScrn").load(url, function(responseTxt, statusTxt, xhr) {
      if(statusTxt == "error") {
         alert("Ooops Error: " + xhr.status + " : " + xhr.statusText);
         return false;
      }
      if(statusTxt == "success") {
         $("#login").hide();
      }
   });
   return false;
}

function afterNewSave(LastRefId) {
   alert("New Record " + LastRefId + " Succesfully Inserted");
   $("#EntryScrn").html("");
   $("#login").fadeIn(500);
   return false;
}