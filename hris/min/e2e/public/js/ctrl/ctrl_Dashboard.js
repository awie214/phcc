$(document).ready(function() {
  $("#PIS_menu").hide();
  $("#AMS_menu").hide();
  $("#PMS_menu").hide();
  $("#myrequestView").hide();
  $("#PIS_Header").click(function () {
     $("#PIS_menu").toggle();
     $("#AMS_menu").hide();
     $("#PMS_menu").hide();
     $("#myrequestView").hide();
  });
  $("#AMS_Header").click(function () {
     $("#AMS_menu").toggle();
     $("#PIS_menu").hide();
     $("#PMS_menu").hide();
     $("#myrequestView").hide();
  });
  $("#PMS_Header").click(function () {
     $("#PMS_menu").toggle();
     $("#AMS_menu").hide();
     $("#PIS_menu").hide();
     $("#myrequestView").hide();
  });
  $("#myrequest").click(function () {
     $("#myrequestView").toggle();
     $("#AMS_menu").hide();
     $("#PIS_menu").hide();
     $("#PMS_menu").hide();
  });
	t = 500;
    
    $("#btnChangePW").click(function () {
       $("#panelChangePW").slideDown(t);
       $("#panelReminders, #panelRequest").slideUp(t);
       $(this).prop("disabled",true);
       $("#newToken, #currenToken, #reToken").val("");
    });
    $("#btnChangeCancel").click(function () {
       $("#panelChangePW").slideUp(t);
       $("#panelReminders, #panelRequest").slideDown(t);
       $("#btnChangePW").prop("disabled",false);
    });
    $("#currentToken").click(function () {
       $("[for=\'cuPW\']").hide(100)
    });
    $("[id*='cancel_']").each(function () {
       $(this).click(function () {
          var refid = $(this).attr("id").split("_")[1];
          if (confirm("Do you want to cancel this request?")) {
             cancelRequest(refid);
          }
       });
    });
    
});
function cancelRequest(refid){
    $.get("trn.e2e.php",
    {
       fn:"cancelRequest",
       refid:refid,
    },
    function(data,status) {
       if (status == "success") {
          try {
             eval(data);
             $("#myrequestView").show();
          } catch (e) {
             if (e instanceof SyntaxError) {
                alert(e.message);
             }
          }
       }
    });
 }

function showEmp(fld,rptName,isDocType) {
   	$("#prnModal").modal();
   	$("#rptContent").attr("src","blank.e2e.php");
   	var rptFile = $("#hRptFile").val();
   	var url = "ReportCaller.e2e.php?file=" + rptFile + "&";
   	url += "&fld=" + fld;
   	url += "&rptName=" + rptName;
    url += "&isdoctype=" + isDocType;
   	url += "&hgParam=" + $("#hgParam").val();
   	$("#prnModal").modal();
   	$("#rptContent").attr("src",url);
}