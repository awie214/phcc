$(document).ready(function(){
   var addParam = "";
   $("#mDASHBOARD").click(function(){
      addParam += "&paramTitle=DASHBOARD";
      addParam += "&hTable=";
      addParam += "&hGridTblHdr=";
      addParam += "&hGridTblFld=";
      $("#hParam").val(addParam);
      gotoscrn("scrnDashboard",addParam);
   });
   $("#mTEST").click(function(){
      gotoscrn("scrnTEST",addParam);
   });
   $("#mREQUESTFOR").click(function(){
      addParam += "&paramTitle=EMPLOYEES REQUEST";
      addParam += "&hTable=";
      addParam += "&hGridTblHdr=";
      addParam += "&hGridTblFld=";
      gotoscrn("scrnReqFor",addParam);
   });
   $("#mLEAVECARD").click(function(){
      addParam += "&paramTitle=MY LEAVE CARD";
      addParam += "&hTable=";
      addParam += "&hGridTblHdr=";
      addParam += "&hGridTblFld=";
      gotoscrn("userLEAVECARD",addParam);
   });
   $("#mSERVICERECORD").click(function(){
      addParam += "&paramTitle=MY SERVICE RECORD";
      addParam += "&hTable=";
      addParam += "&hGridTblHdr=";
      addParam += "&hGridTblFld=";
      gotoscrn("userSERVICERECORD",addParam);
   });
   $("#mPAYSLIP").click(function(){
      addParam += "&paramTitle=MY PAYSLIP";
      addParam += "&hTable=";
      addParam += "&hGridTblHdr=";
      addParam += "&hGridTblFld=";
      gotoscrn("userPAYSLIP",addParam);
   });
   $("#m201FILEATTACH").click(function(){
      addParam += "&paramTitle=EMPLOYEES FILE ATTACHMENT";
      gotoscrn("scrnEmpAttach",addParam);
   });
   $("#wsysAMS, #wmainAMS").click(function () {
      addParam += "&paramTitle=Daily Time Record";
      gotoscrn ("amsTrnDTR",addParam);
   });
   $("#wsysPIS, #wmainPIS").click(function () {
      addParam += "&paramTitle=MASTERFILE";
      gotoscrn ("scrnMasterFile",addParam);
   });
   $("#wsysPAY, #wmainPMS").click(function () {
      addParam += "&paramTitle=EMPLOYEES";
      if ($("#hCompanyID").val() == 2) {
         self.location = "../../../../../../../../../HRIS_PMS/public";   
         //gotoscrn ("sync_payroll_emp",addParam);
      } else {
         gotoscrn ("pmsEmployee",addParam);
      }
      
   });
   $("#wsysLDMS, #wmainLDMS").click(function () {
      addParam += "&paramTitle=INDIVIDUAL DEVELOPMENT PLAN";
      gotoscrn ("ldmsIndividualDevtPlan",addParam);
   });
   $("#sysRMS, #wmainRMS").click(function () {
      addParam += "&paramTitle=Employement Application";
      addParam += "&hTable=Applicant";
      addParam += "&hGridTblHdr=Last Name|First Name|Contact No|Birth Date|Civil Status";
      addParam += "&hGridTblFld=LastName|FirstName|ContactNo|BirthDate|CivilStatus";
      addParam += "&sysScreen=RMS";
      gotoscrn("scrn201File",addParam);
   });
   
   /*$(".MAINSYS").click(function () {
      $.notify("TEST");
   });*/

   /*RMS Side Bar*/
   $("#rmsEmpApp, #wsysRMS").click(function () {
      $("#hmemof").val("rms");
      addParam += "&paramTitle=Employement Application";
      addParam += "&hTable=Applicant";
      addParam += "&hGridTblHdr=Last Name|First Name|Contact No|Birth Date|Civil Status";
      addParam += "&hGridTblFld=LastName|FirstName|ContactNo|BirthDate|CivilStatus";
      addParam += "&sysScreen=RMS";
      gotoscrn("scrn201File",addParam);
      /*var module new Module =*/
   });
   $("#rmsVacantPos").click(function(){
      $("#hmemof").val("rms");
      addParam += "&paramTitle=Vacant Position";
      addParam += "&hTable=vacantposition";
      addParam += "&hGridTblHdr=Position Item|Salary Amount|Plantilla";
      addParam += "&hGridTblFld=PositionItemRefId|SalaryAmount|Plantilla";
      addParam += "&sysScreen=RMS";
      gotoscrn("rmsVacantPosition",addParam);
   });
   $("#rmsSchedInterview").click(function(){
      $("#hmemof").val("rms");
      addParam += "&paramTitle=Schedule For Interview";
      addParam += "&hTable=schedInterview";
      addParam += "&hGridTblHdr=Position Applied|Date Applied|Date Interviewed|Interviewed By|Status Of Initial Interview|Date Interviewed|Interviewed By|Status Of Final Interview|Final Status";
      addParam += "&hGridTblFld=PositionItemRefId|DateApplied|InitialDateInterview|InitialInterviewByRefId|InitialInterviewStatus|FinalDateInterview|FinalInterviewByRefId|FinalInterviewStatus|FinalStatus";
      addParam += "&sysScreen=RMS";
      gotoscrn("rmsSchedInterview",addParam);
   });
   $("#rmsEvaluate").click(function(){
      addParam += "&paramTitle=Evaluate";
      addParam += "&hTable=Applicant";
      addParam += "&hGridTblHdr=Last Name|First Name|PositionApplied";
      addParam += "&hGridTblFld=LastName|FirstName|PositionItemRefId";
      addParam += "&sysScreen=RMS";
      gotoscrn("rmsEvaluate",addParam);
   });
   $("#rmsEmpReq").click(function(){
      addParam += "&paramTitle=Employment Requirements";
      addParam += "&hTable=EmployeesRequirements";
      addParam += "&hGridTblHdr=Name|Position Applied|Division/Dept/Office|Employment Status|Date Hired|Completion Status";
      addParam += "&hGridTblFld=ApplicationRefId|PositionRefId|DivisionRefId|EmpStatusRefId|HiredDate|CompletionStatus";
      addParam += "&sysScreen=RMS";
      gotoscrn("rmsEmploymentReq",addParam);
   });
   $("#rmsReports").click(function(){
      addParam += "&paramTitle=Reports";
      addParam += "&sysScreen=RMS";
      gotoscrn("rmsReports",addParam);
   });

   $("#mHome").click(function () {
      addParam += "&paramTitle=";
      gotoscrn ("scrnWelcome", addParam);
   });

   /*SPMS Side Bar*/
   $("#spmsDASHBOARD").click(function () {
      addParam += "&paramTitle=DASHBOARD";
      gotoscrn ("spmsDashboard", addParam);
   });
   $("#spmsIPCR, #wsysSPMS, #wmainSPMS").click(function () {
      $("#hmemof").val("spms");
      addParam += "&paramTitle=DASHBOARD";
      gotoscrn ("spmsDashboard", addParam);
   });
   $("#spmsOPCR").click(function () {
      addParam += "&paramTitle=OFFICE PERFORMANCE COMMITMENT AND REVIEW [OPCR]";
      gotoscrn ("spmsOPCR", addParam);
   });
   $("#spmsSummRating").click(function () {
      addParam += "&paramTitle=SUMMARY RATING";
      gotoscrn ("spmsSummRating", addParam);
   });
   $("#spmsReports").click(function () {
      addParam += "&paramTitle=REPORTS";
      gotoscrn ("spmsReports", addParam);
   });
   $("#spmsIPS").click(function () {
      addParam += "&paramTitle=INDIVIDUAL PERFORMANCE SCORECARD (IPS)";
      gotoscrn ("spmsIPS", addParam);
   });
   $("#spmsDPS").click(function () {
      addParam += "&paramTitle=DIVISION PERFORMANCE SCORECARD (DPS)";
      gotoscrn ("spmsDPS", addParam);
   });
   $("#spmsOPS").click(function () {
      addParam += "&paramTitle=OFFICE PERFORMANCE SCORECARD (OPS)";
      gotoscrn ("spmsOPS", addParam);
   });
   $("#spms_OPS").click(function () {
      addParam += "&paramTitle=OFFICE PERFORMANCE SCORECARD (OPS)";
      gotoscrn ("spms_OPS", addParam);
   });
   $("#spms_DPS").click(function () {
      addParam += "&paramTitle=DIVISION PERFORMANCE SCORECARD (DPS)";
      gotoscrn ("spms_DPS", addParam);
   });
   $("#spms_IPS").click(function () {
      addParam += "&paramTitle=INDIVIDUAL PERFORMANCE SCORECARD (IPS)";
      gotoscrn ("spms_IPS", addParam);
   });
   $("#spms_report").click(function () {
      addParam += "&paramTitle=Reports";
      gotoscrn ("spms_report", addParam);
   });
   $("#wmainTC").click(function () {
      gotoscrn ("scrnTimeClock", addParam);
   });
   $("#spmsRatingMatrix").click(function () {
      addParam += "&paramTitle=RATING MATRIX";
      gotoscrn ("spmsRatingMatrix", addParam);
   });
   


});


