-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.31-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table phcc_hris_new.dps_details
DROP TABLE IF EXISTS `dps_details`;
CREATE TABLE IF NOT EXISTS `dps_details` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `dps_id` bigint(50) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `objectives_id` varchar(300) DEFAULT NULL,
  `measure` varchar(300) DEFAULT NULL,
  `target` varchar(300) DEFAULT NULL,
  `weight` varchar(300) DEFAULT NULL,
  `weightedscore` varchar(300) DEFAULT NULL,
  `rawscore` varchar(300) DEFAULT NULL,
  `accomplishment` varchar(300) DEFAULT NULL,
  `quality` int(1) DEFAULT NULL,
  `effectiveness` int(1) DEFAULT NULL,
  `timeliness` int(1) DEFAULT NULL,
  `q1` varchar(300) DEFAULT NULL,
  `q2` varchar(300) DEFAULT NULL,
  `q3` varchar(300) DEFAULT NULL,
  `q4` varchar(300) DEFAULT NULL,
  `q5` varchar(300) DEFAULT NULL,
  `e1` varchar(300) DEFAULT NULL,
  `e2` varchar(300) DEFAULT NULL,
  `e3` varchar(300) DEFAULT NULL,
  `e4` varchar(300) DEFAULT NULL,
  `e5` varchar(300) DEFAULT NULL,
  `t1` varchar(300) DEFAULT NULL,
  `t2` varchar(300) DEFAULT NULL,
  `t3` varchar(300) DEFAULT NULL,
  `t4` varchar(300) DEFAULT NULL,
  `t5` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc_hris_new.ips_details
DROP TABLE IF EXISTS `ips_details`;
CREATE TABLE IF NOT EXISTS `ips_details` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `ips_id` bigint(50) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `objectives_id` varchar(300) DEFAULT NULL,
  `measure` varchar(300) DEFAULT NULL,
  `target` varchar(300) DEFAULT NULL,
  `weight` varchar(300) DEFAULT NULL,
  `weightedscore` varchar(300) DEFAULT NULL,
  `rawscore` varchar(300) DEFAULT NULL,
  `accomplishment` varchar(300) DEFAULT NULL,
  `quality` int(1) DEFAULT NULL,
  `effectiveness` int(1) DEFAULT NULL,
  `timeliness` int(1) DEFAULT NULL,
  `q1` varchar(300) DEFAULT NULL,
  `q2` varchar(300) DEFAULT NULL,
  `q3` varchar(300) DEFAULT NULL,
  `q4` varchar(300) DEFAULT NULL,
  `q5` varchar(300) DEFAULT NULL,
  `e1` varchar(300) DEFAULT NULL,
  `e2` varchar(300) DEFAULT NULL,
  `e3` varchar(300) DEFAULT NULL,
  `e4` varchar(300) DEFAULT NULL,
  `e5` varchar(300) DEFAULT NULL,
  `t1` varchar(300) DEFAULT NULL,
  `t2` varchar(300) DEFAULT NULL,
  `t3` varchar(300) DEFAULT NULL,
  `t4` varchar(300) DEFAULT NULL,
  `t5` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc_hris_new.ops_details
DROP TABLE IF EXISTS `ops_details`;
CREATE TABLE IF NOT EXISTS `ops_details` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `ops_id` bigint(50) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `objectives_id` varchar(300) DEFAULT NULL,
  `measure` varchar(300) DEFAULT NULL,
  `target` varchar(300) DEFAULT NULL,
  `weight` varchar(300) DEFAULT NULL,
  `weightedscore` varchar(300) DEFAULT NULL,
  `rawscore` varchar(300) DEFAULT NULL,
  `accomplishment` varchar(300) DEFAULT NULL,
  `accountable` varchar(300) DEFAULT NULL,
  `budget` decimal(15,2) DEFAULT NULL,
  `quality` int(1) DEFAULT NULL,
  `effectiveness` int(1) DEFAULT NULL,
  `timeliness` int(1) DEFAULT NULL,
  `q1` varchar(300) DEFAULT NULL,
  `q2` varchar(300) DEFAULT NULL,
  `q3` varchar(300) DEFAULT NULL,
  `q4` varchar(300) DEFAULT NULL,
  `q5` varchar(300) DEFAULT NULL,
  `e1` varchar(300) DEFAULT NULL,
  `e2` varchar(300) DEFAULT NULL,
  `e3` varchar(300) DEFAULT NULL,
  `e4` varchar(300) DEFAULT NULL,
  `e5` varchar(300) DEFAULT NULL,
  `t1` varchar(300) DEFAULT NULL,
  `t2` varchar(300) DEFAULT NULL,
  `t3` varchar(300) DEFAULT NULL,
  `t4` varchar(300) DEFAULT NULL,
  `t5` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc_hris_new.spms_dps
DROP TABLE IF EXISTS `spms_dps`;
CREATE TABLE IF NOT EXISTS `spms_dps` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `DivisionRefId` bigint(50) DEFAULT NULL,
  `semester` int(1) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `rating` decimal(5,2) DEFAULT NULL,
  `total_rating` decimal(5,2) DEFAULT NULL,
  `premium_point` decimal(5,2) DEFAULT NULL,
  `overall_rating` decimal(5,2) DEFAULT NULL,
  `adjectival_rating` decimal(5,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc_hris_new.spms_ips
DROP TABLE IF EXISTS `spms_ips`;
CREATE TABLE IF NOT EXISTS `spms_ips` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `semester` int(1) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `rating` decimal(5,2) DEFAULT NULL,
  `total_rating` decimal(5,2) DEFAULT NULL,
  `premium_point` decimal(5,2) DEFAULT NULL,
  `overall_rating` decimal(5,2) DEFAULT NULL,
  `adjectival_rating` decimal(5,2) DEFAULT NULL,
  `question1` varchar(300) DEFAULT NULL,
  `question2` varchar(300) DEFAULT NULL,
  `question3` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table phcc_hris_new.spms_ops
DROP TABLE IF EXISTS `spms_ops`;
CREATE TABLE IF NOT EXISTS `spms_ops` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `OfficeRefId` bigint(50) DEFAULT NULL,
  `semester` int(1) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `rating` decimal(5,2) DEFAULT NULL,
  `total_rating` decimal(5,2) DEFAULT NULL,
  `premium_point` decimal(5,2) DEFAULT NULL,
  `overall_rating` decimal(5,2) DEFAULT NULL,
  `adjectival_rating` decimal(5,2) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

DROP TABLE IF EXISTS `objectives`;
CREATE TABLE IF NOT EXISTS `objectives` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `Code` varchar(50) DEFAULT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `refid` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=latin1;