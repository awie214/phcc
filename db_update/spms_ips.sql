-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.31-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5289
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table phcc_latest.spms_ips
CREATE TABLE IF NOT EXISTS `spms_ips` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` bigint(50) DEFAULT NULL,
  `BranchRefId` bigint(50) DEFAULT NULL,
  `EmployeesRefId` bigint(50) DEFAULT NULL,
  `Quarter` int(1) DEFAULT NULL,
  `Year` int(4) DEFAULT NULL,
  `Strategic_Rating` decimal(5,2) DEFAULT NULL,
  `Core_Function_Rating` decimal(5,2) DEFAULT NULL,
  `Total_Rating` decimal(5,2) DEFAULT NULL,
  `Premium_Points` decimal(5,2) DEFAULT NULL,
  `Overall_Rating` decimal(5,2) DEFAULT NULL,
  `Adjectival_Rating` varchar(50) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
